<?php
$_SERVER['DOCUMENT_ROOT'] = getcwd().'/';
require_once $_SERVER['DOCUMENT_ROOT'].'../application/config/Application.php';

date_default_timezone_set("Europe/Moscow");

set_include_path(             
            $_SERVER['DOCUMENT_ROOT'] . '/' . Application::LIB_PATH
            . PATH_SEPARATOR . "../"
            . PATH_SEPARATOR . Application::CONTROLLERS_PATH
            . PATH_SEPARATOR . Application::MODELS_PATH
            . PATH_SEPARATOR . get_include_path()
            );  

chdir($_SERVER['DOCUMENT_ROOT']);

require_once 'SFM/AutoLoad.php';
SFM_AutoLoad::getInstance()->register();

ini_set('error_log','/var/log/php_cron_error.log');


require_once 'application/Initializer.php';
$initializer = new Initializer();