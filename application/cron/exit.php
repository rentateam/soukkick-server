<?php
    require_once 'default.php';
    
    set_time_limit(0);

    $beginTimestamp = time();
    
    $db = SFM_DB::getInstance();
    try 
    {
        $db->beginTransaction();
        $exitAttemptList = Mapper_Action_Exit_Attempt::getInstance()->getListOld($beginTimestamp);
        foreach($exitAttemptList as $attemptItem)
        {
            $user = Mapper_User::getInstance()->getEntityById($attemptItem['user_id']);
            $shop = Mapper_Shop::getInstance()->getEntityById($attemptItem['shop_id']);
            $user->shopExit($shop,$attemptItem['transmitter_type_id'],$beginTimestamp,true);
        }
        $db->commit();
    }
    catch(Exception $e)
    {
        $db->rollback();
        error_log($e->getMessage());
    }        