<?php
    require_once 'default.php';
    
    set_time_limit(0);

    $fromDate = date('Y-m-d',time() - Entity_Action_Enter::TIME_HUNG_ENTER);
    
    $db = SFM_DB::getInstance();
    $db->beginTransaction();
    
    try 
    {
        foreach(Mapper_Shop::getInstance()->getList() as $shop)
        {
            $todayEnters = Mapper_Action_Enter::getInstance()->getListByShopFromDate($shop,$fromDate);
            foreach($todayEnters as $enter)
            {
                $exit = $enter->getCorrespondingExit();
                if($exit === null)
                {
                    $exit = Entity_Action_Exit::add($enter->getUser(),$shop,$enter->getTransmitterTypeId());
                    echo "checkin ".$enter->getId()." did not have a exit, made one: ".$exit->getId()."\r\n";
                }
                else
                {
                    echo "checkin ".$enter->getId()." has a exit ".$exit->getId()."\r\n";
                }
            } 
        }
        $db->commit();
    }
    catch(Exception $e)
    {
        $db->rollback();
        error_log($e->getMessage());
    }
    