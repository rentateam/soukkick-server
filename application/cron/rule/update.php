<?php
    require_once '../application/cron/default.php';
    
    set_time_limit(0);

    $now = time();
    
    $db = SFM_DB::getInstance();
    try 
    {
        $db->beginTransaction();
        
        //Пересчет активности у правил
        foreach(Mapper_Rule_Main::getInstance()->getListPlain() as $rule)
        {
            if(!$rule->isRunningNow(false))
            {
                $rule->setActive(false);
            }
            else
            {
                $rule->setActive(true);
            }
        }
        $db->commit();
        
        $db->beginTransaction();
        
        //Обновление
        $updater = new Rule_AmountUpdater();
        $updater->update();
        $db->commit();
    }
    catch(Exception $e)
    {
        $db->rollback();
        error_log($e->getMessage());
        throw $e;
    }        