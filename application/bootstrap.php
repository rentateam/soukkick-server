<?php

require_once 'config/Application.php';

date_default_timezone_set("Europe/Moscow");

set_include_path(             
            getcwd() . '/' . Application::LIB_PATH
            . PATH_SEPARATOR . "../"
            . PATH_SEPARATOR . Application::CONTROLLERS_PATH
            . PATH_SEPARATOR . Application::MODELS_PATH
            . PATH_SEPARATOR . Application::MODELS_ADMIN_PATH
            . PATH_SEPARATOR . Application::MODELS_REWARD_PATH
            . PATH_SEPARATOR . get_include_path()
            );  


require_once 'SFM/AutoLoad.php';
SFM_AutoLoad::getInstance()->register();

require_once 'Initializer.php';
require_once 'Zend/Controller/Front.php';

// Prepare the front controller. 
$frontController = Zend_Controller_Front::getInstance(); 

// Change to 'production' parameter under production enviroment
$frontController->registerPlugin(new Initializer());   

// Dispatch the request using the front controller. 
$frontController->dispatch();