<?php

//require_once 'Zend/Registry.php';

class Application {

    const PUBLIC_PATH = '../public';
    const CONTROLLERS_PATH = '../application/default/controllers';
    const ADMIN_CONTROLLERS_PATH = '../application/admin/controllers';
    const EDIT_CONTROLLERS_PATH = '../application/edit/controllers';
    const VIEWS_PATH = '../application/default/views/scripts';
    const REPORTS_TEMPLATE_PATH = '../application/default/views/reports/';
    const EDIT_VIEWS_PATH = '../application/edit/views/scripts';
    const ADMIN_VIEWS_PATH = '../application/admin/views/scripts';
    const MODELS_PATH = '../application/default/models/';
    const MODELS_ADMIN_PATH = '../application/admin/models/';
    const MODELS_REWARD_PATH = '../application/reward/models/';
    const CONTROLLERS_PATH_TEST = 'application/default/controllers';
    const MODELS_PATH_TEST = 'application/default/models/';
    const LIB_PATH = '../library';
    const LANG_PATH = '../application/lang/';
    const MAIL_SCRIPT_PATH = '../application/email/';

    const ENVIROMENT_NAME = 'environment';
    const ENVIROMENT_USERNAME = 'user';
    const ENVIROMENT_DEVELOPMENT = 'development';
    const ENVIROMENT_PRODUCTION = 'production';
    const ENVIROMENT_TEST = 'test';

    const CONFIG_NAME = 'config';
    const CONFIG_PATH = '../application/config/main.ini';
    const ENVIRONMENT_CONFIG_PATH = '../application/config/environment.ini';
    const CONFIG_ROUTES_PATH = '../application/config/routes.ini';

    //captcha section
    const FONT_PATH = '/captcha/font/';
    const CAPTCHA_IMG_DIR = '/captcha/';
    const CAPTCHA_IMG_URL = '/captcha/';

    const PLACEHOLDER_DATE_ELEMENT_INIT = 'placeholder_date_element_init';

    const USER_PICTURE_PATH = 'media/image/user/';
    const SHOP_PICTURE_PATH = 'media/image/shop/';
    const PRODUCT_PICTURE_PATH = 'media/image/product/';
    const SALES_PICTURE_PATH = 'media/image/sales/';
    const PRODUCT_SCAN_PICTURE_PATH = 'media/image/scan-product/';
    const PRIZE_PICTURE_PATH = 'media/image/prize/';
    const BANNER_PICTURE_PATH = 'media/image/banner/';
    const PROMOCODE_PICTURE_PATH = 'media/image/promocode/';
    const MALL_PICTURE_PATH = 'media/image/mall/';
    const MALL_MAP_PICTURE_PATH = 'media/image/mall-map/';
    const USERSHOP_PICTURE_PATH = 'media/image/usershop/';
    const ACHIEVEMENT_PICTURE_PATH = 'media/image/achievement/';

    //media section
    const REPORTS_MEDIA_PATH = '/reports/';

    const USER_PICTURE_DEFAULT = 'default.png';
    const USERSHOP_PICTURE_DEFAULT = 'default.png';

    //hashes names corresponde to main.ini secitons
    const HASH_MEMORY_DEFAULT = "defaultMemory";
    const HASH_FILE_DEFAULT = "defaultFile";

    const STATIC_IMAGE = 'image';

    public static function getCaptchaImageDir()
    {
        return $_SERVER['DOCUMENT_ROOT'].self::CAPTCHA_IMG_DIR;
    }

    public static function getCaptchaFontPath()
    {
        return $_SERVER['DOCUMENT_ROOT'].self::FONT_PATH;
    }

    public static function getHostName()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->hostName;
    }

    public static function getStaticURL()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->staticURL;
    }

    public function getMemcacheqHost()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->memcachedAPI->defaultMemcacheq->host;
    }

    public function getMemcacheqPort()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->memcachedAPI->defaultMemcacheq->port;
    }

    public static function getSphinxHost()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->sphinx->host;
    }

    public static function getSphinxPort()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->sphinx->port;
    }

    public static function getSphinxTimeout()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->sphinx->timeout;
    }

    public static function getDefaultEmailAddress()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->mail->from;
    }

    public static function getDefaultEmailTitle()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->mail->titleFrom;
    }

    public static function getAchievementPicturePath($absoluteLink = false)
    {
        if ($absoluteLink)
            $prefix = $_SERVER['DOCUMENT_ROOT'];
        else
            $prefix = 'http://'.Application::getHostName().'/';
        return $prefix . self::ACHIEVEMENT_PICTURE_PATH;
    }

    public static function getUserPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::USER_PICTURE_PATH;
    }

    /**
     * Сторона квадрата, в который должны вписываться картинки пользователей
     */
    public static function getUserPictureSide()
    {
        return 160;
    }

    public static function getTradeNetworkBigPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::USERSHOP_PICTURE_PATH.'big/';
    }

    public static function getTradeNetworkPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::USERSHOP_PICTURE_PATH.'small/';
    }

    public static function getTradeNetworkRectanglePicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::USERSHOP_PICTURE_PATH.'rectangle/';
    }

    /**
     * Сторона квадрата, в который должны вписываться картинки сетей
     */
    public static function getTradeNetworkBigPictureSide()
    {
        return 200;
    }

    public static function getTradeNetworkPictureSide()
    {
        return 110;
    }

    public static function getTradeNetworkRectanglePictureWidth()
    {
        return 278;
    }

    public static function getTradeNetworkRectanglePictureHeight()
    {
        return 58;
    }

    protected static function getStaticImagePrefix($absoluteLink)
    {
        if ($absoluteLink)
            $prefix = '';
        else
            $prefix = 'http://'.Shopfee_Aws_S3::getHostNameByType(self::STATIC_IMAGE).'/'.Shopfee_Aws_S3::getFileFolder();
        return $prefix;
    }

    public static function getProductSmallPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRODUCT_PICTURE_PATH.'small/';
    }

    public static function getProductSmallPictureSide()
    {
        return 210;
    }

    public static function getProductBigPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRODUCT_PICTURE_PATH.'big/';
    }

    public static function getProductBigPictureSide()
    {
        return 400;
    }

    public static function getSalesBigPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::SALES_PICTURE_PATH.'big/';
    }

    public static function getSalesBigPictureWidth()
    {
        return 580;
    }

    public static function getSalesBigPictureHeight()
    {
        return 644;
    }

    public static function getSalesSmallPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::SALES_PICTURE_PATH.'small/';
    }

    public static function getSalesSmallPictureSide()
    {
        return 210;
    }

    public static function getScanProductSmallPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRODUCT_SCAN_PICTURE_PATH.'small/';
    }

    public static function getScanProductSmallPictureWidth()
    {
        return 250;
    }

    public static function getScanProductSmallPictureHeight()
    {
        return 200;
    }

    public static function getScanProductBigPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRODUCT_SCAN_PICTURE_PATH.'big/';
    }

    public static function getScanProductBigPictureWidth()
    {
        return 500;
    }

    public static function getScanProductBigPictureHeight()
    {
        return 400;
    }

    public static function getPrizeBigPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRIZE_PICTURE_PATH.'big/';
    }

    public static function getPrizeBigPictureWidth()
    {
        return 540;
    }

    public static function getPrizeBigPictureHeight()
    {
        return 360;
    }

    public static function getPrizeSmallPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PRIZE_PICTURE_PATH.'small/';
    }

    public static function getPrizeSmallPictureWidth()
    {
        return 180;
    }

    public static function getPrizeSmallPictureHeight()
    {
        return 120;
    }

    public static function getBannerPicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::BANNER_PICTURE_PATH;
    }

    public static function getPromocodePicturePath($absoluteLink = false)
    {
        return self::getStaticImagePrefix($absoluteLink) . self::PROMOCODE_PICTURE_PATH;
    }

    public static function getMallPicturePath($absoluteLink = false)
    {
    	return self::getStaticImagePrefix($absoluteLink) . self::MALL_PICTURE_PATH;
    }

    public static function getMallMapPicturePath($absoluteLink = false)
    {
    	return self::getStaticImagePrefix($absoluteLink) . self::MALL_MAP_PICTURE_PATH;
    }

    public static function getSupportPhone()
    {
        return '8 (926) 472-39-23';
    }

    public static function getLittleSmsUser()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->littleSms->user;
    }

    public static function getLittleSmsKey()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->littleSms->key;
    }

    public static function getAmazonS3Folder()
    {
        return self::getHostName();
    }

    public static function getReportsTemplatePath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . self::REPORTS_TEMPLATE_PATH;
    }

    public static function getReportsTemporallyPath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . self::REPORTS_MEDIA_PATH;
    }

    public static function getConverterCommand()
    {
        return "python /opt/pyodconverter/DocumentConverter.py";
    }

    public static function getMinimalAPIVersion()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->clientAPI->minimalVersion;
    }

    public static function getCurrentAPIVersion()
    {
        return Zend_Registry::get(self::CONFIG_NAME)->clientAPI->currentVersion;
    }
}
?>