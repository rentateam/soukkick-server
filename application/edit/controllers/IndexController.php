<?php
//require_once 'AbstractController.php';
class Edit_IndexController extends Shopfee_Controller_Action
{
    protected $_authShopActions = array('index','exit');

    public function indexAction()
    {
        $this->_redirect('/edit/feed/');
    }

    public function loginAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $cookieLogin = isset($_COOKIE[Auth::COOKIE_SHOP_LOGIN]) ? $_COOKIE[Auth::COOKIE_SHOP_LOGIN] : null;
        $cookiePassword = isset($_COOKIE[Auth::COOKIE_SHOP_PASSWORD]) ? $_COOKIE[Auth::COOKIE_SHOP_PASSWORD] : null;

        $authSuccess = false;
        $remember = isset($_COOKIE[Auth::COOKIE_SHOP_REMEMBER]) ? $_COOKIE[Auth::COOKIE_SHOP_REMEMBER] : false;
        $backUrl = $this->getBackUrl();

        $form = new Form_Login($backUrl);
        $form->setAction('/edit/index/login/');

        //if the user is already logged in
        if (Auth::getCurrentBpaManager())
            $this->_redirect($backUrl);

        //else check the cookie
        else if ($cookieLogin && $cookiePassword) {
            $authResult = Auth::loginShop($cookieLogin, $cookiePassword, false, true);
            if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                $authSuccess = true;
        }

        //else it is just form processing or form rendering
        else {
            //form processing
            if ($this->getRequest()->isPost() && $form->isValid($_POST)) {
                $values = $form->getValues();
                $authResult = Auth::loginShop($values['email'], $values['password'], ($values['remember'] == 'checked' || $values['remember'] == 1));

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode())) {
                    $authSuccess = true;
                } else {
                    $form->getElement('password')->setErrors(array('Неправильный логин или пароль'));
                }
            }
        }

        if ($authSuccess) {
            $this->_redirect('/edit/');
        }
        else {
            $this->view->loginForm = $form;
        }

        /*$registerForm = new Form_RegisterShop($backUrl);
        $this->view->registerForm = $registerForm;*/
        $this->view->backUrl = urlencode($backUrl);
    }
    
    public function exitAction() 
    {
        Auth::logoffShop();
        $this->_redirect("/edit/");
    }
}