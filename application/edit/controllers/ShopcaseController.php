<?php
require_once 'AbstractController.php';
class Edit_ShopcaseController extends Edit_AbstractController
{
    protected $_authShopActions = array('shopcase','check-barcode');
    
    
    public function indexAction() 
    {
        $this->view->pageShopcase = true;
        
        $this->view->productList = $this->view->currentTradeNetwork->getProducts();
        $productForm = new Form_Product(0);
        $this->view->productForm = $productForm;
        
        $this->view->salesList = $this->view->currentTradeNetwork->getSales();
        $salesForm = new Form_Sales(0);
        $this->view->salesForm = $salesForm;

        $this->view->scanProductList = $this->view->currentTradeNetwork->getScanProducts();
        $scanProductForm = new Form_ScanProduct(0);
        $this->view->scanProductForm = $scanProductForm;
        
        $this->view->amountPage = $this->view->currentTradeNetwork->getAmountPage();
        $this->view->currentTradeNetwork = $this->view->currentTradeNetwork;
    }    
    
    public function checkBarcodeAction()
    {
        $barcode = $this->getQuotedParam('barcode');
        
        $scanProduct = Mapper_ScanProduct::getInstance()->getEntityByBarcodeAndTradeNetwork($barcode,$this->view->currentTradeNetwork); 
        $result = $scanProduct === null || $scanProduct->getId() == $this->getQuotedParam('scan_product_id');
        $this->_helper->layout->disableLayout();
        Zend_Controller_Action_HelperBroker::removeHelper('viewRenderer');
        $output = Zend_Json::encode($result);
        $response = $this->getResponse();
        $response->setBody($output)
                ->setHeader('content-type', 'application/json', true);
    }
}