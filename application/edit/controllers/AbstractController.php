<?php

abstract class Edit_AbstractController extends Shopfee_Controller_Action
{
    public function init()
    {
        //Таким образом все действия требуют авторизации
        $this->_authShopActions = array($this->getRequest()->getActionName());

        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('edit');
        $layout->setLayoutPath('../application/edit/layouts');
    }

    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->currentTradeNetwork = $this->currentTradeNetwork;

        if($this->currentTradeNetwork)
        {
            $shopList = new Decorator_ShopListByCity($this->view->currentTradeNetwork->getShops(null));
            $this->view->shopListByCity = $shopList->getShopList();
            $this->view->selectedShopId = $this->getQuotedParam('shop_id');
            $this->view->selectedCityId = $this->getQuotedParam('city_id');
        }
    }

    public function postDispatch()
    {
        parent::postDispatch();
        $this->view->currentShop = $this->extractShop();
    }

    protected function jsonResponse($result)
    {
        $this->_helper->layout->disableLayout();
        Zend_Controller_Action_HelperBroker::removeHelper('viewRenderer');

        $output = Zend_Json::encode($result);
        $response = $this->getResponse();
        $response->setBody($output)
                 ->setHeader('content-type', 'application/json', true);
    }

    protected function extractShop()
    {
        if($this->view->currentTradeNetwork)
        {
            $shop = $this->view->currentTradeNetwork->getShops(null)->current();
            return $shop;
        }
        else
        {
            return null;
        }
    }
}