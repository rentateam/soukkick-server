<?php
require_once 'AbstractController.php';
class Edit_ControlController extends Edit_AbstractController
{
    protected $_authShopActions = array('index','rule-partial','info','stat','rule-add','rule-edit','rule-delete');

    public function init()
    {
        parent::init();
        $this->view->pageControl = true;
    }

    public function indexAction()
    {
        $this->view->pageControlRule = true;

        $criteria = new Criteria_Rule_Main();

        $bpaMain = $this->currentTradeNetwork->getBpaMain();
        $criteria->setBpaMain($bpaMain);
        $criteria->setDeleted(false);

        $selectedShopId = $this->view->selectedShopId;
        if($selectedShopId != null)
        {
            $selectedShop =  Mapper_Shop::getInstance()->getEntityById($selectedShopId);
            $this->checkForNullEntity($selectedShop);
            $criteria->setShop($selectedShop);
        }
        $selectedCityId = $this->view->selectedCityId;
        if($selectedCityId != null)
        {
            $selectedCity =  Mapper_City::getInstance()->getEntityById($selectedCityId);
            $this->checkForNullEntity($selectedCity);
            $criteria->setCity($selectedCity);
        }

        $criteria->setDateType(Criteria_Rule_Main::TYPE_CURRENT);
        $currentRuleList = Mapper_Rule_Main::getInstance()->getListByCriteria($criteria);
        $criteria->setDateType(Criteria_Rule_Main::TYPE_FUTURE);
        $futureRuleList = Mapper_Rule_Main::getInstance()->getListByCriteria($criteria);
        $criteria->setDateType(Criteria_Rule_Main::TYPE_OLD);
        $oldRuleList = Mapper_Rule_Main::getInstance()->getListByCriteria($criteria);

        $this->view->currentRuleList = $currentRuleList;
        $this->view->futureRuleList = $futureRuleList;
        $this->view->oldRuleList = $oldRuleList;
        $this->view->overlapRuleIds = Entity_Rule_Main::getOverlappedIds(array($currentRuleList,$futureRuleList,$oldRuleList));

        $this->view->scanList = $this->currentTradeNetwork->getScanProducts();
    }

    public function rulePartialAction()
    {
        $this->_helper->layout->disableLayout();

        $rule = Mapper_Rule_Main::getInstance()->getEntityById(intval($this->getRequest()->getParam('ruleId')));
        $this->checkForNullEntity($rule);
        if(!$rule->checkEditRight($this->currentTradeNetwork))
        {
            throw new Shopfee_Exception_Access('You are not allowed to edit this rule');
        }

        $this->view->networkShopNumber = $this->currentTradeNetwork->getShops(null)->totalCount();
        $this->view->ruleShopNumber = $rule->getShopNumber();

        $this->view->isOverlapped = $this->getRequest()->getParam('isOverlapped',false);
        $this->view->rule = $rule;
        $this->view->scanList = $this->currentTradeNetwork->getScanProducts();

        $ruleDecorator = new Decorator_Rule_Main($rule);
        $this->view->messages = $ruleDecorator->getWarningMessages($this->currentTradeNetwork);
    }

    public function ruleFormAction()
    {
        $id = intval($this->getRequest()->getParam('id'));
        $form = new Form_Rule_Main($id);
        $scanList = $this->currentTradeNetwork->getScanProducts();
        $form->setTradeNetwork($this->currentTradeNetwork);

        if($id > 0)
        {
            $rule = Mapper_Rule_Main::getInstance()->getEntityById($id);
            $this->checkForNullEntity($rule);
            if(!$rule->checkEditRight($this->currentTradeNetwork))
            {
                throw new Shopfee_Exception_Access('You are not allowed to edit this rule');
            }
            $form->setValues($rule);
            $this->view->rule =  $rule;
        }


        $this->view->form = $form;


        $shopListDecorator = new Decorator_ShopListByCity($this->currentTradeNetwork->getShops(null));
        $this->view->shopList = $shopListDecorator->getShopList();

        $this->view->scanList = $scanList;
    }

    public function infoAction()
    {
        $this->view->pageControlInfo = true;

        $selectedShopId = $this->view->selectedShopId;
        if($selectedShopId)
        {
            $shopList = Mapper_Shop::getInstance()->createAggregate(array($selectedShopId),null,true);
            $selectedShop =  Mapper_Shop::getInstance()->getEntityById($selectedShopId);
            $this->checkForNullEntity($selectedShop);
            $this->view->selectedShop = $selectedShop;
            $this->view->infoTitle = $selectedShop->getName();
        }
        else
        {
            $this->view->infoTitle = $this->view->currentTradeNetwork->getTitle();
            $shopWithTransmitterList = array();
            $shopWithoutTransmitterList = array();
            $thisCityShopListIds = array();
            $shopList = $this->view->currentTradeNetwork->getShops(null);

            foreach($shopList as $shop)
            {
                if(!$this->view->selectedCityId || $shop->getCityId() == $this->view->selectedCityId)
                {
                    if($shop->isTransmitterAvailable())
                    {
                        $shopWithTransmitterList[] = $shop;
                    }
                    else
                    {
                        $shopWithoutTransmitterList[] = $shop;
                    }
                    $thisCityShopListIds[] = $shop->getId();
                }
            }
            $this->view->hasTransmitterNumber = count($shopWithTransmitterList);
            $this->view->shopWithTransmitterList = $shopWithTransmitterList;
            $this->view->shopWithoutTransmitterList = $shopWithoutTransmitterList;

            $shopList = Mapper_Shop::getInstance()->createAggregate($thisCityShopListIds,null,true);
            $this->view->totalNumber = $shopList->totalCount();
        }
        $this->view->shopList = $shopList;
    }

    public function statAction()
    {
        $this->view->pageControlStat = true;
        $selectedShopId = $this->view->selectedShopId;
        if($selectedShopId)
        {
            $selectedShop =  Mapper_Shop::getInstance()->getEntityById($selectedShopId);
            $this->checkForNullEntity($selectedShop);
            $this->view->selectedShop = $selectedShop;
            $this->view->infoTitle = $selectedShop->getName();
        }
        else
        {
            $selectedShop = null;
            $this->view->infoTitle = $this->view->currentTradeNetwork->getTitle();
            $this->view->totalNumber = $this->view->currentTradeNetwork->getShops(null)->totalCount();
        }

        $dateFrom = $this->getQuotedParam('date_from');
        $dateTo = $this->getQuotedParam('date_to');

        $selectedCity =  $this->view->selectedCityId ? Mapper_City::getInstance()->getEntityById($this->view->selectedCityId) : null;
        $stat = new Stat_Rule($this->view->currentTradeNetwork,$dateFrom,$dateTo,$selectedShop,$selectedCity);
        $stat->calculate();
        $this->view->statList = $stat->getStat();
        $this->view->overallAmount = $stat->getOverallAmount();
        $this->view->dateFrom = $dateFrom;
        $this->view->dateTo = $dateTo;

        $this->view->showShoppage = !$this->view->selectedCityId && !$selectedShopId;
        $this->view->showBuy = $this->view->currentTradeNetwork->hasIntegration();

        $this->view->checkinNumber = $stat->getCheckinNumber();
        $this->view->checkinAmount = $stat->getCheckinAmount();
        $this->view->scanNumber = $stat->getScanNumber();
        $this->view->scanAmount = $stat->getScanAmount();
        if($this->view->showBuy)
        {
            $this->view->buyNumber = $stat->getBuyNumber();
            $this->view->buyAmount = $stat->getBuyAmount();
        }
        if($this->view->showShoppage)
        {
            $this->view->shoppageNumber = $stat->getShoppageNumber();
            $this->view->shoppageAmount = $stat->getShoppageAmount();
        }
    }

    public function ruleAddAction()
    {
        $form = new Form_Rule_Main(0);
        $scanList = $this->currentTradeNetwork->getScanProducts();
        $form->setTradeNetwork($this->currentTradeNetwork);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $bpaManager = $this->currentTradeNetwork->getAnyBpaManager();

                $ruleMain = Entity_Rule_Main::add($bpaManager, 'Кампания '.$values['date_from'].' - '.$values['date_to'], date('Y-m-d',strtotime($values['date_from'])),date('Y-m-d',strtotime($values['date_to'])), -1);
                if($values['rule_shop_id'])
                    $ruleMain->setShopIds($values['rule_shop_id']);
                $ruleMain->setChanged(true);

                $ruleItem = Entity_Rule_Item::add($ruleMain, $values['checkin'], Entity_Rule_Item::TYPE_CHECKIN, null, $values['limit_checkin'],true);
                $ruleItem = Entity_Rule_Item::add($ruleMain, $values['shoppage'], Entity_Rule_Item::TYPE_SHOPPAGE, $this->currentTradeNetwork, $values['limit_shoppage'],true);
                if($this->currentTradeNetwork->hasIntegration())
                {
                    $ruleItem = Entity_Rule_Item::add($ruleMain, $values['buy'], Entity_Rule_Item::TYPE_BUY_BARCODE, null, $values['limit_buy'],true);
                }
                foreach($scanList as $scan)
                {
                    $ruleItem = Entity_Rule_Item::add($ruleMain, $values['scan_'.$scan->getId()], Entity_Rule_Item::TYPE_SCAN, $scan, $values['limit_scan_'.$scan->getId()],true);
                }

                $db->commit();
                $this->_redirect('/edit/control/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        else
        {
            throw new Shopfee_Exception('Rule js validator is broken. Errors are:'.var_export($form->getErrors(),true));
        }
    }

    public function ruleSaveAction()
    {
        $id = intval($this->getRequest()->getParam('id'));
        $rule = Mapper_Rule_Main::getInstance()->getEntityById($id);
        $this->checkForNullEntity($rule);
        if(!$rule->checkEditRight($this->currentTradeNetwork))
        {
            throw new Shopfee_Exception_Access('You are not allowed to edit this rule');
        }

        $form = new Form_Rule_Main($id);
        $scanList = $this->currentTradeNetwork->getScanProducts();
        $form->setTradeNetwork($this->currentTradeNetwork);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $bpaManager = $this->currentTradeNetwork->getAnyBpaManager();
                $rule->update(array(
                                        'date_from' => date('Y-m-d',strtotime($values['date_from'])),
                                        'date_to' =>  date('Y-m-d',strtotime($values['date_to'])),
                                    ));
                if($values['rule_shop_id'])
                    $rule->setShopIds($values['rule_shop_id']);

                $rule->setChanged(true);

                //Чекин
                $checkinRuleItem = Entity_Rule_Item::add($rule, $values['checkin'], Entity_Rule_Item::TYPE_CHECKIN, null, $values['limit_checkin'],true);

                //Просмотр витрины
                $shoppageRuleItem = Entity_Rule_Item::add($rule, $values['shoppage'], Entity_Rule_Item::TYPE_SHOPPAGE, $this->currentTradeNetwork, $values['limit_shoppage'],true);


                if($this->currentTradeNetwork->hasIntegration())
                {
                    //Покупки
                    $buyBarcodeRule = Entity_Rule_Item::add($rule, $values['buy'], Entity_Rule_Item::TYPE_BUY_BARCODE, null, $values['limit_buy'],true);
                }


                //Сканы
                foreach($scanList as $scan)
                {
                    $ruleItem = Entity_Rule_Item::add($rule, $values['scan_'.$scan->getId()], Entity_Rule_Item::TYPE_SCAN, $scan, $values['limit_scan_'.$scan->getId()],true);
                }


                $db->commit();
                $this->_redirect('/edit/control/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        else
        {
            throw new Shopfee_Exception('Rule js validator is broken. Errors are:'.var_export($form->getErrors(),true));
        }
    }

    public function ruleDeleteAction()
    {
        $id = intval($this->getRequest()->getParam('id'));
        $rule = Mapper_Rule_Main::getInstance()->getEntityById($id);
        $this->checkForNullEntity($rule);
        if(!$rule->checkEditRight($this->currentTradeNetwork))
        {
            throw new Shopfee_Exception_Access('You are not allowed to edit this rule');
        }
        $bpaManager = $this->currentTradeNetwork->getAnyBpaManager();
        $rule->delete($bpaManager);
        $this->_redirect('/edit/control/');
    }
}