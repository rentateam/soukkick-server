<?php
require_once 'AbstractController.php';
class Edit_SettingsController extends Edit_AbstractController
{
    protected $_authShopActions = array('index');

    public function indexAction()
    {
        $this->view->form = new Form_Shop(0);
        $this->view->shopList = $this->view->currentTradeNetwork->getShops(null);
        $logoForm = new Form_ShopLogo($this->view->currentTradeNetwork->getId());


        if($this->getRequest()->isPost() && $logoForm->isValid($_POST))
        {
            $values = $logoForm->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $params = array();
                $uploadedFileName = $logoForm->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $uploadedRectangleFileName = $logoForm->uploadFile('rectangle_file');
                if($uploadedRectangleFileName)
                {
                    $params['rectangle_picture_path'] = $uploadedRectangleFileName;
                }
                if(!empty($params))
                    $this->view->currentTradeNetwork->update($params);
                $db->commit();
                $this->_redirect('/edit/settings/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $logoForm->setValues($this->view->currentTradeNetwork);
        $this->view->logoForm = $logoForm;
        $this->view->currentTradeNetwork = $this->view->currentTradeNetwork;
    }
}