<?php
require_once 'AbstractController.php';
class Edit_ShopController extends Edit_AbstractController
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function addAction()
    {
        $form = new Form_Shop(0);
        $form->setTradeNetwork($this->currentTradeNetwork);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $city = Mapper_City::getInstance()->getEntityById($values['city']);
                $this->checkForNullEntity($city);
                $shop = Entity_Shop::add($this->currentTradeNetwork, $values['name'], $values['lat'], $values['long'], $values['address'], $city);
                if($values['mall_id'])
                {
                    $mall = Mapper_Mall::getInstance()->getEntityById($values['mall_id']);
                    $this->checkForNullEntity($mall);
                    $shop->setMall($mall);
                }
                $db->commit();
                $this->_redirect('/edit/control/info/?shop_id='.$shop->getId());
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);

        $form = new Form_Shop($id);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $params = array(
                                    'name' => $values['name']
                                );
                if($values['mall_id'])
                {
                    $mall = Mapper_Mall::getInstance()->getEntityById($values['mall_id']);
                    $this->checkForNullEntity($mall);
                    $shop->setMall($mall);
                }
                else
                {
                    $params['address'] = $values['address'];
                    $shop->updateLocation($values['lat'],$values['long']);
                    $shop->setMall(null);
                }
                $shop->update($params);
                $db->commit();
                $this->_redirect('/edit/control/info/?shop_id='.$shop->getId());
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($shop);

        $this->view->shop = $shop;
        $this->view->form = $form;
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
    }

    public function formAction()
    {
        $id = $this->getQuotedParam('id');
        $form = new Form_Shop($id);
        if($id)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($id);
            $this->checkForNullEntity($shop);
            $form->setValues($shop);
        }
        else
        {
            $form->setTradeNetwork($this->currentTradeNetwork);
            $shop = null;
        }

        $this->view->shop = $shop;
        $this->view->form = $form;
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');

        $this->view->mapCanvasId = $this->getRequest()->getParam('mapCanvas');
    }

    public function deleteAction()
    {
        $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);

        $shop->delete();
        $this->_redirect('/edit/control/info/');
    }
}