<?php
require_once 'AbstractController.php';
class Edit_FeedController extends Edit_AbstractController
{
    protected $_authShopActions = array('index','check');

    public function init()
    {
        parent::init();
        $this->_helper->contextSwitch()
                ->addActionContext('check', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        $this->view->pageFeed = true;
        $shop = $this->extractFeedShop();

        $this->view->grouppedEventList = Entity_Event_Shop::getListByShopGroupByDates($this->view->currentTradeNetwork,$shop);
        if(count($this->view->grouppedEventList) > 0)
        {
            $keys = array_keys($this->view->grouppedEventList);
            $this->view->firstEvent = $this->view->grouppedEventList[$keys[0]][0];
        }
        else
        {
            $this->view->firstEvent = null;
        }

        $this->setFeedViewVariables($shop);
    }

    public function checkAction()
    {
        $shop = $this->extractFeedShop();
        $lastDate = $this->getQuotedParam('last_date');
        $eventList = Mapper_Event_Shop::getInstance()->getListByTradeNetwork($this->view->currentTradeNetwork,$shop);

        $view = new Zend_View();
        $view->setScriptPath(realpath(Application::EDIT_VIEWS_PATH));
        $this->view->eventContainer = '';
        $newLastDate = $lastDate;
        foreach($eventList as $event)
        {
            if(strtotime($event->getDateCreated()) > strtotime($lastDate))
            {
                $view->assign(array('event' => $event));
                $this->view->eventContainer.= $view->render('feed/partial.phtml');
                $newLastDate = $event->getDateCreated();
            }
        }
        $this->view->newLastDate = $newLastDate;
        $this->setFeedViewVariables($shop);

        $view->assign(array(
            'visitorList' => $this->view->visitorList,
            'todayVisitorNumber' => $this->view->todayVisitorNumber,
            'yesterdayVisitorNumber' => $this->view->yesterdayVisitorNumber,
            'newPercent' => $this->view->newPercent,
            'oldPercent' => $this->view->oldPercent,
        ));
        $this->view->statContainer = $view->render('include/stat-container.phtml');
    }

    protected function extractFeedShop()
    {
        $id = intval($this->getQuotedParam('shop_id'));
        if($id > 0)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($id);
            if(!$shop->canBeViewed($this->view->currentTradeNetwork))
                throw new Shopfee_Exception('Unauthorized shop view');
        }
        else
        {
            $shop = null;
        }
        return $shop;
    }

    protected function setFeedViewVariables(Entity_Shop $shop = null)
    {
        $invokeObject =  $shop ? $shop : $this->view->currentTradeNetwork;

        $numberOfTodayEnters = $invokeObject->getNumberOfEnters(60*24,60*24) ;
        $this->view->visitorList = $invokeObject->getExistingVisitors();
        $this->view->todayVisitorNumber = $numberOfTodayEnters;
        $this->view->yesterdayVisitorNumber = $invokeObject->getNumberOfEnters(60*24*2,60*24);

        $this->view->newPercent = $numberOfTodayEnters > 0 ? ceil($invokeObject->getNumberOfNewEnters(60*24,60*24)*100/$numberOfTodayEnters) : 0;
        $this->view->oldPercent = 100 - $this->view->newPercent;
    }
}