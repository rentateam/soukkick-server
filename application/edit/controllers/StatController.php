<?php

class Edit_Controller extends Edit_AbstractController
{
    protected $_authShopActions = array('stat','budget');



    public function statAction()
    {
        $this->view->pageStat = true;
    }

    public function statSummaryAction()
    {
        $shopId = $this->getQuotedParam('shop_id');


        $priceStat = $this->view->currentTradeNetwork->getPriceListByShopForDateInterval($this->shopIds,$this->fromDate,$this->toDate);
        $this->view->priceStat = $priceStat;





        if($shopId)
        {
            $shopIds = array($shopId);
        }
        else
        {
            $shopIds = $this->view->currentTradeNetwork->getShops(null)->getListEntitiesId();
        }

        $fromDate = $this->getQuotedParam('date_from',date('Y-m-d',time() - 7*24*60*60));
        $toDate = $this->getQuotedParam('date_to',date('Y-m-d',time()));

        $enterStat = Mapper_Action_Enter::getInstance()->getListByObjectsForDateInterval($shopIds,$fromDate,$toDate);
        $buyStat = array();
        $enterStatResult = array();
        foreach($enterStat as $date => $number)
        {
           $enterStatResult[] = array($date,$number);
        }
        $this->jsonResponse(array($enterStatResult,$buyStat));
    }

    public function statScanAction()
    {
        $shopId = $this->getQuotedParam('shop_id');



        //@FIXME. Temporary crutch.
        $this->view->currentTradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($this->getQuotedParam('trade_network_id'));








        if($shopId)
        {
            $shopIds = array($shopId);
        }
        else
        {
            $shopIds = $this->view->currentTradeNetwork->getShops(null)->getListEntitiesId();
        }


        $fromDate = $this->getQuotedParam('date_from',date('Y-m-d',time() - 7*24*60*60));
        $toDate = $this->getQuotedParam('date_to',date('Y-m-d',time()));

        $scanProductStat = array();
        $scanProductList = $shopId ? $shop->getScanProducts() : $this->view->currentTradeNetwork->getScanProducts();
        foreach($scanProductList as $scanProduct)
        {
            $scanProductStat[$scanProduct->getId()] = Mapper_Action_Scan::getInstance()->getListByObjectsForDateInterval(array($scanProduct->getId()),$fromDate,$toDate);
        }

        $scanStatResult = array();
        foreach($scanProductStat as $id => $contents)
        {
            foreach($contents as $date => $number)
            {
                $scanStatResult[] = array($id,$date,$number);
            }
        }
        $this->jsonResponse($scanStatResult);
    }



    public function budgetAction()
    {
        $this->view->pageBudget = true;
    }


}