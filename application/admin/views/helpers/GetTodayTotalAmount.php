<?php
require_once 'Zend/View/Helper/Partial.php';

class Helper_GetTodayTotalAmount extends Zend_View_Helper_Partial
{
    public function GetTodayTotalAmount()
    {
        $number = Mapper_Trm_Main::getInstance()->getTodayTotalAmount();
        $string = "<span>{$number}</span> points";
        return $string;
    }
}