<?php
class Helper_Query
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function Query()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->view->parameters, 'getQueryString'), $args);
    }
}