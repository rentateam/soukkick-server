<?php
class Admin_ReportsController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'reports';

    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('new');
        $layout->setLayoutPath('../application/admin/layouts/');
    }

    public function indexAction()
    {
        $reports = Mapper_Report::getInstance()->getList();
        $reports->loadEntities();

        if ($deletedId = $this->getRequest()->getParam('deleted')) {
            $this->view->deletedId = $deletedId;
        }

        if ($createdId = $this->getRequest()->getParam('created')) {
            $this->view->createdId = $createdId;
        }

        if ($modifiedId = $this->getRequest()->getParam('modified')) {
            $this->view->modifiedId = $modifiedId;
        }

        $this->view->reports = $reports;
    }

    public function addAction()
    {
        $form = new Form_Report();

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getParams())) {
                $report= Mapper_Report::getInstance()->add(
                    $form->getElement('caption')->getValue(),
                    $form->getElement('query')->getValue()
                );
                $this->_redirect("/admin/reports/?created={$report->getId()}");
            }

        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $report = Mapper_Report::getInstance()->getEntityById($id);
        $this->checkForNullEntity($report);

        $form = new Form_Report();
        $form->setValues($report);

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getParams())) {
                $report->update(array(
                    'caption' => $form->getElement('caption')->getValue(),
                    'query' => $form->getElement('query')->getValue()
                ));
                $this->_redirect("/admin/reports/?modified={$report->getId()}");
            }

        }

        $this->view->form = $form;
    }

    public function runAction()
    {
        $id = $this->getRequest()->getParam('id');
        $report = Mapper_Report::getInstance()->getEntityById($id);
        $this->checkForNullEntity($report);

        try {
            $resultSet = SFM_DB::getInstance('readonly')->fetchAll($report->getQuery());
            $this->view->resultSet = $resultSet;
            $this->view->resultHead = count($resultSet) > 0 ? array_keys(array_pop(array_merge($resultSet))) : array();
        } catch (Exception $e) {
            $this->view->error = $e->getMessage();
        }

        $this->view->report = $report;
    }

    public function exportAction()
    {
        $id = $this->getRequest()->getParam('id');
        $report = Mapper_Report::getInstance()->getEntityById($id);
        $this->checkForNullEntity($report);

        $resultSet = SFM_DB::getInstance('readonly')->fetchAll($report->getQuery());
        $xlsx = new Report_Query($resultSet);
        $xlsx->send($report->getCaption());
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $report = Mapper_Report::getInstance()->getEntityById($id);
        $this->checkForNullEntity($report);

        $report->delete();

        $this->_redirect("/admin/reports/?deleted={$id}");
    }
}