<?php
class Admin_MallMapController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'mall';

    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('new');
        $layout->setLayoutPath('../application/admin/layouts/');

        $this->_helper->contextSwitch()
                    ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function addAction()
    {
        $form = new Form_AdminMallMap(0);
        $mallId = $this->getRequest()->getParam('mall_id');

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $file = $form->uploadFile('file');
                $mall = Mapper_Mall::getInstance()->getEntityById($mallId);
                $this->checkForNullEntity($mall);
                $map = Entity_Mall_Map::add($mall,$values['floor'],$file);
                $db->commit();
                $this->_redirect('/admin/mall/edit/id/'.$mall->getId().'/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setMallId($mallId);
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $map = Mapper_Mall_Map::getInstance()->getEntityById($id);
        $this->checkForNullEntity($map);

        $form = new Form_AdminMallMap($id);
        $form->setValues($map);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $params = array(
                                    'floor' => $values['floor'],
                                );
                $file = $form->uploadFile('file');
                if($file)
                {
                    $params['picture_path'] = $file;
                }
                $map->update($params);
                $db->commit();
                $this->_redirect('/admin/mall/edit/id/'.$map->getMallId().'/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
    }

    public function formAction()
    {
    	$id = $this->getQuotedParam('id');
    	$form = new Form_AdminMallMap($id);
    	if($id)
    	{
    		$map = Mapper_Mall_Map::getInstance()->getEntityById($id);
    		$this->checkForNullEntity($map);
    		$form->setValues($map);
    	}
    	else
    	{
    		$map = null;
    	}

    	$mallId = $this->getRequest()->getParam('mall_id');
    	$form->setMallId($mallId);
    	$this->view->map = $map;
    	$this->view->form = $form;

    	$view = new Zend_View();
    	$view->setScriptPath(array(realpath(Application::ADMIN_VIEWS_PATH),realpath(Application::VIEWS_PATH)));
    	$view->assign(array('form' => $form,'map' => $map));
    	$this->view->html = $view->render('mall-map/form.phtml');
    }

    public function deleteAction()
    {
        $id = $this->getQuotedParam('id');
        $map = Mapper_Mall_Map::getInstance()->getEntityById($id);
        $this->checkForNullEntity($map);
        $mallId = $map->getMallId();
        $map->delete();
        $this->_redirect('/admin/mall/edit/id/'.$mallId.'/');
    }
}