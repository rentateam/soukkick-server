<?php

//require_once 'Spreadsheet/Writer.php';
class Admin_ShopController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'shop';
    
    public function indexAction() 
    {
        $tradeNetworkId = $this->getQuotedParam('trade_network_id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($tradeNetworkId);
        $this->checkForNullEntity($tradeNetwork);
        $shopList = Mapper_Shop::getInstance()->getListByTradeNetwork($tradeNetwork, null);
        $this->view->shopList = $shopList;
        $this->view->tradeNetwork = $tradeNetwork;
        
        $this->view->showGenerate = $tradeNetwork->hasIntegration();
    }

    public function addAction()
    {
        $form = new Form_AdminShop(0);
        $tradeNetworkId = $this->getQuotedParam('trade_network_id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($tradeNetworkId);
        $this->checkForNullEntity($tradeNetwork);
        $form->setApprovedTradeNetwork($tradeNetwork);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $shop = Entity_Shop::add($tradeNetwork,$values['name'],$values['lat'], $values['long'], $values['address']);
                $db->commit();
                $this->_redirect('/admin/shop/index/trade_network_id/'.$tradeNetwork->getId().'/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $this->view->form = $form;
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);
        
        $form = new Form_AdminShop($id,array('tradeNetwork' => $shop->getTradeNetwork()));
        $form->setValues($shop);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $params = array(
                                    'name' => $values['name'],
                                    'address' => $values['address'],
                                );
                
                $oldLat = $shop->getLat();
                $oldLong = $shop->getLongtitude();
                $shop->update($params);
                $shop->updateTransmitterInfo($values['transmitter_info']);
                
                if(isset($values['lat']) && $values['lat'] != $oldLat || isset($values['long']) && $values['long'] != $oldLong)
                {
                    $shop->updateLocation($values['lat'],$values['long']);
                }
                $db->commit();
                $this->_redirect('/admin/shop/index/trade_network_id/'.$shop->getTradeNetworkId().'/');
            }
            catch(Entity_Transmitter_Exception $e)
            {
                $this->view->showRegenerate = true;
                $this->view->collisionMessage = $e->getMessage();
                $this->view->transmitter = $shop->getMainTransmitter();
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        
        $this->view->shop = $shop;
        $this->view->form = $form;
    }
    
    public function deleteAction() 
    {
        $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);
        $tradeNetworkId = $shop->getTradeNetworkId();
        $shop->delete();
        $this->_redirect('/admin/shop/index/trade_network_id/'.$tradeNetworkId.'/');
    }
    
    public function regenerateAction() 
    {
        $id = $this->getQuotedParam('transmitter_id');
        $transmitter = Mapper_Transmitter::getInstance()->getEntityById($id);
        $this->checkForNullEntity($transmitter);
        $transmitter->regenerateFrequency();
        $this->_redirect('/admin/shop/edit/id/'.$transmitter->getShopId().'/');
    }
    
    public function buyBarcodeAction()
    {
        set_time_limit(600);
        $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);
        
        $generator = new Report_BuyBarcode_Generator();
        $generator->setShopList(Mapper_Shop::getInstance()->createAggregate(array($id),null,true))
                  ->build();
        $generator->send('Purchase_confirmation_"'.$shop->getName().', '.$shop->getAddress().'"');
        
        $bpaMain = $shop->getTradeNetwork()->getBpaMain();
        
        //Генерируем штрихкоды
        
        
        $excelTable = array(
                                array(
                                        iconv('utf-8','windows-1251','ID confirmation'),
                                        iconv('utf-8','windows-1251','Barcode'),
                                        iconv('utf-8','windows-1251','Partner name'),
                                        iconv('utf-8','windows-1251','Address')
                                    ),
                            );
                            
        for($i=0;$i<30;$i++)
        {
            $barcode = new Decorator_BuyBarcode(Entity_BuyBarcode::add($shop));
            $excelTable[] = array(
                                        $barcode->getId(),                            
                                        iconv('utf-8','windows-1251',$barcode->getBarcodeShort()),
                                        iconv('utf-8','windows-1251',$bpaMain->getName()),
                                        iconv('utf-8','windows-1251',$shop->getAddress())
                                 );
        }
        
        $data = array();
        foreach($excelTable as $row)
        {
            $data[] = implode(',',$row);
        }

        $body = implode("\r\n",$data);
        $this->_helper->layout->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender();
        $response = $this->getResponse();
        $response->setBody($body)
                ->setHeader('Content-Type', 'text/csv', true)
                ->setHeader("Content-Disposition", "attachment;filename=\"Purchase confirmation ".$shop->getName().', '.$shop->getAddress().".csv\"", true)
                ->setHeader("Content-Length", strlen($body), true);
        
    }
}