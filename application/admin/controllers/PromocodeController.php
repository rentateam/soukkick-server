<?php
class Admin_PromocodeController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'promocode';

    public function indexAction()
    {
        $pageNumber = $this->getRequest()->getParam('page', 1);
        $promocodeList = Mapper_Promocode::getInstance()->getList($pageNumber);

        $paginatorAdapter = new Paginator_Adapter_Null($promocodeList->totalCount());
        $paginator = new Zend_Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage(Aggregate_Promocode::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber($pageNumber);
        if ($promocodeList->totalCount() > Aggregate_Promocode::ITEMS_PER_PAGE)
        	$this->view->paginator = $paginator;

        $this->view->pageUrl = '/admin/promocode/index/';
        $this->view->pageNum = $pageNumber;
        $this->view->promocodeList = $promocodeList;

        $this->view->form = new Form_AdminPromocode(0);
    }

    public function addAction()
    {
        $form = new Form_AdminPromocode(0);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $logo = $form->uploadFile('logo');
                $promocode = Entity_Promocode::add($values['name'],$values['code'], null, $logo);
                $rule = Entity_Rule_Main::add(Entity_Bpa_Manager::getShopfeeBpaManager(), $values['name'], $values['date_from'], $values['date_to']);
                $ruleItem = Entity_Rule_Item::add($rule, $values['amount'], Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE, $promocode, $values['limit'], false);
                $promocode->setBoundRuleItem($ruleItem);
                $db->commit();
                $this->_redirect('/admin/promocode/edit/id/'.$promocode->getId().'/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $promocode = Mapper_Promocode::getInstance()->getEntityById($id);
        $this->checkForNullEntity($promocode);

        $form = new Form_AdminPromocode($id);
        $form->setValues($promocode);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $params = array(
                                    'name' => $values['name'],
                                    'code' => $values['code'],
                                );

                $file = $form->uploadFile('logo');
                if($file)
                {
                    $params['picture_path'] = $file;
                }

                $promocode->update($params);

                $rule = $promocode->getBoundRuleItem()->getRuleMain();

                $rule->setDateFrom($values['date_from']);
                $rule->setDateTo($values['date_to']);
                $rule->setChanged(true);

                $ruleItem = Entity_Rule_Item::add($rule, $values['amount'], Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE, $promocode, $values['limit'], true);
                $promocode->setBoundRuleItem($ruleItem);

                $db->commit();
                $this->_redirect('/admin/promocode/index/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }

        $this->view->promocode = $promocode;
        $this->view->form = $form;
    }
}