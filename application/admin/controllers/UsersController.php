<?php
class Admin_UsersController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'users';

    const PER_PAGE = 25;

    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('new');
        $layout->setLayoutPath('../application/admin/layouts/');
    }

    protected function initFilter()
    {
        $filter = new Form_Users_Filter();

        if ($this->getRequest()->getParam('filter')) {
            $filter->setValues($this->getRequest());
            $this->parameters->import($filter->getValues());
        }

        return $filter;
    }

    public function indexAction()
    {
        $filter = $this->initFilter();
        $this->view->filter = $filter;

        $sort = $this->getRequest()->getParam('sort', Criteria_User::ORDER_DATE_REGISTERED_DESC);
        $this->parameters->sort = $sort;

        $pageNumber = $this->getRequest()->getParam('page', 1);

        $criteria = new Criteria_User();
        $criteria->setSort($sort);
        $criteria->setDateFrom($filter->getElement('date_from')->getValue());
        $criteria->setDateTo($filter->getElement('date_to')->getValue());
        if ($filter->getElement('only_authed')->isChecked()) {
            $criteria->setAuthorized(true);
        }

        $criteria->setSearch($filter->getElement('search_type')->getValue(), $filter->getElement('search_text')->getValue());

        $users = Mapper_User::getInstance()->getListByCriteria($criteria, $pageNumber, self::PER_PAGE);

        $this->view->users = $users;

        $this->view->usersTotalNumber = Mapper_User::getInstance()->getUsersTotalNumber();
        $this->view->authorizedUsersTotalNumber = Mapper_User::getInstance()->getAuthorizedUsersTotalNumber();

        $this->view->usersAuthorizedTotalNumber = Mapper_User::getInstance()->getCount(new Criteria_User());

        $paginatorAdapter = new Paginator_Adapter_Null($users->totalCount());
        $paginatorAdapter->setQueryString($this->parameters->getQueryString());

        $paginator = new Zend_Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage(self::PER_PAGE);
        $paginator->setCurrentPageNumber($pageNumber);
        if ($users->totalCount() > self::PER_PAGE) {
            $this->view->paginator = $paginator;
        }

        $this->view->pageUrl = '/admin/users/index/';
        $this->view->pageNum = $pageNumber;

    }

    public function showAction()
    {
        $this->initFilter();

        $id = $this->getRequest()->getParam('id');
        $user = Mapper_User::getInstance()->getEntityById($id);
        $this->checkForNullEntity($user);

        $this->view->user = $user;
    }

    public function banAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user = Mapper_User::getInstance()->getEntityById($userId);
        $this->checkForNullEntity($user);

        $reason = $this->getRequest()->getParam('reason');

        $user->ban($reason);
        $this->_redirect($user->getAdminUrl());
    }

    public function unbanAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user = Mapper_User::getInstance()->getEntityById($userId);
        $this->checkForNullEntity($user);

        $user->unban();
        $this->_redirect($user->getAdminUrl());
    }
}