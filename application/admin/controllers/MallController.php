<?php
class Admin_MallController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'mall';

    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('new');
        $layout->setLayoutPath('../application/admin/layouts/');

        $this->_helper->contextSwitch()
                    ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        $pageNumber = $this->getRequest()->getParam('page', 1);
        $mallList = Mapper_Mall::getInstance()->getList($pageNumber);

        $paginatorAdapter = new Paginator_Adapter_Null($mallList->totalCount());
        $paginator = new Zend_Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage(Aggregate_Mall::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber($pageNumber);
        if ($mallList->totalCount() > Aggregate_Mall::ITEMS_PER_PAGE) {
        	$this->view->paginator = $paginator;
        }

        $this->view->pageUrl = '/admin/mall/index/';
        $this->view->pageNum = $pageNumber;
        $this->view->mallList = $mallList;

        $this->view->form = new Form_AdminMall(0);
    }

    public function addAction()
    {
        $form = new Form_AdminMall(0);

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $logo = $form->uploadFile('logo');
                $mall = Entity_Mall::add($values['name'],$values['address'],$values['lat'], $values['long']);
                $db->commit();
                $this->_redirect('/admin/mall/edit/id/'.$mall->getId().'/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $mall = Mapper_Mall::getInstance()->getEntityById($id);
        $this->checkForNullEntity($mall);

        $form = new Form_AdminMall($id);
        $form->setValues($mall);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $params = array(
                                    'name' => $values['name'],
                                    'address' => $values['address'],
                                );

                $file = $form->uploadFile('logo');
                if($file)
                {
                    $params['picture_path'] = $file;
                }
                $oldLat = $mall->getLat();
                $oldLong = $mall->getLongtitude();
                $mall->update($params);

                if(isset($values['lat']) && $values['lat'] != $oldLat || isset($values['long']) && $values['long'] != $oldLong)
                {
                    $mall->updateLocation($values['lat'],$values['long']);
                }
                $db->commit();
                $this->_redirect('/admin/mall/index/');
            }
            catch(Entity_Transmitter_Exception $e)
            {
                $this->view->showRegenerate = true;
                $this->view->collisionMessage = $e->getMessage();
                $this->view->transmitter = $e->getTransmitter();
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }

        $this->view->mall = $mall;
        $this->view->form = $form;

        $mapForm = new Form_AdminMallMap(0);
        $mapForm->setMallId($mall->getId());
        $this->view->mapForm = $mapForm;

        $this->view->mallCandidateList = $mall->getShopCandidateList();
        $this->view->mallTagList = Mapper_Mall2Shop::getInstance()->getListUnboundByMall($mall);
        $this->view->mallBoundTagList = Mapper_Mall2Shop::getInstance()->getListBoundByMall($mall);
    }

    public function formAction()
    {
    	$id = $this->getQuotedParam('id');
    	$form = new Form_AdminMall($id);
    	if($id)
    	{
    		$mall = Mapper_Mall::getInstance()->getEntityById($id);
    		$this->checkForNullEntity($mall);
    		$form->setValues($mall);
    	}
    	else
    	{
    		$mall = null;
    	}

    	$this->view->mall = $mall;
    	$this->view->form = $form;

    	$view = new Zend_View();
    	$view->setScriptPath(array(realpath(Application::ADMIN_VIEWS_PATH),realpath(Application::VIEWS_PATH)));
    	$view->assign(array('form' => $form,'mall' => $mall));
    	$this->view->html = $view->render('mall/form.phtml');
    }

    public function bindShopAction()
    {
        $mall2shopId = $this->getQuotedParam('mall2shop_id');
        $mall2shop = Mapper_Mall2Shop::getInstance()->getEntityById($mall2shopId);
        $this->checkForNullEntity($mall2shop);
        $shopId = $this->getQuotedParam('shop_id');
        if($shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            $this->checkForNullEntity($shop);
        }
        else
        {
            $shop = null;
        }

        $mall2shop->setShop($shop);
        $this->_redirect('/admin/mall/edit/id/'.$mall2shop->getMallId().'/');
    }

    public function deleteAction()
    {
    	$id = $this->getQuotedParam('id');
    	$mall = Mapper_Mall::getInstance()->getEntityById($id);
    	$this->checkForNullEntity($mall);
    	$mall->delete();
    	$this->_redirect('/admin/mall/');
    }
}