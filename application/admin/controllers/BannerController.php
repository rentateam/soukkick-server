<?php

class Admin_BannerController extends Shopfee_Controller_Action_Admin 
{
    protected $_currentModule = 'banner';
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page",1);
        
        $bannerList = Mapper_Banner::getInstance()->getList($pageNum);
        $bannerCount = $bannerList->totalCount();  
        $this->view->bannerList = $bannerList;
        $paginator = new Zend_Paginator( new Paginator_Adapter_Null($bannerCount) );
        $paginator->setItemCountPerPage(Aggregate_Banner::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber( $pageNum );
        if($bannerCount > Aggregate_Banner::ITEMS_PER_PAGE)
            $this->view->paginator = $paginator;
            
        $this->view->pageUrl = '/admin/banner/index/';  
        $this->view->pageNum = $pageNum;
    }
    
    public function addAction() 
    {
        $form = new Form_Banner(0);
        
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $uploadedFileName = $form->uploadFile('file');
                $banner = Entity_Banner::add($values['date_expire'],$uploadedFileName);
                $db->commit();
                $this->_redirect('/admin/banner/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $this->view->form = $form;
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $banner = Mapper_Banner::getInstance()->getEntityById($id);
        $this->checkForNullEntity($banner);
        
        $form = new Form_Banner($id);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $params = array(
                                'date_expire' => $values['date_expire'],
                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $banner->update($params);
                $db->commit();
                $this->_redirect('/admin/banner/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($banner);
        
        $this->view->banner = $banner;
        $this->view->form = $form;
    }
    
    public function deleteAction() 
    {
        $id = $this->getQuotedParam('id');
        $banner = Mapper_Banner::getInstance()->getEntityById($id);
        $this->checkForNullEntity($banner);
        $banner->delete();
        $this->_redirect('/admin/banner/');
    }
}