<?php

class Admin_PrizeController extends Shopfee_Controller_Action_Admin 
{
    protected $_currentModule = 'prize';
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page",1);
        
        $prizeList = Mapper_Prize::getInstance()->getList($pageNum);
        $prizeCount = $prizeList->totalCount();  
        $this->view->prizeList = $prizeList;
        $paginator = new Zend_Paginator( new Paginator_Adapter_Null($prizeCount) );
        $paginator->setItemCountPerPage(Aggregate_Prize::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber( $pageNum );
        if($prizeCount > Aggregate_Prize::ITEMS_PER_PAGE)
            $this->view->paginator = $paginator;
            
        $this->view->pageUrl = '/admin/prize/index/';   
        $this->view->pageNum = $pageNum;
    }
    
    public function addAction() 
    {
        $form = new Form_Prize(0);
        
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $uploadedFileName = $form->uploadFile('file');
                $prize = Entity_Prize::add($values['type'],$values['name'],$values['description'],$uploadedFileName,$values['price']);
                $ruleItem = Entity_Rule_Item::add(Entity_Bpa_Main::getShopfeeRule(), $prize->getPrice(), Entity_Rule_Item::TYPE_PRIZE, $prize);
                $db->commit();
                $this->_redirect('/admin/prize/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        /*$fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');*/
        $this->view->form = $form;
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $prize = Mapper_Prize::getInstance()->getEntityById($id);
        $this->checkForNullEntity($prize);
        
        $form = new Form_Prize($id);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $params = array(
                                'name' => $values['name'],
                                'description' => $values['description'],
                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $prize->updateWithMobileCache($params);
                $prize->setPrice($values['price']);
                $db->commit();
                $this->_redirect('/admin/prize/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($prize);
        
        $this->view->prize = $prize;
        $this->view->form = $form;
        /*$fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');*/
    }
    
    public function deleteAction() 
    {
        $id = $this->getQuotedParam('id');
        $prize = Mapper_Prize::getInstance()->getEntityById($id);
        $this->checkForNullEntity($prize);
        $ruleItem = $prize->getRuleItem();
        if($ruleItem)
           $ruleItem->updateActive(false);
        $prize->delete();
        $this->_redirect('/admin/prize/');
    }
}