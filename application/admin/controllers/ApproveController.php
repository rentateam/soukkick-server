<?php

class Admin_ApproveController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'approve';
    
    public function indexAction() 
    {
        $this->view->approveList = Mapper_Trade_Network::getInstance()->getListForApprove(); 
    }

    public function userAction() 
    {
        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);
        
        $tradeNetwork->approve();
        $tradeNetwork->sendApproveLetter();

        $this->_redirect('/admin/user-shop/edit/id/'.$id.'/');
    }
    
    public function deleteAction() 
    {
        $id = $this->getQuotedParam('id');
        $user = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($user);
        $user->delete();

        $this->_redirect('/admin/approve/');
    }
}