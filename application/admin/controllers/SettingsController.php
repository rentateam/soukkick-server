<?php
class Admin_SettingsController extends Shopfee_Controller_Action_Admin
{
    protected $_currentModule = 'settings';

    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('new');
        $layout->setLayoutPath('../application/admin/layouts/');
    }

    public function indexAction()
    {
        if ($deletedId = $this->getRequest()->getParam('deleted')) {
            $this->view->deletedId = $deletedId;
        }

        if ($createdId = $this->getRequest()->getParam('created')) {
            $this->view->createdId = $createdId;
        }

        if ($modifiedId = $this->getRequest()->getParam('modified')) {
            $this->view->modifiedId = $modifiedId;
        }

        $this->view->options = Mapper_Settings_Option::getInstance()->getList();
    }

    public function addAction()
    {
        $form = new Form_Settings_Option();

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getParams())) {

                $caption = $this->getRequest()->getParam('caption');
                $slug = $this->getRequest()->getParam('slug');
                $data = $this->getRequest()->getParam('data');

                $option = Mapper_Settings_Option::getInstance()->add($caption, $slug, $data);
                $this->_redirect("/admin/settings/?created={$option->getId()}");
            }
        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $option = Mapper_Settings_Option::getInstance()->getEntityById($id);
        $this->checkForNullEntity($option);

        $form = new Form_Settings_Option();
        $form->setValues($option);

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getParams())) {
                $option->update(array(
                    'caption' => $this->getRequest()->getParam('caption'),
                    'slug' => $this->getRequest()->getParam('slug'),
                    'data' => $this->getRequest()->getParam('data')
                ));
                $this->_redirect("/admin/settings/?modified={$option->getId()}");
            }
        }

        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $option = Mapper_Settings_Option::getInstance()->getEntityById($id);
        $this->checkForNullEntity($option);

        $option->delete();

        $this->_redirect("/admin/settings/?deleted={$id}");
    }

}