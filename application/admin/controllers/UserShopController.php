<?php

class Admin_UserShopController extends Shopfee_Controller_Action_Admin
{
    const PER_PAGE = 30;

    protected $_currentModule = 'user-shop';

    public function indexAction()
    {
        $filter = new Form_Filter_UserShop();
        $this->parameters->name = $this->getRequest()->getParam('name');

        if ($filter->isValid($this->getRequest()->getParams())) {
            $name = $filter->getElement('name')->getValue();
        } else {
            $name = NULL;
        }

        $pageNum = $this->getQuotedParam("page",1);
        $tradeNetworkList = Mapper_Trade_Network::getInstance()->getList($pageNum, $name, self::PER_PAGE,true);
        $tradeNetworkCount = $tradeNetworkList->totalCount();
        $this->view->tradeNetworkList = $tradeNetworkList;

        $paginatorAdapter = new Paginator_Adapter_Null($tradeNetworkCount);
        $paginatorAdapter->setQueryString($this->parameters->getQueryString());

        $paginator = new Zend_Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage(self::PER_PAGE);
        $paginator->setCurrentPageNumber($pageNum);

        if($tradeNetworkCount > self::PER_PAGE)
            $this->view->paginator = $paginator;

        $this->view->pageUrl = '/admin/user-shop/index/';
        $this->view->pageNum = $pageNum;
        $this->view->filter = $filter;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);

        $form = new Form_AdminTradeNetwork($id);
        $form->setValues($tradeNetwork);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $updateTradeNetworkParams = array(
                                                'title' => $values['title'],
                                                'description' => $values['description'],
                                                'phone' => $values['phone'],
                                                'is_recommended' => $values['is_recommended'] == 'checked'
                                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $updateTradeNetworkParams['picture_path'] = $uploadedFileName;
                }
                $uploadedRectangleFileName = $form->uploadFile('rectangle');
                if($uploadedRectangleFileName)
                {
                    $updateTradeNetworkParams['rectangle_picture_path'] = $uploadedRectangleFileName;
                }
                $tradeNetwork->updateWithMobileCache($updateTradeNetworkParams);

                $tradeNetwork->setIntegrationTypeAndDescription($values['integration_type'] ? $values['integration_type'] : null,$values['integration_description']);
                $db->commit();
                $this->_redirect('/admin/user-shop/');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }

        $this->view->tradeNetwork = $tradeNetwork;
        $this->view->form = $form;
    }

    public function loginAsUserShopAction()
    {
        Auth::logoffShop();
        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);

        $manager = $tradeNetwork->getAnyBpaManager();
        Auth::loginShop($manager->getEmail(), $manager->getPassword(), false, true);
        $this->_redirect('/edit/');
    }

    public function addFromAdminAction()
    {
        Auth::logoffShop();
        $this->_redirect('/register/shop/');
    }

    public function deleteAction()
    {
        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);
        $tradeNetwork->delete();
        $this->_redirect('/admin/user-shop/');
    }

    public function hideAction()
    {
        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);
        $tradeNetwork->setHidden($this->getQuotedParam('is_hidden') ? true : false);
        $this->_redirect('/admin/user-shop/');
    }

    public function buyBarcodeAction()
    {
        set_time_limit(600);

        $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);

        $generator = new Report_BuyBarcode_Generator();
        $generator->setShopList($tradeNetwork->getShops())
                  ->build();
        $generator->send('Purchase_confirmation_"'.$tradeNetwork->getTitle().'"');
    }
}