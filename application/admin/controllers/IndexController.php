<?php

class Admin_IndexController extends Shopfee_Controller_Action_Admin
{
    public function indexAction() 
    {
        if(Acl::canAdmin($this->currentAdmin)){
            $this->_redirect('/admin/user-shop/');
        } else if (Acl::canAdminReward($this->currentAdmin)){
            $this->_redirect('/reward/');
        } else {
            $this->_redirect('/');
        }
    }
    
    public function siteAction() 
    {
        $this->_redirect('/');
    }
    
    public function rewardAction()
    {
        $this->_redirect('/reward/');
    }
    
    public function flushCacheAction()
    {
        SFM_Cache_Memory::getInstance()->flush();
        die();
    }
    
    public function exitAction()
    {
        Auth::logoffAdmin();
        $this->_redirect("/admin/");
    }
}
