<?php

class Admin_LoginController extends Shopfee_Controller_Action
{
    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    
        $cookieLogin = isset($_COOKIE[Auth::COOKIE_ADMIN_LOGIN]) ? $_COOKIE[Auth::COOKIE_ADMIN_LOGIN] : null;
        $cookiePassword = isset($_COOKIE[Auth::COOKIE_ADMIN_PASSWORD]) ? $_COOKIE[Auth::COOKIE_ADMIN_PASSWORD] : null;
        $authSuccess = false;
        $backUrl = $this->getBackUrl();

        $remember = isset($_COOKIE[Auth::COOKIE_ADMIN_REMEMBER]) ? $_COOKIE[Auth::COOKIE_ADMIN_REMEMBER] : false;
        $form = new Form_Login($backUrl, $remember);
        $form->setAction('/admin/login/');

        //if the user is already logged in
        if (Auth::getCurrentAdmin()){
            $this->_redirect($backUrl);
        } else if ($cookieLogin && $cookiePassword) {
            //else check the cookie
            $authResult = Auth::loginAdmin($cookieLogin, $cookiePassword, false, true);
            if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                $authSuccess = true;
        } else {  //else it is just form processing or form rendering
            //form processing
            if ($this->getRequest()->isPost() && $form->isValid($_POST)) {
                $values = $form->getValues();
                $authResult = Auth::loginAdmin($values['email'], $values['password'], ($values['remember'] == 'checked' || $values['remember'] == 1));

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                    $authSuccess = true;
                else
                    $this->view->error = "Неправильный логин или пароль";
            }
        }

        if ($authSuccess) {
            //if the last page was from authorize controller
            //refirect to main page to avoid loops
            if (false === strpos($backUrl, 'login'))
                $this->_redirect(urldecode($backUrl));
            else
                $this->_redirect('/admin/');
        } else {
            $this->view->form = $form;
        }

        $this->view->backUrl = urlencode($backUrl);
    }

    public function forbiddenAction()
    {
        
    }
}