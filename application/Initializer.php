<?php
/**
 *
 * Initializes configuration depndeing on the type of environment
 * (test, development, production, etc.)
 *
 * This can be used to configure environment variables, databases,
 * layouts, routers, helpers and more
 *
 */
class Initializer extends Zend_Controller_Plugin_Abstract
{
    /**
     * @var Zend_Config
     */
    protected static $_config;

    /**
     * @var string Current environment
     */
    protected $_env;

    /**
     * @var string Current user
     */
    protected $_envUser;

    /**
     * @var Zend_Controller_Front
     */
    protected $_front;

    /**
     * @var string Path to application root
     */
    protected $_root;

    /**
     * Constructor
     *
     * Initialize environment, root path,, and configuration.
     *
     * @param  string $env
     * @param  string|null $root
     * @return void
     */
    public function __construct($root = null)
    {
        //Shopfee_Logger::getInstance()->debug( $_SERVER );

        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");

        //get environment from ini file
        $config = new Zend_Config_Ini(Application::ENVIRONMENT_CONFIG_PATH);
        $env = $config->{Application::ENVIROMENT_NAME};
        $envUser = $config->{Application::ENVIROMENT_USERNAME};
        Zend_Registry::set(Application::ENVIROMENT_NAME, $env);

        $this->_setEnv($env);
        $this->_setEnvUser($envUser);
        if (null === $root) {
            $root = realpath(dirname(__FILE__) . '/../');
        }
        $this->_root = $root;

        $this->initConfig();

        $this->_front = Zend_Controller_Front::getInstance();


        // set the test and development environment parameters
        if ($env !== Application::ENVIROMENT_PRODUCTION) {
            require_once 'Zend/Debug.php';
            // Enable all errors so we'll know when something goes wrong. 
            error_reporting(E_ALL & ~E_STRICT /*&& ~E_NOTICE*/);
            ini_set('display_startup_errors', 1);  
            ini_set('display_errors', 1);
            //this don't work with errorController
//          $this->_front->throwExceptions(true);
        } else {
            error_reporting(0);
            ini_set('log_errors', true);
            ini_set('display_startup_errors', 0);
            ini_set('display_errors', 0);
        }
    }

    /**
     * Initialize environment
     *
     * @param  string $env
     * @return void
     */
    protected function _setEnv($env)
    {
        $this->_env = $env;
    }

    /**
     * Initialize environment user
     *
     * @param  string $envUser
     * @return void
     */
    protected function _setEnvUser($envUser)
    {
        $this->_envUser = $envUser;
    }


    /**
     * Init config and set it in registry
     *
     * @return void
     */
    protected function initConfig()
    {
        $config = new Zend_Config_Ini(Application::CONFIG_PATH);
        Zend_Registry::set(Application::ENVIROMENT_NAME, $this->_env);
        //take config from environment's section
        $configSectionName = $this->_env;
        if(!empty($this->_envUser))
            $configSectionName.= '-'.$this->_envUser;
        $config = $config->{$configSectionName};
        Zend_Registry::set(Application::CONFIG_NAME, $config);
    }

    /**
     * Route startup
     *
     * @return void
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
//          $this->initDb();
        $this->initHelpers();
        $this->initView();
        $this->initPlugins();
        $this->initRoutes();
        $this->initControllers();
        $this->initSession();
        $this->initDbQueryProfiler();

    }

    public function initDbQueryProfiler()
    {
        if( $this->_env != Application::ENVIROMENT_PRODUCTION ) {
            $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
            $profiler->setEnabled(true);
            SFM_DB::getInstance()->setProfiler($profiler);
        }
    }

    public function initSession()
    {
        $sessionHandler = new Shopfee_Session_SaveHandler_Memcached(SFM_Cache_Session::getInstance(),3600);
        Zend_Session::setSaveHandler($sessionHandler);
    }


    /**
     * Initialize databases
     *
     * @return void
     */
    public function initDb()
    {

    }

    /**
     * Initialize action helpers
     *
     * @return void
     */
    public function initHelpers()
    {
        // register the default action helpers
        Zend_View_Helper_PaginationControl::setDefaultViewPartial(
            'paginators/default.phtml'
        );
        Zend_Paginator::setDefaultScrollingStyle('Sliding');
    }

    /**
     * Initialize view
     *
     * @return void
     */
    public function initView()
    {
        // Bootstrap layouts
        $layout = Zend_Layout::startMvc(array(
            'layoutPath' => $this->_root .  '/application/default/layouts',
            'layout' => 'main'
        ));
        $layout->getView()->addHelperPath($this->_root .  '/application/default/views/helpers', 'Helper');
        $layout->getView()->addHelperPath($this->_root .  '/application/admin/views/helpers', 'Helper');
        $layout->getView()->addHelperPath($this->_root .  '/application/edit/views/helpers', 'Helper');

        $layout->getView()->addScriptPath('../application/default/views/scripts/');
    }

    /**
     * Initialize plugins
     *
     * @return void
     */
    public function initPlugins()
    {
//        $this->_front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler());
    }

    /**
     * Initialize routes
     *
     * @return void
     */
    public function initRoutes()
    {
        // use nginx for that!

        ///$router = $this->_front->getRouter();
        //$routerConfig = new Zend_Config_Ini(Application::CONFIG_ROUTES_PATH, "production");
        //$router->addConfig($routerConfig, 'routes');
    }

    /**
     * Initialize Controller paths
     *
     * @return void
     */
    public function initControllers()
    {
        $this->_front->addControllerDirectory(Application::CONTROLLERS_PATH, 'default');
        $this->_front->addControllerDirectory(Application::ADMIN_CONTROLLERS_PATH, 'admin');
        $this->_front->addControllerDirectory(Application::EDIT_CONTROLLERS_PATH, 'edit');
        $this->_front->addModuleDirectory('../application/');
    }
}
?>
