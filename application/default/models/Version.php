<?php
class Version
{
    const SUPPORT_UPDATE_REQUIRED = 0;
    const SUPPORT_UPDATE_AVAILABLE = 1;
    const SUPPORT_UPDATE_UP_TO_DATE = 2;

    protected $_minimalVersion = null;
    protected $_currentVersion = null;
    protected $_currentUser = null;

    public function __construct(Entity_User $currentUser = null)
    {
        $this->_minimalVersion = Application::getMinimalAPIVersion();
        $this->_currentVersion = Application::getCurrentAPIVersion();
        $this->_currentUser = $currentUser;
    }

    /**
     * Возвращает статус текущий версии (есть ли обновления и критичны ли они)
     * @param string|float $version
     * @return string
     */
    public function getSupportStatus($version)
    {
        $version = floatval($version);
        if($version >= $this->_currentVersion)
        {
            return self::SUPPORT_UPDATE_UP_TO_DATE;
        }
        else if($version >= $this->_minimalVersion)
        {
            return self::SUPPORT_UPDATE_AVAILABLE;
        }
        else
        {
            return self::SUPPORT_UPDATE_REQUIRED;
        }
    }

    public function setSupportStatusToView(Zend_View $view, $clientVersion, $url = null)
    {
        if($this->_currentUser && $this->_currentUser->isBanned())
        {
            $view->error = 'user_banned';
            $view->errorMessage = 'Пользователь заблокирован';
        }
        else
        {
            $status = $this->getSupportStatus($clientVersion);
            switch($status)
            {
                case self::SUPPORT_UPDATE_REQUIRED;
                    $view->error = 'update_required';
                    $view->errorMessage = 'Необходимо обновление';
                    break;
                case self::SUPPORT_UPDATE_AVAILABLE:
                    $view->error = 'update_available';
                    $view->errorMessage = 'Доступно обновление';
                    break;
                case self::SUPPORT_UPDATE_UP_TO_DATE:
                    break;
            }

            if($url)
                $this->logVersion($clientVersion,$url);
        }

    }

    protected function logVersion($clientVersion,$url)
    {
        $db = SFM_DB::getInstance();
        $params = array(
                            'version' => $clientVersion,
                            'url' => $url,
                            'datetime_created' => date('Y-m-d H:i:s')
                        );
        $db->insert('INSERT DELAYED INTO client_version (version,url,datetime_created) VALUES (:version,:url,:datetime_created)',$params);
    }

}