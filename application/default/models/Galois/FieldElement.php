<?php

class Galois_FieldElement
{
    /**
     * @var Galois_Field
     */
    protected $_gf;
    
    /**
     * @var integer
     */
    protected $_polyValue;
    
    public function __construct(Galois_Field $gf = NULL, $v)
    {
        if ($gf !== NULL)
        {
            $this->_gf = $gf;
            $this->_polyValue = $v & $gf->getSize();
        }
        else
            $this->_polyValue = $v;
    }
    
    public function getPolyValue()
    {
        return $this->_polyValue;
    }
    
    public function divide(Galois_FieldElement $gfe)
    {
        $newElement = clone $this;
        $newElement->_polyValue = $this->_gf->div($this->_polyValue,$gfe->_polyValue);
        return $newElement;
    } 
    
    public function multiply(Galois_FieldElement $gfe)
    {
        $newElement = clone $this;
        $newElement->_polyValue = $this->_gf->mul($this->_polyValue,$gfe->_polyValue);
        return $newElement;
    }
    
    public function add(Galois_FieldElement $gfe)
    {
        $newElement = clone $this;
        $newElement->_polyValue ^= $gfe->_polyValue;
        return $newElement;
    }
    
    /**
     * @param integer $x
     * */       
    public function isEqualTo($x)
    {
        return $this->_polyValue == $x;
    } 
}