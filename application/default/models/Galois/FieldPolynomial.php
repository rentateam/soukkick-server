<?php
class Galois_FieldPolynomial
{
    protected $_gf;
    
    /**
     * @var array of GaloisFieldElement
     */
    protected $_poly = array();
    
    /**
     * 
     * Enter description here ...
     * @param Galois_Field $gf
     * @param integer $size
     * @param array of Galois_FieldElement $gfe
     */
    public function __construct(Galois_Field $gf = NULL, $size = 0, array $gfe = array())
    {
        $this->_gf = $gf;
        
        if(!empty($gfe))
        {
            for($i = 0; $i <= $size; $i++)
            {
                $this->_poly[$i] = $gfe[$i];
            }
        }
        else
        {
            for($i = 0; $i < $size + 1; $i++)
            {
                $this->_poly[$i] = new Galois_FieldElement($gf,0);
            }
        }
   }
    
    public function deg()
    {
        return (count($this->_poly) - 1);
    }
    
    public function divide(Galois_FieldPolynomial $divisor)
    {
        if (
                ($this->_gf->isEqual($divisor->_gf)) &&
                ($this->deg() >= $divisor->deg()) &&
                ($divisor->deg() >= 0)
           )
        {
            $quotient = new Galois_FieldPolynomial($this->_gf, $this->deg() - $divisor->deg() + 1);
            $remainder = new Galois_FieldPolynomial($this->_gf, $divisor->deg() - 1);
        
            for($i = $this->deg(); $i >= 0; $i--)
            {
                if ($i <= $quotient->deg())
                {
                    $new = $remainder->getByIndex($remainder->deg())->divide($divisor->getByIndex($divisor->deg()));
                    $quotient->setByIndex($i,$new);
                    
                    for($j = $remainder->deg(); $j > 0; $j--)
                    {
                        $multiply = $quotient->getByIndex($i)->multiply($divisor->getByIndex($j));
                        $remainder->setByIndex($j,$remainder->getByIndex($j - 1)->add($multiply));
                    }
                    
                    $remainder->setByIndex(0,$this->_poly[$i]->add($quotient->getByIndex($i)->multiply($divisor->getByIndex(0))));
                }
                else
                {
                    for($j = $remainder->deg(); $j > 0; $j--)
                    {
                        $remainder->setByIndex($j,$remainder->getByIndex($j - 1));
                    }
                    $remainder->setByIndex(0,$this->_poly[$i]);
                }
            }
        
            $this->simplify($remainder);
            $this->_poly = $remainder->_poly;
        }
        
        return $this;
    }
    
    private function simplify(Galois_FieldPolynomial $polynomial)
    {
        if(!empty($this->_poly))
        {
            $polinomialLength = count($polynomial->_poly); 
            $last = $polinomialLength - 1;
            
            while(($last >= 0) && ($polinomialLength > 0))
            {
                if ($polynomial->_poly[$last]->isEqualTo(0))
                    array_pop($polynomial->_poly);
                else
                    break;
                
                $last = count($polynomial->_poly) - 1;
            }
        }
    }
    
    /**
     * @var GaloisFieldElement
     * */        
    public function getByIndex($i)
    {
        if(isset($this->_poly[$i]))
            return $this->_poly[$i];
        else 
            return null;
    }
    
    public function setByIndex($i,$value)
    {
        $this->_poly[$i] = $value;
    }
    
    public function valid()
    {
        return count($this->_poly) > 0;
    }
}