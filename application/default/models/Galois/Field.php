<?php
class Galois_Field
{
    /**
     * @var integer
     */
    protected $_power;
    /**
     * @var integer
     */
    protected $_fieldSize;
    /**
     * @var integer
     */
    protected $_primPolyHash;
    /**
     * @var array of integer
     */
    protected $_alphaTo = array();
    /**
     * @var array of integer
     */
    protected $_indexOf = array();
    
    const GFERROR = -1;
    
    /**
     * @param integer $pwr
     * @param array of integer $primitivePoly
     */
    public function __construct($pwr, array $primitivePoly)
    {
        $this->_power = $pwr;
        $this->_fieldSize = pow($pwr,2) - 1;
        
      /*#if !defined(NO_GFLUT)

        mul_table   = new GFSymbol* [($this->_fieldSize + 1)];
        div_table   = new GFSymbol* [($this->_fieldSize + 1)];
        exp_table   = new GFSymbol* [($this->_fieldSize + 1)];
        mul_inverse = new GFSymbol  [($this->_fieldSize + 1) * 2];

        for (unsigned int i = 0; i < ($this->_fieldSize + 1); i++)
        {
           mul_table[i] = new GFSymbol [($this->_fieldSize + 1)];
           div_table[i] = new GFSymbol [($this->_fieldSize + 1)];
           exp_table[i] = new GFSymbol [($this->_fieldSize + 1)];
        }

      #else

        mul_table   = new GFSymbol *[1];
        div_table   = new GFSymbol *[1];
        exp_table   = new GFSymbol *[1];
        mul_inverse = new GFSymbol  [1];

      #endif*/

        $this->_primPolyHash = 0xAAAAAAAA;
        
        for ($i = 0; $i < $pwr; $i++)
        {
            $this->_primPolyHash += (($i & 1) == 0) ? (  ($this->_primPolyHash <<  7) ^ $primitivePoly[$i] ^ ($this->_primPolyHash >> 3)) :
                                            (~(($this->_primPolyHash << 11) ^ $primitivePoly[$i] ^ ($this->_primPolyHash >> 5)));
        }

        $this->generateField($primitivePoly);
    } 
    
    public function isEqual(Galois_Field $to)
    {
        return (($this->_power == $to->_power) && ($this->_primPolyHash == $to->_primPolyHash)) ;
    } 
    
    
    public function generateField(array $primitivePoly)
    {
        /*
         Note: It is assumed that the degree of the primitive
               polynomial will be equivelent to the m value as
               in GF(2^m)
        */
        
        /*
         need to update using stanford method for prim-poly generation.
        */
        $mask = 1;

        $this->_alphaTo[$this->_power] = 0;
        
        for ($i = 0; $i < $this->_power; $i++)
        {
            $this->_alphaTo[$i]           = $mask;
            $this->_indexOf[ $this->_alphaTo[$i]] = $i;
            
            if ($primitivePoly[$i] != 0)
            {
                $this->_alphaTo[$this->_power] ^= $mask;
            }
            
            $mask <<= 1;
        }

      $this->_indexOf[$this->_alphaTo[$this->_power]] = $this->_power;

      $mask >>= 1;

      for ($i = $this->_power + 1; $i < $this->_fieldSize; $i++)
      {
         if ($this->_alphaTo[$i - 1] >= $mask)
           $this->_alphaTo[$i] = $this->_alphaTo[$this->_power] ^ (($this->_alphaTo[$i - 1] ^ $mask) << 1);
         else
           $this->_alphaTo[$i] = $this->_alphaTo[$i - 1] << 1;

         $this->_indexOf[$this->_alphaTo[$i]] = $i;
      }

      $this->_indexOf[0] = self::GFERROR;
      $this->_alphaTo[$this->_fieldSize] = 1;

      /*#if !defined(NO_GFLUT)

        for ($i = 0; i < $this->_fieldSize + 1; $i++)
        {
           for ($j = 0; j < $this->_fieldSize + 1; $j++)
           {
              mul_table[$i][j] = gen_mul(i,j);
              div_table[i][j] = gen_div(i,j);
              exp_table[i][j] = gen_exp(i,j);
           }
        }

        for ($i = 0; $i < ($this->_fieldSize + 1); $i++)
        {
           mul_inverse[i] = gen_inverse(i);
           mul_inverse[i + ($this->_fieldSize + 1)] = mul_inverse[i];
        }

      #endif*/
   }
   
   public function getSize()
   {
       return $this->_fieldSize;
   }
   
    /**
     *  @param integer a
     *  @param integer b     
     */     
    public function div($a,$b) 
    {
        if (($a == 0) || ($b == 0))
          return 0;
        else
          return $this->_alphaTo[$this->fastModulus($this->_indexOf[$a] - $this->_indexOf[$b] + $this->_fieldSize)];
    }
    
    /**
     *  @param integer a
     *  @param integer b     
     */     
    public function mul($a,$b) 
    {
        if (($a == 0) || ($b == 0))
          return 0;
        else
          return $this->_alphaTo[$this->fastModulus($this->_indexOf[$a] + $this->_indexOf[$b])];
    }
    
    /**
     * @param integer $x
     * */       
    private function fastModulus($x)
    {
        while ($x >= (int)$this->_fieldSize)
        {
            $x -= $this->_fieldSize;
            $x  = ($x >> $this->_power) + ($x & (int)$this->_fieldSize);
        }
        return $x;
    }
}