<?php
class Entity_Sales extends Entity_ShopsBound
{
    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getShortDescription()
    {
        return $this->short_description;
    }

    public function getShopIds()
    {
        return $this->mapper->getShopIdsBySales($this);
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        if(isset($params['shop_ids']))
        {
            $this->mapper->replaceShopIds($this,$params['shop_ids']);
        }
        $return = parent::update($params);
        $this->clearCacheByShopIds();
        return $return;
    }

    protected function clearCacheByShopIds()
    {
        foreach($this->getShopIds() as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Sales::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        $this->getTradeNetwork()->deleteInfoMobileCache();
    }

    public function delete()
    {
        $shopIds = $this->getShopIds();
        Mapper_Sales::getInstance()->replaceShopIds($this,array());
        $return = parent::delete();
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Sales::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        //Предполагаем, что все магазины принадлежат к одной сети - просто берем последний
        if($shop)
            Mapper_Sales::getInstance()->clearCacheByTradeNetwork($shop->getTradeNetwork());
        $this->getTradeNetwork()->deleteInfoMobileCache();

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getBigPicturePathFull(true));
        $s3->delete($this->getSmallPicturePathFull(true));

        return $return;
    }

    public static function add($name,Entity_Trade_Network $tradeNetwork,array $shopIds,$description,$shortDescription,$picture)
    {
        $entity = Mapper_Sales::getInstance()->add($name,$tradeNetwork,$shopIds,$description,$shortDescription);
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Sales::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        Mapper_Sales::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        $tradeNetwork->deleteInfoMobileCache();
        return $entity;
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getSalesBigPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    public function getSmallPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getSalesSmallPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/sales_big_'.$filenameOriginalExtension;
        $result1 = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullBigPicturePath,Application::getSalesBigPictureWidth());
        $s3->move($pngFileName,Application::getSalesBigPicturePath(true).$filenamePngExtension);

        $fullSmallPicturePath = '/tmp/sales_small_'.$filenameOriginalExtension;
        $result2 = copy($picturePath, $fullSmallPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullSmallPicturePath,Application::getSalesSmallPictureSide());
        $s3->move($pngFileName,Application::getSalesSmallPicturePath(true).$filenamePngExtension);

        if($result1 && $result2)
        {
            $result2 = $result2 && parent::update(array('picture_path' => $filenamePngExtension));
        }
        return $result2;
    }

    public function getJsonObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'picture' => $this->getBigPicturePathFull(false), /**@deprecated*/
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                        'description' => $this->getDescription()
                    );
    }

    public function getJsonReplicationObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'picture' => $this->getBigPicturePathFull(false), /**@deprecated*/
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                        'description' => $this->getDescription()
        );
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }
}