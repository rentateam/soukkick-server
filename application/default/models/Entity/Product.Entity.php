<?php
class Entity_Product extends Entity_ShopsBound implements Interface_Actionable
{
    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getShopIds()
    {
        return $this->mapper->getShopIdsByProduct($this);
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        if(isset($params['shop_ids']))
        {
            $this->mapper->replaceShopIds($this,$params['shop_ids']);
        }
        $return = parent::update($params);
        $this->clearCacheByShopIds();
        return $return;
    }

    protected function clearCacheByShopIds()
    {
        foreach($this->getShopIds() as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Product::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        $tradeNetwork = $this->getTradeNetwork();
        Mapper_Product::getInstance()->clearCacheFavouriteByTradeNetwork($tradeNetwork);
        $tradeNetwork->deleteInfoMobileCache();
    }

    public function delete()
    {
        foreach(Mapper_Action_Scan::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        $shopIds = $this->getShopIds();
        Mapper_Product::getInstance()->replaceShopIds($this,array());
        $tradeNetwork = $this->getTradeNetwork();
        $return = parent::delete();
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Product::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
            Mapper_Product::getInstance()->clearCacheByShop($shop);
        }
        Mapper_Product::getInstance()->clearCacheFavouriteByTradeNetwork($tradeNetwork);
        $tradeNetwork->deleteInfoMobileCache();

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getBigPicturePathFull(true));
        $s3->delete($this->getSmallPicturePathFull(true));

        return $return;
    }

    public static function add($name,Entity_Trade_Network $tradeNetwork,array $shopIds,$description,$price,$discount,$picture)
    {
        $entity = Mapper_Product::getInstance()->add($name,$tradeNetwork,$shopIds,$description,$price,$discount);
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_Product::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        Mapper_Product::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_Product::getInstance()->clearCacheFavouriteByTradeNetwork($tradeNetwork);
        $tradeNetwork->deleteInfoMobileCache();
        return $entity;
    }

    public function getSmallPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getProductSmallPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getProductBigPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullSmallPicturePath = '/tmp/product_small_'.$filenameOriginalExtension;
        $result1 = copy($picturePath, $fullSmallPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToSideWithBorders($fullSmallPicturePath,Application::getProductSmallPictureSide());
        $s3->move($pngFileName,Application::getProductSmallPicturePath(true).$filenamePngExtension);

        $fullBigPicturePath = '/tmp/product_big_'.$filenameOriginalExtension;
        $result2 = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToSideWithBorders($fullBigPicturePath,Application::getProductBigPictureSide());
        $s3->move($pngFileName,Application::getProductBigPicturePath(true).$filenamePngExtension);


        if($result2 && $result1)
        {
            $result2 = $result2 && parent::update(array('picture_path' => $filenamePngExtension));
        }
        return $result2;
    }

    public function getDescriptionForDisplay()
    {
        return $this->getDescription();
    }

    public function getJsonObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'price' => $this->getPrice(),
                        'description' => $this->getDescription(),
                        'discount' => $this->getDiscount(),
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                    );
    }

    public function getJsonReplicationObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'description_short' => $this->getPrice(),
                        'description_full' => $this->getDescription(),
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                    );
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }
}