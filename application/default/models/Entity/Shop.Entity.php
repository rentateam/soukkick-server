<?php
class Entity_Shop extends SFM_Entity implements Interface_Actionable
{
    const SCAN_MAXIMUM_DISTACE = 1;            //сколько максимум может быть отдален пользователь от магазина, чтобы сделать скан. В км.
    const NEARBY_NUMBER = 40;

    protected static $_types = array(
                                        'Одежда и аксессуары',
                                        'Электроника и бытовая техника',
                                        'Магазин одежды',
                                        'Парфюмерия и косметика',
                                        'Продуктовый магазин',
                                        'Кафе',
                                        'Бар',
                                    );

    const SIZE_1 = 1;
    const SIZE_2 = 2;
    const SIZE_3 = 3;
    const SIZE_4 = 4;
    const SIZE_5 = 5;

    protected static $_sizes = array(
                                        self::SIZE_1 => '1',
                                        self::SIZE_2 => '2-5',
                                        self::SIZE_3 => '6-10',
                                        self::SIZE_4 => '11-25',
                                        self::SIZE_5 => '26+',
                                    );

    public function getName()
    {
        return $this->name;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getTradeNetworkId()
    {
        return $this->trade_network_id;
    }

    public function getCityId()
    {
        return $this->city_id;
    }

    public function getTransmitterInfo()
    {
        return $this->transmitter_info;
    }

    public function isDeleted()
    {
        return $this->is_deleted;
    }

    public function isHidden()
    {
        return $this->is_hidden;
    }

    /**
     * @return Entity_Trade_Network
     */
    public function getTradeNetwork()
    {
        return $this->getComputed('trade_network_id');
    }

    /**
     * @return Entity_City
     */
    public function getCity()
    {
        return $this->getComputed('city_id');
    }

    public static function add(Entity_Trade_Network $tradeNetwork, $name, $lat = 0, $long = 0, $address = '', Entity_City $city = null)
    {
        if($city === null)
            $city = Mapper_City::getInstance()->getEntityById(Entity_City::ID_MOSCOW);

        $entity = Mapper_Shop::getInstance()->add($tradeNetwork,$name,$lat,$long,$address,$city);
        Mapper_Shop::getInstance()->clearCacheGeneral();
        Mapper_Shop::getInstance()->clearCacheByByTradeNetwork($tradeNetwork);

        foreach($tradeNetwork->getProducts() as $product)
        {
            $product->addShop($entity);
        }
        foreach($tradeNetwork->getSales() as $sales)
        {
            $sales->addShop($entity);
        }
        foreach($tradeNetwork->getScanProducts() as $scanProduct)
        {
            $scanProduct->addShop($entity);
        }
        return $entity;
    }

    public function updateLocation($lat,$long)
    {
        foreach($this->getTransmitters(null,null) as $transmitter)
        {
            $transmitter->updateLocation($lat,$long);
        }
        return parent::update(array('lat' => $lat, 'longtitude' => $long));
    }

    public function getTransmitters($typeId = null, $isHidden = false)
    {
        return Mapper_Transmitter::getInstance()->getListByShopAndType($this,$typeId,$isHidden);
    }

    public function deleteInfoMobileCache(Aggregate_User $userList = null)
    {
        /*if($userList === null)
            $userList = Mapper_User::getInstance()->getListPlain();

        foreach($userList as $user)
        {
            //стираем ключи внутренностей магазина
            $object = new Memcached_Object_Shop_Info($user,$this);
            $object->delete();
        }*/
    }

    /**
     * @param integer $pageNum
     * @return Aggregate_Product
     */
    public function getProducts($pageNum = 0)
    {
        return Mapper_Product::getInstance()->getListByShop($this,$pageNum);
    }

    /**
     * @param integer $pageNum
     * @return Aggregate_ScanProduct
     */
    public function getScanProducts($pageNum = 0)
    {
        return Mapper_ScanProduct::getInstance()->getListByShop($this,$pageNum);
    }

    /**
     * @param integer $pageNum
     * @return Aggregate_Sales
     */
    public function getSales($pageNum = 0)
    {
        return Mapper_Sales::getInstance()->getListByShop($this,$pageNum);
    }

    public function deleteHard()
    {
        $tradeNetwork = $this->getTradeNetwork();
        foreach($this->getProducts() as $product)
        {
            $product->deleteShop($this);
        }
        foreach($this->getScanProducts() as $product)
        {
            $product->deleteShop($this);
        }
        foreach($this->getSales() as $sales)
        {
            $sales->deleteShop($this);
        }
        foreach($this->getTransmitters(null,null) as $transmitter)
        {
            $transmitter->delete();
        }
        foreach(Mapper_Action_Checkin::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        foreach(Mapper_Action_Enter::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        foreach(Mapper_Action_Shoppage::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        foreach(Mapper_Action_Scan::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        foreach(Mapper_Event_Shop::getInstance()->getListByShop($this) as $event)
        {
            $event->delete();
        }
        Mapper_Event_Shop::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        foreach(Mapper_Action_Exit::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        foreach(Mapper_Favourite_TradeNetwork::getInstance()->getListByTradeNetwork($tradeNetwork) as $favTradeNetwork)
        {
            $favTradeNetwork->delete();
        }

        $mall2shop = Mapper_Mall2Shop::getInstance()->getEntityByShop($this);
        if($mall2shop)
            return $mall2shop->delete();

        Mapper_Product::getInstance()->deleteByShop($this);
        Mapper_Product::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_Sales::getInstance()->deleteByShop($this);
        Mapper_Sales::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_ScanProduct::getInstance()->deleteByShop($this);
        Mapper_ScanProduct::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_Action_Exit_Attempt::getInstance()->clearByShop($this);
        Mapper_Rule_Main::getInstance()->clearMainRuleIds($this);
        return parent::delete();
    }

    public function delete()
    {
        $tradeNetwork = $this->getTradeNetwork();

        $return = $this->update(array('is_deleted' => 1));
        Mapper_Shop::getInstance()->clearCacheByByTradeNetwork($tradeNetwork);
        Mapper_Shop::getInstance()->clearCacheGeneral();

        //Выкидываем передатчики
        $this->updateTransmitterInfo('');
        foreach($this->getTransmitters(null,null) as $transmitter)
        {
            $transmitter->delete();
        }

        //Выкидываем точку из правил
        Mapper_Rule_Main::getInstance()->clearMainRuleIds($this);

        //Выкидываем неиспользованные штрихкоды
        foreach(Mapper_BuyBarcode::getInstance()->getListActiveByShop($this) as $buyBarcode)
        {
            $buyBarcode->delete();
        }

        //Отвязываем товары, сканы, акции
        Mapper_Product::getInstance()->deleteByShop($this);
        Mapper_Product::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_Sales::getInstance()->deleteByShop($this);
        Mapper_Sales::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        Mapper_ScanProduct::getInstance()->deleteByShop($this);
        Mapper_ScanProduct::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        return $return;
    }

    public static function calculateDistance(Entity_Shop $shop, $myLat, $myLong)
    {
        $lat1 = $myLat;
        $long1 = $myLong;
        $lat2 = $shop->getLat();
        $long2 = $shop->getLongtitude();
        $pk = 180/3.14169;

        $a1 = $lat1 / $pk;
        $a2 = $long1 / $pk;
        $b1 = $lat2 / $pk;
        $b2 = $long2 / $pk;

        $t1 = cos($a1)*cos($a2)*cos($b1)*cos($b2);
        $t2 = cos($a1)*sin($a2)*cos($b1)*sin($b2);
        $t3 = sin($a1)*sin($b1);
        $tt = acos($t1 + $t2 + $t3);

        return 6366000*$tt;
    }

    public static function sortListByDistance(Aggregate_Shop $shopList,$myLat, $myLong)
    {
        $distances = array();
        foreach($shopList as $shop)
        {
            $distances[$shop->getId()] = self::calculateDistance($shop, $myLat, $myLong);
        }
        asort($distances);
        $aggregate = Mapper_Shop::getInstance()->createAggregate(array_keys($distances));
        $aggregate->loadEntities();
        return $aggregate;
    }

    protected function isShopFavourite(Entity_User $user)
    {
        return $this->getTradeNetwork()->isFavourite($user);
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        return $this->getTradeNetwork()->getBigPicturePathFull($absoluteLink);
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        return $this->getTradeNetwork()->getPicturePathFull($absoluteLink);
    }

    public function getRectanglePicturePathFull($absoluteLink = false)
    {
        return $this->getTradeNetwork()->getRectanglePicturePathFull($absoluteLink);
    }

    public function getDescription()
    {
        return $this->getTradeNetwork()->getDescription();
    }

    public function getJsonObject(Entity_User $user = null, $includeInner = false, $includeTransmitters = false)
    {
        $mainTransmitter = $this->getMainTransmitter();
        $amountEnter = $mainTransmitter ? $mainTransmitter->getAmountEnter() : 0;
        $result = array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'description' => $this->getDescription(),
                        'lat' => $this->getLat(),
                        'long' => $this->getLongtitude(),
                        'big_picture' => $this->getBigPicturePathFull(),
                        'middle_picture' => $this->getPicturePathFull(),        /*@deprecated*/
                        'small_picture' => $this->getPicturePathFull(),
                        'rectangle_picture' => $this->getRectanglePicturePathFull(),
                        'amount_enter' => $amountEnter,
                        'amount_page' => $this->getTradeNetwork()->getAmountPage(),
                        'address' => $this->getAddress(),
                        'is_scan_available' => $this->isScanAvailable($user),
                        'user_shop_id' => $this->getTradeNetworkId()
                    );

        if($user !== null)
        {
            $tradeNetwork = $this->getTradeNetwork();
            $actionCheckin = new Action_Single_Checkin($user);
            $actionTradeNetwork = new Action_Single_TradeNetworkPage($user);
            $actionTradeNetwork->setTradeNetwork($tradeNetwork);

            $result['was_checkin'] = $actionCheckin->hasShopCheckinForDate($this);
            $result['was_shoppage'] = $actionTradeNetwork->hasTradeNetworkPageForDate();
            $result['is_favourite'] = $this->isShopFavourite($user);

            if($tradeNetwork->hasIntegration())
            {
                $actionBuyBarcode = new Action_Single_BuyBarcode($user);
                $isAvailable = !$actionBuyBarcode->hasBuyBarcodeForDate($tradeNetwork);
                $amount = 0;
                //Проверяем, есть ли активные rule-item-ы
                if($isAvailable)
                {
                    $valueItem = new Value_Rule_Item_ShopAndType($this,Entity_Rule_Item::TYPE_BUY_BARCODE);
                    $activeRuleItem = $valueItem->getActiveRuleItem(null);
                    if($activeRuleItem && $activeRuleItem->canSpendTotal())
                    {
                        $isAvailable = true;
                        $renderOtkat = true;
                        $amount = $activeRuleItem->getAmount();
                    }
                    else
                    {
                        $renderOtkat = false;
                    }
                }
                else
                {
                    $renderOtkat = true;
                }

                if($renderOtkat)
                {
                    $otkat = array(
                                        'description' => $tradeNetwork->getIntegrationDescription(),
                                        'type' => $tradeNetwork->getIntegrationType(),
                                        'amount' => $amount,
                                        'is_available' => $isAvailable
                                    );
                    $result['otkat'] = $otkat;
                }
            }
        }

        if($includeInner)
        {
            $shopInner = array(
                                    'product_list' => $this->getProducts(),
                                    'sales_list' => $this->getSales(),
                                    'scanproduct_list' => $this->getScanProducts(),
                                );

            foreach($shopInner as $title => $list)
            {
                $result[$title] = array();
                foreach($list as $element)
                {
                    $result[$title][] = $element->getJsonObject($user);
                }
            }
        }

        if($includeTransmitters)
        {
            $transmitterList = array();
            foreach($this->getTransmitters() as $transmitter)
            {
                $transmitterList[] = $transmitter->getJsonObject($user,false);
            }
            $result['transmitter_list'] = $transmitterList;
        }

        return $result;
    }

    public function getJsonReplicationObject()
    {
        $mainTransmitter = $this->getMainTransmitter();
        $amountEnter = $mainTransmitter ? $mainTransmitter->getAmountEnter() : 0;
        $result = array(
                        'id' => $this->getId(),
                        'lat' => $this->getLat(),
                        'long' => $this->getLongtitude(),
                        'address' => $this->getAddress(),
                        'user_shop_id' => $this->getTradeNetworkId()
        );

        $tradeNetwork = $this->getTradeNetwork();
        if($tradeNetwork->hasIntegration())
        {
            $amount = 0;

            //Проверяем, есть ли активные rule-item-ы
            $valueItem = new Value_Rule_Item_ShopAndType($this,Entity_Rule_Item::TYPE_BUY_BARCODE);
            $activeRuleItem = $valueItem->getActiveRuleItem(null);
            $amount = $activeRuleItem ? $activeRuleItem->getAmount() : 0;

            $otkat = array(
                            'description' => $tradeNetwork->getIntegrationDescription(),
                            'type' => $tradeNetwork->getIntegrationType(),
                            'amount' => $amount,
                            'active_rule_item_id' => $activeRuleItem ? $activeRuleItem->getId() : '0'
            );
            $result['otkat'] = $otkat;
        }

        $transmitterList = array();
        foreach($this->getTransmitters() as $transmitter)
        {
            $transmitterList[] = $transmitter->getJsonReplicationObject();
        }
        $result['transmitter_list'] = $transmitterList;

        $mall2shop = Mapper_Mall2Shop::getInstance()->getEntityByShop($this);
        if($mall2shop)
        {
            $result['mall_id'] = $mall2shop->getMallId();
            $result['shop_map_tag'] = $mall2shop->getTag()->getName();
        }
        else
        {
            $result['mall_id'] = 0;
            $result['shop_map_tag'] = '';
        }

        return $result;
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }

    public function getMall()
    {
        $mall2shop = Mapper_Mall2Shop::getInstance()->getEntityByShop($this);
        if($mall2shop)
        {
            return $mall2shop->getMall();
        }
        return null;
    }

    public static function getTypes()
    {
        return self::$_types;
    }

    public static function getSizes()
    {
        return self::$_sizes;
    }

    public static function getType($typeIndex)
    {
        return self::$_types[$typeIndex];
    }

    public static function getSize($sizeIndex)
    {
        return self::$_sizes[$sizeIndex];
    }

    public function getNumberOfEnters($minutesAgo, $minutesLength)
    {
        return Mapper_Action_Checkin::getInstance()->getCountUsersByObjectAndMinutes(array($this->getId()),$minutesAgo,$minutesLength);
    }

    public function getNumberOfNewEnters($minutesAgo, $minutesLength)
    {
        return Mapper_Action_Checkin::getInstance()->getCountNewUsersByObjectAndMinutes(array($this->getId()),$minutesAgo,$minutesLength);
    }

    /**
     * Посетители в магазине
     */
    public function getExistingVisitors()
    {
        return Mapper_User::getInstance()->getExistingVisitorsAggregate($this);
    }

    /**
     * Посетители в магазине, только из базы - чистая бизнес-логика.
     */
    public function getExistingVisitorIdsFromBase()
    {
        $fromDate = date('Y-m-d H:i:s',time() - Entity_Action_Checkin::MINUTES_CHECKIN_IS_VISITOR*60);
        $lastHourCheckins = Mapper_Action_Checkin::getInstance()->getListByShopFromDate($this,$fromDate);
        $visitorIds = array();
        foreach($lastHourCheckins as $checkin)
        {
            $visitorIds[] = $checkin->getUserId();
        }
        $visitorIds = array_unique($visitorIds);
        return $visitorIds;
    }

    /**
     * Берем стоимость с первого передатчика типа "Чекин" - пока такие все
     */
    public function getAmountEnter()
    {
        $mainTransmitter = $this->getMainTransmitter();
        return $mainTransmitter ? $this->getMainTransmitter()->getAmountEnter() : 0;
    }

    public function canBeViewed(Entity_Trade_Network $tradeNetwork)
    {
        return $this->getTradeNetworkId() == $tradeNetwork->getId();
    }

    /**
     * Главный (входной) передатчик - пока у нас один тип, и в магазине всегда только один передатчик.
     */
    public function getMainTransmitter()
    {
        $defaultTransmitterList = $this->getTransmitters(Entity_Transmitter_Type::ID_DEFAULT);
        return $defaultTransmitterList->current() ? $defaultTransmitterList->current() : null;
    }

    /**
     * Есть ли доступные сканы в магазине.
     * + В том числе проверяет на активные правила.
     * @return boolean
     */
    public function isScanAvailable()
    {
        $scanProducts = $this->getScanProducts();
        if($scanProducts->count() > 0)
        {
            foreach($scanProducts as $scanProduct)
            {
                //Если нашли активное правило - значит, можно делать сканы
                if($scanProduct->getActiveRuleItemId())
                    return true;
            }
            //Если не нашли - то нельзя
            return false;
        }
        else
        {
            return false;
        }
    }

    public function isTransmitterAvailable()
    {
        return $this->getMainTransmitter() !== null;
    }

    public function updateTransmitterInfo($transmitterInfo)
    {
        if($transmitterInfo != $this->getTransmitterInfo())
        {
            if(empty($transmitterInfo))
            {
                $transmitter = $this->getMainTransmitter();
                if($transmitter)
                {
                    $transmitter->delete();
                }
            }
            //генерим новый передатчик только, если раньше ничего не было
            else if(!$this->getTransmitterInfo())
            {
                Entity_Transmitter::add($this, Entity_Transmitter_Type::ID_DEFAULT, 0, $this->getLat(), $this->getLongtitude());
            }
            $result = $this->update(array('transmitter_info' => $transmitterInfo));
            return $result;
        }
        else
        {
            return true;
        }
    }

    public function setHidden($isHidden)
    {
        $return = $this->update(array('is_hidden' => $isHidden ? '1' : '0'));
        Mapper_Shop::getInstance()->clearCacheByByTradeNetwork($this->getTradeNetwork());
        Mapper_Shop::getInstance()->clearCacheGeneral();

        //Выкидываем передатчики
        Mapper_Transmitter::getInstance()->clearCacheByShopAndType($this,Entity_Transmitter_Type::ID_DEFAULT);
        return $return;
    }

    public function setMall(Entity_Mall $mall = null, $tagName = '')
    {
        $params = array();
        if($mall !== null)
        {
            if(!$tagName)
                $tagName = $this->getName();
            $mall2shop = Entity_Mall2Shop::add($mall, $tagName, $this);
            $result = $this->updateLocation($mall->getLat(), $mall->getLongtitude());
            $result = $result && $this->setAddress($mall->getAddress());
            return $result;
        }
        else
        {
            $mall2shop = Mapper_Mall2Shop::getInstance()->getEntityByShop($this);
            if($mall2shop)
                $result = $mall2shop->delete();
            else
                $result = false;
        }
    }

    public function setAddress($address)
    {
        return $this->update(array('address' => $address));
    }
}