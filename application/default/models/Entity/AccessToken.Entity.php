<?php
class Entity_AccessToken extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getToken()
    {
        return $this->token;
    }

    public function getTradeNetworkId()
    {
        return $this->trade_network_id;
    }
    
    /**
     * @return Entity_Trade_Network
     */
    public function getTradeNetwork()
    {
        return $this->getComputed('trade_network_id');
    }
    
    public function getShopId()
    {
        return $this->shop_id;
    }
    
    /**
     * @return Entity_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }
    
    public static function add(Entity_Trade_Network $network, Entity_Shop $shop = null)
    {
        $token = self::generateToken();
        $entity = Mapper_AccessToken::getInstance()->add($token,$network,$shop);
        return $entity;
    }
    
    public static function generateToken()
    {
        do 
        {
            $token = md5(md5(uniqid('pwoertpgldfgertpij')));
        }
        while(Mapper_AccessToken::getInstance()->getEntityByToken($token) !== null);
        return $token;
    }
}