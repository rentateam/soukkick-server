<?php 
class Entity_BuyBarcode extends SFM_Entity implements Interface_Actionable, Interface_ActionItem
{
    const BARCODE_LENGTH = 13;
    
    public function getBarcode() 
    {
        return $this->barcode;
    }
    
    public function getPrice() 
    {
        return $this->price;
    }
    
    public function isActive() 
    {
        return $this->is_active;
    }
    
    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }
    
    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }
    
    public function getShopId()
    {
        return $this->shop_id;
    }
    
    public function getCounterDate()
    {
        return $this->counter_date;
    }
    
    /**
     * @return Entity_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }
    
    public static function add(Entity_Shop $shop, $barcode = null)
    {
        if($barcode == null)
            $barcode = self::generateBarcode();
        
        if(($existingBarcode = Mapper_BuyBarcode::getInstance()->getEntityByBarcode($barcode)) !== null)
            throw new Shopfee_Exception_Entity('Active buy barcode '.$barcode.' already exists in shop '.$existingBarcode->getShop()->getId().'!');
        $entity = Mapper_BuyBarcode::getInstance()->add($shop,$barcode);
        
        Mapper_BuyBarcode::getInstance()->clearCacheActiveByShop($shop);
        $shop->deleteInfoMobileCache();
        return $entity;
    }
    
    public static function generateBarcode()
    {
        do 
        {
            $barcodeArray = array();
            for($i=0;$i<self::BARCODE_LENGTH - 1;$i++)
            {
                $barcodeArray[] = (string)rand(0,9);
            }
            
            $barcodeArray[] = self::getEan13LastDigit($barcodeArray);
            $barcode = implode('',$barcodeArray);
        }
        while(Mapper_BuyBarcode::getInstance()->getEntityByBarcode($barcode) !== null);
        return $barcode;
    }
    
    public static function getEan13LastDigit(array $codeArray)
    {
        /*шаг 1: складываются все цифры четно нумерованных позиций;
        шаг 2: результат шага 1 умножается на 3;
        шаг З: складываются все цифры нечетно нумерованных позиций;
        шаг 4: складываются результаты шагов 2 и 3;
        шаг 5: если результат шага 4 делится на 10 без остатка, то контрольным числом является нуль, если нет, то выполняется шаг 6;
        шаг 6: находится ближайшее число кратное 10, но больше, чем значение шага 4;
        шаг 7: контрольным числом является разность результатов шагов 6 и 4.*/
        
        
        //шаг 1: складываются все цифры четно нумерованных позиций;
        $step1 = 0;
        for($i=self::BARCODE_LENGTH - 2;$i>0;$i-=2)
        {
            $step1 += $codeArray[$i];
        }
        
        //шаг 2: результат шага 1 умножается на 3;
        $step2 = $step1*3;
        
        //шаг З: складываются все цифры нечетно нумерованных позиций;
        $step3 = 0;
        for($i=self::BARCODE_LENGTH - 3;$i>=0;$i-=2)
        {
            $step3 += $codeArray[$i];
        }
        
        //шаг 4: складываются результаты шагов 2 и 3;
        $step4 = $step2 + $step3;
        
        //шаг 5: если результат шага 4 делится на 10 без остатка, то контрольным числом является нуль, 
        if($step4%10 == 0)
        {
            return 0;
        }
        else
        {
            //если нет, то выполняется шаг 6;
            //шаг 6: находится ближайшее число кратное 10, но больше, чем значение шага 4;
            $step6 = $step4 + 10 - $step4%10;
            
            //шаг 7: контрольным числом является разность результатов шагов 6 и 4
            return 10 - $step4%10;
        }
        
        return $digit;
    }
    
    public function passivate()
    {
        $this->update(array('is_active' => 0));
        $this->setActiveRuleItem(null);
        Mapper_BuyBarcode::getInstance()->clearCacheActiveByShop($this->getShop());
    }
    
    public function delete()
    {
        foreach(Mapper_Action_BuyBarcode::getInstance()->getListByObject($this) as $action)
        {
            $action->delete();
        }
        $shop = $this->getShop(); 
        $return = parent::delete();
        Mapper_BuyBarcode::getInstance()->clearCacheByShop($shop);
        $shop->deleteInfoMobileCache();
        return $return;
    }
    
    public function setPrice($price)
    {
        if($this->getPrice() != $price)
        {
            return $this->update(array('price' => $price));
        }
        else
        {
            return true;
        }
    }
    
    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        //Если уже активирован - не привязываем правило
        if(!$this->isActive())
        {
            $ruleItem = null;
        }
        
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        $this->setPrice($ruleItem ? $ruleItem->getAmount() : 0);
        //Скидываем кэш по магазину - ведь там есть объект otkat
        $this->getShop()->deleteInfoMobileCache();
        return $result;
    }
    
    public function setCounterDate($date)
    {
        return $this->update(array('counter_date' => $date));
    }
}