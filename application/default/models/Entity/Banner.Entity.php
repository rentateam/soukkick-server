<?php
class Entity_Banner extends Entity_Pictured
{
    public function getName()
    {
        return $this->name;
    }

    public function getDateExpire()
    {
        return $this->date_expire;
    }

    public static function add($dateExpire,$picture)
    {
        $entity = Mapper_Banner::getInstance()->add($dateExpire);
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        return $entity;
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getBannerPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filename = $this->getId().'.'.$ext;

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/banner_small_'.$filename;
        $result1 = copy($picturePath, $fullBigPicturePath);
        //Shopfee_Util_Image::imQualityScaleToWidth($fullBigPicturePath,Application::getPrizeBigPictureWidth());
        $s3->move($fullBigPicturePath,Application::getBannerPicturePath(true).$filename);
        if($result1)
        {
            $result1 = $result1 && parent::update(array('picture_path' => $filename));
        }

        return $result1;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        $return = parent::update($params);
        return $return;
    }

    public function getJsonObject()
    {
        $result = array(
                            'id' => $this->getId(),
                            'expires_at' => $this->getDateExpire(),
                            'picture_url' => $this->getPicturePathFull(),
                        );
        return $result;
    }

    public function getJsonReplicationObject()
    {
        $result = array(
                        'id' => $this->getId(),
                        'expires_at' => $this->getDateExpire(),
                        'picture_url' => $this->getPicturePathFull(),
        );
        return $result;
    }

    public static function getFirstBanner()
    {
        $bannerList = Mapper_Banner::getInstance()->getList();
        return $bannerList->count() ? $bannerList->current() : null;
    }

    public function delete()
    {
        parent::delete();
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getPicturePathFull(true));
    }
}