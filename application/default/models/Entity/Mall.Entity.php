<?php
class Entity_Mall extends Entity_Pictured
{
    /**
     * В км. Магазины в данном радиусе от ТЦ будут считаться кандидатами к тому, чтобы быть в него включены.
     * @var double
     */
    const RADIUS_SHOP_COORDINATE = 2;

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getLat()
    {
    	return $this->lat;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public static function add($name,$address,$lat,$long,$logo)
    {
        $entity = Mapper_Mall::getInstance()->add($name,$address,$lat,$long);
        //Загрузка логотипа
        if($logo)
        {
        	$entity->uploadPicture($logo);
        }
        return $entity;
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getMallPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filename = $this->getId().'.'.$ext;

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/mall_'.$filename;
        $result1 = copy($picturePath, $fullBigPicturePath);
        $s3->move($fullBigPicturePath,Application::getMallPicturePath(true).$filename);
        if($result1)
        {
            $result1 = $result1 && parent::update(array('picture_path' => $filename));
        }

        return $result1;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        if(isset($params['address']) && $params['address'] != $this->getAddress())
        {
            foreach($this->getShops() as $shop)
            {
                $shop->setAddress($params['address']);
            }
        }
        $return = parent::update($params);
        return $return;
    }

    public function getJsonReplicationObject()
    {
        $mallDecorator = new Decorator_Mall($this);
        $result = array(
                            'id' => $this->getId(),
                            'name' => $this->getName(),
                            'address' => $this->getAddress(),
                            'lat' => $this->getLat(),
                            'lon' => $this->getLongtitude(),
                            'logo_url' => $this->getPicturePathFull(),
                            'map_array' => $mallDecorator->getMapUrlList(),
                            'shop_array' => $this->getShopList()->getListEntitiesId()
        );
        return $result;
    }

    public function delete()
    {
        foreach($this->getMaps() as $map)
        {
            $map->delete();
        }

        $lat = $this->getLat();
        $long = $this->getLongtitude();
        $address = $this->getAddress();
        foreach($this->getShops() as $shop)
        {
            $shop->updateLocation($lat,$long);
            $shop->setAddress($this->getAddress());
            $shop->setMall(null);
        }

        parent::delete();
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getPicturePathFull(true));
    }

    /**
     * Список магазинов, которые являются кандидатами на включение в ТЦ.
     * @return Aggregate_Shop
     */
    public function getShopCandidateList()
    {
        $shopByCoordinates = Mapper_Shop::getInstance()->getListByCoordinates($this->getLat(),$this->getLongtitude(),self::RADIUS_SHOP_COORDINATE);
        $presentIds = $this->getShops()->getListEntitiesId();
        $candidateIds = array_diff($shopByCoordinates->getListEntitiesId(),$presentIds);
        $shopByCoordinates = Mapper_Shop::getInstance()->createAggregate($candidateIds,null,true);
        return $shopByCoordinates;
    }

    public function getMaps()
    {
        return Mapper_Mall_Map::getInstance()->getListByMall($this);
    }

    public function updateLocation($lat,$long)
    {
        foreach($this->getShops() as $shop)
        {
            $shop->updateLocation($lat,$long);
        }
        return parent::update(array('lat' => $lat, 'longtitude' => $long));
    }

    public function getShops()
    {
        return Mapper_Shop::getInstance()->getListByMall($this);
    }
}