<?php
abstract class Entity_ShopsBound extends Entity_Pictured
{
    public function getTradeNetworkId()
    {
        return $this->trade_network_id;
    }
    
    /**
     * @return Entity_Trade_Network
     */
    public function getTradeNetwork()
    {
        return $this->getComputed('trade_network_id');
    }
    
    public function addShop(Entity_Shop $shop)
    {
        $shopIds = $this->getShopIds();
        $shopIds[] = $shop->getId();
        $shopIds = array_unique($shopIds);
        $this->mapper->replaceShopIds($this,$shopIds);
        $this->clearCacheByShopIds();
    }
    
    public function deleteShop(Entity_Shop $shop)
    {
        $shopIds = $this->getShopIds();
        $key = array_search($shop->getId(), $shopIds); 
        if($key !== false)
        {
            unset($shopIds[$key]);
        }
        $this->mapper->replaceShopIds($this,$shopIds);
        $this->clearCacheByShopIds();
    }
    
    abstract protected function clearCacheByShopIds();
}