<?php
class Entity_Transmitter extends SFM_Entity implements Interface_Actionable, Interface_ActionItem
{
    const GPS_ACCURACY                = 200;        //погрешность gps, м
    const TRANSMITTER_CHANGE_DISTANCE = 300;        //изменение списка передатчиков, раз в N м

    const FREQUENCY_CODE_DIST           = 4;    //минимальное кодовое расстояние между последовательностями
    const MIN_FREQUENCY                 = 6;
    const MAX_FREQUENCY                 = 14;

    protected static $_frequencies = array(
                                            6 => 20408,
                                            20491,
                                            20576,
                                            20661,
                                            20746,
                                            20833,
                                            20920,
                                            21008,
                                            21097,
                                            21186,
                                            21276,
                                            21367,
                                            21459,
                                            21551,
                                            21645,
                                            21739,
                                            21834,
                                            21929,
                                        );

    protected static $_synchroFrequencies = array(
                                                    1 => 20000,
                                                    20080,
                                                    20161,
                                                    20242,
                                                    20325,
                                                 );

    public function getId()
    {
        return $this->id;
    }

    public function getFrequency()
    {
        return $this->frequency;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function getAmountEnter()
    {
        return $this->amount_enter;
    }

    public function getLastModified()
    {
        return $this->last_modified;
    }

    public function getShopId()
    {
        return $this->shop_id;
    }

    /**
     * @return Entity_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public function getTransmitterTypeId()
    {
        return $this->transmitter_type_id;
    }

    /**
     * @return Entity_Transmitter_Type
     */
    public function getTransmitterType()
    {
        return $this->getComputed('transmitter_type_id');
    }

    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    public static function add(Entity_Shop $shop, $typeId, $amountEnter, $lat = 0, $long = 0)
    {
        $frequencyString = implode(',',self::getGeneratedFrequencySequence($lat,$long,0));
        $entity = Mapper_Transmitter::getInstance()->add($shop,$typeId,$frequencyString,$amountEnter,$lat,$long);
        Mapper_Transmitter::getInstance()->clearCacheByShopAndType($shop,$typeId);
        return $entity;
    }

    public function getFrequencyList()
    {
        return explode(',',$this->getFrequency());
    }

    protected static function getGeneratedFrequencySequence($lat,$long,$transmitterId)
    {
        /*$now = time();
        $timeStampHex = dechex($now);

        $gf = new Galois_Field(4,array(1,1,0,0,1));    //x4+x+1
        $codeLength = 15;
        $fecLength = 4;
        $data = '';

        $coeff = array(7,8,12,13,1);
        $codeElements = array();
        foreach($coeff as $oneCoeff)
        {
            $codeElements[] = new Galois_FieldElement($gf,$oneCoeff);
        }

        $coeff = array(0,0,0,0,8,8,8);
        for($i=0;$i<$codeLength-$fecLength-3;$i++)
        {
            $coeff[] = hexdec(substr($timeStampHex.'',$i,1));
        }


        $divisionElements = array();
        foreach($coeff as $oneCoeff)
        {
            $divisionElements[] = new Galois_FieldElement($gf,$oneCoeff);
        }
        $generator = new Galois_FieldPolynomial($gf,count($codeElements) - 1,$codeElements);
        $message = new Galois_FieldPolynomial($gf,count($divisionElements) - 1,$divisionElements);

        $parities = $message->divide($generator);


        $generatedSymbolsNumber = count($coeff) - $fecLength;
        $result = array_slice($coeff, $fecLength, $generatedSymbolsNumber);
        $result1 = array();
        for($i = 0; $i < $fecLength; $i++)
        {
            $elementByIndex = $parities->getByIndex($fecLength - $i - 1);
            $result1[] = $elementByIndex ? $elementByIndex->getPolyValue() : 0;
        }
        $result = array_merge($result1,$result);

        $return = array();
        foreach($result as $item)
        {
            $return[] = self::$_frequencies[$item+8];
        }

        //Временный костыль, пока у передатчика только 17 частот, а не 20.
        unset($return[4]);
        unset($return[5]);
        unset($return[6]);
        $return = array_values($return);*/

        $collisionTransmitter = null;
        do
        {
            $return = array();
            $timeSlotsNotSync = 12;
            $minFrequency = self::MIN_FREQUENCY;
            $maxFrequency = self::MAX_FREQUENCY;

            for($i=0;$i<$timeSlotsNotSync;$i++)
            {
                $return[$i] = self::$_frequencies[rand($minFrequency,$maxFrequency)];
            }

            $collisionTransmitter = self::otherFrequencyExists($return,$lat, $long, $transmitterId);
        }
        while($collisionTransmitter !== null);

        return $return;
    }

    /**
     * Существует ли пересекающаяся последовательность частот в области вещания передатчиков (т.е. можно ли спутать 2 похожие последовательности)
     * @param float $lat
     * @param float $long
     * @param array $frequencyList
     * @param integer $transmitterId Id передатчика, для которого генерируется последовательность. Может быть 0, если передатчик еще не существует.
     * @return Entity_Transmitter|null
     */
    public static function otherFrequencyExists(array $frequencyList,$lat, $long, $transmitterId)
    {
        $transmitterList = Mapper_Transmitter::getInstance()->getListByCoordinates($lat,$long);
        foreach($transmitterList as $transmitter)
        {
            if($transmitter->getCodeDistance($frequencyList) <= self::FREQUENCY_CODE_DIST && $transmitterId != $transmitter->getId())
            {
                return $transmitter;
            }
        }
        return null;
    }

    public static function getEncodedFrequencyList(array $inputFreqList, $currentTime = null,$useEncoding = false)
    {
        if(!$useEncoding)
            return $inputFreqList;


        $encodedList = $inputFreqList;

        //!!!вся арифметика должна быть 16-битная, что равносильно (%65536)!!!

        $offsetTableSize = 4;
        $offsetTable = array(5,7,11,13);
        $invalidFrqNumber = -1;

        //получение значения текущего времени
        $currentTime = $currentTime ? $currentTime : time();
        $utc = getdate($currentTime);

        //расчет константы сдвига, зависящей от времени
        $s = (($utc['year']) * 2 + ($utc['mon']) * 3 + $utc['mday'] * 7) % 256;

        //сдвиг частот передатчика
        $timeSlotsSync = count(self::$_synchroFrequencies);
        $mainFrequencies = array();
        for($i = self::MIN_FREQUENCY; $i <= self::MAX_FREQUENCY; $i++)
        {
            $mainFrequencies[] = $i;
        }

        $j = 0;
        foreach($encodedList as &$inputFreq)
        {
            //расчет константы сдвига для текущего слота
            $df = $s * $offsetTable[($j + $timeSlotsSync) % $offsetTableSize];
            //определение номера частоты текущего слота
            $frqNumber = $invalidFrqNumber;
            $maxFrequenciesCount = self::MAX_FREQUENCY - self::MIN_FREQUENCY + 1;

            for($i = 0; $i < $maxFrequenciesCount; $i++)
            {
                if(self::$_frequencies[$mainFrequencies[$i]] == $inputFreq)
                {
                    $frqNumber = $mainFrequencies[$i];
                    break;
                }
            }
            //если задана частота, не входящая в основную последовательность
            if($frqNumber == $invalidFrqNumber)
            {
                throw new Entity_Transmitter_Exception("Задана частота, не входящая в основную последовательность.");
            }
            //сдвиг частоты текущего слота
            else
            {
                $newFrqNumber = (($frqNumber - ($timeSlotsSync + 1) + $df) % $maxFrequenciesCount) + ($timeSlotsSync + 1);
                $inputFreq = self::$_frequencies[$newFrqNumber];
            }
            $j++;
        }
        return $encodedList;
    }

    public function getMyEncodedFrequencyList($useEncoding)
    {
        return self::getEncodedFrequencyList($this->getFrequencyList(),null,$useEncoding);
    }

    protected function getCodeDistance($frequencyList)
    {
        $distance = 0;
        $myFrequencyList = $this->getFrequencyList();
        $i = 0;
        foreach($frequencyList as $frequency)
        {
            if($frequency != $myFrequencyList[$i])
            {
                $distance++;
            }
            $i++;
        }
        return $distance;
    }

    public static function getFrequencies()
    {
        return self::$_frequencies;
    }

    public static function getSynchroFrequencies()
    {
        return self::$_synchroFrequencies;
    }

    public function updateLocation($lat,$long)
    {
        $collisionTransmitter = self::otherFrequencyExists($this->getFrequencyList(),$lat,$long, $this->getId());
        $return = parent::update(array('lat' => $lat, 'longtitude' => $long));
        if($collisionTransmitter !== null)
        {
            $exception = new Entity_Transmitter_Exception('Sequence '.implode(' ',$this->getFrequencyForAdmin()).' is in conflict with sequence '.implode(' ',$collisionTransmitter->getFrequencyForAdmin()).' of shop '.$collisionTransmitter->getShop()->getName());
            $exception->setTransmitter($this);
            throw $exception;
        }
        return $return;
    }

    public function getDescriptionForDisplay()
    {
        return $this->getDescription();
    }

    public function regenerateFrequency()
    {
        $frequencyString = implode(',',self::getGeneratedFrequencySequence($this->getLat(),$this->getLongtitude(),$this->getId()));
        return parent::update(array('frequency' => $frequencyString));
    }

    public function getFrequencyForAdmin()
    {
        $allFrequencies = self::getFrequencies();
        $frequencyList = $this->getFrequencyList();
        $keys = array_keys(self::$_synchroFrequencies);
        foreach($frequencyList as $fr)
        {
            $keys[] = array_search($fr, $allFrequencies);
        }
        return $keys;
    }

    protected function setAmountEnter($price)
    {
        if($price != $this->getAmountEnter())
        {
            $result = parent::update(array('amount_enter' => $price));
            //Скидывание кэша у родительского магазина
            $result = $result && $this->getShop()->deleteInfoMobileCache();
            return $result;
        }
        else
        {
            return true;
        }
    }

    public function getJsonObject(Entity_User $user = null, $includeShop = true)
    {
        $shop = $this->getShop();
        $result = array(
                        'id' => $this->getId(),
                        'frequencyList' => $this->getFrequencyList(),
                        'lat' => $this->getLat(),
                        'long' => $this->getLongtitude(),
                        'amount_enter' => $this->getAmountEnter(),
                        'type' => $this->getTransmitterTypeId(),
                        'active_rule_item_id' => intval($this->getActiveRuleItemId())
                    );

        if($includeShop)
        {
            $result['shop'] = $shop->getJsonObject($user);
        }

        if($user !== null)
        {
            $action = new Action_Single_Checkin($user);
            $result['was_checkin'] = $action->hasShopCheckinForDate($shop,$this->getTransmitterTypeId());
        }

        return $result;
    }

    public function getJsonReplicationObject()
    {
        $result = array(
                        'id' => $this->getId(),
                        'frequencyList' => $this->getFrequencyList(),
                        'amount_enter' => $this->getAmountEnter(),
                        'active_rule_item_id' => intval($this->getActiveRuleItemId())
        );
        return $result;
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        return $result && $this->setAmountEnter($ruleItem ? $ruleItem->getAmount() : 0);
    }

    public function delete()
    {
        $shop = $this->getShop();
        $typeId = $this->getTransmitterTypeId();
        $result = parent::delete();
        Mapper_Transmitter::getInstance()->clearCacheByShopAndType($shop,$typeId);
        return $result;
    }
}