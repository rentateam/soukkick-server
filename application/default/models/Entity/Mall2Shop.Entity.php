<?php
class Entity_Mall2Shop extends SFM_Entity
{
    public function getMallId()
    {
        return $this->mall_id;
    }

    public function getShopId()
    {
        return $this->shop_id;
    }

    public function getTagId()
    {
        return $this->tag_id;
    }

    public function getMallMapId()
    {
    	return $this->mall_map_id;
    }

    public function getMall()
    {
        return $this->getComputed('mall_id');
    }

    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public function getTag()
    {
        return $this->getComputed('tag_id');
    }

    public function getMallMap()
    {
    	return $this->getComputed('mall_map_id');
    }

    public static function add(Entity_Mall $mall, $tagName, Entity_Shop $shop = null, Entity_Mall_Map $map = null)
    {
        $tag = Entity_Tag::add($tagName);
        $entity = Mapper_Mall2Shop::getInstance()->getEntityByMallAndTag($mall, $tag);
        if($entity === null)
        {
            $entity = Mapper_Mall2Shop::getInstance()->add($mall,$tag,$shop,$map);
        }
        else
        {
            $entity->setShop($shop);
        }
        return $entity;
    }

    public function setShop(Entity_Shop $shop = null)
    {
        return $this->update(array('shop_id' => $shop ? $shop->getId() : null));
    }
}