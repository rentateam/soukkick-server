<?php
class Entity_Prize_Promo extends SFM_Entity implements Interface_Actionable
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrizeId()
    {
        return $this->prize_id;
    }
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function isActive()
    {
        return $this->is_active;
    }
    
    public function getPrize()
    {
        return $this->getComputed('prize_id');
    }
    
    public static function add(Entity_Prize $prize, $code)
    {
        $entity = Mapper_Prize_Promo::getInstance()->add($prize,$code);
        return $entity;
    }
    
    public function useCode()
    {
        return $this->update(array('is_active' => 0));
    }
}