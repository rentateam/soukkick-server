<?php
class Entity_Prize_Order extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrizeId()
    {
        return $this->prize_id;
    }
    
    public function getUserId()
    {
        return $this->user_id;
    }
    
    public function getPrizeOrderStatusId()
    {
        return $this->prize_order_status_id;
    }
    
    public function getPrize()
    {
        return $this->getComputed('prize_id');
    }
    
    public function getUser()
    {
        return $this->getComputed('user_id');
    }
    
    public function getPrizeOrderStatus()
    {
        return $this->getComputed('prize_order_status_id');
    }
    
    public function getDatetimeCreated()
    {
        return $this->datetime_created;
    }
    
    public function getUserName()
    {
        return $this->user_name;
    }
    
    public function getAddress()
    {
        return $this->address;
    }
    
    public function getPhone()
    {
        return $this->phone;
    }
    
    public function getPostIndex()
    {
        return $this->post_index;
    }
    
    public function getCountry()
    {
        return $this->country;
    }
    
    public function getCity()
    {
        return $this->city;
    }
    
    public static function add(Entity_User $user, Entity_Prize $prize)
    {
        $address = new Decorator_User_Address($user->getAddress());
        $entity = Mapper_Prize_Order::getInstance()->add($user,$prize,$user->getName().' '.$user->getSurname(),$address->toStringWithoutIndex(),$address->getPhone(),$address->getPostIndex(),$address->getCountry(),$address->getCity());
        $prize->getStock()->decreaseNumberAvailable($entity);
        return $entity;
    }
    
    public function updateStatus($newStatusId)
    {
        if($newStatusId != $this->getPrizeOrderStatusId())
        {
            $return = $this->update(array('prize_order_status_id' => $newStatusId));
            switch($newStatusId)
            {
                case Entity_Prize_Order_Status::ID_REJECTED:
                    $this->getPrize()->getStock()->increaseNumberAvailable($this);
                    break;
                case Entity_Prize_Order_Status::ID_DELIVERED:
                    $this->getPrize()->getStock()->increaseNumberDelivered();
                    break;
            }
            return $return;
        }
        else
        {
            return true;
        }
    }
    
    public function getAdminUrl()
    {
        return '/reward/order/view/id/'.$this->getId().'/';
    }
}