<?php
class Entity_Prize_Stock extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrizeId()
    {
        return $this->prize_id;
    }
    
    public function getNumberAvailable()
    {
        return $this->number_available;
    }
    
    public function getNumberDelivered()
    {
        return $this->number_delivered;
    }
    
    public function getPrize()
    {
        return $this->getComputed('prize_id');
    }
    
    public static function add(Entity_Prize $prize)
    {
        $entity = Mapper_Prize_Stock::getInstance()->getEntityByPrize($prize);
        if($entity === null)
            $entity = Mapper_Prize_Stock::getInstance()->add($prize);
        return $entity;
    }
    
    public function updateNumberAvailable(Entity_User_Admin $admin, $newValue, $comment)
    {
        $currentNumberAvailable = $this->getNumberAvailable();
        Entity_Prize_Stock_History::add($admin,$this,$newValue,$comment);
        $return = $this->update(array('number_available' => $newValue));
        //Очистка агрегата наличия наград
        if($newValue <= 0 && $currentNumberAvailable > 0 || $newValue > 0 && $currentNumberAvailable <= 0)
            Mapper_Prize::getInstance()->clearCacheStock();
        return $return;
    }
    
    public function increaseNumberDelivered()
    {
        return $this->update(array('number_delivered' => $this->getNumberDelivered() + 1));
    }
    
    public function decreaseNumberAvailable(Entity_Prize_Order $order)
    {
        $admin = Mapper_User_Admin::getInstance()->getEntityById(Entity_User_Admin::ID_ADMIN);
        return $this->updateNumberAvailable($admin,$this->getNumberAvailable() - 1,Decorator_Prize_Stock_History::getIncreaseDecreaseComment($order));
    }
    
    public function increaseNumberAvailable(Entity_Prize_Order $order)
    {
        $admin = Mapper_User_Admin::getInstance()->getEntityById(Entity_User_Admin::ID_ADMIN);
        return $this->updateNumberAvailable($admin,$this->getNumberAvailable() + 1,Decorator_Prize_Stock_History::getIncreaseDecreaseComment($order));
    }
    
    public function getAdminUrl()
    {
        return '/reward/stock/view/id/'.$this->getId().'/';
    }
}