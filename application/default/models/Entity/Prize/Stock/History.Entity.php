<?php
class Entity_Prize_Stock_History extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrizeStockId()
    {
        return $this->prize_stock_id;
    }
    
    public function getDatetimeCreated()
    {
        return $this->datetime_created;
    }
    
    public function getDifference()
    {
        return $this->difference;
    }
    
    public function getComment()
    {
        return $this->comment;
    }
    
    public function getUserAdminId()
    {
        return $this->user_admin_id;
    }
    
    public function getPrizeStock()
    {
        return $this->getComputed('prize_stock_id');
    }
    
    public function getUserAdmin()
    {
        return $this->getComputed('user_admin_id');
    }
    
    public static function add(Entity_User_Admin $admin,Entity_Prize_Stock $stock,$newValue,$comment)
    {
        return Mapper_Prize_Stock_History::getInstance()->add($admin,$stock,$newValue - $stock->getNumberAvailable(),$comment);
    }
}