<?php
class Entity_Prize_Order_Status extends SFM_Entity
{
    const ID_NEW = 1;
    const ID_APPROVED = 2;
    const ID_IN_PROGRESS = 3;
    const ID_DELIVERED = 4;
    const ID_REJECTED = 5;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public static function getActiveIds()
    {
        return array(self::ID_NEW,self::ID_APPROVED,self::ID_IN_PROGRESS);
    }
}