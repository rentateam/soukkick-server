<?php
class Entity_Prize_Order_History extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrizeOrderId()
    {
        return $this->prize_order_id;
    }
    
    public function getDatetimeCreated()
    {
        return $this->datetime_created;
    }
    
    public function getDetail()
    {
        return $this->detail;
    }
    
    public function getComment()
    {
        return $this->comment;
    }
    
    public function getUserAdminId()
    {
        return $this->user_admin_id;
    }
    
    public function getPrizeOrder()
    {
        return $this->getComputed('prize_order_id');
    }
    
    public function getUserAdmin()
    {
        return $this->getComputed('user_admin_id');
    }
    
    public static function add(Entity_User_Admin $admin,Entity_Prize_Order $order,$detail,$comment)
    {
        return Mapper_Prize_Order_History::getInstance()->add($admin,$order,$detail,$comment);
    }
}