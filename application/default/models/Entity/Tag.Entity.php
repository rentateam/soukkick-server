<?php
class Entity_Tag extends SFM_Entity
{
    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function add($name)
    {
        $entity = Mapper_Tag::getInstance()->getEntityByName($name);
        if($entity === null)
        {
            $entity = Mapper_Tag::getInstance()->add($name);
        }
        return $entity;
    }

    public function delete()
    {
        foreach(Mapper_Mall2Shop::getInstance()->getListByTag($this) as $entity)
        {
            $entity->delete();
        }
        parent::delete();
    }
}