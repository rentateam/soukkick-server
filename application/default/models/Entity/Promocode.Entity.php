<?php
class Entity_Promocode extends Entity_Pictured implements Interface_Actionable, Interface_ActionItem
{
    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }

    public function getBoundRuleItemId()
    {
        return $this->bound_rule_item_id;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getBoundRuleItem()
    {
        return $this->getComputed('bound_rule_item_id');
    }

    public static function add($name,$code,Entity_Rule_Item $item = null, $picture = null)
    {
        $entity = Mapper_Promocode::getInstance()->add($name, mb_strtoupper($code), $item);
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        return $entity;
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getPromocodePicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filename = $this->getId().'.'.$ext;

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/promocode_small_'.$filename;
        $result1 = copy($picturePath, $fullBigPicturePath);
        $s3->move($fullBigPicturePath,Application::getPromocodePicturePath(true).$filename);
        if($result1)
        {
            $result1 = $result1 && parent::update(array('picture_path' => $filename));
        }

        return $result1;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        $return = parent::update($params);
        return $return;
    }

    public function delete()
    {
        parent::delete();
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getPicturePathFull(true));
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        return parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
    }

    public function setBoundRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        return parent::update(array('bound_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
    }
}