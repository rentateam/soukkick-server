<?php
class Entity_Trade_Network extends Entity_Pictured implements Interface_Actionable, Interface_ActionItem
{
    const INTEGRATION_TYPE_ONLINE = 'bill';
    const INTEGRATION_TYPE_CARD = 'card';
    const INTEGRATION_TYPE_CODE = 'code';

    public function isActive()
    {
        return $this->is_active;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getBpaMainId()
    {
        return $this->bpa_main_id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getShopInfo()
    {
        return $this->shop_info;
    }

    public function getRectanglePicturePath()
    {
        return $this->rectangle_picture_path;
    }

    public function getAmountPage()
    {
        return $this->amount_page;
    }

    public function isRecommended()
    {
        return $this->is_recommended;
    }

    public function getRegDate()
    {
       return $this->reg_date;
    }

    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }

    public function getBpaMain()
    {
        return $this->getComputed('bpa_main_id');
    }

    public function getIntegrationDescription()
    {
       return $this->integration_description;
    }

    public function getIntegrationType()
    {
       return $this->integration_type;
    }

    public function isDeleted()
    {
        return $this->is_deleted;
    }

    public function isHidden()
    {
        return $this->is_hidden;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    public static function add(Entity_Bpa_Main $bpaMain,$title,$description,$picture,$rectanglePicture,$phone,$isActive,$shopInfo,$amountPage = 0, $isRecommended = false)
    {
        $entity = Mapper_Trade_Network::getInstance()->add($bpaMain,$title,$description,$phone,$isActive,$shopInfo,$amountPage,$isRecommended);
        Mapper_Trade_Network::getInstance()->clearCacheRecommended();
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        if($rectanglePicture)
        {
            $entity->uploadRectanglePicture($rectanglePicture);
        }
        return $entity;
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getTradeNetworkBigPicturePath($absoluteLink).$picturePath.$postfix : Application::getTradeNetworkBigPicturePath($absoluteLink).Application::USERSHOP_PICTURE_DEFAULT);
    }

    public function getRectanglePicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getRectanglePicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getTradeNetworkRectanglePicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getTradeNetworkPicturePath($absoluteLink).$picturePath.$postfix : Application::getTradeNetworkPicturePath($absoluteLink).Application::USERSHOP_PICTURE_DEFAULT);
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/network_small_'.$filenameOriginalExtension;
        $result = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToSideWithBorders($fullBigPicturePath,Application::getTradeNetworkBigPictureSide());
        $s3->move($pngFileName,Application::getTradeNetworkBigPicturePath(true).$filenamePngExtension);


        $fullPicturePath = '/tmp/network_big_'.$filenameOriginalExtension;
        $result2 = copy($picturePath, $fullPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToSideWithBorders($fullPicturePath,Application::getTradeNetworkPictureSide());
        $s3->move($pngFileName,Application::getTradeNetworkPicturePath(true).$filenamePngExtension);


        $result = $result && $result2;
        if($result)
        {
            $result = $result && parent::update(array('picture_path' => $filenamePngExtension));
        }
        if($result)
        {
            $this->deleteInfoMobileCache();
        }
        return $result;
    }

    protected function uploadRectanglePicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/network_rect_'.$filenameOriginalExtension;
        $result = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullBigPicturePath,Application::getTradeNetworkRectanglePictureWidth());

        if($result)
        {
            $result = $result && parent::update(array('rectangle_picture_path' => $filenamePngExtension));
        }
        if($result)
        {
            $this->deleteInfoMobileCache();
        }

        return $result;
    }

    public function update($params)
    {
        if(isset($params['is_recommended']))
        {
            Mapper_Trade_Network::getInstance()->clearCacheRecommended();
        }
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        if(isset($params['rectangle_picture_path']))
        {
            $this->uploadRectanglePicture($params['rectangle_picture_path']);
            unset($params['rectangle_picture_path']);
        }

        return parent::update($params);
    }

    /**
     * Обновление сущности со скидыванием кэшей для телефонов (перебор всех пользователей)
     * @param array $params
     */
    public function updateWithMobileCache($params)
    {
        $previousRecommended = $this->isRecommended();
        $result = $this->update($params);

        if(isset($params['is_recommended']) && $params['is_recommended'] != $previousRecommended)
        {
            $this->deleteRecommendedMobileCache();
        }

        return $result;
    }

    protected function deleteRecommendedMobileCache(Aggregate_User $userList = null)
    {
        if($userList === null)
            $userList = Mapper_User::getInstance()->getListPlain();

        foreach($userList as $user)
        {
            //стираем ключ рекомендованных магазинов
            $object = new Memcached_Object_RecommendedTradeNetworks($user);
            $object->delete();
        }
    }

    public static function serializeShopInfo($city, $shopNumber)
    {
        $info = array(
                        'city' => $city,
                        'number' => $shopNumber,
                    );
        return serialize($info);
    }

    public static function unserializeShopInfo($info)
    {
        return unserialize($info);
    }

    public function approve()
    {
        //Создаем первый магазин
        $shopInfo = self::unserializeShopInfo($this->getShopInfo());
        Entity_Shop::add($this,$this->getTitle(),0, 0, '');

        return $this->update(array('is_active' => 1));
    }

    public function sendApproveLetter()
    {
        $emailFrom = Application::getDefaultEmailAddress();

        $subject = 'Ваш магазин активирован на shopfee.ru';
        $params = array('tradeNetwork' => $this);

        return Shopfee_Mailer::getInstance()->sendLetter($params, 'approve.phtml', $emailFrom, $this->getAnyBpaManager()->getEmail(), Application::getDefaultEmailTitle(), '', $subject);
    }

    /**
     * @return Aggregate_Shop
     */
    public function getShops($isHidden = false)
    {
        return Mapper_Shop::getInstance()->getListByTradeNetwork($this,$isHidden);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Product
     */
    public function getProducts($pageNumber = 0)
    {
        return Mapper_Product::getInstance()->getListByTradeNetwork($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Sales
     */
    public function getSales($pageNumber = 0)
    {
        return Mapper_Sales::getInstance()->getListByTradeNetwork($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_ScanProduct
     */
    public function getScanProducts($pageNumber = 0)
    {
        return Mapper_ScanProduct::getInstance()->getListByTradeNetwork($this,$pageNumber);
    }

    public function getNumberOfEnters($minutesAgo, $minutesLength)
    {
        return Mapper_Action_Checkin::getInstance()->getCountUsersByObjectAndMinutes($this->getShops(null)->getListEntitiesId(),$minutesAgo,$minutesLength);
    }

    public function getNumberOfNewEnters($minutesAgo, $minutesLength)
    {
        return Mapper_Action_Checkin::getInstance()->getCountNewUsersByObjectAndMinutes($this->getShops(null)->getListEntitiesId(),$minutesAgo,$minutesLength);
    }

    /**
     * Посетители во всех магазинах сети
     */
    public function getExistingVisitors()
    {
        $shops = $this->getShops(null);
        $visitorIds = array();
        foreach($shops as $shop){
            $thisShopVisitors = $shop->getExistingVisitors();
            $visitorIds = array_merge($visitorIds,$thisShopVisitors->getListEntitiesId());
        }
        $visitorIds = array_unique($visitorIds);
        $aggregate = Mapper_User::getInstance()->createAggregate($visitorIds);
        $aggregate->loadEntities();
        return $aggregate;
    }

    protected function setAmountPage($amount)
    {
        if($amount != $this->getAmountPage())
        {
            return $this->update(array('amount_page' => $amount));
        }
        else
        {
            return true;
        }
    }

    /**
     * Добавление/удаление сети из избранного
     * @param Entity_User $user
     * @param boolean $isFavourite
     * @throws Entity_Favourite_Exception
     */
    public function setFavourite(Entity_User $user, $isFavourite)
    {
        if($isFavourite)
        {
            Entity_Favourite_TradeNetwork::add($user, $this);
        }
        else
        {
            $fav = Mapper_Favourite_TradeNetwork::getInstance()->getEntityByUserAndTradeNetwork($user,$this);
            if($fav !== null)
            {
                $fav->delete();
            }
            else
            {
                throw new Entity_Favourite_Exception('Entity with user '.$user->getId().' and user shop '.$this->getId().' does not exist');
            }
        }
        $userAggregate = Mapper_User::getInstance()->createAggregate(array($user->getId()));
        $userAggregate->loadEntities();
        foreach($this->getShops() as $shop)
        {
            $shop->deleteInfoMobileCache($userAggregate);
        }
    }

    public function deleteInfoMobileCache(Aggregate_User $userList = null)
    {
        /*if($userList === null)
            $userList = Mapper_User::getInstance()->getListPlain();

        foreach($userList as $user)
        {
            //стираем ключи внутренностей сети
            $object = new Memcached_Object_TradeNetwork_Info($user,$this);
            $object->delete();
        }*/
    }

    public function isFavourite(Entity_User $user)
    {
        return Mapper_Favourite_TradeNetwork::getInstance()->getEntityByUserAndTradeNetwork($user,$this) !== null;
    }

    public function getJsonObject(Entity_User $user,$includeInner = false)
    {
        $actionTradeNetwork = new Action_Single_TradeNetworkPage($user);
        $actionTradeNetwork->setTradeNetwork($this);
        $result = array(
                            'id' => $this->getId(),
                            'title' => $this->getTitle(),
                            'description' => $this->getDescription(),
                            'big_picture' => $this->getBigPicturePathFull(),
                            'small_picture' => $this->getPicturePathFull(),
                            'rectangle_picture' => $this->getRectanglePicturePathFull(),
                            'amount_page' => $this->getAmountPage(),
                            'is_shoppage' => $actionTradeNetwork->hasTradeNetworkPageForDate(),
                            'is_favourite' => $this->isFavourite($user),
                            'active_rule_item_id' => $this->getActiveRuleItemId()
                        );
        $productList = array();
        foreach($this->getFavouriteProducts() as $product)
        {
            $productList[] = $product->getJsonObject();
        }
        $result['favouriteProductList'] = $productList;

        if($includeInner)
        {
            $shopInner = array(
                                    'product_list' => $this->getProducts(),
                                    'sales_list' => $this->getSales(),
                                    'scanproduct_list' => $this->getScanProducts(),
                                );

            foreach($shopInner as $title => $list)
            {
                $result[$title] = array();
                foreach($list as $element)
                {
                    $result[$title][] = $element->getJsonObject($user);
                }
            }
        }

        return $result;
    }

    public function getJsonReplicationObject()
    {
        $result = array(
                            'id' => $this->getId(),
                            'title' => $this->getTitle(),
                            'description' => $this->getDescription(),
                            'big_picture' => $this->getBigPicturePathFull(),
                            'small_picture' => $this->getPicturePathFull(),
                            'rectangle_picture' => $this->getRectanglePicturePathFull(),
                            'amount_page' => $this->getAmountPage(),
                            'active_rule_item_id' => intval($this->getActiveRuleItemId())
                        );



        $shopInner = array(
                                'product_list' => $this->getProducts(),
                                'sales_list' => $this->getSales(),
                                'scanproduct_list' => $this->getScanProducts(),
                                'shop' => $this->getShops(false)
                            );

        foreach($shopInner as $title => $list)
        {
            $result[$title] = array();
            foreach($list as $element)
            {
                $result[$title][] = $element->getJsonReplicationObject();
            }
        }

        return $result;
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }

    /**
     * @return Aggregate_Product
     */
    public function getFavouriteProducts()
    {
        return Mapper_Product::getInstance()->getListFavouriteByTradeNetwork($this);
    }

    /**
     * @return Entity_Bpa_Manager
     */
    public function getAnyBpaManager()
    {
        return Mapper_Bpa_Manager::getInstance()->getListByTradeNetwork($this)->current();
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        return $result && $this->setAmountPage($ruleItem ? $ruleItem->getAmount() : 0);
    }

    public function setIntegrationTypeAndDescription($type,$description)
    {
        if($type != $this->getIntegrationType() || $description != $this->getIntegrationDescription())
        {
            if($type == null)
            {
                //Очистка правил по покупкам - пока не реализована
            }
            return $this->update(array('integration_type' => $type, 'integration_description' => $description));
        }
        else
        {
            return true;
        }
    }

    public static function getIntegrationTypes()
    {
        return array(
                        '' => 'No integration',
                        self::INTEGRATION_TYPE_ONLINE => 'On-Line',
                        self::INTEGRATION_TYPE_CARD => 'Bank card',
                        self::INTEGRATION_TYPE_CODE => 'Purchase confirmation',
                    );
    }

    public function hasIntegration()
    {
        return $this->getIntegrationType() !== null;
    }

    public function delete()
    {
        $return = $this->update(array('is_deleted' => 1));
        if($this->isRecommended())
        {
            Mapper_Trade_Network::getInstance()->clearCacheRecommended();
        }
        foreach(Mapper_Favourite_TradeNetwork::getInstance()->getListByTradeNetwork($this) as $fav)
        {
            $fav->delete();
        }
        foreach($this->getShops(null) as $shop)
        {
            $shop->delete();
        }
        return $return;
    }

    public function setHidden($isHidden)
    {
        $return = $this->update(array('is_hidden' => $isHidden ? '1' : '0'));
        if($this->isRecommended())
        {
            Mapper_Trade_Network::getInstance()->clearCacheRecommended();
        }
        foreach($this->getShops(null) as $shop)
        {
            $shop->setHidden($isHidden);
        }

        //Тяжелая операция - очистить всем пользователям избранное
        $userList = Mapper_User::getInstance()->getListPlain();
        foreach($userList as $user)
        {
            Mapper_Favourite_TradeNetwork::getInstance()->clearCacheByUser($user);
        }

        return $return;
    }
}