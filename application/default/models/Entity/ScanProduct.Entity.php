<?php
class Entity_ScanProduct extends Entity_ShopsBound implements Interface_Actionable, Interface_ActionItem
{
    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getBarcode()
    {
        return $this->barcode;
    }

    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    public function getShopIds()
    {
        return $this->mapper->getShopIdsByScanProduct($this);
    }

    public function setShopIds(array $shopIds)
    {
        return $this->update(array('shop_ids' => $shopIds));
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        if(isset($params['shop_ids']))
        {
            $this->mapper->replaceShopIds($this,$params['shop_ids']);
        }
        $return = parent::update($params);
        $this->clearCacheByShopIds();
        return $return;
    }

    protected function clearCacheByShopIds()
    {
        foreach($this->getShopIds() as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_ScanProduct::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        $this->getTradeNetwork()->deleteInfoMobileCache();
    }

    public function delete()
    {
        foreach($this->getScans() as $scan)
        {
            $scan->delete();
        }
        $ruleList = Mapper_Rule_Main::getInstance()->getListByBpaMain($this->getTradeNetwork()->getBpaMain(),null);
        foreach($ruleList as $rule)
        {
            foreach(Mapper_Rule_Item::getInstance()->getListFullByRuleMainAndActionType($rule,Entity_Rule_Item::TYPE_SCAN,$this) as $item)
            {
                $item->delete();
            }
        }
        $shopIds = $this->getShopIds();
        Mapper_ScanProduct::getInstance()->replaceShopIds($this,array());
        $return = parent::delete();
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_ScanProduct::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
        //Предполагаем, что все магазины принадлежат к одной сети - просто берем последний
        if($shop)
            Mapper_ScanProduct::getInstance()->clearCacheByTradeNetwork($shop->getTradeNetwork());
        $this->getTradeNetwork()->deleteInfoMobileCache();
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getBigPicturePathFull(true));
        $s3->delete($this->getSmallPicturePathFull(true));
        return $return;
    }

    public static function add($name,$description,Entity_Trade_Network $tradeNetwork,$barcode,$picture)
    {
        $entity = Mapper_ScanProduct::getInstance()->add($name,$description,$tradeNetwork,$barcode,0);
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        $entity->clearShopCache();
        Mapper_ScanProduct::getInstance()->clearCacheByTradeNetwork($tradeNetwork);
        $tradeNetwork->deleteInfoMobileCache();
        return $entity;
    }

    public function getSmallPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getScanProductSmallPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getScanProductBigPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $fullSmallPicturePath = '/tmp/scan_small_'.$filenameOriginalExtension;
        $result1 = copy($picturePath, $fullSmallPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullSmallPicturePath,Application::getScanProductSmallPictureWidth());
        $s3->move($pngFileName,Application::getScanProductSmallPicturePath(true).$filenamePngExtension);

        $fullBigPicturePath = '/tmp/scan_big_'.$filenameOriginalExtension;
        $result2 = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullBigPicturePath,Application::getScanProductBigPictureWidth());
        $s3->move($pngFileName,Application::getScanProductBigPicturePath(true).$filenamePngExtension);

        if($result1 && $result2)
        {
            $result2 = $result2 && parent::update(array('picture_path' => $filenamePngExtension));
        }
        return $result2;
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Scan
     */
    public function getScans($pageNumber = 0)
    {
        return Mapper_Action_Scan::getInstance()->getListByObject($this,$pageNumber);
    }

    public function getDescriptionForDisplay()
    {
        return $this->getDescription();
    }

    public function getJsonObject(Entity_User $user)
    {
        $action = new Action_Single_Scan($user);
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'description' => $this->getDescriptionForDisplay(),
                        'price' => $this->getPrice(),
                        'barcode' => $this->getBarcode(),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'scanned' => $action->hasScannedBarcodeForDate($this),
                        'active_rule_item_id' => $this->getActiveRuleItemId()
                    );
    }

    public function getJsonReplicationObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'description' => $this->getDescriptionForDisplay(),
                        'price' => $this->getPrice(),
                        'barcode' => $this->getBarcode(),
                        'small_picture' => $this->getSmallPicturePathFull(false),
                        'big_picture' => $this->getBigPicturePathFull(false),
                        'active_rule_item_id' => intval($this->getActiveRuleItemId())
        );
    }

    public function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }

    protected function setPrice($price)
    {
        if($price != $this->getPrice())
        {
            return $this->update(array('price' => $price));
        }
        else
        {
            return true;
        }
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        $result = $result && $this->setPrice($ruleItem ? $ruleItem->getAmount() : 0);
        $this->clearShopCache();
        return $result;
    }

    protected function clearShopCache()
    {
        $shopIds = $this->getShopIds();
        foreach($shopIds as $shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            Mapper_ScanProduct::getInstance()->clearCacheByShop($shop);
            $shop->deleteInfoMobileCache();
        }
    }
}