<?php
class Entity_Action_Exit_Attempt extends SFM_Entity
{
    const TIME_CONVERT_ATTEMPT = 900;    //время, после которого попытка чекаута считается удачной - в секундах
    
    public function getId()
    {
        return $this->id;
    }

    public function getDateTimeCreated()
    {
        return $this->datetime_created;
    }
    
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     *  @return Enitty_User
     */
    public function getUser()
    {
        return $this->getComputed('user_id');
    }
    
    public function getShopId()
    {
        return $this->shop_id;
    }
    
    /**
     *  @return Enitty_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }
    
    public function getTransmitterTypeId()
    {
        return $this->transmitter_type_id;
    }
    
    /**
     *  @return Enitty_Transmitter
     */
    public function getTransmitterType()
    {
        return $this->getComputed('transmitter_type_id');
    }
    
    public static function add(Entity_User $user, Entity_Shop $shop, $type)
    {
        $lastAttempt = Mapper_Action_Exit_Attempt::getInstance()->getByTradeNetworkAndTransmitterType($user,$shop,$type);
        if($lastAttempt)
            throw new Action_Exception_Duplicate();
        $entity = Mapper_Action_Exit_Attempt::getInstance()->add($user,$shop,$type);
        return $entity;
    }
}