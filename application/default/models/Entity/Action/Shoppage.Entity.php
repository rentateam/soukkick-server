<?php
class Entity_Action_Shoppage extends Entity_Action_Priced
{
    public function getTradeNetworkId()
    {
        return $this->trade_network_id;
    }

    public function getShopId()
    {
        return 0;
    }

    protected function getObjectId()
    {
        return $this->getTradeNetworkId();
    }

    /**
     *  @return Enitty_Trade_Network
     */
    public function getTradeNetwork()
    {
        return $this->getComputed('trade_network_id');
    }

    public static function add(Entity_User $user, Entity_Trade_Network $tradeNetwork,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_Shoppage::getInstance()->add($user,$tradeNetwork,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Shoppage::getInstance(), $user, $tradeNetwork, $dateCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getTradeNetwork()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getTradeNetwork()->getTitle();
    }
}