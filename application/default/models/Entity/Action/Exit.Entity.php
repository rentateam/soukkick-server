<?php
class Entity_Action_Exit extends Entity_Action_Abstract
{
    public function getShopId()
    {
        return $this->shop_id;
    }

    protected function getObjectId()
    {
        return $this->getShopId();
    }

    /**
     *  @return Enitty_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public function getTransmitterTypeId()
    {
        return $this->transmitter_type_id;
    }

    /**
     *  @return Enitty_Transmitter
     */
    public function getTransmitterType()
    {
        return $this->getComputed('transmitter_type_id');
    }

    public static function add(Entity_User $user, Entity_Shop $shop,$type, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_Exit::getInstance()->add($user,$shop,$type,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Exit::getInstance(), $user, $shop,$dateCreated);
        Mapper_Action_Exit::getInstance()->clearCacheByObjectAndUserAndTypeForDate($shop,$user,$type,$dateCreated);
        Mapper_Action_Exit::getInstance()->clearCacheLastExit($user, $shop, $type);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getShop()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getShop()->getName();
    }

    public function delete()
    {
        foreach(Mapper_Event_Shop::getInstance()->getListByObjectAndType($this,Entity_Event_Shop::TYPE_EXIT) as $event)
        {
            $event->delete();
        }
        return parent::delete();
    }
}