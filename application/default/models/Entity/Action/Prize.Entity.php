<?php
class Entity_Action_Prize extends Entity_Action_Priced
{
    public function getPrizeId()
    {
        return $this->prize_id;
    }

    protected function getShopId()
    {
        return 0;
    }

    protected function getObjectId()
    {
        return $this->getPrizeId();
    }

    /**
     *  @return Enitty_Prize
     */
    public function getPrize()
    {
        return $this->getComputed('prize_id');
    }

    public static function add(Entity_User $user, Entity_Prize $prize,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_Prize::getInstance()->add($user,$prize,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Prize::getInstance(), $user, $prize,$dateCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getPrize()->getBigPicturePathFull();
    }

    public function getName()
    {
        return $this->getPrize()->getName();
    }
}