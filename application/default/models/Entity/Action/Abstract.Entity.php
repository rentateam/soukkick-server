<?php
abstract class Entity_Action_Abstract extends SFM_Entity
{
    abstract protected function getObjectId();

    public function getId()
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getDateTimeCreated()
    {
        return $this->datetime_created;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function update($params)
    {
        return parent::update($params);
    }

    /**
     *  @return Enitty_User
     */
    public function getUser()
    {
        return $this->getComputed('user_id');
    }

    protected static function clearCacheAfterAdd(Mapper_Action_Abstract $mapper, Entity_User $user, Interface_Actionable $object, $date)
    {
        $mapper->clearCacheByUser($user);
        $mapper->clearCacheByObject($object);
        $mapper->clearCacheByObjectAndUserForDate($object,$user, $date);
    }
}