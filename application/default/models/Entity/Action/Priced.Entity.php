<?php
abstract class Entity_Action_Priced extends Entity_Action_Abstract
{
    public function getTrmMainId()
    {
        return $this->trm_main_id;
    }
    
    public function getPrice()
    {
        return $this->getTrmMain()->getAmount();
    }
    
    /**
     * @return Entity_Trm_Main
     */
    public function getTrmMain()
    {
        return $this->getComputed('trm_main_id');
    }

    public function getJsonObject()
    {
        $json = parent::getJsonObject();
        $json['price'] = $this->getPrice();
        return $json;
    } 
}