<?php
class Entity_Action_BuyBarcode extends Entity_Action_Priced
{
    /*1 - Кода нет в базе
    2 - Код уже был активирован
    3 - Пользователь уже сканировал код сегодня
    4 - Неактивен ruleitem, по которому сканируется данный код
    5 - Пользователь не вошел в магазин.
    6 - Штрих-код не соответствует магазину*/
    const ERROR_NO_BUYBARCODE = 1;
    const ERROR_IS_INACTIVE = 2;
    const ERROR_DUPLICATE = 3;
    const ERROR_INACTIVE_RULE = 4;
    const ERROR_NO_SHOP_ENTER = 5;
    const ERROR_WRONG_SHOP = 6;


    public function getBuyBarcodeId()
    {
        return $this->buy_barcode_id;
    }

    protected function getObjectId()
    {
        return $this->getBuyBarcodeId();
    }

    protected function getShopId()
    {
        return $this->getBuyBarcode()->getShopId();
    }

    /**
     *  @return Enitty_BuyBarcode
     */
    public function getBuyBarcode()
    {
        return $this->getComputed('buy_barcode_id');
    }

    public static function add(Entity_User $user, Entity_BuyBarcode $barcode, Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_BuyBarcode::getInstance()->add($user,$barcode,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_BuyBarcode::getInstance(), $user, $barcode,$dateCreated);
        Mapper_Action_BuyBarcode::getInstance()->clearCacheByObjectAndUserAndTypeForDate($barcode,$user,$dateCreated);
        Mapper_Action_BuyBarcode::getInstance()->clearCacheByTradeNetworkAndUserForDate($barcode->getShop()->getTradeNetwork(),$user,$dateCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getBuyBarcode()->getShop()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getBuyBarcode()->getName();
    }
}