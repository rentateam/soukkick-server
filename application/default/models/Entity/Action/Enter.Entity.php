<?php
class Entity_Action_Enter extends Entity_Action_Abstract
{
    const TIME_HUNG_ENTER = 3600;        //время в секундах, после которого вход считается повисшим, и ему назначается автоматический выход

    public function getShopId()
    {
        return $this->shop_id;
    }

    protected function getObjectId()
    {
        return $this->getShopId();
    }

    /**
     *  @return Enitty_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public function getTransmitterTypeId()
    {
        return $this->transmitter_type_id;
    }

    /**
     *  @return Enitty_Transmitter
     */
    public function getTransmitterType()
    {
        return $this->getComputed('transmitter_type_id');
    }

    public static function add(Entity_User $user, Entity_Transmitter $transmitter, $dateCreated = null, $timeCreated = null)
    {
        $shop = $transmitter->getShop();
        $entity = Mapper_Action_Enter::getInstance()->add($user,$shop,$transmitter->getTransmitterTypeId(),$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Enter::getInstance(), $user, $shop,$dateCreated);
        Mapper_Action_Enter::getInstance()->clearCacheByObjectAndUserAndTypeForDate($shop,$user,$transmitter->getTransmitterTypeId(),$dateCreated);
        Mapper_Action_Enter::getInstance()->clearCacheLastEnter($user,$shop,$transmitter->getTransmitterTypeId());
        Entity_Event_Shop::add($shop,Entity_Event_Shop::TYPE_ENTER,$entity,$dateCreated,$timeCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getShop()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getShop()->getName();
    }

    public function getCorrespondingExit()
    {
        $lastExit = Mapper_Action_Exit::getInstance()->getLastExit($this->getUser(),$this->getShop(),$this->getTransmitterTypeId());
        if($lastExit)
        {
            return $lastExit->getDateTimeCreated() > $this->getDateTimeCreated() ? $lastExit : null;
        }
        else
        {
            return null;
        }
    }

    public function delete()
    {
        foreach(Mapper_Event_Shop::getInstance()->getListByObjectAndType($this,Entity_Event_Shop::TYPE_ENTER) as $event)
        {
            $event->delete();
        }
        return parent::delete();
    }
}