<?php
class Entity_Action_Prize_Promo extends Entity_Action_Priced
{
    public function getPrizePromoId()
    {
        return $this->prize_promo_id;
    }

    protected function getShopId()
    {
        return 0;
    }

    protected function getObjectId()
    {
        return $this->getPrizePromoId();
    }

    /**
     *  @return Enitty_Prize_Promo
     */
    public function getPrizePromo()
    {
        return $this->getComputed('prize_promo_id');
    }

    public static function add(Entity_User $user, Entity_Prize_Promo $prizePromo,Entity_Trm_Main $transaction,$dateCreated = null,$timeCreated = null)
    {
        $entity = Mapper_Action_Prize_Promo::getInstance()->add($user,$prizePromo,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Prize_Promo::getInstance(), $user, $prizePromo,$dateCreated.' '.$timeCreated);
        return $entity;
    }

    public function getPrize()
    {
        return $this->getPrizePromo()->getPrize();
    }

    protected function getPicture()
    {
        return $this->getPrize()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getPrize()->getName();
    }
}