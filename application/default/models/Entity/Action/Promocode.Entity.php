<?php
class Entity_Action_Promocode extends Entity_Action_Priced
{
    public function getPromocodeId()
    {
        return $this->promocode_id;
    }

    protected function getObjectId()
    {
        return $this->getPromocodeId();
    }

    /**
     *  @return Enitty_Promocode
     */
    public function getPromocode()
    {
        return $this->getComputed('promocode_id');
    }

    public static function add(Entity_User $user, Entity_Promocode $promocode, Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_Promocode::getInstance()->add($user,$promocode,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Promocode::getInstance(), $user, $promocode,$dateCreated);
        return $entity;
    }
}