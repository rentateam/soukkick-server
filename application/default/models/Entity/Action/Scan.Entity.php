<?php
class Entity_Action_Scan extends Entity_Action_Priced
{
    public function getScanProductId()
    {
        return $this->scan_product_id;
    }

    public function getShopId()
    {
        return $this->shop_id;
    }

    /**
     *  @return Enitty_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    protected function getObjectId()
    {
        return $this->getScanProductId();
    }

    /**
     *  @return Enitty_ScanProduct
     */
    public function getScanProduct()
    {
        return $this->getComputed('scan_product_id');
    }

    public static function add(Entity_User $user, Entity_ScanProduct $product, Entity_Shop $shop, Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $entity = Mapper_Action_Scan::getInstance()->add($user,$product,$shop,$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Scan::getInstance(), $user, $product, $dateCreated);
        Entity_Event_Shop::add($shop,Entity_Event_Shop::TYPE_SCAN,$entity,$dateCreated,$timeCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getScanProduct()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getScanProduct()->getName();
    }

    public function delete()
    {
        foreach(Mapper_Event_Shop::getInstance()->getListByObjectAndType($this,Entity_Event_Shop::TYPE_SCAN) as $event)
        {
            $event->delete();
        }
        return parent::delete();
    }
}