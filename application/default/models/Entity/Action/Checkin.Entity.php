<?php
class Entity_Action_Checkin extends Entity_Action_Priced
{
    const MINUTES_CHECKIN_IS_VISITOR = 15;

    public function getShopId()
    {
        return $this->shop_id;
    }

    protected function getObjectId()
    {
        return $this->getShopId();
    }

    /**
     *  @return Enitty_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public function getTransmitterTypeId()
    {
        return $this->transmitter_type_id;
    }

    /**
     *  @return Enitty_Transmitter
     */
    public function getTransmitterType()
    {
        return $this->getComputed('transmitter_type_id');
    }

    public static function add(Entity_User $user, Entity_Transmitter $transmitter, Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        $shop = $transmitter->getShop();
        $entity = Mapper_Action_Checkin::getInstance()->add($user,$shop,$transmitter->getTransmitterTypeId(),$transaction,$dateCreated,$timeCreated);
        self::clearCacheAfterAdd(Mapper_Action_Checkin::getInstance(), $user, $shop,$dateCreated);
        Mapper_Action_Checkin::getInstance()->clearCacheByObjectAndUserAndTypeForDate($shop,$user,$transmitter->getTransmitterTypeId(),$dateCreated);
        Mapper_User::getInstance()->addToExistingVisitorsAggregate($entity);
        Entity_Event_Shop::add($shop,Entity_Event_Shop::TYPE_CHECKIN,$entity,$dateCreated,$timeCreated);
        return $entity;
    }

    protected function getPicture()
    {
        return $this->getShop()->getBigPicturePathFull();
    }

    protected function getName()
    {
        return $this->getShop()->getName();
    }
}