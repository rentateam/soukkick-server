<?php
class Entity_Action_Log extends SFM_Entity
{
    const RESULT_TYPE_INFO = 0;
    const RESULT_TYPE_WARNING = 1;
    const RESULT_TYPE_ERROR = 2;

    public function getId()
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getActionType()
    {
        return $this->action_type;
    }

    public function getAlat()
    {
        return $this->alat;
    }

    public function getAlong()
    {
        return $this->along;
    }

    public function getAdate()
    {
        return $this->adate;
    }

    public function getAtime()
    {
        return $this->atime;
    }

    public function getActiveRuleItemId()
    {
        return $this->active_rule_item_id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getTrmMainId()
    {
        return $this->trm_main_id;
    }

    /**
     * @return Entity_Trm_Main
     */
    public function getTrmMain()
    {
        return $this->getComputed('trm_main_id');
    }

    public function getObjectId()
    {
        return $this->object_id;
    }

    public function getParam1()
    {
        return $this->param1;
    }

    public function getParam2()
    {
        return $this->param2;
    }

    public function getResultType()
    {
        return $this->result_type;
    }

    public function getResultCode()
    {
        return $this->result_code;
    }

    public static function getResultTypeByCode($code)
    {
        switch($code)
        {
            case Action_Single_Abstract::INFO_NO_SHOP_ENTER:
            case Action_Single_Abstract::INFO_SUCCESS:
                return self::RESULT_TYPE_INFO;
                break;
            case Action_Single_Abstract::WARNING_FRQ_LIST_WRONG:
            case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
            case Action_Single_Abstract::WARNING_NOT_IN_SHOP:
            case Action_Single_Abstract::WARNING_WRONG_SHOP:
                return self::RESULT_TYPE_WARNING;
                break;
            default:
                return self::RESULT_TYPE_ERROR;
        }
    }

    public static function add(Entity_User $user, $actionType, $lat, $long, $date, $time, $ip, Entity_Rule_Item $activeRuleItem = null, $objectId = null, $param1 = null, $param2 = null)
    {
        $entity = Mapper_Action_Log::getInstance()->add($user, $actionType, $lat, $long, $date, $time, $ip, $activeRuleItem, $objectId, $param1, $param2);
        return $entity;
    }

    public function updateResultCode($code, Entity_Trm_Main $transaction = null)
    {
        $params = array(
                            'result_type' => self::getResultTypeByCode($code),
                            'result_code' => $code,
                            'trm_main_id' => $transaction === null ? null : $transaction->getId()
                        );
        return $this->update($params);
    }
}