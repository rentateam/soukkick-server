<?php
class Entity_Prize extends Entity_Pictured implements Interface_Actionable, Interface_ActionItem
{
    const TYPE_OWN = 1;
    const TYPE_PROMO = 2;

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getActiveRuleItemId()
    {
       return $this->active_rule_item_id;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    public static function add($type,$name,$description,$picture, $price)
    {
        $entity = Mapper_Prize::getInstance()->add($type,$name,$description,$price);
        Mapper_Prize::getInstance()->clearCacheGeneral();
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
        }
        if($type == self::TYPE_OWN)
            Entity_Prize_Stock::add($entity);
        $entity->deleteMobileCache();
        return $entity;
    }

    public function getBigPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getPrizeBigPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    public function getSmallPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getPrizeSmallPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;
        $filenamePngExtension = $this->getId().'.png';

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/prize_big_'.$filenameOriginalExtension;
        $result1 = copy($picturePath, $fullBigPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullBigPicturePath,Application::getPrizeBigPictureWidth());
        $s3->move($pngFileName,Application::getPrizeBigPicturePath(true).$filenamePngExtension);


        $fullSmallPicturePath = '/tmp/prize_small_'.$filenameOriginalExtension;
        $result2 = copy( $picturePath, $fullSmallPicturePath);
        $pngFileName = Shopfee_Util_Image::imQualityScaleToWidth($fullSmallPicturePath,Application::getPrizeSmallPictureWidth());
        $s3->move($pngFileName,Application::getPrizeSmallPicturePath(true).$filenamePngExtension);


        if($result1 && $result2)
        {
            $result2 = $result2 && parent::update(array('picture_path' => $filenamePngExtension));
        }

        return $result2;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        $return = parent::update($params);
        return $return;
    }

    /**
     * Обновление сущности со скидыванием кэшей для телефонов (перебор всех пользователей)
     * @param array $params
     */
    public function updateWithMobileCache($params)
    {
        $result = $this->update($params);
        $this->deleteMobileCache();
        return $result;
    }

    public function deleteMobileCache(Aggregate_User $userList = null)
    {
        /*if($userList === null)
            $userList = Mapper_User::getInstance()->getListPlain();

        foreach($userList as $user)
        {
            //стираем ключи наград
            $object = new Memcached_Object_PrizeList($user);
            $object->delete();
        }*/
    }

    public function delete()
    {
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getBigPicturePathFull(true));
        $s3->delete($this->getSmallPicturePathFull(true));
        return $this->setActive(0);
    }

    public function setActive($isActive)
    {
        $return = $this->update(array('is_active' => $isActive ? 1 : 0));
        Mapper_Prize::getInstance()->clearCacheGeneral();
        Mapper_Prize::getInstance()->clearCacheStock();
        return $return;
    }

    public function getDescriptionForDisplay()
    {
        return strip_tags(nl2br($this->getDescription()));
    }

    /**
     * Получить соответствующее правило для приза.
     * @return Entity_Rule_Item|null. Если null - это косяк, т.к. правила должны быть всегда.
     */
    public function getRuleItem()
    {
        $rule = Entity_Bpa_Main::getShopfeeRule();
        foreach($rule->getItems(/*true*/) as $item)
        {
            if($item->getActionType() == Entity_Rule_Item::TYPE_PRIZE && $item->getActionObjectId() == $this->getId())
                return $item;
        }
        return null;
    }

    public function getPriceFromRuleItem()
    {
        $ruleItem = $this->getRuleItem();
        return $ruleItem ? $ruleItem->getAmount() : 0;
    }

    public function setPrice($price)
    {
        if($this->getPrice() != $price)
        {
            $ruleItem = $this->getRuleItem();
            $ruleItem->setAmount($price);
            if($price > 0)
            {
                $ruleItem->updateActive(true);
            }
            else
            {

            }
            return $this->update(array('price' => $price));
        }
        else
        {
            return true;
        }
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        return $result && $this->setPrice($ruleItem ? $ruleItem->getAmount() : 0);
    }

    public function getStock()
    {
        if($this->getType() != self::TYPE_OWN)
        {
            throw new Shopfee_Exception('Trying to get stock of prize type '.$this->getType());
        }
        return Mapper_Prize_Stock::getInstance()->getEntityByPrize($this);
    }

    public function getJsonObject()
    {
        $prizeListItem = array(
                                    'id' => $this->getId(),
                                    'type' => $this->getType(),
                                    'name' => $this->getName(),
                                    'description' => $this->getDescriptionForDisplay(),
                                    'price' => $this->getPrice(),
                                    'big_picture' => $this->getBigPicturePathFull(),
                                    'small_picture' => $this->getBigPicturePathFull(),
                            );
        return $prizeListItem;
    }

    public function getJsonReplicationObject()
    {
        $prizeListItem = array(
                        'id' => $this->getId(),
                        'type' => $this->getType(),
                        'name' => $this->getName(),
                        'description' => $this->getDescriptionForDisplay(),
                        'price' => $this->getPrice(),
                        'big_picture' => $this->getBigPicturePathFull(),
                        'small_picture' => $this->getBigPicturePathFull(),
        );
        return $prizeListItem;
    }

    public static function getTypes()
    {
        return array(
                        self::TYPE_OWN => 'Награды',
                        self::TYPE_PROMO => 'Промокоды',
                    );
    }

    public function getDescriptionForHistory(Entity_Trm_Main $transaction)
    {
        switch($this->getType())
        {
            case self::TYPE_OWN:
                return $this->getName();
            case self::TYPE_PROMO:
                $promo = Mapper_Action_Prize_Promo::getInstance()->getEntityByTrmMain($transaction);
                //@TODO. Тут надо будет менять текст
                return $this->getName() . ', код ' . $promo->getPrizePromo()->getCode();
            default:
                return '';
        }
    }

}