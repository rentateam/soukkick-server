<?php
class Entity_Mall_Map extends Entity_Pictured
{
    public function getFloor()
    {
        return $this->floor;
    }

    public function getMallId()
    {
        return $this->mall_id;
    }

    /**
     * @var Entity_Mall
     */
    public function getMall()
    {
        return $this->getComputed('mall_id');
    }

    public static function add(Entity_Mall $mall,$floor,$picture)
    {
        $entity = Mapper_Mall_Map::getInstance()->getEntityByMallAndFloor($mall, $floor);
        if($entity === null)
        {
            $entity = Mapper_Mall_Map::getInstance()->add($mall,$floor);
        }
        //Загрузка логотипа
        if($picture)
        {
            $entity->uploadPicture($picture);
            $entity->parseMap($picture);
        }
        return $entity;
    }

    public function getFilePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getMallMapPicturePath($absoluteLink).$picturePath.$postfix : '');
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filename = $this->getMallId().'_'.$this->getFloor().'.'.$ext;

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullBigPicturePath = '/tmp/mall_map_'.$filename;
        $result1 = copy($picturePath, $fullBigPicturePath);
        $s3->move($fullBigPicturePath,Application::getMallMapPicturePath(true).$filename);
        if($result1)
        {
            $result1 = $result1 && parent::update(array('picture_path' => $filename));
        }

        return $result1;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        $return = parent::update($params);
        return $return;
    }

    public function delete()
    {
        foreach(Mapper_Tag::getInstance()->getTagListByMap($this) as $tag)
        {
            $tag->delete();
        }
        parent::delete();
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getFilePathFull(true));
    }

    public function parseMap($path)
    {
        $parser = new Parser_Mall_Map($path, $this);
        $parser->extractAndSaveTagList();
    }
}