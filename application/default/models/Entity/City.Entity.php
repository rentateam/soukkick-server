<?php
class Entity_City extends SFM_Entity
{
    const ID_MOSCOW = 1;
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}
