<?php
class Entity_User_Admin_Type extends SFM_Entity
{
    const ID_TOTAL = 1;
    const ID_REWARD = 2;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
}