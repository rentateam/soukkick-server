<?php 
class Entity_User_Push extends SFM_Entity
{
    const ENUM_TYPE_IPHONE = 'iphone';
    const ENUM_TYPE_ANDROID = 'android';
    
    public function getId() 
    {
        return $this->id;
    }
    
    public function getUserId() 
    {
        return $this->user_id;
    }
    
    public function getUser() 
    {
        return $this->getComputed('user_id');
    }
    
    public function getToken() 
    {
        return $this->token;
    }
    
    public function getType() 
    {
        return $this->type;
    }
    
    public static function add(Entity_User $user, $token, $type)
    {
        $entity = Mapper_User_Push::getInstance()->getEntityByUser($user);
        if(!$entity)
        {
            $entity = Mapper_User_Push::getInstance()->add($user, $token, $type);
        }
        else
        {
            $entity->setToken($token);
        }
        return $entity;
    }
    
    public function setToken($token)
    {
        return $this->update(array('token' => $token));
    }
}