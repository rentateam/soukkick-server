<?php
class Entity_User_Admin extends SFM_Entity
{
    const ID_ADMIN = 1;
    
    public function getPassword()
    {
        return $this->password;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getSalt()
    {
        return $this->salt;
    }
    
    public function getUserAdminTypeId()
    {
        return $this->user_admin_type_id;
    }
    
    public function update($params)
    {
        if (isset($params['password']))
            $params['password'] = Password::encryptPassword($params['password']);
    
        return parent::update($params);
    }
    
    public function setPassword($password)
    {
        return $this->update(array('password' => $password));
    }
}