<?php
abstract class Entity_User_Abstract extends Entity_Pictured
{
    public function getPassword()
    {
        return $this->password;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getFullName()
    {
        return $this->name. ' ' . $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRegDate()
    {
        return $this->reg_date;
    }

    public function getPicturePath() 
    {
        return $this->picture_path;
    }
    
    public function getSalt() 
    {
        return $this->salt;
    }
    
    public function update($params)
    {
        if (isset($params['password']))
            $params['password'] = Password::encryptPassword($params['password']);
            
        return parent::update($params);
    }
    
    public function setPassword($password)
    {
        return $this->update(array('password' => $password));
    }

    public function delete()
    {
        return parent::delete();
    }
}