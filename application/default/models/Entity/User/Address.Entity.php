<?php 
class Entity_User_Address extends SFM_Entity
{
    public function getId() 
    {
        return $this->id;
    }
    
    public function getUserId() 
    {
        return $this->user_id;
    }
    
    public function getUser() 
    {
        return $this->getComputed('user_id');
    }
    
    public function getCountry() 
    {
        return $this->country;
    }
    
    public function getCity() 
    {
        return $this->city;
    }
    
    public function getPostIndex() 
    {
        return $this->post_index;
    }
    
    public function getStreet() 
    {
        return $this->street;
    }
    
    public function getHouse() 
    {
        return $this->house;
    }
    
    public function getFlat() 
    {
        return $this->flat;
    }
    
    public function getPhone() 
    {
        return $this->phone;
    }
    
    public static function add(Entity_User $user, $country, $city, $index, $street, $house, $flat, $phone)
    {
        $entity = Mapper_User_Address::getInstance()->add($user, $country, $city, $index, $street, $house, $flat, $phone);
        return $entity;
    }
    
    public function update($params)
    {
        return parent::update($params);
    }
    
    public function getJsonObject()
    {
        return array(
                        'id' => $this->getId(),
                        'country' => $this->getCountry(),
                        'city' => $this->getCity(), 
                        'post_index' => $this->getPostIndex(), 
                        'street' => $this->getStreet(),
                        'house' => $this->getHouse(),
                        'flat' => $this->getFlat(),
                        'phone' => $this->getPhone(),
                    );
    } 
    
    public static function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }
    
    public function toString()
    {
        $result = array();
        $fields = array(
                            $this->getPostIndex(),
                            $this->getCountry(),
                            $this->getCity(),
                            $this->getStreet(),
                            $this->getHouse(),
                            $this->getFlat(),
                        );
        foreach($fields as $field)
        {
            if($field)
                $result[] = $field;
        }
        return implode(',',$result);
    }
    
    public function setPhone($phone)
    {
        if($phone != $this->getPhone())
        {
            return $this->update(array('phone' => $phone));
        }
        else
        {
            return true;
        }
    }    
}