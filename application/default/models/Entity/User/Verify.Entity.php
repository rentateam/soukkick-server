<?php
class Entity_User_Verify extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function isVerified()
    {
        return $this->is_verified;
    }

    public function getUser()
    {
        return $this->getComputed('user_id');
    }

    public static function add(Entity_User $user)
    {
        $entity = Mapper_User_Verify::getInstance()->add($user, self::generateCode());
        return $entity;
    }

    protected static function generateCode()
    {
        return strtoupper(substr(md5(uniqid()), 0, 4));
    }

    public function regenerateCode()
    {
        return $this->update(array('code' => self::generateCode()));
    }

    public function setVerified($isVerified)
    {
        $return = $this->update(array('is_verified' => $isVerified ? '1' : '0'));
        return $return;
    }
}