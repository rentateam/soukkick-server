<?php 
class Entity_Favourite_TradeNetwork extends SFM_Entity 
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getTradeNetworkId() 
    {
        return $this->trade_network_id;
    }
    
    public function getUserId() 
    {
        return $this->user_id;
    }
    
    public function getSortOrder() 
    {
        return $this->sort_order;
    }
    
    public function getTradeNetwork() 
    {
        return $this->getComputed('trade_network_id');
    }
    
    public function getUser() 
    {
        return $this->getComputed('user_id');
    }
    
    public function add(Entity_User $user, Entity_Trade_Network $tradeNetwork)
    {
        $entity = Mapper_Favourite_TradeNetwork::getInstance()->getEntityByUserAndTradeNetwork($user,$tradeNetwork);
        if($entity === null)
        {
            $entity = Mapper_Favourite_TradeNetwork::getInstance()->add($user,$tradeNetwork,self::getDefaultSortOrder($user));
            Mapper_Favourite_TradeNetwork::getInstance()->clearCacheByUser($user);
            return $entity;
        }
        else
            throw new Entity_Favourite_Exception('Entity with user '.$user->getId().' and user shop '.$tradeNetwork->getId().' already exists');
    }
    
    protected static function getDefaultSortOrder(Entity_User $user)
    {
        $children = Mapper_Favourite_TradeNetwork::getInstance()->getListByUser($user);
        $lastChild = null;
        foreach($children as $lastChild)
        {
            
        }
        if($lastChild !== null)
            return $lastChild->getSortOrder() + 10;
        else
            return 10;
    }
    
    public function delete()
    {
        $user = $this->getUser();
        Mapper_Favourite_TradeNetwork::getInstance()->clearCacheByUser($user);
        return parent::delete();
    }
}