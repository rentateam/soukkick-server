<?php
class Entity_Report extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getCaption()
    {
        return $this->caption;
    }

    public function getEditUrl()
    {
        return "/admin/reports/edit/id/{$this->getId()}/";
    }

    public function getRunUrl()
    {
        return "/admin/reports/run/id/{$this->getId()}/";
    }

    public function getDeleteUrl()
    {
        return "/admin/reports/delete/id/{$this->getId()}/";
    }

    public function getExportUrl()
    {
        return "/admin/reports/export/id/{$this->getId()}/";
    }
}