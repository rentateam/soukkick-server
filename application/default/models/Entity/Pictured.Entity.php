<?php
abstract class Entity_Pictured extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }
    
    public function getLastModified()
    {
        return $this->last_modified;
    }
    
    public function getPicturePath() 
    {
        return $this->picture_path;
    }
    
    public function update($params)
    {
        $params['last_modified'] = date('Y-m-d H:i:s');
        return parent::update($params);
    }
}