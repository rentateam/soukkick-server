<?php
class Entity_Settings_Option extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getCaption()
    {
        return $this->caption;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getEditUrl()
    {
        return "/admin/settings/edit/id/{$this->getId()}/";
    }

    public function getDeleteUrl()
    {
        return "/admin/settings/delete/id/{$this->getId()}/";
    }
}