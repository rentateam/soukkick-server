<?php
class Entity_Trm_Main extends SFM_Entity
{
    public function getId()
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getRuleItemId()
    {
        return $this->rule_item_id;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getShopId()
    {
        return $this->shop_id;
    }

    public function getActionDate()
    {
        return $this->action_date;
    }

    public function getActionTime()
    {
        return $this->action_time;
    }

    public function getActionLat()
    {
        return $this->action_lat;
    }

    public function getActionLong()
    {
        return $this->action_long;
    }

    public function getTrmMainStatusId()
    {
        return $this->trm_main_status_id;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getRuleItem()
    {
        return $this->getComputed('rule_item_id');
    }

    /**
     * @return Entity_User
     */
    public function getUser()
    {
        return $this->getComputed('user_id');
    }

    /**
     * @return Entity_Shop
     */
    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    /**
     * @return Entity_Trm_Main_Status
     */
    public function getTrmMainStatus()
    {
        return $this->getComputed('trm_main_status_id');
    }

    public static function add(Entity_Rule_Item $ruleItem, Entity_User $user, $amount, $actionDate, $actionTime, $actionLat, $actionLong, Entity_Shop $shop = null)
    {
        $entity = Mapper_Trm_Main::getInstance()->add($ruleItem, $user, $amount, $actionDate, $actionTime, $actionLat, $actionLong, $shop);
        Mapper_Trm_Main::getInstance()->clearCacheByUser($user);
        try
        {
            $ruleItem->spend($entity);
            $bpaMain = $ruleItem->getRuleMain()->getBpaMain();
        }
        //Если по правилу нельзя тратить деньги (протух лимит) - пробуем потратить жестко и списать с shopfee
        catch(Rule_Exception $e)
        {
            $ruleItem->spendMoreThanLimit($entity);
            $bpaMain = Mapper_Bpa_Main::getInstance()->getEntityById(Entity_Bpa_Main::ID_SHOPFEE);
        }
        $bpaMain->addAmount(-$amount);
        return $entity;
    }

    public function getJsonObject()
    {
        $dateCreated = $this->getDateCreated();
        return array(
                        'id' => $this->getId(),
                        'user_id' => intval($this->getUserId()),
                        'picture' => (string)$this->getPicture(),
                        'description' => (string)$this->getDescription(),
                        'amount' => abs(intval($this->getAmount())),
                        'date' => $this->getActionDate(),
                        'time' => $this->getActionTime(),
                        'type' => $this->getActionTypeForPhone(),
                        'rule_item_id' => intval($this->getRuleItemId()),
                        'ext_id' => intval($this->getRuleItem()->getActionObjectId()),
                        'status' => $this->getTrmMainStatus()->getName()
                    );
    }

    protected function getPicture()
    {
        $ruleItem = $this->getRuleItem();
        switch($ruleItem->getActionType())
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                return $this->getShop()->getBigPicturePathFull();
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                return $ruleItem->getActionObject()->getBigPicturePathFull();
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                return $ruleItem->getActionObject()->getBigPicturePathFull();
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                return $ruleItem->getActionObject()->getBigPicturePathFull();
                break;
            case Entity_Rule_Item::TYPE_ACHIEVEMENT:
                return $ruleItem->getActionObject()->getPictureUrl();
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_GIFT:
                return "http://".Application::getStaticURL()."/imgs/shopfee-gift.png";
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                return "http://".Application::getStaticURL()."/imgs/Profile_bage_promo.png";
                break;
            default:
                return "http://".Application::getStaticURL().'/img/small_logo.png';
                break;
        }
    }

    protected function getActionTypeForPhone()
    {
        $ruleItem = $this->getRuleItem();
        switch($ruleItem->getActionType())
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                return 'checkin';
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                return 'shoppage';
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                return 'scan';
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                return 'reward';
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                return 'purchase';
                break;
            case Entity_Rule_Item::TYPE_ACHIEVEMENT:
            case Entity_Rule_Item::TYPE_SHOPFEE_GIFT:
                return "achievement";
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                return 'achievement';
                break;
            default:
                return '';
                break;
        }
    }

    public function getDescription()
    {
        $ruleItem = $this->getRuleItem();
        switch($ruleItem->getActionType())
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                $shop = $this->getShop();
                return $shop->getTradeNetwork()->getTitle().', '.$shop->getAddress();
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                return 'View '.$ruleItem->getActionObject()->getTitle();
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                $scan = $ruleItem->getActionObject();
                return $scan ? $scan->getName().', '.$scan->getTradeNetwork()->getTitle() : '(the product is deleted)';
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                return $ruleItem->getActionObject()->getDescriptionForHistory($this);
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                $shop = $this->getShop();
                return 'Purchase: '.$shop->getTradeNetwork()->getTitle().', '.$shop->getAddress();
                break;
            case Entity_Rule_Item::TYPE_ACHIEVEMENT:
                return $ruleItem->getActionObject()->getName();
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_GIFT:
                return 'Earlier points';
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                return $ruleItem->getActionObject()->getName();
                break;
            default:
                return '';
                break;
        }
    }
}