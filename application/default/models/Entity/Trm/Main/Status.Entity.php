<?php
class Entity_Trm_Main_Status extends SFM_Entity
{
    const ID_NEW = 1;
    const ID_CONFIRMED = 2;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}