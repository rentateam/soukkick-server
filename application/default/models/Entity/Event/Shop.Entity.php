<?php
class Entity_Event_Shop extends SFM_Entity
{
    const TYPE_ENTER       = 1;
    const TYPE_SCAN        = 2;
    const TYPE_EXIT    = 3;
    const TYPE_CHECKIN    = 4;

    public function getId()
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function getObjectId()
    {
        return $this->object_id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getShopId()
    {
        return $this->shop_id;
    }

    public function getObject()
    {
        return $this->getComputed('object_id');
    }

    public function getShop()
    {
        return $this->getComputed('shop_id');
    }

    public static function add(Entity_Shop $shop,$type,$object = null,$dateCreated = null,$timeCreated = null)
    {
        $entity = Mapper_Event_Shop::getInstance()->add($shop,$type,$object, $dateCreated,$timeCreated);
        Mapper_Event_Shop::getInstance()->clearCacheByTradeNetwork($shop->getTradeNetwork(),$shop);
        return $entity;
    }

    public static function getListByShopGroupByDates(Entity_Trade_Network $tradeNetwork,Entity_Shop $shop = null)
    {
        $eventList = Mapper_Event_Shop::getInstance()->getListByTradeNetwork($tradeNetwork,$shop);
        $result = array();
        $today = strtotime(date('Y-m-d 00:00:00'));
        foreach($eventList as $event)
        {
            $dateTimestamp = strtotime($event->getDateCreated());
            if($today < $dateTimestamp)
            {
                $title = 'сегодня';
            }
            else
            {
                $dateParts = getdate($dateTimestamp);
                $title = $dateParts['mday'].' '.Shopfee_Util_Date::fullMonth2($dateParts['mon']);
            }
            if(!isset($result[$title]))
                $result[$title] = array();

            $result[$title][] = $event;
        }
        return $result;
    }
}