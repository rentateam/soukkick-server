<?php
class Entity_Rule_Main extends SFM_Entity implements Interface_Spendable
{
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDateTimeCreated()
    {
        return $this->datetime_created;
    }

    public function getBpaMainId()
    {
        return $this->bpa_main_id;
    }

    public function getCreatedBpaManagerId()
    {
        return $this->created_bpa_manager_id;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getDateFrom()
    {
        return $this->date_from;
    }

    public function getDateTo()
    {
        return $this->date_to;
    }

    public function getMaxSpendAmount()
    {
        return $this->max_spend_amount;
    }

    public function getSpentAmount()
    {
        return $this->spent_amount;
    }

    public function isDeleted()
    {
        return $this->is_deleted;
    }

    public function getDeletedBpaManagerId()
    {
        return $this->deleted_bpa_manager_id;
    }

    public function getDateTimeDeleted()
    {
        return $this->datetime_deleted;
    }

    public function isChanged()
    {
        return $this->is_changed;
    }

    /**
     * @return Entity_Bpa_Main
     */
    public function getBpaMain()
    {
        return $this->getComputed('bpa_main_id');
    }

    public static function add(Entity_Bpa_Manager $manager, $name, $dateFrom, $dateTo, $maxSpendAmount = -1)
    {
        $entity = Mapper_Rule_Main::getInstance()->add($manager,$name, $dateFrom, $dateTo, $maxSpendAmount);
        return $entity;
    }

    public function setActive($active)
    {
        $return = $this->update(array('is_active' => $active ? '1' : '0'));
        if(!$active)
        {
            foreach($this->getItems(true) as $item)
            {
                $item->updateActive($active);
            }
        }
        return $return;
    }

    public function checkAndSetNotActiveItems()
    {
        //Проверяем, кому из детей не хватит денег исходя из текущих лимитов на детей и папашку
        $items = $this->getItems(true);
        foreach($items as $item)
        {
            if(!$item->canSpendTotal())
                $item->updateActive(false,false);
        }
    }

    public function tryMakeNotActive()
    {
        //Если все дети неактивны или не изменены, делаем папашку неактивным
        foreach($this->getItems(false) as $item)
        {
            if($item->isActive() || $item->isChanged())
                return;
        }
        $this->setActive(false);
    }

    public function delete(Entity_Bpa_Manager $manager)
    {
        $params = array(
                            'is_deleted' => 1,
                            'is_active' => '0',
                            'is_changed' => '0',
                            'deleted_bpa_manager_id' => $manager->getId(),
                            'datetime_deleted' => date('Y-m-d H:i:s')
                        );
        $return = $this->update($params);
        foreach($this->getItems() as $item)
        {
            $item->delete();
        }
        return $return;
    }

    public function addSpendAmount($amount)
    {
        $this->update(array('spent_amount' => $this->getSpentAmount() + $amount));
    }

    /**
     * @param $onlyActive boolean
     * @return Aggregate_Rule_Item
     */
    public function getItems($onlyActive = false)
    {
        return Mapper_Rule_Item::getInstance()->getListByRuleMain($this,$onlyActive);
    }

    public function checkEditRight(Entity_Trade_Network $network)
    {
        return $network->getBpaMainId() == $this->getBpaMainId();
    }

    public function getShopIds()
    {
        return Mapper_Rule_Main::getInstance()->getShopIds($this);
    }

    public function getShopNumber()
    {
        return count($this->getShopIds());
    }

    public function isRunningNow($checkActivity = true)
    {
        $now = time();
        $dateFrom = date('Y-m-d 00:00:00',strtotime($this->getDateFrom()));
        $dateTo = date('Y-m-d 23:59:59',strtotime($this->getDateTo()));
        return (!$checkActivity || $this->isActive()) && (strtotime($dateFrom) <= $now && $now <= strtotime($dateTo));
    }

    /**
     * Возвращает массив id пересекающихся по датам сущностей
     * @param array $ruleMainAggregateList
     * @return array of integer
     */
    public static function getOverlappedIds(array $ruleMainAggregateList)
    {
        $overlappedIds = array();
        $plainRuleList = array();
        foreach($ruleMainAggregateList as $aggregate)
        {
            foreach($aggregate as $rule)
            {
                if(!isset($plainRuleList[$rule->getId()]))
                    $plainRuleList[$rule->getId()] = $rule;
            }
        }

        foreach($plainRuleList as $rule)
        {
            foreach($plainRuleList as $ruleToCompare)
            {
                if($rule->getId() != $ruleToCompare->getId() && $rule->overlaps($ruleToCompare))
                {
                    $overlappedIds[] = $rule->getId();
                    $overlappedIds[] = $ruleToCompare->getId();
                }
            }
        }

        $overlappedIds = array_unique($overlappedIds);
        return $overlappedIds;
    }

    /**
     * Пересекается ли данное правило с текущим по времени
     * @param Entity_Rule_Main $rule
     * @return boolean
     */
    protected function overlaps(Entity_Rule_Main $rule)
    {
        $s1 = strtotime($this->getDateFrom());
        $s2 = strtotime($rule->getDateFrom());
        $e1 = strtotime($this->getDateTo());
        $e2 = strtotime($rule->getDateTo());
        if(
            $s1 < $s2 && $e1 < $s2
            ||
            $s1 > $s2 && $e2 < $s1
        )
        {
            return false;
        }
        return true;
    }

    public function getMaxSpendAmountSum()
    {
        $sum = 0;
        foreach($this->getItems(true) as $item)
        {
            if($item->getMaxSpendAmount() > 0)
                $sum += $item->getMaxSpendAmount();
        }
        return $sum;
    }

    public function setDateFrom($dateFrom)
    {
        if($this->getDateFrom() != $dateFrom)
        {
            $return = $this->update(array('date_from' => $dateFrom));
            $this->setChanged(true);
            return $return;
        }
    }

    public function setDateTo($dateTo)
    {
        if($this->getDateTo() != $dateTo)
        {
            $return = $this->update(array('date_to' => $dateTo));
            $this->setChanged(true);
            return $return;
        }
    }

    public function update(array $params)
    {
        return parent::update($params);
    }

    public function setChanged($isChanged)
    {
        return parent::update(array('is_changed' => $isChanged ? '1' : '0'));
    }

    public function setShopIds(array $shopIds)
    {
        Mapper_Rule_Main::getInstance()->setShopIds($this,$shopIds);
    }

    public function applyChanges()
    {
        $this->setChanged(false);
        $currentItems = $this->getItems(true);

        $changedItems = Mapper_Rule_Item::getInstance()->getListChangedByRuleMain($this);
        foreach($changedItems as $changedItem)
        {
            foreach($currentItems as $currentItem)
            {
                //Если это то же правило
                if($currentItem->isSuchRuleItem($changedItem))
                {
                    $changedItem->applyLimitAndStat($currentItem);
                    $currentItem->delete();
                    continue;
                }
            }
            if($this->isActive())
                $changedItem->updateActive(true);
            $changedItem->checkCanSpendTotal();

            $changedItem->setChanged(false);
        }
    }
}