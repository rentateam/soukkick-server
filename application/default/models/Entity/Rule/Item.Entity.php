<?php
class Entity_Rule_Item extends SFM_Entity implements Interface_Spendable
{
    const TYPE_CHECKIN = 1;
    const TYPE_SHOPPAGE = 2;
    const TYPE_SCAN = 3;
    const TYPE_FACEBOOK = 4;
    const TYPE_CONTACT = 5;
    const TYPE_PROMOCODE = 6;
    const TYPE_PRIZE = 7;
    const TYPE_ACHIEVEMENT = 8;
    const TYPE_BUY_BARCODE = 9;
    const TYPE_SHOPFEE_GIFT = 10;
    const TYPE_SHOPFEE_PROMOCODE = 11;

    public function getId()
    {
        return $this->id;
    }

    public function getRuleMainId()
    {
        return $this->rule_main_id;
    }

    public function getActionObjectId()
    {
        return $this->action_object_id;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getMaxSpendAmount()
    {
        return $this->max_spend_amount;
    }

    public function getSpentAmount()
    {
        return $this->spent_amount;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getActionType()
    {
        return $this->action_type;
    }

    public function isDeleted()
    {
        return $this->is_deleted;
    }

    public function isChanged()
    {
        return $this->is_changed;
    }

    /**
     * @return Entity_Rule_Main
     */
    public function getRuleMain()
    {
        return $this->getComputed('rule_main_id');
    }

    /**
     * @return Interface_ActionItem
     */
    public function getActionObject()
    {
        return $this->getComputed('action_object_id');
    }

    public static function add(Entity_Rule_Main $rule, $amount, $type, $actionObject = null, $maxSpendAmount = -1, $isChangedRuleItem = false)
    {
        //Правило, такое же, как данное, но уже измененное ранее и не успевшее вступить в силу
        $suchRuleItem = null;

        $entity = null;
        //Список измененных правил - надо взять его сначала
        if($isChangedRuleItem)
        {
            $changedItems = Mapper_Rule_Item::getInstance()->getListChangedByRuleMain($rule);
            //Удалить такое же правило, но измененное, если оно есть
            foreach($changedItems as $changedItem)
            {
                //Если это то же правило
                if(self::isSuchRuleItemByParts($changedItem,$rule,$type,$actionObject))
                {
                    $suchRuleItem = $changedItem;
                    break;
                }
            }

            if($suchRuleItem)
            {
                $suchRuleItem->delete();
            }
            else
            {
                //Иначе
                //если ничего не изменилось по ценам/лимитам, не добавляем лишнее, возвращаем старое неизмененное правило
                $activeEntity = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionType($rule,$type,$actionObject);
                if($activeEntity)
                {
                    if($amount == $activeEntity->getAmount() && $maxSpendAmount == $activeEntity->getMaxSpendAmount())
                    {
                        $entity = $activeEntity;
                    }
                }
            }

            if(!$entity)
            {
                $entity = Mapper_Rule_Item::getInstance()->add($rule, $amount, $type, $actionObject, $maxSpendAmount);
                $entity->setChanged(true);
                $entity->setActive(false);
            }
        }
        else
        {
            $entity = Mapper_Rule_Item::getInstance()->add($rule, $amount, $type, $actionObject, $maxSpendAmount);
        }

        return $entity;
    }

    public function updateActive($active)
    {
        $return = $this->setActive($active);
        //Если делаем его неактивным, запускаем проверку по родительскому RuleMain.
        if(!$active)
        {
            $this->getRuleMain()->tryMakeNotActive();
            $this->removeThisFromActive();
        }
        return $return;
    }

    public function setActive($active)
    {
        return $this->update(array('is_active' => $active ? '1' : '0'));
    }

    public function delete()
    {
        $params = array('is_deleted' => 1);
        $return = $this->update($params);
        $this->setChanged(false);
        $this->setActive(false);
        $this->removeThisFromActive();
        return $return;
    }

    /**
     * Грохнуть текущее правило из активных по всем возможным привязанным объектам.
     * Вызывается при деактивации или удалении правила.
     */
    protected function removeThisFromActive()
    {
        $actionObjects = $this->getActionObjects();
        foreach($actionObjects as $actionObject)
        {
            /*@var Interface_ActionItem*/
            $activeRuleItem = $actionObject->getActiveRuleItem();
            if($activeRuleItem && $activeRuleItem->getId() == $this->getId())
            {
                $actionObject->setActiveRuleItem(null);
                //Для сканов - чистим еще и магазинный кросс
                if($this->getActionType() == Entity_Rule_Item::TYPE_SCAN)
                {
                    $actionObject->setShopIds(array());
                }
            }
        }
    }

    /**
     * Список объектов, на которые распространяется данное правило
     */
    protected function getActionObjects()
    {
        $actionObjects = array();
        $ruleMain = $this->getRuleMain();
        $shopIdList = Mapper_Rule_Main::getInstance()->getShopIds($ruleMain);
        switch($this->getActionType())
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                $shopList = Mapper_Shop::getInstance()->createAggregate($shopIdList,null,true);
                foreach($shopList as $shop)
                {
                    if($shop->getMainTransmitter())
                    {
                        $actionObjects[] = $shop->getMainTransmitter();
                    }
                }
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                foreach(Mapper_Trade_Network::getInstance()->getListByBpaMain($ruleMain->getBpaMain()) as $tradeNetwork)
                {
                    $actionObjects[] = $tradeNetwork;
                }
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                foreach(Mapper_BuyBarcode::getInstance()->getListByShopIds($shopIdList) as $buyBarcode)
                {
                    $actionObjects[] = $buyBarcode;
                }
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                foreach(Mapper_ScanProduct::getInstance()->getListByShopIds($shopIdList) as $scan)
                {
                    $actionObjects[] = $scan;
                }
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                //Пока все промокоды shopfee-шные, можно отдавать их все
                $actionObjects = Mapper_Promocode::getInstance()->getList()->getContent();
                break;
        }
        return $actionObjects;
    }

    /**
     * Совпадает ли указанное правило с данным (т.е., одно и то же ли правило по смыслу).
     * Считается, что совпадает, если совпадают тип и объект правила
     * @param Entity_Rule_Item $compareItem
     * @return false
     */
    public function isSuchRuleItem(Entity_Rule_Item $compareItem)
    {
        return self::isSuchRuleItemByParts($compareItem,$this->getRuleMain(),$this->getActionType(),$this->getActionObject());
    }

    protected static function isSuchRuleItemByParts(Entity_Rule_Item $compareItem,Entity_Rule_Main $rule, $acitonType, $actionObject = null)
    {
        return $compareItem->getRuleMainId() == $rule->getId() && $compareItem->getActionType() == $acitonType && ($actionObject && $compareItem->getActionObjectId() == $actionObject->getId() || !$compareItem->getActionObjectId());
    }


    /**
     * Потратить фишки данного правила
     */
    public function spend(Entity_Trm_Main $transaction)
    {
        if(!$this->canSpendByRule())
        {
            throw new Rule_Exception('Trying to spend more than allowed in rule '.$this->getRuleMainId());
        }
        if(!$this->canSpend())
        {
            throw new Rule_Exception('Trying to spend more than allowed in rule item '.$this->getId());
        }

        $this->update(array('spent_amount' => $this->getSpentAmount() + $this->getAmount()));
        $this->getRuleMain()->addSpendAmount($this->getAmount());

        //Дергаем папашку, чтобы он поотключал правила, на которые ему не хватает денег
        $this->getRuleMain()->checkAndSetNotActiveItems();

        //Если на правиле закончились деньги (не хватает денег на следующий раз) - вырубаем его
        $this->checkCanSpendTotal();
    }

    /**
     * Потратить фишки данного правила без учета лимита
     */
    public function spendMoreThanLimit(Entity_Trm_Main $transaction)
    {
        $this->update(array('spent_amount' => $this->getSpentAmount() + $this->getAmount()));
        $this->getRuleMain()->addSpendAmount($this->getAmount());
    }

    /**
     * Проверяем, можно ли тратить бабло на правиле. Если нет - выключает его
     */
    public function checkCanSpendTotal()
    {
        //Если на правиле закончились деньги (не хватает денег на следующий раз)
        if(!$this->canSpendTotal())
        {
            $this->updateActive(false);
            if($this->getActionType() == Entity_Rule_Item::TYPE_BUY_BARCODE)
            {
                //Скидываем otkat в магазине
                foreach($this->getActionObjects() as $object)
                {
                    $valueItem = new Value_Rule_Item_ShopAndType($object->getShop(),$this->getActionType());
                    $valueItem->setActiveRuleItemForActionObject(null,$object);
                }
            }
        }
    }

    public function canSpendTotal()
    {
        return $this->canSpend() && $this->canSpendByRule();
    }

    public function canSpend()
    {
        return $this->getAmount() > 0 && $this->canSpendByEntity($this);
    }

    public function canSpendByRule()
    {
        return $this->canSpendByEntity($this->getRuleMain());
    }

    protected function canSpendByEntity(Interface_Spendable $entity)
    {
        $spentAmount = $entity->getSpentAmount();
        $maxSpendAmount = $entity->getMaxSpendAmount();
        if($maxSpendAmount == -1 || $spentAmount + $this->getAmount() <= $maxSpendAmount)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function getAmountForFacebook()
    {
        return self::getAmountByShopfeeActionType(Entity_Achievement::ID_FACEBOOK);
    }

    public static function getAmountForContact()
    {
        return self::getAmountByShopfeeActionType(Entity_Achievement::ID_CONTACT);
    }

    public static function getAmountForPromocode()
    {
        return self::getAmountByShopfeeActionType(Entity_Achievement::ID_PROMOCODE);
    }

    public static function getAmountByShopfeeActionType($achievementId)
    {
        $achievement = Mapper_Achievement::getInstance()->getEntityById($achievementId);
        $ruleItem = $achievement->getActiveRuleItem();
        return $ruleItem ? $ruleItem->getAmount() : 0;
    }

    public function setAmount($amount)
    {
        $return = $this->update(array('amount' => $amount));
        if($amount == 0)
        {
            $this->updateActive(false);
        }
    }

    public function setChanged($isChanged)
    {
        return parent::update(array('is_changed' => $isChanged ? '1' : '0'));
    }

    public function applyLimitAndStat(Entity_Rule_Item $item)
    {
        $params = array(
                            'spent_amount' => $item->getSpentAmount(),
                        );
        return $this->update($params);
    }
}