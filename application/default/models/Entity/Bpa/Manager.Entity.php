<?php
class Entity_Bpa_Manager extends Entity_User_Abstract
{
    public function getId()
    {
        return $this->id;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getBpaMainId()
    {
        return $this->bpa_main_id;
    }

    /**
     * @return Entity_Bpa_Main
     */
    public function getBpaMain()
    {
        return $this->getComputed('bpa_main_id');
    }

    public static function add(Entity_Bpa_Main $bpaMain,$email,$password,$name,$surname)
    {
        $salt = md5(uniqid());
        $encryptedPassword = Password::encryptPassword($password);
        $entity = Mapper_Bpa_Manager::getInstance()->add($bpaMain,$email,$encryptedPassword,$name,$surname,$salt);
        return $entity;
    }

    public static function getShopfeeBpaManager()
    {
        $shopfee = Mapper_Bpa_Main::getInstance()->getEntityById(Entity_Bpa_Main::ID_SHOPFEE);
        return Mapper_Bpa_Manager::getInstance()->getListByBpaMain($shopfee)->current();
    }
}