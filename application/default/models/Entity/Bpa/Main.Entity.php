<?php
class Entity_Bpa_Main extends SFM_Entity
{
    const DEFAULT_CONVERSION_RATE_TO_FISHKI = 1;
    const DEFAULT_CONVERSION_RATE_FROM_FISHKI = 1;

    const ID_SHOPFEE = 100;    //наш мега-пользователь Shopfee

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getFishkiAmount()
    {
        return $this->fishki_amount;
    }

    public function getConversionRateToFishki()
    {
        return $this->conversion_rate_to_fishki;
    }

    public function getConversionRateFromFishki()
    {
        return $this->conversion_rate_from_fishki;
    }

    public static function add($name,$description,$conversionRateToFishki,$conversionRateFromFishki)
    {
        $entity = Mapper_Bpa_Main::getInstance()->add($name,$description,$conversionRateToFishki,$conversionRateFromFishki);
        return $entity;
    }

    public function addAmount($amount)
    {
        $resultAmount = $this->getFishkiAmount() + $amount;
        return $this->update(array('fishki_amount' =>  $resultAmount));
    }

    /**
     * Возвращает правило пользователя Shopfee. Правил много (из-за промокодов), но возвращает первое.
     */
    public static function getShopfeeRule()
    {
        $ruleMainList = self::getAllShopfeeRules();
        return $ruleMainList->current();
    }

    /**
     * Возвращает все правила пользователя Shopfee.
     */
    public static function getAllShopfeeRules()
    {
        $shopfee = Mapper_Bpa_Main::getInstance()->getEntityById(Entity_Bpa_Main::ID_SHOPFEE);
        $ruleMainList = Mapper_Rule_Main::getInstance()->getListByBpaMain($shopfee);
        return $ruleMainList;
    }
}