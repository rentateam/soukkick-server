<?php
class Entity_Achievement extends SFM_Entity implements Interface_ActionItem
{
    const ID_FACEBOOK = 1;
    const ID_CONTACT = 2;
    const ID_PROMOCODE = 3;
    const ID_VERIFY = 4;

    public function getName()
    {
        return $this->name;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getAvailableAtLevel()
    {
        return $this->available_at_level;
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function getActiveRuleItemId()
    {
        return $this->active_rule_item_id;
    }

    /**
     * @return Entity_Rule_Item
     */
    public function getActiveRuleItem()
    {
       return $this->getComputed('active_rule_item_id');
    }

    public function getPictureUrl()
    {
        return 'http://'.Application::getStaticURL().$this->picture;
    }

    public function getJsonObject()
    {
        return array(
                        'id' => $this->getId(),
                        'name' => $this->getName(),
                        'picture' => $this->getPictureUrl(),
                        'amount' => $this->getAmount(),
                        'available_at_level' => $this->getAvailableAtLevel(),
                    );
    }

    protected function setAmount($amount)
    {
        return $this->update(array('amount' => $amount));
    }

    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null)
    {
        $result = parent::update(array('active_rule_item_id' => $ruleItem ? $ruleItem->getId() : null));
        return $result && $this->setAmount($ruleItem ? $ruleItem->getAmount() : 0);
    }
}