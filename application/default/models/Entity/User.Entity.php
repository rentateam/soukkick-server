<?php
class Entity_User extends Entity_User_Abstract
{
    const DEFAULT_NAME = 'Новый';
    const DEFAULT_SURNAME = 'Пользователь';

    public function getAmount()
    {
        return $this->amount;
    }

    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    public function getNumberPrize()
    {
        return $this->number_prize;
    }

    public function getNumberCheckin()
    {
        return $this->number_checkin;
    }

    public function getNumberInvited()
    {
        return $this->number_invited;
    }

    public function getNumberScan()
    {
        return $this->number_scan;
    }

    public function getNumberShows()
    {
        return Mapper_User::getInstance()->getNumberShows($this);
    }

    public function getNumberBuyBarcode()
    {
        return $this->number_buy_barcode;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function isMale()
    {
        return $this->is_male;
    }

    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    public function getContactId()
    {
        return $this->contact_id;
    }

    public function getPromoCode()
    {
        return $this->promo_code;
    }

    public function getUdid()
    {
        return $this->udid;
    }

    public function getCleanUdid()
    {
        return $this->clean_udid;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getFloatTime()
    {
        return $this->float_time;
    }

    public function getBannedReason()
    {
        return Mapper_User::getInstance()->getBannedReason($this);
    }

    public function isBanned()
    {
        return (false === is_null(Mapper_User::getInstance()->getBannedReason($this)));
    }

    public function ban($reason)
    {
        Mapper_User::getInstance()->ban($this, $reason);
    }

    public function unban()
    {
        Mapper_User::getInstance()->unban($this);
    }

    public function getDateRegistration()
    {
        return $this->date_registration;
    }

    public function increaseAmount($amount)
    {
        $newAmount = (float) $this->getAmount() + (float) $amount;
        $newAmount = ($newAmount < 0) ? 0 : $newAmount;
        $updateParams = array('amount' => $newAmount);
        if($amount > 0)
        {
            $updateParams['total_amount'] = (float) $this->getTotalAmount() + (float) $amount;
        }
        return $this->update($updateParams);
    }

    public function increaseInvited()
    {
        return $this->update(array('number_invited' => $this->getNumberInvited() + 1));
    }

    public function increaseActionNumber($fieldName)
    {
        return $this->update(array($fieldName => $this->getInfo($fieldName) + 1));
    }

    public function delete()
    {
        foreach($this->getScans() as $item)
        {
            $item->delete();
        }
        foreach($this->getShopCheckins() as $item)
        {
            $item->delete();
        }
        foreach($this->getShopEnters() as $item)
        {
            $item->delete();
        }
        foreach($this->getShoppages() as $item)
        {
            $item->delete();
        }
        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
        $s3->delete($this->getPicturePathFull(true));
        return parent::delete();
    }

    public static function add($udid,$cleanUdid)
    {
        $salt = md5(uniqid());
        //Для старых клиентов - берем udid хотя бы какой прислали - пускай и зашифрованный.
        if(!$cleanUdid)
        {
            $cleanUdid = $udid;
        }
        $entity = Mapper_User::getInstance()->add($udid,$cleanUdid,$salt);
        Mapper_User::getInstance()->clearCacheByUdid($udid);
        Entity_User_Address::add($entity,'','','','','','','');
        return $entity;
    }

    public function getPicturePathFull($absoluteLink = false)
    {
        $picturePath = $this->getPicturePath();
        $postfix = $absoluteLink ? '' : '?'.strtotime($this->getLastModified());
        return ($picturePath && $picturePath != 'NULL' ? Application::getUserPicturePath($absoluteLink).$picturePath.$postfix : Application::getUserPicturePath($absoluteLink).Application::USER_PICTURE_DEFAULT);
    }

    protected function uploadPicture($picturePath)
    {
        $ext = SFM_Util_File::getExt(basename($picturePath));
        $filenameOriginalExtension = $this->getId().'.'.$ext;

        $s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

        $fullPicturePath = '/tmp/user_'.$filenameOriginalExtension;
        $result = copy($picturePath, $fullPicturePath);
        Shopfee_Util_Image::imScaleToSideWithBorders($fullPicturePath,Application::getUserPictureSide());
        $s3->move($fullPicturePath,Application::getUserPicturePath(true).$filenameOriginalExtension);

        if($result)
        {
            $result = $result && parent::update(array('picture_path' => $filenameOriginalExtension));
        }
        return $result;
    }

    public function update($params)
    {
        if(isset($params['picture_path']))
        {
            $this->uploadPicture($params['picture_path']);
            unset($params['picture_path']);
        }
        return parent::update($params);
    }

    public function setPicture($picture)
    {
        return $this->uploadPicture($picture);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Action_Scan
     */
    public function getScans($pageNumber = 0)
    {
        return Mapper_Action_Scan::getInstance()->getListByUser($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Action_Prize
     */
    public function getPrizes($pageNumber = 0)
    {
        return Mapper_Action_Prize::getInstance()->getListByUser($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Action_Checkin
     */
    public function getShopCheckins($pageNumber = 0)
    {
        return Mapper_Action_Checkin::getInstance()->getListByUser($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Action_Enter
     */
    public function getShopEnters($pageNumber = 0)
    {
        return Mapper_Action_Enter::getInstance()->getListByUser($this,$pageNumber);
    }

    /**
     * @param integer $pageNumber
     * @return Aggregate_Action_Shoppage
     */
    public function getShoppages($pageNumber = 0)
    {
        return Mapper_Action_Shoppage::getInstance()->getListByUser($this,$pageNumber);
    }

    /**
     * @param Entity_Shop $shop
     * @param integer|null $whenTimestamp
     * @return Entity_Action_Exit
     */
    public function shopExit(Entity_Shop $shop,$type,$whenTimestamp = null, $deleteExitAttempts = true)
    {
        $entity = Entity_Action_Exit::add($this, $shop,$type);
        if($deleteExitAttempts)
            Mapper_Action_Exit_Attempt::getInstance()->clearOldByUserAndShop($this,$shop,$type,$whenTimestamp);
        return $entity;
    }

    /**
     * Выходил ли пользователь из этого магазина сегодня
     * @param Entity_Shop $shop
     * @param integer|null $type Тип передатчика
     * @return boolean
     */
    public function hasShopExitForDate(Entity_Shop $shop,$type = null, $date = null)
    {
        //@TODO. Можно бы тут использовать memcachedb
        $list = Mapper_Action_Exit::getInstance()->getListByObjectAndUserAndTypeForDate($shop, $this, $type, $date);
        return ($list->totalCount() > 0 ? true : false);
    }

    public function getJsonObject()
    {
        $isPhoneVerified = false;
        $verifyCode = Mapper_User_Verify::getInstance()->getEntityByUser($this);
        if($verifyCode){
            $isPhoneVerified = $verifyCode->isVerified();
        }

        return array(
            'id' => $this->getId(),
            'name' => (string)$this->getName(),
            'surname' => (string)$this->getSurname(),
            'email' => (string)$this->getEmail(),
            'amount' => $this->getAmount(),
            'total_amount' => $this->getTotalAmount(),
            'birthdate' => (string)$this->getBirthdate(),
            'isMale' => $this->isMale() ? '1' : '0',
            'picture' => $this->getPicturePathFull(),
            'number_scan' => $this->getNumberScan(),
            'number_prize' => $this->getNumberPrize(),
            'number_checkin' => $this->getNumberCheckin(),
            'facebook_id' => intval($this->getFacebookId()),
            'contact_id' => intval($this->getContactId()),
            'udid' => (string)$this->getUdid(),
            'promo_code' => (string)$this->getPromoCode(),
            'is_used_any_promo_code' => $this->isUsedAnyPromoCode() ? '1' : '0',
            'level' => $this->getLevel(),
            'is_banned' => $this->isBanned(),
            'is_phone_verified' => $isPhoneVerified ? '1' : '0',
            'favourite_trade_network_ids' => $this->getFavouriteTradeNetworkIds(),
            'user_address' => $this->getAddress()->getJsonObject()
        );
    }

    /**
     * @return integer[]
     */
    protected function getFavouriteTradeNetworkIds()
    {
        $tradeNetworkIds = array();
        $favouriteTradeNetworks = $this->getFavouriteTradeNetworks();

        /** @var $favouriteTradeNetwork Entity_Favourite_TradeNetwork */
        foreach ($favouriteTradeNetworks as $favouriteTradeNetwork) {
            $tradeNetworkIds[] = (int) $favouriteTradeNetwork->getTradeNetworkId();
        }

        return $tradeNetworkIds;
    }

    public static function getNullJsonObject()
    {
        return array(
                        'id' => 0,
                    );
    }

    /**
     * @return Aggregate_Favourite_TradeNetwork
     */
    public function getFavouriteTradeNetworks()
    {
        return Mapper_Favourite_TradeNetwork::getInstance()->getListByUser($this);
    }

    /**
     * @return Entity_User_Address
     */
    public function getAddress()
    {
        return Mapper_User_Address::getInstance()->getEntityByUser($this);
    }

    /**
     * Умная привязка профиля facebook.
     * @param integer $facebookId
     * @throws Entity_User_Exception_Facebook
     * @return boolean
     */
    public function updateFacebookId($facebookId)
    {
        $existingEntity = Mapper_User::getInstance()->getEntityByFacebookId($facebookId);
        if($existingEntity === null)
        {
            $oldFacebookId = $this->getFacebookId();
            $this->update(array('facebook_id' => $facebookId));
            $this->deleteUdid();
            Mapper_User::getInstance()->clearCacheByFacebookId($facebookId);

            $amount = 0;
            if($oldFacebookId === null)
            {
                //Начисляем баллы за facebook
                $actionMaker = new Action_Single_Achievement_Facebook($this);
                $action = $actionMaker->make();
                $amount = $action['amount'];
            }
            return $amount;
        }
        else if($existingEntity->getId() == $this->getId())
        {
            return 0;
        }
        else
        {
            throw new Entity_User_Exception_Facebook('User with id '.$existingEntity->getId().' already has facebook id '.$facebookId);
        }
    }

    /**
     * Умная привязка профиля ВКонтакте.
     * @param integer $contactId
     * @throws Entity_User_Exception_Contact
     * @return boolean
     */
    public function updateContactId($contactId)
    {
        $existingEntity = Mapper_User::getInstance()->getEntityByContactId($contactId);
        if($existingEntity === null)
        {
            $oldContactId = $this->getContactId();
            $this->update(array('contact_id' => $contactId));
            $this->deleteUdid();
            Mapper_User::getInstance()->clearCacheByContactId($contactId);

            $amount = 0;
            if($oldContactId === null)
            {
                //Начисляем баллы за привязку
                $actionMaker = new Action_Single_Achievement_Contact($this);
                $action = $actionMaker->make();
                $amount = $action['amount'];
            }
            return $amount;
        }
        else if($existingEntity->getId() == $this->getId())
        {
            return 0;
        }
        else
        {
            throw new Entity_User_Exception_Contact('User with id '.$existingEntity->getId().' already has contact id '.$contactId);
        }
    }

    public function setNames($name,$surname, $isMale, $birthdate, $onlyIfNotEmpty)
    {
        $params = array();
        if(!$onlyIfNotEmpty || (!$this->getName() || $this->getName() == self::DEFAULT_NAME))
            $params['name'] = $name;
        if(!$onlyIfNotEmpty || (!$this->getSurname() || $this->getSurname() == self::DEFAULT_SURNAME))
            $params['surname'] = $surname;
        if(!$onlyIfNotEmpty || $this->isMale() === null)
            $params['is_male'] = $isMale ? '1' : '0';
        if(!$onlyIfNotEmpty || !$this->getBirthdate())
            $params['birthdate'] = $birthdate;

        if(!empty($params))
            return $this->update($params);
    }

    public function setEmailAndPassword($email,$password)
    {
        $params = array();
        //Если мыло уже есть, менять мы его не можем.
        if(!$this->getEmail())
        {
            $existingEntity = Mapper_User::getInstance()->getEntityByEmail($email);
            if($existingEntity === null || $existingEntity->getId() == $this->getId())
            {
                $params = array('email' => $email);
            }
            else
            {
                throw new Entity_User_Exception_Email('User with id '.$existingEntity->getId().' already has email '.$email);
            }
        }

        if($password)
            $params['password'] = $password;

        if(!empty($params))
        {
            $result = $this->update($params);
            $this->deleteUdid();
            Mapper_User::getInstance()->clearCacheByEmail($email);
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function deleteUdid()
    {
        $udid = $this->getUdid();
        $result = $this->update(array('udid' => null,'date_registration' => date('Y-m-d H:i:s')));
        Mapper_User::getInstance()->clearCacheByUdid($udid);
        return $result;
    }

    public function isUsedAnyPromoCode()
    {
        return Mapper_User::getInstance()->isUsedAnyPromoCode($this);
    }

    public static function generatePromoCode($udid)
    {
        $code = strtoupper( substr( md5(uniqid().$udid) , 0, 6) );
        return $code;
    }

    public function getLevel()
    {
        $points = $this->getTotalAmount();
        $level = 0;
        $min = 100;
        $max = 200;
        for($i=3; $points>=$max; $i++)
        {
            $tmp = $max;
            $max = (2*$max-$min)+100;
            $min = $tmp;
            $level = $i;
        }

        if($points>=0 && $points<50)
        {
            $level = 0;
        }
        else if ($points>=50 && $points<100)
        {
            $level = 1;
        }
        else if ($points>=100 && $points<200)
        {
            $level = 2;
        }
        return $level;
    }

    public function getTrmMains($showAll = false)
    {
        return Mapper_Trm_Main::getInstance()->getListByUser($this,$showAll ? 0 : 1);
    }

    public function setCleanUdid($cleanUdid)
    {
        if($cleanUdid != $this->getCleanUdid() && !empty($cleanUdid))
        {
            return $this->update(array('clean_udid' => $cleanUdid));
        }
        else
        {
            return true;
        }
    }

    public function getPushToken()
    {
        $push = Mapper_User_Push::getInstance()->getEntityByUser($this);
        return $push ? $push->getToken() : null;
    }

    public function getAdminUrl()
    {
        return "/admin/users/show/id/{$this->id}/";
    }

    /**
     * @return Aggregate_User
     */
    public function getInvitedUsers()
    {
        return Mapper_User::getInstance()->getInvitedUsers($this);
    }

    /**
     * @return Entity_User
     */
    public function getInvitedBy()
    {
        return Mapper_User::getInstance()->getInvitedBy($this);
    }

    public function getVerifyCode()
    {
        $verifyCodeEntity = Mapper_User_Verify::getInstance()->getEntityByUser($this);
        return $verifyCodeEntity !== null ? $verifyCodeEntity->getCode() : null;
    }

    public function usePrizePromoCode(Entity_Prize $prize)
    {
        $prizePromo = Mapper_Prize_Promo::getInstance()->getAnyByPrize($prize);
        if($prizePromo === null)
        {
            throw new Prize_Promo_Exception(Prize_Promo_Exception::TYPE_NO_PROMOCODE);
        }

        $prizePromo->useCode();

        //Направляем СМС и письмо.
        $phone = $this->getAddress()->getPhone();
        if($phone)
        {
            $messageSender = new Message_LittleSms();
            $messageSender->sendSms($phone,'Код потверждения: '.$prizePromo->getCode());
        }

        $subject = 'Промокод от shopfee.ru';
        $params = array('code' => $prizePromo->getCode());
        Shopfee_Mailer::getInstance()->sendLetter($params, 'prizepromo.phtml', Application::getDefaultEmailAddress(), $this->getEmail(), Application::getDefaultEmailTitle(), '', $subject);
        return $prizePromo;
    }

    public function canRegister()
    {
        return Mapper_User::getInstance()->getCountByUdidToday($this->getCleanUdid()) == 0;
    }

    public function setFloatTime($floatTime)
    {
        return $this->update(array('float_time' => $floatTime));
    }
}