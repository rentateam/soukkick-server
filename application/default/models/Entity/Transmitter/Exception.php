<?php
class Entity_Transmitter_Exception extends Shopfee_Exception_Entity
{
    protected $_transmitter;

    public function setTransmitter(Entity_Transmitter $transmitter)
    {
        $this->_transmitter = $transmitter;
    }

    public function getTransmitter()
    {
        return $this->_transmitter;
    }
}