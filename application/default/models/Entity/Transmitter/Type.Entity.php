<?php 
class Entity_Transmitter_Type extends SFM_Entity
{
    const ID_DEFAULT = 1;
    
    public function getId()
    {
        return $this->id;
    }
                                    
    public function getName() 
    {
        return $this->name;
    }
}