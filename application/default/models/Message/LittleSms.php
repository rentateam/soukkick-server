<?
require_once 'LittleSMS.class.php';

class Message_LittleSms
{
    protected $_api;
    const DEFAULT_SENDER = 'Shopfee'; 
    
    
    public function __construct($testMode = false)
    {
        $this->_api = new LittleSMS(Application::getLittleSmsUser(), Application::getLittleSmsKey(), true, $testMode);
    }     
    
    public function sendSms($phone,$text)
    {
        //$response = $api->getResponse();
        if($this->_api->sendSMS($phone, $text, self::DEFAULT_SENDER))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}