<?
class Decorator_Rule_Main extends Decorator_Abstract
{
    const GROUPPED_ITEM_CURRENT = 'current';
    const GROUPPED_ITEM_CHANGED = 'changed';
    const GROUPPED_ITEM_OVERALL = 'overall';

    const WARNING_MESSAGE_NO_CHECKIN = 1;
    const WARNING_MESSAGE_NO_SCAN = 2;

    public function __construct(Entity_Rule_Main $rule)
    {
        $this->_item = $rule;
    }

    public function getDateFrom()
    {
        return date('d.m.Y',strtotime($this->_item->getDateFrom()));
    }

    public function getDateTo()
    {
        return date('d.m.Y',strtotime($this->_item->getDateTo()));
    }

    public function getSpentAmount()
    {
        return $this->getAmountWithRoubles($this->_item->getSpentAmount());
    }

    public function getMaxSpendAmount()
    {
        $amount = $this->getAmountWithRoubles($this->_item->getMaxSpendAmount());
        return $amount != -1 ? $amount : '';
    }

    protected function getAmountWithRoubles($amount)
    {
        return $amount.' руб.';
    }

    public function getGrouppedItems()
    {
        $grouppedItems = array();
        $grouppedItems[self::GROUPPED_ITEM_OVERALL] = array(
                                                                self::GROUPPED_ITEM_CURRENT => array('spent_amount' => 0,'max_spend_amount' => 0),
                                                                self::GROUPPED_ITEM_CHANGED => array('spent_amount' => 0,'max_spend_amount' => 0),
                                                            );
        $allItems = Mapper_Rule_Item::getInstance()->getStatListByRuleMain($this->_item);
        foreach($allItems as $allItem)
        {
            $ruleItemId = $allItem['rule_item_id'];
            $ruleItem = Mapper_Rule_Item::getInstance()->getEntityById($ruleItemId);

            $key = self::getGrouppedItemKey($ruleItem);
            if(!isset($grouppedItems[$key]))
            {
                $grouppedItems[$key] = array(
                                                self::GROUPPED_ITEM_CURRENT => array(),
                                                self::GROUPPED_ITEM_CHANGED => array(),
                                            );
                foreach($grouppedItems[$key] as $type => $val)
                {
                    $grouppedItems[$key][$type] = array(
                                                            'action_type' => null,
                                                            'amount' => 0,
                                                            'user_number' => 0,
                                                            'spent_amount' => 0,
                                                            'max_spend_amount' => 0
                                                        );
                }
            }

            //Если правило изменено
            if($ruleItem->isChanged())
            {
                $type = self::GROUPPED_ITEM_CHANGED;
            }
            else
            {
                $type = self::GROUPPED_ITEM_CURRENT;
            }

            $ruleItemDecorator = new Decorator_Rule_Item($ruleItem);
            $grouppedItems[$key][$type]['action_type'] = $ruleItemDecorator->getActionTypeName();
            $grouppedItems[$key][$type]['user_number'] += $allItem['trm_number'];

            //Если были транзакции
            if($allItem['amount'] !== null)
            {
                $grouppedItems[$key][$type]['spent_amount'] += $allItem['amount'];
                //$grouppedItems[self::GROUPPED_ITEM_OVERALL][$type]['spent_amount'] += $allItem['amount'];
            }

            $grouppedItems[$key][$type]['amount'] = !$ruleItem->isDeleted() ? $ruleItem->getAmount() : 0;
            $grouppedItems[$key][$type]['max_spend_amount'] = $ruleItem->getMaxSpendAmount();
            //if(!$ruleItem->isDeleted()/* || $ruleItem->isChanged()*/)
            //{
                //$grouppedItems[self::GROUPPED_ITEM_OVERALL][$type]['max_spend_amount'] += $ruleItem->getMaxSpendAmount();
            //}
        }

        foreach($grouppedItems as $key => $keyContent)
        {
            foreach($keyContent as $type => $content)
            {
                $grouppedItems[self::GROUPPED_ITEM_OVERALL][$type]['max_spend_amount'] += $content['max_spend_amount'];
                $grouppedItems[self::GROUPPED_ITEM_OVERALL][$type]['spent_amount'] += $content['spent_amount'];
            }
        }
        return $grouppedItems;
    }

    protected static function getGrouppedItemKey(Entity_Rule_Item $item)
    {
        return $item->getRuleMainId().'@'.$item->getActionType().'@'.$item->getActionObjectId();
    }

    public function getWarningMessages(Entity_Trade_Network $network)
    {
        $messages = array();

        //В данной кампании есть магазины без передатчиков, а ставка правила за чекин больше 0.
        $checkinRule = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionTypeForm($this->_item,Entity_Rule_Item::TYPE_CHECKIN,null,false);
        if($checkinRule)
        {
            $shopList = Mapper_Shop::getInstance()->createAggregate($this->_item->getShopIds(),null,true);
            foreach($shopList as $shop)
            {
                if(!$shop->isTransmitterAvailable())
                {
                    $messages[self::WARNING_MESSAGE_NO_CHECKIN] = true;
                    break;
                }
            }
        }


        //У данной кампании нет сканов.
        if(count($network->getScanProducts()) == 0)
        {
            $messages[self::WARNING_MESSAGE_NO_SCAN] = true;
        }

        return $messages;
    }

    public function isPast()
    {
        return strtotime($this->_item->getDateTo()) - time() < 0;
    }
}