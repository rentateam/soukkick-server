<?
class Decorator_Rule_Item extends Decorator_Abstract
{
    public function __construct(Entity_Rule_Item $item)
    {
        $this->_item = $item;
    }

    public function getActionTypeName()
    {
        switch($this->_item->getActionType())
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                return 'Посещения';
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                return 'Просмотр витрины';
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                $scan = $this->_item->getActionObject();
                //Если скан еще живой
                if($scan)
                {
                    $scanName = $scan->getName();
                }
                else
                {
                    $scanName = '(товар удален)';
                }
                return 'Скан <span class="scan_item">'.$scanName.'</span>';
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                return 'Покупки';
                break;
            default:
                return '';
        }
    }

    public function getAmount()
    {
        return $this->getAmountWithRoubles($this->_item->getAmount());
    }

    public function getSpentAmount()
    {
        return $this->getAmountWithRoubles($this->_item->getSpentAmount());
    }

    public function getMaxSpendAmount()
    {
        return $this->getAmountWithRoubles($this->_item->getMaxSpendAmount());
    }

    protected function getAmountWithRoubles($amount)
    {
        return ($amount ? $amount : 0).' руб.';
    }
}