<?
class Decorator_Shop extends Decorator_Abstract
{
    public function __construct(Entity_Shop $item)
    {
        $this->_item = $item;
    }
    
    public function getAddressForHtmlAttribute()
    {
        return htmlspecialchars(trim($this->getAddress()));
    }
    
    public function getAddressForJs()
    {
        return str_replace("'","\'",(trim($this->getAddress())));
    }
}