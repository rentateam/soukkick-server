<?php
class Decorator_Trm_Main
{
    protected $_item;

    public function __construct(Entity_Trm_Main $user)
    {
        $this->_item = $user;
    }

    public function __call($name, $args) {
        $return = call_user_func_array(array($this->_item, $name), $args);
        return $return;
    }

    public function getRegDate()
    {
        $datetime = DateTime::createFromFormat('Y-m-d G:i:s', $this->_item->getRegDate());
        return ($datetime === false) ? '—' : $datetime->format('d.m.Y');
    }

    public function getEmail()
    {
        $email = $this->_item->getEmail();
        return is_null($email) ? '—' : $email;
    }

    public function getPrizes()
    {
        $prizes = $this->_item->getPrizes();
        return $prizes->count();
    }
}