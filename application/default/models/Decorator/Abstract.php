<?php
abstract class Decorator_Abstract
{
    /** @var SFM_Business */
    protected $_item;

    public function __call($method, $args) {
        return call_user_func_array(
            array($this->_item, $method),
            $args
        );
    }
    
    public function getItem()
    {
        return $this->_item;
    }
}