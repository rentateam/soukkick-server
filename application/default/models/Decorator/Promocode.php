<?
class Decorator_Promocode extends Decorator_Abstract
{
    public function __construct(Entity_Promocode $item)
    {
        $this->_item = $item;
    }

    public function getDate()
    {
        $ruleMain = $this->getRuleMain();
        return $ruleMain->getDateFrom().' - '.$ruleMain->getDateTo();
    }

    public function isActive()
    {
        $ruleMain = $this->getRuleMain();
        return strtotime($ruleMain->getDateTo()) >= time();
    }

    protected function getRuleMain()
    {
        return $this->_item->getBoundRuleItem()->getRuleMain();
    }

    public function getAmount()
    {
        return $this->_item->getBoundRuleItem()->getAmount();
    }

    public function getLimit()
    {
        return $this->_item->getBoundRuleItem()->getmaxSpendAmount();
    }
}