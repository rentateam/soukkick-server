<?php
class Decorator_User_Address extends Decorator_Abstract
{
    public function __construct(Entity_User_Address $item)
    {
        $this->_item = $item;
    }
    
    public function toString()
    {
        return $this->toStringFull(true);
    }
    
    public function toStringShort()
    {
        return $this->toStringFull(false);
    }
    
    public function toStringWithoutIndex()
    {
        return $this->toStringFull(false,false);
    }
    
    protected function toStringFull($isFull, $includeIndex = true)
    {
        $result = array();
        $fields = array(
                        $this->_item->getStreet(),
                        $this->_item->getHouse(),
                        $this->_item->getFlat(),
        );
        
        if($isFull)
        {
            $extra = array(
                                $this->_item->getCountry(),
                                $this->_item->getCity(),
                            );
            if($includeIndex)
            {
                $extra = array_merge(array($this->_item->getPostIndex()),$extra);
            }
            $fields = array_merge(
                                    $extra,
                                    $fields
                                );
        }
        foreach($fields as $field)
        {
            if($field)
                $result[] = $field;
        }
        return implode(',',$result);
    }
}