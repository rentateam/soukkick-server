<?
class Decorator_Mall extends Decorator_Abstract
{
    public function __construct(Entity_Mall $item)
    {
        $this->_item = $item;
    }

    public function getMapUrlList()
    {
        $urlList = array();
        $mapList = $this->_item->getMapList();
        foreach($mapList as $map)
        {
            $urlList[] = $map->getURL();
        }
        return $urlList;
    }


}