<?
class Decorator_BuyBarcode extends Decorator_Abstract
{
    public function __construct(Entity_BuyBarcode $item)
    {
        $this->_item = $item;
    }
    
    public function getBarcodeShort()
    {
        return substr($this->getBarcode(),0,12);
    }
}