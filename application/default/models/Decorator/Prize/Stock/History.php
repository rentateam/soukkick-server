<?php
class Decorator_Prize_Stock_History extends Decorator_Abstract
{
    public function __construct(Entity_Prize_Stock_History $item)
    {
        $this->_item = $item;
    }
    
    public static function getIncreaseDecreaseComment(Entity_Prize_Order $order)
    {
        return '<a href="'.$order->getAdminUrl().'">'.$order->getPrize()->getName().'</a> → '.$order->getPrizeOrderStatus()->getName();
    }
    
    public function getUsername()
    {
        return $this->_item->getUserAdmin()->getName();
    }
    
    public function getDifference()
    {
        $diff = $this->_item->getDifference();
        if($diff > 0)
        {
            return '+'.$diff;
        }
        else
        {
            return $diff;
        }
    }
}