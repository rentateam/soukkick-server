<?
class Decorator_ShopListByCity extends Decorator_Abstract
{
    public function __construct(Aggregate_Shop $shopList)
    {
        $this->_item = $shopList;
    }
    
    public function getShopList()
    {
        $cityList = array();
        foreach($this->_item as $shop)
        {
            $city = $shop->getCity();
            $cityName = $city ? $city->getName() : 'Город не определен';
            $cityId = $city ? $city->getId() : 0;
            if(!isset($cityList[$cityId]))
                $cityList[$cityId] = array('cityName' => $cityName,'shopList' => array());

            $cityList[$cityId]['shopList'][] = $shop;
        }
        return $cityList;
    }
}