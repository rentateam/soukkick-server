<?php
class Decorator_Trm_Main
{
    protected $_item;

    public function __construct(Entity_Trm_Main $transaction)
    {
        $this->_item = $transaction;
    }

    public function __call($name, $args) {
        $return = call_user_func_array(array($this->_item, $name), $args);
        return $return;
    }

    public function getDateCreated()
    {
        $datetime = DateTime::createFromFormat('Y-m-d G:i:s', $this->_item->getDateCreated());
        return ($datetime === false) ? '—' : $datetime->format('d.m.Y G:i');
    }
}