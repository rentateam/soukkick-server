<?php
class Rule_Trm_Factory
{
    public static function create(Entity_User $user, $actionDate, $actionTime, $actionLat, $actionLong, $actionType,Interface_ActionItem $actionObject = null, Entity_Shop $shop = null, Entity_Rule_Item $explicitRuleItem = null)
    {
        switch($actionType)
        {
            case Entity_Rule_Item::TYPE_ACHIEVEMENT:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $actionObject->getAmount();
                $shop = null;
                break;
            case Entity_Rule_Item::TYPE_CHECKIN:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $actionObject->getAmountEnter();
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $actionObject->getAmountPage();
                $shop = null;
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $actionObject->getPrice();
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = -$actionObject->getPrice();
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $actionObject->getPrice();
                break;
            case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                $ruleItem = $actionObject->getActiveRuleItem();
                $amount = $ruleItem->getAmount();
                break;
            default:
                throw new Rule_Exception('Undefined action type '.$actionType.' specified');
        }

        if($explicitRuleItem !== null)
        {
            $ruleItem = $explicitRuleItem;
            $amount = $ruleItem->getAmount();
        }
        $transaction = Entity_Trm_Main::add($ruleItem, $user, $amount, $actionDate, $actionTime, $actionLat, $actionLong, $shop);
        $user->increaseAmount($amount);
        return $transaction;
    }
}