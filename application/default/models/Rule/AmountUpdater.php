<?php
class Rule_AmountUpdater
{
    public function update()
    {
        //Обновляем все, что относится к Shopfee
        $this->updateShopfeeRules();


        $perPage = 10;
        $shopList = Mapper_Shop::getInstance()->getList(1,$perPage);
        $totalShopCount = $shopList->totalCount();
        for($i=1;$i<=ceil($totalShopCount/$perPage);$i++)
        {
            $shopList->clearLoadedEntities();
            $shopList->loadEntitiesForCurrentPage($i,$perPage);
            foreach($shopList as $shop)
            {
                //Формируем список rule-item-ов, которые действуют в магазине. С учетом конкуренции.
                $rules = Mapper_Rule_Main::getInstance()->getListByShop($shop);
                if($rules->count() == 0)
                    continue;


                foreach($rules as $rule)
                {
                    $rule->applyChanges();
                }

                $ruleItemList = $this->getRuleItemListFromRuleMainList($rules);


                //Обновляем таблицу сканов
                $this->updateScanProducts($shop, $ruleItemList);

                //Обновляем значения полей чекинов
                $this->updateCheckins($shop, $ruleItemList);

                //Обновляем значения полей штрихкодов за покупки
                $this->updateBuyBarcodes($shop, $ruleItemList);

                //Предзагружаем объекты действий для каждого магазина, чтобы жили в кэше
                $this->preloadShopValues($shop);


            }
        }


        //Теперь по сетям
        $perPage = 100;
        $networkList = Mapper_Trade_Network::getInstance()->getList(1,null,$perPage);
        $totalNetworkCount = $networkList->totalCount();
        for($i=1;$i<=ceil($totalNetworkCount/$perPage);$i++)
        {
            $networkList->loadEntitiesForCurrentPage($i,$perPage);
            foreach($networkList as $network)
            {
                $ruleMainList = Mapper_Rule_Main::getInstance()->getListByBpaMain($network->getBpaMain(),Entity_Rule_Item::TYPE_SHOPPAGE);
                foreach($ruleMainList as $rule)
                {
                    $rule->applyChanges();
                }
                $ruleItemList = $this->getRuleItemListFromRuleMainList($ruleMainList);

                //Обновляем значения полей просмотра витрины
                $this->updateShopPages($network, $ruleItemList);
            }
        }


        //#1858
        SFM_Cache_Memory::getInstance()->flush();
    }

    /**
     * Замещает имеющееся правило, либо оставляет его таким же. Проверка на возможность использования правила (активность, остаток денег на правиле).
     * Логика:
     * 1. Если имеющееся правило относится к другому Bpa_Main, добавляем правило рядом с имеющимся.
     * 2. Если нет - то смотрим, больше ли дает новое правило. Если да - старое выкидываем, новое добавляем. Если нет - выкидываем новое.
     * @param array $ruleItemList
     * @param Entity_Rule_Item $ruleItem
     * @return array
     */
    protected function tryReplaceRuleItem(array $ruleItemList,Entity_Rule_Item $ruleItem)
    {
        $rule = $ruleItem->getRuleMain();

        //Проверяем, пролезает ли правило по лимитам.
        $ruleItem->checkCanSpendTotal();

        //Если оно после этого неактивно - выкидываем его
        if(!$ruleItem->isActive())
            return $ruleItemList;

        $actionType = $ruleItem->getActionType();
        $actionObjectId = $ruleItem->getActionObjectId();
        if(!isset($ruleItemList[$actionType][$actionObjectId]))
            $ruleItemList[$actionType][$actionObjectId] = array();

        $hasSuchBpaMain = false;
        foreach($ruleItemList[$actionType][$actionObjectId] as $index => &$ruleItemIterate)
        {
            if($ruleItemIterate->getRuleMain()->getBpaMainId() == $rule->getBpaMainId())
            {
                $hasSuchBpaMain = true;
                //У кого больше....
                if($ruleItem->getAmount() > $ruleItemIterate->getAmount())
                {
                    //......тот и папка
                    $ruleItemIterate = $ruleItem;
                }
                break;
            }
        }
        //Иначе такого правила нет - добавляем
        if(!$hasSuchBpaMain)
            $ruleItemList[$actionType][$actionObjectId][] = $ruleItem;

        return $ruleItemList;
    }

    protected function updateScanProducts(Entity_Shop $shop, array $ruleItemList)
    {
        //Скидывание старых цен
        if(!isset($ruleItemList) || !isset($ruleItemList[Entity_Rule_Item::TYPE_SCAN]))
        {
            foreach(Mapper_ScanProduct::getInstance()->getListByShop($shop) as $scanProduct)
            {
                $scanProduct->setActiveRuleItem(null);
            }
        }
        Mapper_ScanProduct::getInstance()->deleteByShop($shop);

        if(isset($ruleItemList[Entity_Rule_Item::TYPE_SCAN]))
        {
            $scanProductIds = array_keys($ruleItemList[Entity_Rule_Item::TYPE_SCAN]);
            Mapper_ScanProduct::getInstance()->replaceObjectIds($shop, $scanProductIds);
            foreach($scanProductIds as $scanProductId)
            {
                $ruleItem = self::getRuleItemWithMaximumPrice($ruleItemList[Entity_Rule_Item::TYPE_SCAN][$scanProductId]);
                Mapper_ScanProduct::getInstance()->getEntityById($scanProductId)->setActiveRuleItem($ruleItem);
            }
        }
    }

    protected function updateCheckins(Entity_Shop $shop, array $ruleItemList)
    {
        if(isset($ruleItemList[Entity_Rule_Item::TYPE_CHECKIN]))
        {
            //За чекин даем только мы или сторонние программы лояльности - т.е. правило одно.
            $objectKeys = array_keys($ruleItemList[Entity_Rule_Item::TYPE_CHECKIN]);
            $checkinRuleItem = self::getRuleItemWithMaximumPrice($ruleItemList[Entity_Rule_Item::TYPE_CHECKIN][$objectKeys[0]]);
        }
        else
        {
            $checkinRuleItem = null;
        }

        $mainTransmitter = $shop->getMainTransmitter();
        if($mainTransmitter)
        {
            $mainTransmitter->setActiveRuleItem($checkinRuleItem);
        }
    }

    protected function updateShopPages(Entity_Trade_Network $tradeNetwork, array $ruleItemList)
    {
        if(isset($ruleItemList[Entity_Rule_Item::TYPE_SHOPPAGE]))
        {
            //За просмотр витрины даем только мы или сторонние программы лояльности - т.е. правило одно.
            $objectKeys = array_keys($ruleItemList[Entity_Rule_Item::TYPE_SHOPPAGE]);
            $shoppageRuleItem = self::getRuleItemWithMaximumPrice($ruleItemList[Entity_Rule_Item::TYPE_SHOPPAGE][$objectKeys[0]]);
        }
        else
        {
            $shoppageRuleItem = null;
        }
        $tradeNetwork->setActiveRuleItem($shoppageRuleItem);
    }

    protected function updateBuyBarcodes(Entity_Shop $shop, array $ruleItemList)
    {
        if(isset($ruleItemList[Entity_Rule_Item::TYPE_BUY_BARCODE]))
        {
            //За покупки дает только сеть - т.е. правило одно.
            $objectKeys = array_keys($ruleItemList[Entity_Rule_Item::TYPE_BUY_BARCODE]);
            $buyBarcodeRuleItem = self::getRuleItemWithMaximumPrice($ruleItemList[Entity_Rule_Item::TYPE_BUY_BARCODE][$objectKeys[0]]);
        }
        else
        {
            $buyBarcodeRuleItem = null;
        }

        $barcodeListByShop = Mapper_BuyBarcode::getInstance()->getListByShop($shop);
        foreach($barcodeListByShop as $buyBarcode)
        {
            //Автоматически проставятся пустые правила тем, кто уже активирован
            $buyBarcode->setActiveRuleItem($buyBarcodeRuleItem);
        }
    }

    protected function updateShopfeeRules()
    {
        foreach(Entity_Bpa_Main::getAllShopfeeRules() as $rule)
        {
            $rule->applyChanges();
            foreach($rule->getItems(true) as $item)
            {
                switch($item->getActionType())
                {
                    case Entity_Rule_Item::TYPE_PRIZE:
                    case Entity_Rule_Item::TYPE_ACHIEVEMENT:
                    case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                        $item->getActionObject()->setActiveRuleItem($item);
                        break;
                }
            }
        }
    }

    /**
     * Выбирает правило с наибольшей стоимостью. Правила должны быть только рабочие! Не проводит проверок на активность правил и их родителей.
     * @param array $ruleItemList
     * @return Entity_Rule_Item
     */
    public static function getRuleItemWithMaximumPrice($ruleItemList)
    {
        $maximumPriceRule = null;
        foreach($ruleItemList as $ruleItem)
        {
            if(!$maximumPriceRule || $maximumPriceRule->getAmount() < $ruleItem->getAmount())
                $maximumPriceRule = $ruleItem;
        }
        return $maximumPriceRule;
    }

    /**
     * Получает список item-ов правил из списка правил (rule_main).
     * @param Aggregate_Rule_Main $rules
     * @return array()
     */
    protected function getRuleItemListFromRuleMainList(Aggregate_Rule_Main $rules)
    {
        $ruleItemList = array();
        foreach($rules as $rule)
        {
            $thisRuleItems = $rule->getItems(true);
            foreach($thisRuleItems as $ruleItem)
            {
                $ruleItemList = $this->tryReplaceRuleItem($ruleItemList,$ruleItem);
            }
        }
        return $ruleItemList;
    }

    /**
     * Предзагрузка объекта активных действий по каждому типу действий
     * @param Entity_Shop $shop
     */
    protected function preloadShopValues(Entity_Shop $shop)
    {
        $actionTypes = array(
                                //Entity_Rule_Item::TYPE_CHECKIN,
                                Entity_Rule_Item::TYPE_BUY_BARCODE,
                                //Entity_Rule_Item::TYPE_SCAN
                            );

        foreach($actionTypes as $actionType)
        {
            $itemValue = new Value_Rule_Item_ShopAndType($shop,$actionType);
            $itemValue->set(null);
            $itemValue->get();
        }
    }
}