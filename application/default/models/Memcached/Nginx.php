<?php
class Memcached_Nginx
{
    /**
     * @var Memcached_Nginx
     */
    protected static $instance;

    /**
     * Singleton
     * 
     * @return Memcached_Nginx
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function set(Memcached_Object_Abstract $object)
    {
        return SFM_Cache_Memory::getInstance()->setRaw($object->generateKey(), $object->serialize(),$object->getExpiration());
    }
    
    public function get($key)
    {
        return (SFM_Cache_Memory::getInstance()->getRaw($key) !== null);
    }
    
    public function delete(Memcached_Object_Abstract $object)
    {
        return SFM_Cache_Memory::getInstance()->deleteRaw($object->generateKey());
    }
}