<?php
abstract class Memcached_Object_Abstract
{
    const CACHE_PREFIX = 'Memcached_Object';
    
    abstract public function generateKey();
    abstract public function serialize();
    
    /**
     * @var Entity_User
     */
    protected $_currentUser = null;
    
    public function setCurrentUser(Entity_User $user)
    {
        $this->_currentUser = $user;
    }
    
    public function set()
    {
        Memcached_Nginx::getInstance()->set($this);
    }
    
    public function delete()
    {
        Memcached_Nginx::getInstance()->delete($this);
    }
    
    public function exists()
    {
        return Memcached_Nginx::getInstance()->get($this->generateKey());
    }
    
    public function getExpiration()
    {
        return 0;
    }
}