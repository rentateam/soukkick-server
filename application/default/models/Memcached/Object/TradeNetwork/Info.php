<?php
class Memcached_Object_TradeNetwork_Info extends Memcached_Object_Abstract
{
    /**
     * @var Entity_Trade_Network
     */
    protected $_tradeNetwork = null;
    
    protected $_url = 'usershop-info';
    
    public function __construct(Entity_User $currentUser, Entity_Trade_Network $tradeNetwork)
    {
        $this->setCurrentUser($currentUser);
        $this->_tradeNetwork = $tradeNetwork;
    }
    
    public function generateKey()
    {
        return self::CACHE_PREFIX."_TradeNetwork_".$this->_url."User".SFM_Cache_Memory::KEY_DILIMITER.$this->_currentUser->getId()."TradeNetwork".SFM_Cache_Memory::KEY_DILIMITER.$this->_tradeNetwork->getId();
    }
    
    public function serialize()
    {
        return '';
    }
}