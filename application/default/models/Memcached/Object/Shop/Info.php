<?php
class Memcached_Object_Shop_Info extends Memcached_Object_Shop_Abstract
{
    /**
     * @var Entity_Shop
     */
    protected $_shop = null;
    
    protected $_url = 'shop-info';
    
    public function __construct(Entity_User $currentUser, Entity_Shop $shop)
    {
        $this->setCurrentUser($currentUser);
        $this->_shop = $shop;
    }
    
    public function generateKey()
    {
        return self::CACHE_PREFIX."_Shop_".$this->_url."User".SFM_Cache_Memory::KEY_DILIMITER.$this->_currentUser->getId()."Shop".SFM_Cache_Memory::KEY_DILIMITER.$this->_shop->getId();
    }
    
    public function serialize()
    {
        return '';
    }
}