<?php
class Memcached_Object_PrizeList extends Memcached_Object_Abstract
{
    public function __construct(Entity_User $currentUser)
    {
        $this->setCurrentUser($currentUser);
    }
    
    public function generateKey()
    {
        return self::CACHE_PREFIX."_PrizeList".SFM_Cache_Memory::KEY_DILIMITER."User".SFM_Cache_Memory::KEY_DILIMITER.$this->_currentUser->getId();
    }
    
    public function serialize()
    {
        return '';
    }
}