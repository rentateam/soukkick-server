<?php
class Action_Single_Achievement_Contact extends Action_Single_Achievement_Abstract
{
    protected $_achievementId = Entity_Achievement::ID_CONTACT;

    public function makeAction()
    {
        $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType, $this->_achievement);
        return array('amount' => $transaction->getAmount(),'trm_main' => $transaction);
    }
}