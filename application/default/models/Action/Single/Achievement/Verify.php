<?php
class Action_Single_Achievement_Verify extends Action_Single_Achievement_Abstract
{
    /**
     * @var Entity_User_Verify
     */
    protected $_verify;

    /**
     * @var string
     */
    protected $_code;

    protected $_achievementId = Entity_Achievement::ID_VERIFY;

    public function setCode($code)
    {
        $this->_code = $code;
        return $this;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => 0,'trm_main' => null);
        $verifyCode = Mapper_User_Verify::getInstance()->getEntityByUser($this->_user);
        if(!$verifyCode)
        {
            return $errorReturn;
        }
        if($verifyCode->getCode() == $this->_code)
        {
            $verifyCode->setVerified(true);
            $tr = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType, $this->_achievement);
            return array('amount' => $this->_achievement->getAmount(),'trm_main' => $tr);
        }
        else
        {
            $verifyCode->setVerified(false);
            return $errorReturn;
        }
    }

    protected function getParam1()
    {
        return $this->_code;
    }
}