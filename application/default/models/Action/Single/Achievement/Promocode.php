<?php
class Action_Single_Achievement_Promocode extends Action_Single_Achievement_Abstract
{
    /**
     * @var string
     */
    protected $_promoCode;

    protected $_achievementId = Entity_Achievement::ID_PROMOCODE;

    public function setPromocode($promoCode)
    {
        $this->_promoCode = $promoCode;
        return $this;
    }

    public function makeAction()
    {
        $fromUser = Mapper_User::getInstance()->getEntityByPromoCode($this->_promoCode);
        if(!$fromUser)
        {
            throw new Action_Exception_Promocode('No user was found by promo code ' . $this->_promoCode . '!');
        }
        else if(Mapper_User::getInstance()->isUsedAnyPromoCode($this->_user))
        {
            throw new Action_Exception_Promocode('Promo code was used! - ' . $fromUser->getPromoCode());
        }
        else if($fromUser->getId() == $this->_user->getId())
        {
            throw new Action_Exception_Promocode('User '.$this->_user->getId().' tried to use own promo code!');
        }
        else
        {
            Mapper_User::getInstance()->usePromoCode($fromUser, $this->_user);
            $tr1 = Rule_Trm_Factory::create($fromUser, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType, $this->_achievement);
            $tr2 = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType, $this->_achievement);
            return array('amount' => $this->_achievement->getAmount(),'trm_main' => $tr1);
        }
    }
}