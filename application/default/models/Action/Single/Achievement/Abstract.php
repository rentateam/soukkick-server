<?php
abstract class Action_Single_Achievement_Abstract extends Action_Single_Abstract
{
    /**
     * @var Entity_Achievement
     */
    protected $_achievement;

    protected $_actionType = Entity_Rule_Item::TYPE_ACHIEVEMENT;
    protected $_achievementId;

    public function __construct(Entity_User $user)
    {
        parent::__construct($user);

        if($this->_achievementId)
        {
            $this->setAchievement(Mapper_Achievement::getInstance()->getEntityById($this->_achievementId));
        }
    }

    public function setAchievement(Entity_Achievement $achievement)
    {
        $this->_achievement = $achievement;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_achievementId;
    }

    protected function getParam1()
    {
        return null;
    }

    protected function getParam2()
    {
        return null;
    }
}