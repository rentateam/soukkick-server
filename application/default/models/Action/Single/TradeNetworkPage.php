<?php
class Action_Single_TradeNetworkPage extends Action_Single_Abstract
{
    /**
     * @var Entity_Trade_Network
     */
    protected $_tradeNetwork;

    protected $_actionType = Entity_Rule_Item::TYPE_SHOPPAGE;

    public function setTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        $this->_tradeNetwork = $tradeNetwork;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_tradeNetwork->getId();
    }

    protected function getParam1()
    {
        return null;
    }

    protected function getParam2()
    {
        return null;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null);
        $userAggregate = Mapper_User::getInstance()->createAggregate(array($this->_user->getId()));
        $userAggregate->loadEntities();

        if($this->_tradeNetwork->isHidden())
        {
            $this->setError(Action_Single_Abstract::ERROR_WRONG_PARAMETER);
            return $errorReturn;
        }

        if($this->hasTradeNetworkPageForDate())
        {
            $this->_tradeNetwork->deleteInfoMobileCache($userAggregate);
            $this->setError(self::ERROR_DUPLICATE);
            return $errorReturn;
        }


        if($this->_tradeNetwork->getActiveRuleItemId() === null)
        {
            $this->setError(self::WARNING_NO_RULE_ITEM);
        }

        $activeRuleItem = $this->getActiveRuleItem($this->_tradeNetwork);
        if($activeRuleItem !== null)
        {
            $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType,$this->_tradeNetwork,null,$activeRuleItem);
            $entity = Entity_Action_Shoppage::add($this->_user, $this->_tradeNetwork, $transaction, $this->_actionDate, $this->_actionTime);

            $this->_tradeNetwork->deleteInfoMobileCache($userAggregate);
            return array('amount' => $entity->getPrice(),'trm_main' => $transaction);
        }
        else
        {
            $this->setError(self::ERROR_WRONG_PARAMETER);
            return array('amount' => -1,'trm_main' => null);
        }
    }

    /**
     * Входил ли пользователь уже на страницу этого магазина сегодня
     * @return boolean
     */
    public function hasTradeNetworkPageForDate()
    {
        //@TODO. Можно бы тут использовать memcachedb
        $list = Mapper_Action_Shoppage::getInstance()->getListByObjectAndUserForDate($this->_tradeNetwork, $this->_user, $this->_actionDate);
        return ($list->totalCount() > 0 ? true : false);
    }

}