<?php
class Action_Single_Promocode extends Action_Single_Abstract
{
    /**
     * @var string
     */
    protected $_code;

    /**
     * @var Entity_Promocode|null
     */
    protected $_promocode;

    protected $_actionType = Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE;

    public function setCode($code)
    {
        $this->_code = $code;
        $this->_promocode = Mapper_Promocode::getInstance()->getEntityByCode($code);
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_promocode ? $this->_promocode->getId() : null;
    }

    protected function getParam1()
    {
        return $this->_code;
    }

    protected function getParam2()
    {
        return null;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null);
        if($this->_promocode === null)
        {
            $this->setError(self::ERROR_NO_SHOPFEE_PROMOCODE);
            return $errorReturn;
        }

        if(!$this->_promocode->getActiveRuleItemId())
        {
            $this->setError(self::WARNING_NO_RULE_ITEM);
            return $errorReturn;
        }

        //данный промокод уже введен
        if($this->hasPromocode())
        {
            $this->setError(self::ERROR_DUPLICATE);
            return $errorReturn;
        }

        //пользователь - аноним. Вводить ничего нельзя
        if($this->_user->getUdid())
        {
            $this->setError(self::ERROR_SHOPFEE_PROMOCODE_ANONYMOUS);
            return $errorReturn;
        }

        $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType,$this->_promocode);
        $entity = Entity_Action_Promocode::add($this->_user, $this->_promocode, $transaction, $this->_actionDate, $this->_actionTime);
        return array('amount' => $transaction->getAmount(),'trm_main' => $transaction);
    }

    /**
     * Использовал ли пользователь промокод
     * @return boolean
     */
    public function hasPromocode()
    {
        $entity = Mapper_Action_Promocode::getInstance()->getEntityByUserAndPromocode($this->_user, $this->_promocode);
        return $entity !== null;
    }

}