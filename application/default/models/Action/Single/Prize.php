<?php
class Action_Single_Prize extends Action_Single_Abstract
{
    /**
     * @var Entity_Prize
     */
    protected $_prize;

    protected $_actionType = Entity_Rule_Item::TYPE_PRIZE;

    public function setPrize(Entity_Prize $prize)
    {
        $this->_prize = $prize;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_prize->getId();
    }

    protected function getParam1()
    {
        return null;
    }

    protected function getParam2()
    {
        return null;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null);
        $prizePrice = $this->_prize->getPrice();
        if($this->_user->getAmount() < $prizePrice)
        {
            $this->setError(self::ERROR_AMOUNT_INSUFFICIENT);
            return $errorReturn;
        }

        $activeRuleItem = $this->_prize->getActiveRuleItem();
        if(!$activeRuleItem || !$activeRuleItem->isActive())
        {
            $this->setError(self::WARNING_NO_RULE_ITEM);
            return $errorReturn;
        }

        $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType, $this->_prize);
        $return = array();
        switch($this->_prize->getType())
        {
            case Entity_Prize::TYPE_OWN:
                Entity_Prize_Order::add($this->_user,$this->_prize);
                $entity = Entity_Action_Prize::add($this->_user, $this->_prize, $transaction, $this->_actionDate, $this->_actionTime);
                break;
            case Entity_Prize::TYPE_PROMO:
                try
                {
                    $prizePromo = $this->_user->usePrizePromoCode($this->_prize);
                    $entity = Entity_Action_Prize_Promo::add($this->_user, $prizePromo, $transaction, $this->_actionDate, $this->_actionTime);
                    $return['code'] = $prizePromo->getCode();
                }
                catch(Prize_Promo_Exception $e)
                {
                    $this->setError(self::ERROR_NO_PRIZE_PROMOCODE);
                    return $errorReturn;
                }
                break;
        }

        $this->_user->increaseActionNumber('number_prize');
        $return['amount'] = -$entity->getPrice();
        $return['trm_main'] = $transaction;
        return $return;
    }
}