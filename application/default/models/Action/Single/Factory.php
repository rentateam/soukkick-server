<?php
class Action_Single_Factory
{
    public static function create(Entity_User $user, $actionType)
    {
        switch($actionType)
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                return new Action_Single_Checkin($user);
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                return new Action_Single_TradeNetworkPage($user);
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                return new Action_Single_Scan($user);
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                return new Action_Single_BuyBarcode($user);
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                return new Action_Single_Prize($user);
                break;
            case Entity_Rule_Item::TYPE_FACEBOOK:
                return new Action_Single_Facebook($user);
                break;
            case Entity_Rule_Item::TYPE_CONTACT:
                return new Action_Single_Contact($user);
                break;
            case Entity_Rule_Item::TYPE_PROMOCODE:
                return new Action_Single_Promocode($user);
                break;
            default:
                return new Action_Single_Dummy($user);
        }
    }
}