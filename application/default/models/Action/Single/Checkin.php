<?php
class Action_Single_Checkin extends Action_Single_Abstract
{
    /**
     * @var Entity_Transmitter
     */
    protected $_transmitter;

    /**
     * @var array()
     */
    protected $_inputFrqList = array();

    /**
     * @var boolean
     */
    protected $_useEncoding = false;

    protected $_actionType = Entity_Rule_Item::TYPE_CHECKIN;

    public function setTransmitter(Entity_Transmitter $transmitter)
    {
        $this->_transmitter = $transmitter;
        return $this;
    }

    public function setFrqList(array $inputFrqList)
    {
        //т.к. ключи могут прийти в алфавитном порядке, а не числовом
        ksort($inputFrqList);
        $this->_inputFrqList = $inputFrqList;
        return $this;
    }

    public function setUseEncoding($useEncoding)
    {
        $this->_useEncoding = $useEncoding;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_transmitter->getId();
    }

    protected function getParam1()
    {
        return implode(',',$this->_inputFrqList);
    }

    protected function getParam2()
    {
        return $this->_useEncoding;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null, 'wasCheckin' => false);
        if(!$this->_isOldApi)
        {
            //Проверка по закодированной последовательности
            if(!empty($this->_inputFrqList))
            {
                $transmitterFrqList = $this->_transmitter->getMyEncodedFrequencyList($this->_useEncoding);
                $i = 0;
                foreach($this->_inputFrqList as $inputFreq)
                {
                    $needFreq = $transmitterFrqList[$i];
                    if($inputFreq != $needFreq)
                    {
                        $this->setError(self::WARNING_FRQ_LIST_WRONG);
                        break;
                    }
                    $i++;
                }
            }
            else
            {
                $this->setError(self::WARNING_FRQ_LIST_WRONG);
            }
        }

        $shop = $this->_transmitter->getShop();
        $transmitterTypeId = $this->_transmitter->getTransmitterTypeId();

        //Логика входов/выходов.
        $this->enterLogic();


        if($this->hasShopCheckinForDate($shop,$transmitterTypeId))
        {
            $this->setError(self::ERROR_DUPLICATE);
            return $errorReturn;
        }


        $checkinPrice = 0;
        $wasCheckin = false;

        //Если нет активного rule-itemа
        $activeRuleItem = $this->getActiveRuleItem($this->_transmitter);

        //Если есть либо имеющийся rule-item, либо имевшийся на тот момент
        $transaction = null;
        if($activeRuleItem !== null)
        {
            $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType,$this->_transmitter,$shop,$activeRuleItem);
            $checkin = Entity_Action_Checkin::add($this->_user, $this->_transmitter, $transaction, $this->_actionDate, $this->_actionTime);
            $this->_user->increaseActionNumber('number_checkin');
            $checkinPrice = $checkin->getPrice();
            $wasCheckin = true;
            return array('amount' => $checkinPrice, 'wasCheckin' => $wasCheckin,'trm_main' => $transaction);
        }
        else
        {
            $this->setError(self::ERROR_WRONG_PARAMETER);
            return $errorReturn;
        }

    }

    /**
     * Можно ли пользователю входить в магазин на данный тип передатчика
     * @param Entity_Shop $shop
     * @param integer $type Тип передатчика
     * @return boolean
     */
    public function canShopEnter(Entity_Shop $shop,$type)
    {
        $lastEnter = Mapper_Action_Enter::getInstance()->getLastEnter($this->_user,$shop,$type);
        return !$lastEnter || $lastEnter->getCorrespondingExit() !== null;
    }

    /**
     * Входил ли пользователь уже в этот магазин сегодня (на конкретный тип передатчика или вообще)
     * @param Entity_Shop $shop
     * @param integer|null $type Тип передатчика
     * @return boolean
     */
    public function hasShopCheckinForDate(Entity_Shop $shop,$type = null)
    {
        //@TODO. Можно бы тут использовать memcachedb
        $list = Mapper_Action_Checkin::getInstance()->getListByObjectAndUserAndTypeForDate($shop, $this->_user, $type, $this->_actionDate);
        return ($list->totalCount() > 0 ? true : false);
    }

    /**
     * Логика входов/выходов. Пока здесь, не выпилена.
     */
    protected function enterLogic()
    {
        $shop = $this->_transmitter->getShop();
        $transmitterTypeId = $this->_transmitter->getTransmitterTypeId();

        if(!$this->canShopEnter($shop,$transmitterTypeId))
        {
        	//Очистка попыток входа в магазин по типу передатчика
        	Mapper_Action_Exit_Attempt::getInstance()->clearOldByUserAndShop($this->_user,$shop,$transmitterTypeId);
        }
        //Если есть висячий вход в магазин,
        $anyLastEnter = Mapper_Action_Enter::getInstance()->getAnyLastEnter($this->_user);
        if($anyLastEnter && !$anyLastEnter->getCorrespondingExit())
        {
        	$exit = Entity_Action_Exit::add($this->_user,$anyLastEnter->getShop(),$anyLastEnter->getTransmitterTypeId(), $this->_actionDate, $this->_actionTime);
        }
        Entity_Action_Enter::add($this->_user,$this->_transmitter);
    }
}