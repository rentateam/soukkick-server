<?php
class Action_Single_BuyBarcode extends Action_Single_Abstract
{
    /**
     * @var string
     */
    protected $_barcode;

    /**
     * @var Entity_Shop
     */
    protected $_shop;

    protected $_actionType = Entity_Rule_Item::TYPE_BUY_BARCODE;

    public function setBarcode($barcode)
    {
        $this->_barcode = $barcode;
        return $this;
    }

    public function setShop(Entity_Shop $shop)
    {
        $this->_shop = $shop;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_shop ? $this->_shop->getId() : null;
    }

    protected function getParam1()
    {
        return $this->_barcode;
    }

    protected function getParam2()
    {
        return null;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null);
        $buyBarcode = Mapper_BuyBarcode::getInstance()->getEntityByBarcode($this->_barcode);
        if($buyBarcode === null)
        {
            $this->setError(self::ERROR_NO_BUYBARCODE);
            return $errorReturn;
            //throw new Action_Exception_BuyBarcode_NoEntity();
        }

        $shop = $buyBarcode->getShop();
        //Уже имеется скан за сегодня
        if($this->hasBuyBarcodeForDate($shop->getTradeNetwork()))
        {
            $this->setError(self::ERROR_DUPLICATE);
            return $errorReturn;
        }

        //Если магаз скрыт - возвращаем "Неверный штрихкод"
        if($shop->isHidden())
        {
            $this->setError(self::ERROR_NO_BUYBARCODE);
            return $errorReturn;
        }

        $activeRuleItem = $buyBarcode->getActiveRuleItem();
        if($activeRuleItem === null)
        {
            $this->setError(self::WARNING_NO_RULE_ITEM);
            return $errorReturn;
        }

        //Берем последний вход в магазин
        $lastEnter = Mapper_Action_Enter::getInstance()->getLastEnter($this->_user,$shop,Entity_Transmitter_Type::ID_DEFAULT);
        //Если его нет или он сделан не сегодня - посылаем его
        if(!$lastEnter || strtotime($lastEnter->getDateCreated()) < strtotime(date('Y-m-d')))
        {
            $this->setError(self::INFO_NO_SHOP_ENTER);
        }

        if($this->_shop !== null)
        {
            //Штрихкод не от того магазина
            if($buyBarcode->getShopId() != $this->_shop->getId())
            {
                $this->setError(self::WARNING_WRONG_SHOP);
            }
        }


        $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType,$buyBarcode,$shop);
        $entity = Entity_Action_BuyBarcode::add($this->_user, $buyBarcode, $transaction, $this->_actionDate, $this->_actionTime);
        $this->_user->increaseActionNumber('number_buy_barcode');
        $buyBarcode->passivate();
        return array('amount' => $entity->getPrice(),'trm_main' => $transaction);
    }

    /**
     * Сканировал ли пользователь уже штрих-код за покупку в этой сети сегодня
     * @param Entity_Trade_Network $tradeNetwork
     * @return boolean
     */
    public function hasBuyBarcodeForDate(Entity_Trade_Network $tradeNetwork)
    {
        $list = Mapper_Action_BuyBarcode::getInstance()->getListByTradeNetworkAndUserForDate($tradeNetwork, $this->_user, $this->_actionDate);
        return ($list->totalCount() > 0 ? true : false);
    }

}