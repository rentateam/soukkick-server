<?php
class Action_Single_Scan extends Action_Single_Abstract
{
    /**
     * @var string
     */
    protected $_barcode;

    /**
     * @var Entity_Shop
     */
    protected $_shop;

    /**
     * @var double
     */
    protected $_lat;

    /**
     * @var double
     */
    protected $_long;

    protected $_actionType = Entity_Rule_Item::TYPE_SCAN;

    public function setBarcode($barcode)
    {
        $this->_barcode = $barcode;
        return $this;
    }

    public function setShop(Entity_Shop $shop)
    {
        $this->_shop = $shop;
        return $this;
    }

    public function setLat($lat)
    {
        $this->_lat = $lat;
        return $this;
    }

    public function setLong($long)
    {
        $this->_long = $long;
        return $this;
    }

    protected function getObjectId()
    {
        return $this->_shop ? $this->_shop->getId() : null;
    }

    protected function getParam1()
    {
        return $this->_barcode;
    }

    protected function getParam2()
    {
        return null;
    }

    public function makeAction()
    {
        $errorReturn = array('amount' => -1,'trm_main' => null);
        $product = Mapper_ScanProduct::getInstance()->getEntityByBarcodeAndTradeNetwork($this->_barcode,$this->_shop->getTradeNetwork());

        if($product === null)
        {
            $this->setError(self::ERROR_NO_SCAN_PRODUCT);
            return $errorReturn;
        }
        if($this->hasScannedBarcodeForDate($product))
        {
            $this->setError(self::ERROR_DUPLICATE);
            return $errorReturn;
        }
        if(Entity_Shop::calculateDistance($this->_shop, $this->_lat, $this->_long) >= Entity_Shop::SCAN_MAXIMUM_DISTACE*1000)
        {
            $this->setError(self::WARNING_NOT_IN_SHOP);
        }

        if($product->getActiveRuleItemId() === null)
        {
            $this->setError(self::WARNING_NO_RULE_ITEM);
        }

        $activeRuleItem = $this->getActiveRuleItem($product);
        if($activeRuleItem !== null)
        {
            $transaction = Rule_Trm_Factory::create($this->_user, $this->_actionDate, $this->_actionTime, $this->_lat, $this->_long, $this->_actionType,$product,$this->_shop,$activeRuleItem);
            $entity = Entity_Action_Scan::add($this->_user, $product,$this->_shop,$transaction, $this->_actionDate, $this->_actionTime);

            $this->_user->increaseActionNumber('number_scan');

            //Очистка кэшей магазина и сети
            $this->_shop->deleteInfoMobileCache();
            $this->_shop->getTradeNetwork()->deleteInfoMobileCache();

            return array('amount' => $entity->getPrice(),'trm_main' => $transaction);
        }
        else
        {
            $this->setError(self::ERROR_WRONG_PARAMETER);
            return array('amount' => -1,'trm_main' => null);
        }
    }

    /**
     * Сканировал ли пользователь уже штрих-код сегодня
     * @param Entity_ScanProduct
     * @return boolean
     */
    public function hasScannedBarcodeForDate(Entity_ScanProduct $scanProduct)
    {
        //@TODO. Можно бы тут использовать memcachedb
        $list = Mapper_Action_Scan::getInstance()->getListByObjectAndUserForDate($scanProduct, $this->_user, $this->_actionDate);
        return ($list->totalCount() > 0 ? true : false);
    }

    /**
     * Логгирование неудачной попытки скана. Пока костыльное
     */
    public function logHackScan()
    {
        $params = array(
                        'user_id' => $this->_user->getId(),
                        'date_created' => date('Y-m-d H:i:s'),
                        'barcode' => $this->_barcode,
                        'shop_id' => $this->_shop->getId(),
                        'lat' => $this->_lat,
                        'longtitude' => $this->_long
        );
        SFM_DB::getInstance()->insert("INSERT INTO user_hack_scan (user_id,date_created,barcode,shop_id,lat,longtitude) VALUES (:user_id,:date_created,:barcode,:shop_id,:lat,:longtitude)",$params);
    }

}