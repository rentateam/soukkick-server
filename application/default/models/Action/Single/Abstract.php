<?php
abstract class Action_Single_Abstract
{
    const INFO_SUCCESS = 200;
    const INFO_NO_SHOP_ENTER = 516;

    const WARNING_NO_RULE_ITEM = 412;
    const WARNING_FRQ_LIST_WRONG = 511;
    const WARNING_NOT_IN_SHOP = 512;
    const WARNING_WRONG_SHOP = 517;

    const ERROR_AMOUNT_INSUFFICIENT = 402;
    const ERROR_DUPLICATE = 403;
    const ERROR_WRONG_ACTION_TYPE = 405;
    const ERROR_WRONG_PARAMETER = 417;
    const ERROR_UNKNOWN = 500;
    const ERROR_NOT_IMPLEMENTED = 505;
    const ERROR_NO_SCAN_PRODUCT = 513;
    const ERROR_NO_BUYBARCODE = 514;
    const ERROR_BUYBARCODE_INACTIVE = 515;
    const ERROR_NO_PRIZE_PROMOCODE = 518;
    const ERROR_NO_USER_PROMOCODE = 519;
    const ERROR_NO_SHOPFEE_PROMOCODE = 520;
    const ERROR_SHOPFEE_PROMOCODE_ANONYMOUS = 521;

    const ERROR_AMOUNT_VALUE = -1;
    const ERROR_AMOUNT_VALUE_OLD = 0;

    protected $_user;
    protected $_actionType;
    protected $_activeRuleItemId;
    protected $_lat;
    protected $_long;
    protected $_actionDate;
    protected $_actionTime;
    protected $_ip;
    /**
     * @var Entity_Action_Log|null
     */
    protected $_logEntity = null;

    protected $_error;
    protected $_warning;

    protected $_userFloat;

    /**
     * Если true - идет запрос со старого АПИ.
     * @var boolean
     */
    protected $_isOldApi = false;

    protected $_errorAmountValue;

    public function __construct(Entity_User $user)
    {
        $this->_user = $user;
        $this->_userFloat = $user->getFloatTime();
        $now = date('Y-m-d H:i:s');
        $this->setActionDate($now,$now);
        $this->setActionTime($now,$now);
        $this->_errorAmountValue = self::ERROR_AMOUNT_VALUE;
    }

    abstract protected function makeAction();
    abstract protected function getObjectId();
    abstract protected function getParam1();
    abstract protected function getParam2();

    public function getActionType()
    {
        return $this->_actionType;
    }

    public function setActiveRuleItemId($activeRuleItemId)
    {
        $this->_activeRuleItemId = $activeRuleItemId;
        return $this;
    }

    public function setLat($lat)
    {
        $this->_lat = $lat;
        return $this;
    }

    public function setLong($long)
    {
        $this->_long = $long;
        return $this;
    }

    public function setActionDate($actionDate, $clientDateTime)
    {
        $this->_actionDate = self::getDateWithClientDatetime($actionDate, $clientDateTime, 'Y-m-d');
        return $this;
    }

    public function setActionTime($actionTime, $clientDateTime)
    {
        $this->_actionTime = self::getDateWithClientDatetime($actionTime, $clientDateTime, 'H:i:s');
        return $this;
    }

    public function setOldApi($isOldApi)
    {
        $this->_isOldApi = $isOldApi;
        $this->_errorAmountValue = $isOldApi ? self::ERROR_AMOUNT_VALUE_OLD : self::ERROR_AMOUNT_VALUE;
        return $this;
    }

    protected static function getDateWithClientDatetime($date, $clientDateTime, $format)
    {
        $now = time();
        $clientTimestamp = strtotime($clientDateTime);
        $dateTimestamp = strtotime($date);
        $difference = $now - $clientTimestamp;
        return date($format,$dateTimestamp + $difference);
    }

    public function getActionDate()
    {
        return $this->_actionDate;
    }

    public function getActionTime()
    {
        return $this->_actionTime;
    }

    public function setIp($ip)
    {
        $this->_ip = $ip;
        return $this;
    }

    public function make()
    {
        $db = SFM_DB::getInstance();

        if($this->_error === null)
        	$this->setError(self::INFO_SUCCESS);

        //Логгируем попытку действия в отдельной транзакции
        $db->beginTransaction();
        $this->log();
        $db->commit();

        $duplicateActionLog = null;

        //Если ошибок нет - пытаемся сделать действие
        if($this->_error === self::INFO_SUCCESS)
        {
            $duplicateActionLog = $this->getDuplicateActionLogEntity();
            if($duplicateActionLog === null)
            {
                //Если проверка поплавка прошла, проводим транзакцию
                if($this->isFloatValid())
                {
                    //Само действие
                    $db->beginTransaction();
                    $actionResult = $this->makeAction();
                    $db->commit();
                }
                else
                {
                    $this->_error = self::ERROR_UNKNOWN;
                }
            }
            else
            {
                $trmMain = $duplicateActionLog->getTrmMain();
                $this->_error = $duplicateActionLog->getResultCode();
                if($trmMain)
                {
                    $actionResult = array('trm_main' => $trmMain, 'amount' => $trmMain->getAmount());
                }
            }
        }

        //Если ошибка критическая
        if(Entity_Action_Log::getResultTypeByCode($this->_error) == Entity_Action_Log::RESULT_TYPE_ERROR)
        {
            $actionResult = array('trm_main' => null, 'amount' => $this->_errorAmountValue);
        }

        //Результат действия
        $actionResult['error'] = $this->_error;
        $db->beginTransaction();
        if(!$duplicateActionLog)
        {
            $this->updateLog($actionResult['error'],isset($actionResult['trm_main']) ? $actionResult['trm_main'] : null);
        }
        else
        {
            $this->deleteLog();
        }
        $db->commit();

        return $actionResult;
    }

    /**
     * Вернуть правило, только если оно есть, протухло и не удалено.
     * @return Entity_Rule_Item|null
     */
    protected function getExpiredLimitEntity()
    {
        //if($this->_activeRuleItemId !== null)
        if(intval($this->_activeRuleItemId) > 0)
        {
            $ruleItem = Mapper_Rule_Item::getInstance()->getEntityById($this->_activeRuleItemId);
            if($ruleItem !== null && !$ruleItem->isActive() && $ruleItem->getActionType() == $this->_actionType)
            {
                return $ruleItem;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     *  Вернуть rule item, который должен использоваться в действии. В порядке приоритета
     *  1. Указанный явно.
     *  2. Если его нет, взятый из объекта действия.
     *  3. Если его нет, null
     *  @param Interface_ActionItem $actionObject объект действия
     *  @return Entity_Rule_Item|null
     **/
    protected function getActiveRuleItem(Interface_ActionItem $actionObject)
    {
        //Если он указан явно
        if(intval($this->_activeRuleItemId) > 0)
        {
            $ruleItem = Mapper_Rule_Item::getInstance()->getEntityById($this->_activeRuleItemId);
            //Если существует и имеет тот же тип
            $actionObjectId = $ruleItem->getActionType() == Entity_Rule_Item::TYPE_CHECKIN ? null : $actionObject->getId();
            if($ruleItem !== null && $ruleItem->getActionType() == $this->_actionType && $ruleItem->getActionObjectId() == $actionObjectId)
            {
                return $ruleItem;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return $actionObject->getActiveRuleItem();
        }
    }

    public function setError($error)
    {
        $this->_error = $error;
        return $this;
    }

    public function getError()
    {
        return $this->_error;
    }

    protected function log()
    {
        $activeRuleItem = $this->_activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($this->_activeRuleItemId) : null;
        $this->_logEntity = Entity_Action_Log::add($this->_user, $this->_actionType, $this->_lat, $this->_long, $this->_actionDate, $this->_actionTime, $this->_ip,$activeRuleItem, $this->getObjectId(), $this->getParam1(), $this->getParam2());
    }

    protected function updateLog($resultCode, Entity_Trm_Main $transaction = null)
    {
        if($this->_logEntity)
        {
            $this->_logEntity->updateResultCode($resultCode, $transaction);
        }
    }

    protected function deleteLog()
    {
    	if($this->_logEntity)
    	{
    		$this->_logEntity->delete();
    	}
    }

    /**
     * Получение уже проведенной попытки транзакции по уникальным для этого полям.
     * @return Entity_Action_Log|null
     */
    protected function getDuplicateActionLogEntity()
    {
        $actionLog = Mapper_Action_Log::getInstance()->getEntityByTrmKey($this->_user, $this->_actionDate, $this->_actionTime, $this->_actionType);
        if($actionLog && $actionLog->getId() == $this->_logEntity->getId())
        {
            $actionLog = null;
        }
        return $actionLog;
    }

    protected function isFloatValid()
    {
        //Если дата/время события меньше поплавка - reject
        if(strtotime($this->getActionDate().' '.$this->getActionTime()) < strtotime($this->_userFloat)){
            return false;
        }

        return true;
    }
}