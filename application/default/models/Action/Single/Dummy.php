<?php
class Action_Single_Dummy extends Action_Single_Abstract
{
    protected $_actionType = null;

    public function makeAction()
    {
        throw new Action_Exception_Dummy('I am a dummy!');
    }

    protected function getObjectId()
    {
        return null;
    }

    protected function getParam1()
    {
        return null;
    }

    protected function getParam2()
    {
        return null;
    }
}