<?php
class Action_Bulk
{
    const DATE_API_INTRODUCE = '2012-12-01';

    /**
     * @var Entity_User
     */
    protected $_user;

    /**
     * @var array
     */
    protected $_actionJson;

    protected $_ip;
    protected $_clientDateTime;

    public function __construct(Entity_User $user, $actionJson)
    {
        $this->_user = $user;
        $this->_actionJson = $actionJson;
        $this->_clientDateTime = date('Y-m-d H:i:s');
    }

    public function setIp($ip)
    {
        $this->_ip = $ip;
        return $this;
    }

    public function setClientDateTime($clientDateTime)
    {
        $this->_clientDateTime = $clientDateTime;
        return $this;
    }

    public function make()
    {
        $results = array();

        foreach($this->_actionJson as $actionJsonItem)
        {
            $index = $actionJsonItem['anum'];
            $actionMaker = Action_Single_Factory::create($this->_user, $actionJsonItem['type']);
            $actionMaker->setIp($this->_ip);
            try
            {
                $this->setParameters($actionMaker,$actionJsonItem);
            }
            catch(Action_Exception_NoEntity $e)
            {
                $actionMaker->setError(Action_Single_Abstract::ERROR_WRONG_PARAMETER);
            }


            $result = $actionMaker->make();
            $amount = $result['amount'];
            $errorcode = $result['error'];

            $results[] = array(
                                    'anum' => $index,
                                    'amount' => $amount,
                                    'errorcode' => $errorcode
                                );
        }
        $this->_user->setFloatTime(date('Y-m-d H:i:s'));
        return $results;
    }

    protected function setParameters(Action_Single_Abstract $actionMaker, $json)
    {
        $activeRuleItemId = self::getActiveRuleItemId($json);
        $actionMaker->setLat($json['alat'])
                    ->setLong($json['alon'])
                    ->setActionDate($json['adate'].' '.$json['atime'], $this->_clientDateTime)
                    ->setActionTime($json['adate'].' '.$json['atime'], $this->_clientDateTime)
                    ->setActiveRuleItemId($activeRuleItemId);

        //Проверка даты/времени события
        if(!$this->isDateTimeValid(strtotime($actionMaker->getActionDate().' '.$actionMaker->getActionTime())))
        {
            $actionMaker->setError(Action_Single_Abstract::ERROR_UNKNOWN);
        }

        switch($json['type'])
        {
            case Entity_Rule_Item::TYPE_CHECKIN:
                $transmitter = Mapper_Transmitter::getInstance()->getEntityById($json['transmitter_id']);
                $this->checkForNullEntity($transmitter);
                $frqList = isset($json['frq']) ? $json['frq'] : array();

                $actionMaker->setTransmitter($transmitter);
                $actionMaker->setFrqList($frqList);
                $actionMaker->setUseEncoding(isset($json['use_encoding']) && $json['use_encoding'] == 1 ? true : false);
                break;
            case Entity_Rule_Item::TYPE_SHOPPAGE:
                $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($json['user_shop_id']);
                $this->checkForNullEntity($tradeNetwork);
                $actionMaker->setTradeNetwork($tradeNetwork);
                break;
            case Entity_Rule_Item::TYPE_SCAN:
                $shop = Mapper_Shop::getInstance()->getEntityById($json['shop_id']);
                $this->checkForNullEntity($shop);
                $actionMaker->setBarcode($json['barcode'])
                            ->setShop($shop);
                break;
            case Entity_Rule_Item::TYPE_BUY_BARCODE:
                $shopId = isset($json['shop_id']) ? $json['shop_id'] : null;
                if($shopId)
                {
                    $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
                    $this->checkForNullEntity($shop);
                }
                else
                {
                    $shop = null;
                }
                $actionMaker->setBarcode($json['barcode'])
                            ->setShop($shop);
                break;
            case Entity_Rule_Item::TYPE_PRIZE:
                $prize = Mapper_Prize::getInstance()->getEntityById($json['prize_id']);
                $this->checkForNullEntity($prize);
                $actionMaker->setPrize($prize);
                break;
            default:
                throw new Action_Exception_NoEntity();
        }
    }

    protected function isDateTimeValid($actionTimestamp)
    {
        //Если дата/время некорректна синтаксически
        if($actionTimestamp === 0){
            return false;
        }

        //Если дата события меньше даты введения API - reject
        if(strtotime(self::DATE_API_INTRODUCE) > $actionTimestamp){
            return false;
        }

        //Если ДАТА/время события больше NOW - reject
        if($actionTimestamp > time()){
            return false;
        }

        return true;
    }

    protected static function getActiveRuleItemId($actionItemJson)
    {
        return isset($actionItemJson['active_rule_item_id']) ? $actionItemJson['active_rule_item_id'] : null;
    }

    protected static function checkForNullEntity(SFM_Entity $entity = null)
    {
        if($entity === null)
        {
            throw new Action_Exception_NoEntity();
        }
    }
}