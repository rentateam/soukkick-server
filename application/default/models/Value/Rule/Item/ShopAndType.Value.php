<?php
class Value_Rule_Item_ShopAndType extends SFM_Value
{
    /**
     * @var Entity_Shop
     */
    protected $_shop;
    
    /**
     * @var integer
     */
    protected $_actionType;
    
    public function __construct(Entity_Shop $shop, $actionType)
    {
        $this->_shop = $shop;
        $this->_actionType = $actionType;
    }
    
    protected function getCacheKey()
    {
        return $this->getCacheKeyBy(array($this->_shop),$this->_actionType);
    }

    protected function load()
    {
        $activeRuleItemIds = array();
        $activeRuleItemList = Mapper_Rule_Item::getInstance()->getListByShopAndActionType($this->_shop, $this->_actionType);
        $grouppedRuleItemList = array();
        
        foreach($activeRuleItemList as $activeRuleItem)
        {
            $actionObjectId = $activeRuleItem->getActionObjectId() === null ? 0 : $activeRuleItem->getActionObjectId();
            if(!isset($grouppedRuleItemList[$actionObjectId]))
                $grouppedRuleItemList[$actionObjectId] = array();
            
            $grouppedRuleItemList[$actionObjectId][] = $activeRuleItem;
        }
        
        foreach($grouppedRuleItemList as $actionObjectId => $ruleItemList)
        {
            $ruleWithMaximumPrice = Rule_AmountUpdater::getRuleItemWithMaximumPrice($ruleItemList);
            $actionObjectId = $activeRuleItem->getActionObjectId() === null ? 0 : $activeRuleItem->getActionObjectId();
            $activeRuleItemIds[$actionObjectId] = $ruleWithMaximumPrice->getId();
        }
        return $activeRuleItemIds;
    }
    
    /**
     * Получить текущий Rule item по объекту.
     * @param Interface_ActionItem $actionObject
     * @return Entity_Rule_Item|null
     */
    public function getActiveRuleItem(Interface_ActionItem $actionObject = null)
    {
        $this->get();
        $actionObjectId = $actionObject === null ? 0 : $actionObject->getId();
        if(!isset($this->value[$actionObjectId]))
        {
            return null;
        }
        else
        {
            return Mapper_Rule_Item::getInstance()->getEntityById($this->value[$actionObjectId]);
        }
    }
    
    public function setActiveRuleItemForActionObject(Entity_Rule_Item $ruleItem = null, Interface_ActionItem $actionObject = null)
    {
        $this->get();
        $actionObjectId = $actionObject === null ? 0 : $actionObject->getId();
        $this->value[$actionObjectId] = $ruleItem ? $ruleItem->getId() : null;
        $this->set($this->value);
    }
}