<?php
class Aggregate_Shop extends SFM_Aggregate 
{
    /**
     * Метод перенесен на sql, потому больше не нужен. Потребуется, если надо будет увеличить число магазинов в одной сети до нескольких.
     * @param integer $allowTradeNetworks
     * @return Aggregate_Shop
     */
    public function removeDuplicateTradeNetwork($allowTradeNetworks)
    {
        $existingTradeNetworks = array();
        $shopIds = array();
        foreach($this->getContent() as $shop)
        {
            $tradeNetworkId = $shop->getTradeNetworkId();
            if(!isset($existingTradeNetworks[$tradeNetworkId]))
                $existingTradeNetworks[$tradeNetworkId] = array();
                
            if(count($existingTradeNetworks[$tradeNetworkId]) < $allowTradeNetworks)
            {
                $existingTradeNetworks[$tradeNetworkId][] = $shop->getId();
                $shopIds[] = $shop->getId();
            }
        }
        
        return new Aggregate_Shop($shopIds,$this->mapper,null,true);
    }
}