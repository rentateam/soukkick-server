<?php
interface Interface_ActionItem
{
    public function getActiveRuleItem();
    public function setActiveRuleItem(Entity_Rule_Item $ruleItem = null);
}