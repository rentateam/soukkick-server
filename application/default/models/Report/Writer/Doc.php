<?php
class Report_Writer_Doc extends Report_Writer_Abstract
{

    public function output()
    {
        header("Content-Type: application/msword");
        header("Content-Disposition: attachment; filename=\"{$this->report->getName()}.doc\"");
        header("Cache-Control: max-age=0");

        $this->convertTo('doc');

        return file_get_contents($this->getFilePath() . '.doc');
    }

}