<?php
/**
 * Create ODT
 */
class Report_Writer_Odt extends Report_Writer_Abstract
{

    public function output()
    {
        header("Content-Type: application/odt");
        header("Content-Disposition: attachment; filename=\"".urlencode($this->report->getName()).".odt\"");
        header("Cache-Control: max-age=0");

        $this->convertTo('odt');

        return file_get_contents($this->getFilePath() . '.odt');
    }

}
