<?php
/**
 * Create PDF
 */
class Report_Writer_Pdf extends Report_Writer_Abstract
{

    public function output()
    {
        header("Content-Type: application/pdf");
        header("Content-Disposition: attachment; filename=\"{$this->report->getName()}.pdf\"");
        header("Cache-Control: max-age=0");

        $this->convertTo('pdf');
        
        return file_get_contents($this->getFilePath('pdf'));
    }

}
