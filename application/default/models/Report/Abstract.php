<?php
abstract class Report_Abstract implements Report_Interface
{

    /** @var string */
    protected $identity;

    /** @var string */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->init();
        $this->identity = uniqid("", true);
    }

    protected function init()
    {

    }

    /**
     * @return string
     */
    public function getIdentity()
    {
        return $this->identity;
    }
}