<?php
class Report_Prize_Order extends Report_WordAbstract implements Report_Interface
{
    /** @var Entity_Prize_Order */
    protected $_order;
    
    public function setPrizeOrder(Entity_Prize_Order $order)
    {
        $this->_order = $order;
        return $this;
    }

    public function build()
    {
        $this->name = '№'.$this->_order->getId();
        $user = $this->_order->getUser();
        $address = new Decorator_User_Address($user->getAddress());
        
        $parameters = array(
            'USER_NAME'             => $this->_order->getUsername(),
            'USER_SURNAME'              => '',
            'PHONE'               => $this->_order->getPhone(),
            'ADDRESS'               => $this->_order->getAddress(),
            'COUNTRY'               => $this->_order->getCountry(),
            'CITY'                   => $this->_order->getCity(),
            'POST_INDEX'              => $this->_order->getPostIndex(),
        );

        foreach ($parameters as $placeholder => $parameter) {
            $this->template->setValue($placeholder, $parameter);
        }
    }
}