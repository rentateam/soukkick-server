<?php
interface Report_Interface
{
    public function getName();

    public function getIdentity();

    public function build();

    public function save($path);
}