<?php
class Report_Query
{
    protected $array = array();
    protected $head = array();
    protected $excel;

    public function __construct(array $array)
    {
        $this->array = $array;
        $this->head = count($array) > 0 ? array_keys(array_pop(array_merge($array))) : array();
        $this->excel = $this->makeReport();
    }

    protected function makeReport()
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        foreach ($this->head as $columnIndex => $header) {
            $cell = $sheet->getCellByColumnAndRow($columnIndex, 1);
            $dimension = $sheet->getColumnDimensionByColumn($columnIndex);
            $dimension->setAutoSize(true);
            $cell->setValue($this->head[$columnIndex]);
        }

        // iterate rows
        for ($arrayIndex = 0; $arrayIndex < count($this->array); $arrayIndex++) {

            // get row index
            $rowIndex = 2 + $arrayIndex;

            // get row
            $row = $this->array[$arrayIndex];

            // iterate columns
            $columnIndex = 0;
            foreach ($row as $value) {
                $value = is_null($value) ? 'NULL' : $value;
                $sheet->getCellByColumnAndRow($columnIndex, $rowIndex)->setValue($value);
                $columnIndex++;
            }
        }

        return $excel;
    }

    public function send($title)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $title .  '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
        die;
    }
}