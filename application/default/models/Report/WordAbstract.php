<?php
abstract class Report_WordAbstract extends Report_Abstract implements Report_Interface
{

    /** @var PHPWord_Template */
    protected $template;

    /**
     * @return string
     */
    protected function getReportTemplate()
    {
        $file = str_replace('Report_', '', get_class($this)) . '.odt';
        $path = Application::getReportsTemplatePath() . $file;
        return $path;
    }

    /**
     * @param $path string
     */
    public function save($path)
    {
        $this->template->save($path);
    }

    protected function init()
    {
        $path = $this->getReportTemplate();
        if (false === file_exists($path)) {
            throw new Exception("Please, create report template");
        }

        $to = Application::getReportsTemporallyPath() . time() . '.odt';
        $this->template = new PHPWord_Template($path, $to);
    }

    public function build()
    {

    }

}
