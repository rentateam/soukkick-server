<?php
class Report_BuyBarcode_Generator
{
    /** @var Aggregate_Shop */
    protected $_shopList;
    protected $_headList = array('ID confirmation','Barcode','Partner name','Address');
    protected $_barcodesPerShop = 30;
    protected $excel;
    
    public function setShopList($shopList)
    {
        $this->_shopList = $shopList;
        return $this;
    }

    public function build()
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        foreach ($this->_headList as $columnIndex => $header) 
        {
            $cell = $sheet->getCellByColumnAndRow($columnIndex, 1);
            $dimension = $sheet->getColumnDimensionByColumn($columnIndex);
            $dimension->setAutoSize(true);
            $cell->setValue($header);
        }
    
        if(count($this->_shopList))
        {
            $rowIndex = 2;
            $this->_shopList->rewind();
            $bpaMain = $this->_shopList->current()->getTradeNetwork()->getBpaMain();
            foreach($this->_shopList as $shop)
            {
                //Генерируем штрихкоды
                for($i=0;$i<$this->_barcodesPerShop;$i++)
                {
                    $barcode = new Decorator_BuyBarcode(Entity_BuyBarcode::add($shop));
                    $sheet->getCellByColumnAndRow(0, $rowIndex)->setValue($barcode->getId());
                    $sheet->getCellByColumnAndRow(1, $rowIndex)->setValue($barcode->getBarcodeShort());
                    $sheet->getCellByColumnAndRow(2, $rowIndex)->setValue($bpaMain->getName());
                    $sheet->getCellByColumnAndRow(3, $rowIndex)->setValue($shop->getAddress());
                    $rowIndex++;
                }
            }
        }
        
        $this->excel = $excel;
    }
    
    public function send($title)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . str_replace('"','\"',str_replace(' ','_',$title)) .  '.xlsx"',true);
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
        die;
    }
}