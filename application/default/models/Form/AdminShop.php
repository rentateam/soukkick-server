<?php
class Form_AdminShop extends Form_Abstract
{
    protected $_id;
    protected $_currentUser;

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_currentUser = isset($options['tradeNetwork']) ? $options['tradeNetwork'] : null;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/admin/shop/edit/' : '/admin/shop/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Name',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $lat = new Zend_Form_Element_Text('lat', array(
            'required'    => false,
            'label'       => 'Latitude',
            'validators'  => array(
                array('Float', true)
            ),
            'value'       => 0,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($lat);
        
        $long = new Zend_Form_Element_Text('long', array(
            'required'    => true,
            'label'       => 'Longitude',
            'validators'  => array(
                array('Float', true)
            ),
            'value'       => 0,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($long);
        
        $address = new Zend_Form_Element_Textarea('address', array(
            'required'    => false,
            'label'       => 'Address',
            'validators'  => array(
                array('StringLength', true, 0, 300)
            ),
        ));
        $this->addElement($address);
        
        if($this->_id)
        {
            $transmitterInfo = new Zend_Form_Element_Text('transmitter_info', array(
                'required'    => false,
                'label'       => 'Beacon Ids',
                'validators'  => array(
                    array('StringLength', true, 0, 500)
                ),
                'filters'     => array('StringTrim'),
            ));
            $this->addElement($transmitterInfo);
        }
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $this->addCancelButton();

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Add' : 'Save')
        ));
                
        $this->addElement($submit);

        if($this->_id != 0){
            $this->addDeleteButton();
        }

        
        parent::init();
    }
    
    public function setApprovedTradeNetwork(Entity_Trade_Network $user)
    {
        $this->setAction($this->getAction().'trade_network_id/'.$user->getId().'/');
        $shopInfo = Entity_Trade_Network::unserializeShopInfo($user->getShopInfo());
        
        //so far number of shops is not stored anywhere
        $this->getElement('name')->setValue($user->getTitle());
        $this->getElement('address')->setValue($shopInfo['city']);
    }
    
    public function getValuesArray(Entity_Shop $shop)
    {
        return array(
                        'name' => $shop->getName(),
                        'description' => $shop->getDescription(),
                        'address' => $shop->getAddress(),
                        'transmitter_info' => $shop->getTransmitterInfo(),
                        'lat' => $shop->getLat(),
                        'long' => $shop->getLongtitude(),
                    );
    }

    protected function addDeleteButton()
    {
        $delete = new Zend_Form_Element_Button('delete', array(
            'label'       => 'Delete Shop',
            'class'       => 'btn btn-lg btn-danger deletable',
            'data-button-url' => '/admin/shop/delete/id/'.$this->_id
        ));
        $this->addElement($delete);
    }

    protected function getCancelUrl()
    {
        return '/admin/shop/index/trade_network_id/'.$this->_currentUser->getId().'/';
    }
}
?>