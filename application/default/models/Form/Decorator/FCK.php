<?php
class Form_Decorator_FCK extends Zend_Form_Decorator_Abstract
{
    protected $_value;
    protected $_id; 
    
    public function __construct($options = null)
    {
        $this->_value = $options['value'];
        $this->_id = $options['id'];
        unset($options['value']);
        unset($options['id']);
        
        parent::__construct($options);
    }
    
    public function render($content)
    {
        $fckBasePath = '/js/lib/fckeditor/2.6.3/';
        Zend_Loader::loadFile('fckeditor.php',array($_SERVER['DOCUMENT_ROOT'].$fckBasePath),true);

        $oFCKeditor = new FCKeditor($this->_id);
        $oFCKeditor->BasePath = $fckBasePath ;

        $oFCKeditor->Value = $this->_value;

        ob_start();
        $oFCKeditor->Create();
        $result = ob_get_clean();
        return $result;
    }
} 