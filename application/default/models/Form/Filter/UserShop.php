<?php
class Form_Filter_UserShop extends Form_Abstract
{
    public function init()
    {
        $name = new Zend_Form_Element_Text('name', array(
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
    }
}