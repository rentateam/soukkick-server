<?php
class Form_Product extends Form_Abstract
{
    protected $_id;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setAction($this->_id ? '/product/edit/' : '/product/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Название',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Описание',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);
        
        $price = new Zend_Form_Element_Text('price', array(
            'required'    => false,
            'label'       => 'Заголовок',
            'value'       => '',
            'validators'  => array(
                array('StringLength', true, 0, 30)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($price);
        
        $discount = new Zend_Form_Element_Text('discount', array(
            'required'    => false,
            'label'       => 'Скидка',
            'validators'  => array(
                array('Int', true)
            ),
            'value'       => 0,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($discount);
        
        $pictureSide = Application::getProductBigPictureSide();
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Картинка ('.$pictureSide.'x'.$pictureSide.')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Product $product)
    {
        return array(
                        'name' => $product->getName(),
                        'description' => $product->getDescription(),
                        'discount' => $product->getDiscount(),
                        'price' => $product->getPrice(),
                    );
    }
}
?>