<?php
class Form_Sales extends Form_Abstract
{
    protected $_id;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }
    
    public function setShopId($shopId)
    {
        $this->setAction(($this->_id ? '/sales/edit/' : '/sales/add/').'shop_id/'.$shopId.'/');
    } 
    
    public function init()
    {
        $this->setAction($this->_id ? '/sales/edit/' : '/sales/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => false,
            'label'       => 'Название',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Описание',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);
        
        $shortDescription = new Zend_Form_Element_Textarea('short_description', array(
            'required'    => false,
            'label'       => 'Описание',
            'validators'  => array(
                array('StringLength', true, 0, 3000)
            ),
        ));
        $this->addElement($shortDescription);
        
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Картинка ('.Application::getSalesBigPictureWidth().'x'.Application::getSalesBigPictureHeight().')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Sales $sales)
    {
        return array(
                        'name' => $sales->getName(),
                        'description' => $sales->getDescription(),
                        'short_description' => $sales->getShortDescription(),
                    );
    }
}
?>