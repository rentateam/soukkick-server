<?php

class Form_RegisterShop extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_register_shop';
    protected $_checkPasswordConfirm = true;
    
    public function __construct($backurl)
    {
        $this->_backurl = $backurl;
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        parent::__construct();
    }
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/register/shop/');
        $this->setAttrib('id',$this->_id);
        
        $email = new Zend_Form_Element_Text('email', array(
            'required'    => true,
            'autocomplete' => 'off',
            'validators'  => array(
                array('EmailAddress', true),
                array('StringLength', false, array(3, 50)),
                array(new Form_Validate_TradeNetworkNotExistsByEmail(),false)
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($email);
        
        
        $type = new Zend_Form_Element_Select('type', array(
            'multiOptions' => Entity_Shop::getTypes() 
        ));
        $this->addElement($type);
        
        $shopName = new Zend_Form_Element_Text('shop_name', array(
            'required'    => true,
            'validators'  => array(
                array('StringLength', false, array(1, 500)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($shopName);
        
        $city = new Zend_Form_Element_Text('city', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', false, array(0, 50)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($city);
        
        $surname = new Zend_Form_Element_Text('surname', array(
            'required'    => true,
            'validators'  => array(
                array('StringLength', false, array(0, 50)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($surname);
        
       
        $size = new Zend_Form_Element_Select('size', array(
            'multiOptions' => Entity_Shop::getSizes()
        ));
        $this->addElement($size);
        
        $phone = new Zend_Form_Element_Text('phone', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', false, array(0, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($phone);
        
        $file = new Zend_Form_Element_File('photo', array(
            'required'    => true,
            'adapter'     => 'Http',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_Extension',false,array('jpg', 'gif', 'png')),
                //array('File_FilesSize',false,array(5*1024*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        $file->removeDecorator('Label');
        
        $password = new Zend_Form_Element_Password('password', array(
            'required'    => true,
            'label'       => 'Пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($password);
        
        $passwordConfirm = new Zend_Form_Element_Password('password_confirm', array(
            'required'    => true,
            'label'       => 'Подтвердите пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($passwordConfirm);
        
        // set redirect after login
        $redirect = new Zend_Form_Element_Hidden('backurl', array(
            'required'    => false,
            'value'       => $this->_backurl
        ));
        
        $this->addElement($redirect);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function isValid($data)
    {
        //check if the password confirm corresponds with the password
        $passwordConfirm = ($data['password'] === $data['password_confirm']);
        
        $otherCheck = parent::isValid($data);
        if(!$passwordConfirm && $this->_checkPasswordConfirm)
            $this->getElement('password_confirm')->addErrors(array('Введенные пароли не совпадают'));
            
        if(!$this->_checkPasswordConfirm)
            $passwordConfirm = true;
            
        return $passwordConfirm && $otherCheck; 
    }
    
    public function getValuesArray($entity)
    {
        return array();
    }
}

?>