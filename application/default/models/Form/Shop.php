<?php
class Form_Shop extends Form_Abstract
{
    protected $_id;
    protected $_currentUser;

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_currentUser = isset($options['tradeNetwork']) ? $options['tradeNetwork'] : null;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/edit/shop/edit/' : '/edit/shop/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);

        $name = new Zend_Form_Element_Text('name', array(
            'required'    => false,
            'label'       => 'Название',
            'validators'  => array(
                array('StringLength', true, 0, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);

        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Описание',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);

        $address = new Zend_Form_Element_Textarea('address', array(
            'required'    => false,
            'label'       => 'Адрес',
            'validators'  => array(
                array('StringLength', true, 0, 300)
            ),
        ));
        $this->addElement($address);

        $lat = new Zend_Form_Element_Text('lat', array(
            'required'    => false,
            'label'       => 'Широта',
            'validators'  => array(
                array('Float', true)
            ),
            'value'       => 0,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($lat);

        $long = new Zend_Form_Element_Text('long', array(
            'required'    => false,
            'label'       => 'Долгота',
            'validators'  => array(
                array('Float', true)
            ),
            'value'       => 0,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($long);

        $mallId = new Zend_Form_Element_Select('mall_id', array(
            'required'    => false,
            'label'       => 'ТЦ',
            'multiOptions' => array_merge(array('0' => 'Вне ТЦ'),Form_Helper_Select::aggregateToList(Mapper_Mall::getInstance()->getList(), 'name'))
        ));
        $this->addElement($mallId);

        $city = new Zend_Form_Element_Select('city', array(
            'required'    => true,
            'label'       => 'Город',
            'multiOptions' => Form_Helper_Select::aggregateToList(Mapper_City::getInstance()->getListPlain(), 'name')
        ));
        $this->addElement($city);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));

        $this->addElement($submit);

        parent::init();
    }

    public function getValuesArray(Entity_Shop $shop)
    {
        $mall = $shop->getMall();
        $shop = new Decorator_Shop($shop);
        return array(
                        'name' => $shop->getName(),
                        'description' => $shop->getDescription(),
                        'address' => $shop->getAddressForHtmlAttribute(),
                        'lat' => $shop->getLat(),
                        'long' => $shop->getLongtitude(),
                        'city' => $shop->getCityId(),
                        'mall_id' => $mall ? $mall->getId() : 0
                    );
    }

    public function setTradeNetwork(Entity_Trade_Network $user)
    {
        $this->getElement('name')->setValue($user->getTitle());
        $this->getElement('description')->setValue($user->getDescription());
    }
}
?>