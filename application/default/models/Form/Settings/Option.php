<?php
class Form_Settings_Option extends Form_Abstract
{
    public function init()
    {
        $id = new Zend_Form_Element_Text('id');
        $this->addElement($id);

        $caption = new Zend_Form_Element_Text('caption', array(
            'required' => true,
            'validators' => array(
                new Form_Validate_StringLength(array(3, 255))
            )
        ));
        $this->addElement($caption);

        $slug = new Zend_Form_Element_Text('slug', array(
            'required' => true,
            'validators' => array(
                new Form_Validate_StringLength(array(3, 255)),
                new Form_Validate_OptionExistsBySlug($this->getElement('id'))
            )
        ));
        $this->addElement($slug);

        $data = new Zend_Form_Element_Text('data', array(
            'required' => true,
            'validators' => array(
                new Form_Validate_StringLength(array(0, 65535))
            )
        ));
        $this->addElement($data);
    }

    public function getValuesArray(Entity_Settings_Option $option)
    {
        return array(
            'id' => $option->getId(),
            'caption' => $option->getCaption(),
            'slug' => $option->getSlug(),
            'data' => $option->getData()
        );
    }
}