<?php
class Form_AdminMall extends Form_Abstract
{
    protected $_id;

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/admin/mall/edit/' : '/admin/mall/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);

        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Название',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);

        $lat = new Zend_Form_Element_Text('lat', array(
            'required'    => false,
            'label'       => 'Широта',
            'validators'  => array(
                array('Float', true)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($lat);

        $long = new Zend_Form_Element_Text('long', array(
            'required'    => true,
            'label'       => 'Долгота',
            'validators'  => array(
                array('Float', true)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($long);

        $address = new Zend_Form_Element_Textarea('address', array(
            'required'    => true,
            'label'       => 'Адрес',
            'validators'  => array(
                array('StringLength', true, 0, 300)
            ),
        ));
        $this->addElement($address);

        $logo = new Zend_Form_Element_File('logo', array(
    		'required'    => false,//$this->_id == 0,
    		'adapter'     => 'Http',
    		'name'        => 'logo',
    		'validators'  => array(
    				array('StringLength', false, array(0, 400)),
    				array('File_FilesSize',false,array(64*1024))
    		),
    		'filters'     => array('StringTrim'),
        ));
        $this->addElement($logo);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));

        $this->addElement($submit);

        parent::init();
    }

    public function getValuesArray(Entity_Mall $mall)
    {
        return array(
                        'name' => $mall->getName(),
                        'address' => $mall->getAddress(),
                        'lat' => $mall->getLat(),
                        'long' => $mall->getLongtitude(),
                    );
    }
}
?>