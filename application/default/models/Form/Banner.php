<?php
class Form_Banner extends Form_Abstract
{
    protected $_id;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setAction($this->_id ? '/admin/banner/edit/' : '/admin/banner/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $dateExpire = new Zend_Form_Element_Text('date_expire', array(
            'required'    => true,
            'label'       => 'Дата протухания',
            'class'       => 'date_element',
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($dateExpire);
        
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Картинка',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Banner $banner)
    {
        return array(
                        'date_expire' => $banner->getDateExpire(),
                    );
    }
}
?>