<?php
class Form_Login extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_login';
    
    public function __construct($backurl)
    {
        $this->_backurl = $backurl;
        parent::__construct();
    }

    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/login/');
        $this->setAttrib('id',$this->_id);        
 
        $email = new Zend_Form_Element_Text('email', array(
            'required'    => false,
            'name'        => 'email',
            'label'       => 'E-mail',
            'maxlength'   => '30',
            /*'validators'  => array(
                array('StringLength', false, array(3, 30))
             ),*/
            'filters'     => array('StringTrim','StringToLower'),
        ));

        $this->addElement($email);
      
        $password = new Zend_Form_Element_Password('password', array(
            'required'    => false,
            'label'       => 'Password',
            'maxlength'   => '30',
        ));
        
        $this->addElement($password);
        
        // set redirect after login
        $redirect = new Zend_Form_Element_Hidden('backurl', array(
            'required'    => false,
            'value'       => $this->_backurl
        ));
        
        $this->addElement($redirect);
        
        $facebookId = new Zend_Form_Element_Text('facebook_id', array(
            'required'    => false,
            'label'       => 'facebook id',
            'filters'     => array('Int'),
        ));
        $this->addElement($facebookId);
        
        $contactId = new Zend_Form_Element_Text('contact_id', array(
            'required'    => false,
            'label'       => 'contact id',
            'filters'     => array('Int'),
        ));
        $this->addElement($contactId);
        
        $udid = new Zend_Form_Element_Text('udid', array(
            'label'       => 'udid',
            'required'    => false,
        ));
        $this->addElement($udid);
        
        $cleanUdid = new Zend_Form_Element_Text('clean_udid', array(
            'label'       => 'clean udid',
            'required'    => false,
        ));
        $this->addElement($cleanUdid);
        
         // remember me checkbox
        $rememberOptions = array(
                                    'required'    => false,
                                    'label'       => 'Remember me',
                                );
        if($this->_remember)
        {
            $rememberOptions['checkedValue'] = 'checked';
            $rememberOptions['value'] = 'checked';
        }   
        $remember = new Zend_Form_Element_Checkbox('remember', $rememberOptions);
        $this->addElement($remember);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Login'
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray($entity)
    {
        return array();
    }
    
    public function isValid($data)
    {
        $result = parent::isValid($data);
        if($result && empty($data['email']) && empty($data['password']) && empty($data['facebook_id']) && empty($data['udid']))
        {
            $this->getElement('email')->addError('Email and password should not be empty!');
            return false;
        }
        return $result;
    }
}

?>