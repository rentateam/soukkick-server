<?php
class Form_AdminMallMap extends Form_Abstract
{
    protected $_id;

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/admin/mall-map/edit/' : '/admin/mall-map/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);

        $floor = new Zend_Form_Element_Text('floor', array(
            'required'    => true,
            'label'       => 'Этаж',
            'validators'  => array(
                array('StringLength', false, array(0, 10)),
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($floor);

        $file = new Zend_Form_Element_File('file', array(
    		'required'    => $this->_id == 0,
    		'adapter'     => 'Http',
    		'name'        => 'file',
    		'validators'  => array(
    				array('StringLength', false, array(0, 400)),
    				array('File_FilesSize',false,array(64*1024))
    		),
    		'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $mallId = new Zend_Form_Element_Hidden('mall_id', array());
        $this->addElement($mallId);

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));

        $this->addElement($submit);

        parent::init();
    }

    public function getValuesArray(Entity_Mall_Map $map)
    {
        return array(
                        'mall_id' => $map->getMallId(),
                        'floor' => $map->getFloor(),
                    );
    }

    public function setMallId($mallId)
    {
        $this->getElement('mall_id')->setValue($mallId);
    }
}
?>