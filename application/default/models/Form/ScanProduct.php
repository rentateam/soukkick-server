<?php
class Form_ScanProduct extends Form_Abstract
{
    protected $_id;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }
    
    public function setShopId($shopId)
    {
        $this->setAction(($this->_id ? '/scan-product/edit/' : '/scan-product/add/').'shop_id/'.$shopId.'/');
    } 
    
    public function init()
    {
        $this->setAction($this->_id ? '/scan-product/edit/' : '/scan-product/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Название',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Описание',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);
        
        $barcode = new Zend_Form_Element_Text('barcode', array(
            'required'    => true,
            'label'       => 'Штрих-код',
            'validators'  => array(
                array('StringLength', true, 1, 30)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($barcode);
        
        $height = Application::getScanProductBigPictureHeight()*2;
        $width = Application::getScanProductBigPictureWidth()*2;
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Картинка ('.$width.'x'.$height.')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        /*$height = Application::getScanProductBigPictureHeight();
        $width = Application::getScanProductBigPictureWidth();
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Картинка ('.$width.'x'.$height.')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);*/

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_ScanProduct $product)
    {
        return array(
                        'name' => $product->getName(),
                        'description' => $product->getDescription(),
                        'barcode' => $product->getBarcode(),
                    );
    }
}
?>