<?php
class Form_Users_Filter extends Form_Abstract
{
    public function init()
    {
        $filter = new Zend_Form_Element_Hidden('filter');
        $this->addElement($filter);

        $dateFrom = new Zend_Form_Element_Text('date_from');
        $this->addElement($dateFrom);

        $dateTo = new Zend_Form_Element_Text('date_to');
        $this->addElement($dateTo);

        $onlyAuthed = new Zend_Form_Element_Checkbox('only_authed');
        $this->addElement($onlyAuthed);

        $fieldId = new Zend_Form_Element_Checkbox('field_id');
        $this->addElement($fieldId);

        $fieldRegister = new Zend_Form_Element_Checkbox('field_register');
        $this->addElement($fieldRegister);

        $fieldSocial = new Zend_Form_Element_Checkbox('field_social');
        $this->addElement($fieldSocial);

        $fieldAmount = new Zend_Form_Element_Checkbox('field_amount');
        $this->addElement($fieldAmount);

        $fieldCheckIn = new Zend_Form_Element_Checkbox('field_checkin');
        $this->addElement($fieldCheckIn);

        $fieldScan = new Zend_Form_Element_Checkbox('field_scan');
        $this->addElement($fieldScan);

        $fieldViews = new Zend_Form_Element_Checkbox('field_views');
        $this->addElement($fieldViews);

        $fieldPromo = new Zend_Form_Element_Checkbox('field_promo');
        $this->addElement($fieldPromo);

        $fieldInvitedBy = new Zend_Form_Element_Checkbox('field_invited_by');
        $this->addElement($fieldInvitedBy);

        $fieldRewards = new Zend_Form_Element_Checkbox('field_rewards');
        $this->addElement($fieldRewards);

        $searchType = new Zend_Form_Element_Select('search_type', array(
            'multiOptions' => array(
                Criteria_User::EMAIL => 'По E-mail',
                Criteria_User::NAME => 'По имени'
            )
        ));
        $this->addElement($searchType);

        $searchText = new Zend_Form_Element_Text('search_text');
        $this->addElement($searchText);

        $this->setValuesFromArray(array(
            'filter'           => 1,
            'date_from'        => Mapper_User::getInstance()->getRegisterDateMin(),
            'date_to'          => Mapper_User::getInstance()->getRegisterDateMax(),
            'only_authed'      => NULL,
            'field_id'         => 1,
            'field_register'   => 0,
            'field_social'     => 0,
            'field_amount'     => 0,
            'field_checkin'    => 0,
            'field_scan'       => 0,
            'field_views'      => 0,
            'field_promo'      => 0,
            'field_invited_by' => 0,
            'field_rewards'    => 0,
            'search_type'      => Criteria_User::NAME,
            'search_text'      => ''
        ));

        parent::init();
    }

    public function getValuesArray(Zend_Controller_Request_Abstract $request)
    {
        $values = array(
            'date_from'        => $request->getParam('date_from'),
            'date_to'          => $request->getParam('date_to'),
            'only_authed'      => $request->getParam('only_authed'),
            'field_id'         => $request->getParam('field_id'),
            'field_register'   => $request->getParam('field_register'),
            'field_social'     => $request->getParam('field_social'),
            'field_amount'     => $request->getParam('field_amount'),
            'field_checkin'    => $request->getParam('field_checkin'),
            'field_scan'       => $request->getParam('field_scan'),
            'field_views'      => $request->getParam('field_views'),
            'field_promo'      => $request->getParam('field_promo'),
            'field_invited_by' => $request->getParam('field_invited_by'),
            'field_rewards'    => $request->getParam('field_rewards'),
            'search_type'      => $request->getParam('search_type'),
            'search_text'      => $request->getParam('search_text')
        );

        return $values;
    }
}