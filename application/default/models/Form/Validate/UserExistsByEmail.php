<?php

require_once "Zend/Validate/Abstract.php";
require_once "Mapper/User.Mapper.php";

class Form_Validate_UserExistsByEmail extends Zend_Validate_Abstract
{
    const INVALID            = 'userExistsByLogin';
    
    public function setMessageTemplates()
    {
        $this->_messageTemplates = array(
                                            self::INVALID => ''
                                        );
    }
    
    public function isValid($slug)
    {
        $this->setMessageTemplates();
        if(Mapper_User::getInstance()->getByEmail($slug) === null)
        {
            $this->_error(self::INVALID);
            return false;
        }
        else 
            return true;
    }
}
?>