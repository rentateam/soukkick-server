<?php
class Form_Validate_TradeNetworkExistsByEmail extends Zend_Validate_Abstract
{
    const INVALID            = 'tradeNetworkExistsByLogin';
    protected $_exceptUser    = null;
    
    public function setMessageTemplates()
    {
        $this->_messageTemplates = array(
                                            self::INVALID => 'Этот email занят'
                                        );
    }
    
    public function __construct($exceptUser = null)
    {
        $this->_exceptUser = $exceptUser;
        //parent::__construct();
    }
    
    public function isValid($slug)
    {
        $this->setMessageTemplates();
        $selectedUser = Mapper_Bpa_Manager::getInstance()->getByEmail($slug);
        if($selectedUser === null)
        {
            $this->_error(self::INVALID);
            return false;
        }
        else 
        {
            if(!$this->_exceptUser || $selectedUser->getId() != $this->_exceptUser->getId())
                return true;
            else
                return false;
        }
    }
}
?>