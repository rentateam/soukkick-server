<?php
class Form_Validate_OptionExistsBySlug extends Zend_Validate_Abstract
{
    const INVALID = 'optionExistsBySlug';

    protected $_messageTemplates = array(
        self::INVALID => 'Указанный ключ уже существует'
    );

    protected $idField;

    public function __construct($idField)
    {
        $this->idField = $idField;
    }

    public function isValid($slug)
    {
        $result = true;

        $option = Mapper_Settings_Option::getInstance()->getEntityByUniqueFields(array('slug' => $slug));

        if ($option && $option->getId() != $this->idField->getValue()) {
            $this->_error(self::INVALID);
            $result = false;
        }

        return $result;
    }
}