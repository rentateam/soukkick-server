<?
class Form_Validate_StringLength extends Zend_Validate_StringLength
{
    public function __construct($options = array())
    {
        $options['encoding'] = 'utf-8';
        parent::__construct($options);
    }
}