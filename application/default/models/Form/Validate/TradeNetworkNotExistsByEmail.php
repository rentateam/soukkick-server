<?php
class Form_Validate_TradeNetworkNotExistsByEmail extends Form_Validate_TradeNetworkExistsByEmail
{
    const INVALID            = 'emailShopBusy';
    
    public function setMessageTemplates()
    {
        $this->_messageTemplates = array(
                                            self::INVALID => 'Этот email занят'
                                        );
    }
    
    public function isValid($slug)
    {
        $this->setMessageTemplates();
        $result = parent::isValid($slug);
        if($result === false)
            return true;
        else
        {
            $this->_error(self::INVALID);
            return false;   
        }
    }
}
?>