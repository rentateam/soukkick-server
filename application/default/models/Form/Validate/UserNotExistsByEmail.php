<?php
require_once "Form/Validate/UserExistsByEmail.php";

class Form_Validate_UserNotExistsByEmail extends Form_Validate_UserExistsByEmail
{
    const INVALID            = 'emailBusy';
    
    public function setMessageTemplates()
    {
        $this->_messageTemplates = array(
                                            self::INVALID => 'Пользователь уже существует'
                                        );
    }
    
    public function isValid($slug)
    {
        $this->setMessageTemplates();
        $result = parent::isValid($slug);
        if($result === false)
            return true;
        else
        {
            $this->_error(self::INVALID);
            return false;   
        }
    }
}
?>