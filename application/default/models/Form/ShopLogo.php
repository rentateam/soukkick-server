<?php
class Form_ShopLogo extends Form_Abstract
{
    protected $_id;
    protected $_currentUser;
    
    public function init()
    {
        $this->setAction('/edit/settings/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $file = new Zend_Form_Element_File('file', array(
            'required'    => false,
            'adapter'     => 'Http',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $rectangleFile = new Zend_Form_Element_File('rectangle', array(
            'required'    => false,
            'adapter'     => 'Http',
            'name'        => 'rectangle_file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($rectangleFile);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ''
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Trade_Network $user)
    {
        return array();
    }
}
?>