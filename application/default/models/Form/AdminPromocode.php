<?php
class Form_AdminPromocode extends Form_Abstract
{
    protected $_id;

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/admin/promocode/edit/' : '/admin/promocode/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);

        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Name',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);

        $code = new Zend_Form_Element_Text('code', array(
            'required'    => true,
            'label'       => 'Code',
            'validators'  => array(
                array('StringLength', true, 1, 20)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($code);

        $amount = new Zend_Form_Element_Text('amount', array(
            'required'    => true,
            'label'       => 'Price',
            'validators'  => array(
                array('Int', true)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($amount);

        $limit = new Zend_Form_Element_Text('limit', array(
            'required'    => true,
            'label'       => 'Limit',
            'validators'  => array(
                array('Int', true)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($limit);

        $dateFrom = new Zend_Form_Element_Text('date_from', array(
            'required'    => true,
            'label'       => 'Start date',
            'filters'     => array('StringTrim'),
            'attribs'     => array(
                'class' => 'date_element'
            )
        ));
        $this->addElement($dateFrom);

        $dateTo = new Zend_Form_Element_Text('date_to', array(
            'required'    => true,
            'label'       => 'End date',
            'filters'     => array('StringTrim'),
            'attribs'     => array(
                'class' => 'date_element'
            )
        ));
        $this->addElement($dateTo);

        $logo = new Zend_Form_Element_File('logo', array(
    		'required'    => false,//$this->_id == 0,
    		'adapter'     => 'Http',
    		'name'        => 'Logo',
    		'validators'  => array(
    				array('StringLength', false, array(0, 400)),
    				array('File_FilesSize',false,array(64*1024))
    		),
    		'filters'     => array('StringTrim'),
        ));
        $this->addElement($logo);

        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $this->addCancelButton();

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Add' : 'Save')
        ));

        $this->addElement($submit);

        parent::init();
    }

    public function getValuesArray(Entity_Promocode $promocode)
    {
        $ruleItem = $promocode->getBoundRuleItem();
        $ruleMain = $ruleItem->getRuleMain();
        return array(
                        'name' => $promocode->getName(),
                        'code' => $promocode->getCode(),
                        'amount' => $ruleItem->getAmount(),
                        'limit' => $ruleItem->getMaxSpendAmount(),
                        'date_from' => $ruleMain->getDateFrom(),
                        'date_to' => $ruleMain->getDateTo(),
                    );
    }

    protected function getCancelUrl()
    {
        return '/admin/promocode/';
    }
}
?>