<?php

class Form_Register extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_register';
    protected $_checkPasswordConfirm = true;
    
    public function __construct($backurl)
    {
        $this->_backurl = $backurl;
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        parent::__construct();
    }
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/register/');
        $this->setAttrib('id',$this->_id);
        
        $email = new Zend_Form_Element_Text('email', array(
            'required'    => true,
            'label'       => 'Эл.почта',
            'autocomplete' => 'off',
            'validators'  => array(
                array('EmailAddress', true),
                new Form_Validate_StringLength(array(3, 50)),
            ),
            'filters'     => array('StringTrim','StringToLower'),
        ));

        $this->addElement($email);
        
        
        $password = new Zend_Form_Element_Password('password', array(
            'required'    => true,
            'label'       => 'Пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($password);
        
        $passwordConfirm = new Zend_Form_Element_Password('password_confirm', array(
            'required'    => true,
            'label'       => 'Подтвердите пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($passwordConfirm);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'name'        => 'name',
            'label'       => 'Имя',
            'validators'  => array(
                new Form_Validate_StringLength(array(1, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $surname = new Zend_Form_Element_Text('surname', array(
            'required'    => true,
            'name'        => 'surname',
            'label'       => 'Фамилия',
            'validators'  => array(
                new Form_Validate_StringLength(array(1, 50)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($surname);
        
        $file = new Zend_Form_Element_File('photo', array(
            'required'    => false,
            'adapter'     => 'Http',
            'label'       => 'Фотография',
            'name'        => 'photo',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 400)),
                array('File_Extension',false,array('jpg', 'gif', 'png')),
                //array('File_FilesSize',false,array(5*1024*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $birthdate = new Zend_Form_Element_Text('birthdate', array(
            'required'    => true,
            'label'       => 'Дата рождения',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($birthdate);
        
        $gender = new Zend_Form_Element_Hidden('is_male', array(
            'required'    => false,
        ));
        $this->addElement($gender);
        
        // set redirect after login
        $redirect = new Zend_Form_Element_Hidden('backurl', array(
            'required'    => false,
            'value'       => $this->_backurl
        ));
        $this->addElement($redirect);
        
        $facebookId = new Zend_Form_Element_Text('facebook_id', array(
            'label'       => 'facebook id',
            'required'    => false,
            'filters'     => array('Int'),
        ));
        
        $this->addElement($facebookId);
        
        $contactId = new Zend_Form_Element_Text('contact_id', array(
            'label'       => 'contact id',
            'required'    => false,
            'filters'     => array('Int'),
        ));
        
        $this->addElement($contactId);
        
        $promoCode = new Zend_Form_Element_Text('promo_code', array(
            'label'       => 'promo code',
            'required'    => false,
        ));
        $this->addElement($promoCode);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function isValid($data)
    {
        if($data['facebook_id'] || $data['contact_id'])
        {
            $this->getElement('name')->setRequired(false);
            $this->getElement('surname')->setRequired(false);
            $this->getElement('email')->setRequired(false);
            $this->getElement('birthdate')->setRequired(false);
            $this->getElement('password')->setRequired(false);
            $this->getElement('password_confirm')->setRequired(false);
            $this->_checkPasswordConfirm = false;
            $passwordConfirm = false;
        }
        else
        {
            //check if the password confirm corresponds with the password
            $passwordConfirm = ($data['password'] === $data['password_confirm']);
        }
        
        /*$emailElement = $this->getElement('email');
        //Если у пользователя еще не было мыла, ставим валидатор на уникальность
        //Если было - не ставим, т.к. в этом случае оно все равно не поменяется
        if(!$emailElement->getValue())
        {
            $emailElement->addValidator(new Form_Validate_UserNotExistsByEmail(),false);
        }*/
        
        $otherCheck = parent::isValid($data);
        if(!$passwordConfirm && $this->_checkPasswordConfirm)
            $this->getElement('password_confirm')->addErrors(array('Введенные пароли не совпадают'));
            
        if(!$this->_checkPasswordConfirm)
            $passwordConfirm = true;
            
        return $passwordConfirm && $otherCheck; 
    }
    
    public function getValuesArray(Entity_User $user)
    {
        //Метод урезанный - т.к. кроме email ничего не надо
        return array(
            'email' => $user->getEmail()
        );
    }
    
    public function setAndroidCrutch($set)
    {
        if($set)
        {
            $this->setEnctype(Zend_Form::ENCTYPE_URLENCODED);
            $this->removeElement('photo');
        }
    }
}

?>