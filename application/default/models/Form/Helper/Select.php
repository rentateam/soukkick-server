<?php
class Form_Helper_Select
{
    /**
     * Делает из агрегата список для селекта id => поле titleField 
     * @param SFM_Aggregate|array $aggregate
     * @param string $titleField
     */
    public static function aggregateToList($aggregate,$titleField)
    {
        $list = array();
        foreach($aggregate as $entity)
        {
            $list[$entity->getId()] = $entity->getProto($titleField);
        }
        return $list;
    }
}