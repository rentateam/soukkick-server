<?php
class Form_Profile_Photo extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_profile_photo';
    
    public function __construct($options = array())
    {
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/profile/photo/');
        $this->setAttrib('id',$this->_id);
        
        $file = new Zend_Form_Element_File('photo', array(
            'required'    => true,
            'adapter'     => 'Http',
            'label'       => 'Фотография',
            'name'        => 'photo',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 400)),
                array('File_Extension',false,array('jpg', 'gif', 'png')),
                //array('File_FilesSize',false,array(5*1024*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function getValuesArray($entity)
    {
        return array();
    }
}
?>