<?php

class Form_Profile_Password extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_profile_password';
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/profile/password/');
        $this->setAttrib('id',$this->_id);
        
        $oldPassword = new Zend_Form_Element_Password('old_password', array(
            'required' => true,
            'autocomplete' => 'off',
            'maxlength' => '30',
        ));
        $this->addElement($oldPassword);
        
        $password = new Zend_Form_Element_Password('password', array(
            'required'    => true,
            'label'       => 'Пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($password);
        
        $passwordConfirm = new Zend_Form_Element_Password('password_confirm', array(
            'required'    => true,
            'label'       => 'Подтвердите пароль',
            'autocomplete' => 'off',
            'maxlength'   => '30',
        ));
        $this->addElement($passwordConfirm);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function isValid($data)
    {
        /*//check if the password confirm corresponds with the password
        $passwordConfirm = ($data['password'] === $data['password_confirm']);
        
        $otherCheck = parent::isValid($data);
        if(!$passwordConfirm && $this->_checkPasswordConfirm)
            $this->getElement('password_confirm')->addErrors(array('Введенные пароли не совпадают'));
            
        if(!$this->_checkPasswordConfirm)
            $passwordConfirm = true;
            
        return $passwordConfirm && $otherCheck; */
        
        $otherCheck = parent::isValid($data);
        
        //check if the password confirm corresponds with the password
        if(array_key_exists('password', $data)){
            $passwordConfirm = ($data['password'] === $data['password_confirm']);
        } else {
            $passwordConfirm = true;
        }
        
        if(!Password::validateHash($data['old_password'], $data['old_password_template'])){
            $this->getElement('old_password')->addError('Неверный старый пароль');
            $oldPasswordConfirm = false;
        } else {
            $oldPasswordConfirm = true;
        }
        
        if (!$passwordConfirm)
            $this->getElement('password_confirm')->addError('Пароли не совпадают');
            
        return $oldPasswordConfirm && $passwordConfirm && $otherCheck;
    }
    
    public function getValuesArray($entity)
    {
        return array();
    }
}
?>