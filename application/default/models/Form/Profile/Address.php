<?php

class Form_Profile_Address extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_profile_address';
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/profile/address/');
        $this->setAttrib('id',$this->_id);
        
        $country = new Zend_Form_Element_Text('country', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 100)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($country);
        
        
        $city = new Zend_Form_Element_Text('city', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 100)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($city);
        
        $index = new Zend_Form_Element_Text('post_index', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 6)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($index);
        
        $street = new Zend_Form_Element_Text('street', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 200)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($street);
        
        $house = new Zend_Form_Element_Text('house', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 10)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($house);
        
        $flat = new Zend_Form_Element_Text('flat', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 10)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($flat);
        
        $phone = new Zend_Form_Element_Text('phone', array(
            'required'    => false,
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 30)),
            ),
            'filters'     => array('StringTrim'),
        ));

        $this->addElement($phone);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function getValuesArray($address)
    {
        return array(
                        'id' => $address->getId(),
                        'country' => $address->getCountry(),
                        'city' => $address->getCity(), 
                        'post_index' => $address->getPostIndex(), 
                        'street' => $address->getStreet(),
                        'house' => $address->getHouse(),
                        'flat' => $address->getFlat(),
                        'phone' => $address->getPhone(),
                    );
    }
}

?>