<?php

class Form_Profile_Name extends Form_Abstract
{
    protected $_backurl;
    protected $_id = 'form_profile_name';
    
    public function init()
    {
        $this->setMethod('post');
        $this->setAction('/profile/name/');
        $this->setAttrib('id',$this->_id);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => false,
            'name'        => 'name',
            'label'       => 'Имя',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $surname = new Zend_Form_Element_Text('surname', array(
            'required'    => false,
            'name'        => 'surname',
            'label'       => 'Фамилия',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 50)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($surname);
        
        $birthdate = new Zend_Form_Element_Text('birthdate', array(
            'required'    => true,
            'label'       => 'Дата рождения',
            'validators'  => array(
                new Form_Validate_StringLength(array(0, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($birthdate);
        
        $gender = new Zend_Form_Element_Hidden('is_male', array(
            'required'    => false,
        ));
        $this->addElement($gender);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => 'Зарегистрироваться',
        ));
        
        $this->addElement($submit);
        
        parent::init();
        
    }
    
    public function getValuesArray($entity)
    {
        return array();
    }
}
?>