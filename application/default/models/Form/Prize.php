<?php
class Form_Prize extends Form_Abstract
{
    protected $_id;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_specialChars = false;
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setAction($this->_id ? '/admin/prize/edit/' : '/admin/prize/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $type = new Zend_Form_Element_Select('type', array(
            'required'    => true,
            'label'       => 'Type',
            'multiOptions'  => Entity_Prize::getTypes(),
        ));
        $this->addElement($type);
        
        $name = new Zend_Form_Element_Text('name', array(
            'required'    => true,
            'label'       => 'Name',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($name);
        
        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Description',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);
        
        $price = new Zend_Form_Element_Text('price', array(
            'required'    => true,
            'label'       => 'Price',
            'validators'  => array(
                array('Int', true)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($price);
        
        $file = new Zend_Form_Element_File('file', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Logo ('.Application::getPrizeBigPictureWidth().'x'.Application::getPrizeBigPictureHeight().')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $this->addCancelButton();
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Add' : 'Save')
        ));
                
        $this->addElement($submit);

        if($this->_id != 0){
            $this->addDeleteButton();
        }
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Prize $prize)
    {
        return array(
                        'name' => $prize->getName(),
                        'type' => $prize->getType(),
                        'description' => $prize->getDescription(),
                        'price' => $prize->getPriceFromRuleItem(),
                    );
    }

    protected function addDeleteButton()
    {
        $delete = new Zend_Form_Element_Button('delete', array(
            'label'       => 'Delete reward',
            'class'       => 'btn btn-lg btn-danger deletable',
            'data-button-url' => '/admin/prize/delete/id/'.$this->_id
        ));
        $this->addElement($delete);
    }

    protected function getCancelUrl()
    {
        return '/admin/prize/';
    }
}
?>