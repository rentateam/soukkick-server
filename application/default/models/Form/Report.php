<?php

class Form_Report extends Form_Abstract
{
    public function init()
    {
        $id = new Zend_Form_Element_Text('id');
        $this->addElement($id);

        $caption = new Zend_Form_Element_Text('caption', array(
            'required' => true,
            'validators' => array(
                new Form_Validate_StringLength(array(3, 255))
            )
        ));
        $this->addElement($caption);

        $query = new Zend_Form_Element_Text('query', array(
            'required' => true,
            'validators' => array(
                new Form_Validate_StringLength(array(1, 10000))
            )
        ));
        $this->addElement($query);
    }

    public function getValuesArray(Entity_Report $report)
    {
        return array(
            'id' => $report->getId(),
            'caption' => $report->getCaption(),
            'query' => $report->getQuery()
        );
    }
}