<?php


class Form_Abstract extends Zend_Form 
{
    protected $_class = 'abs';
    protected $_specialChars = true;
    
    public function __construct($options = array()) 
    {
        parent::__construct($options); 
    }
    
    public function init()
    {
        parent::init();

        $this->loadDefaultDecorators();
        $this->setAttrib('class',$this->_class);
        
        $formErrors = $this->getView()->getHelper('formErrors');
        $formErrors->setElementStart('<p class="error control-label">')
                   ->setElementSeparator('<br />')
                   ->setElementEnd('</p>');



        foreach($this->getElements() as $element){
            /**
             * @var Zend_Form_Element $element
             */
            $element->removeDecorator('DtDdWrapper');


            if($element instanceof Zend_Form_Element_Submit || $element instanceof Zend_Form_Element_Button){
                $element->removeDecorator('HtmlTag');
            } else {
                $htmlTagDecorator = $element->getDecorator('HtmlTag');
                if(false === $htmlTagDecorator){
                    $htmlTagDecorator = $element->addDecorator('HtmlTag');
                }
                $elementWrapperClass = 'form-group';
                if($element instanceof Zend_Form_Element_Checkbox){
                    $elementWrapperClass = 'checkbox';
                } else if($element instanceof Zend_Form_Element_Hidden){
                    $elementWrapperClass = '';
                }
                $htmlTagDecorator->setOptions(
                    array(
                        'tag' => 'div',
                        'class' => $elementWrapperClass,
                ));
            }

            $elementClass = 'form-control';
            if($element instanceof Zend_Form_Element_Checkbox){
                $elementClass = '';
            } else if($element instanceof Zend_Form_Element_File){
                $elementClass = '';
            } else if($element instanceof Zend_Form_Element_Hidden){
                $elementClass = '';
            } else if($element instanceof Zend_Form_Element_Button){
                $elementClass = '';
            } else if($element instanceof Zend_Form_Element_Submit){
                $elementClass = 'btn btn-lg btn-warning';
            }

            if($elementClass)
                $element->setOptions(array('class' => $elementClass));
        }

        $translator = new Zend_Translate('array',
                                        Application::MODELS_PATH.'lang/form.php',
                                        'ru');
        $this->setTranslator($translator);
    }
    
    public function setValues(/*SFM_Entity*/ $entity)
    {
        $values = $this->getValuesArray($entity);
        $this->setValuesFromArray($values);
    } 
    
    public function setValuesFromArray($values)
    {
        foreach($values as $name => $value)
        {
            $element = $this->getElement($name);
            if($element){
                $element->setValue($value);
            }
        }
    } 
    
    public function getValueForDisplay($elementName)
    {
        $element = $this->getElement($elementName);
        $value = parent::getValue($elementName);
        //if it is not a button
        if($this->_specialChars) {
            $filter = new Zend_Filter_HtmlEntities(ENT_COMPAT,'UTF-8');
            $value = $filter->filter($value);
        }
            
        return $value;
    }
        
    public function getValuesArray($entity)
    {
        return array();
    }
    
    public function uploadFile($fileFormElement)
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->receive();
        
        // Returns the file names from the $pictureFormElement form element
        $names = $upload->getFileName($fileFormElement);
        
        return $names;
   }

    public function isValid($data)
    {
        $result = parent::isValid($data);
        foreach($this->getElements() as $element){
            /**
             * @var Zend_Form_Element $element
             */
            if($element->getMessages()){
                $htmlTagDecorator = $element->getDecorator('HtmlTag');
                $htmlTagDecorator->setOption('class',$htmlTagDecorator->getOption('class').' has-error');
            }
        }

        return $result;
    }

    protected function addCancelButton()
    {
        $cancel = new Zend_Form_Element_Button('cancel', array(
            'label'       => 'Cancel',
            'class'       => 'btn btn-lg btn-default',
            'data-button-url' => $this->getCancelUrl()
        ));
        $this->addElement($cancel);
    }

    protected function getCancelUrl()
    {
        return $this->getAction();
    }
}
?>