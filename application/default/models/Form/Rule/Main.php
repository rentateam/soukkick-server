<?php
class Form_Rule_Main extends Form_Abstract
{
    protected $_id;
    
    protected $_scanList = array();

    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setAction($this->_id ? '/edit/control/rule-save/' : '/edit/control/rule-add/');
        $this->setMethod(self::METHOD_POST);
        
        $dateFrom = new Zend_Form_Element_Text('date_from', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($dateFrom);
        
        $dateTo = new Zend_Form_Element_Text('date_to', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($dateTo);
        
        $shopIds = new Zend_Form_Element_Multiselect('rule_shop_id', array(
            'required'    => false,
            'multiOptions' => array(),
            'value'         => array()
        ));
        $shopIds->setRegisterInArrayValidator(false);
        $this->addElement($shopIds);
        
        $checkin = new Zend_Form_Element_Text('checkin', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($checkin);
        
        $checkinLimit = new Zend_Form_Element_Text('limit_checkin', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($checkinLimit);
        
        $shoppage = new Zend_Form_Element_Text('shoppage', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($shoppage);
        
        $shoppageLimit = new Zend_Form_Element_Text('limit_shoppage', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($shoppageLimit);
        
        $buy = new Zend_Form_Element_Text('buy', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($buy);
        
        $buyLimit = new Zend_Form_Element_Text('limit_buy', array(
            'required'    => false,
            'filters'     => array('StringTrim'),
            'validators'    => array(
                array('Int', true),
            ),
            'value'         => 0
        ));
        $this->addElement($buyLimit);
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        parent::init();
    }
    
    protected function setScanList(Aggregate_ScanProduct $scanList)
    {
        $this->_scanList = $scanList;
        foreach($scanList as $scan)
        {
            $scanElement = new Zend_Form_Element_Text('scan_'.$scan->getId(), array(
                'required'    => false,
                'filters'     => array('StringTrim'),
                'validators'    => array(
                    array('Int', true),
                ),
                'value'         => 0
            ));
            $this->addElement($scanElement);
            
            $scanLimit = new Zend_Form_Element_Text('limit_scan_'.$scan->getId(), array(
                'required'    => false,
                'filters'     => array('StringTrim'),
                'validators'    => array(
                    array('Int', true),
                ),
                'value'         => 0
            ));
            $this->addElement($scanLimit);
        }
    }
    
    public function setTradeNetwork(Entity_Trade_Network $network)
    {
        $shops = $network->getShops();
        if($shops->count() > 0)
        {
            $this->getElement('rule_shop_id')->setRequired(true);
        }
        $this->setScanList($network->getScanProducts());
        
        if(!$network->hasIntegration())
        {
            $this->removeElement('buy');
            $this->removeElement('limit_buy');
        }
    }
    
    public function getValuesArray(Entity_Rule_Main $rule)
    {
        $checkinRule = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionTypeForm($rule,Entity_Rule_Item::TYPE_CHECKIN);
        $shoppageRule = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionTypeForm($rule,Entity_Rule_Item::TYPE_SHOPPAGE);
        $return = array(
                            'checkin' => $checkinRule ? $checkinRule->getAmount() : '0',
                            'shoppage' => $shoppageRule ? $shoppageRule->getAmount() : '0',
                            'limit_checkin' => $checkinRule && $checkinRule->getMaxSpendAmount() != -1 ? $checkinRule->getMaxSpendAmount() : '0',
                            'limit_shoppage' => $shoppageRule && $shoppageRule->getMaxSpendAmount() != -1 ? $shoppageRule->getMaxSpendAmount() : '0',
                            'date_from' => $rule->getDateFrom(),
                            'date_to' => $rule->getDateTo(),
                            'rule_shop_id' => $rule->getShopIds(),
                        );
                        
        if($this->getElement('buy'))
        {
            $buyRule = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionTypeForm($rule,Entity_Rule_Item::TYPE_BUY_BARCODE);
            $return['buy'] = $buyRule ? $buyRule->getAmount() : '0';
            $return['limit_buy'] = $buyRule && $buyRule->getMaxSpendAmount() != -1 ? $buyRule->getMaxSpendAmount() : '0';
        }
        
        foreach($this->_scanList as $scan)
        {
            $scanRule = Mapper_Rule_Item::getInstance()->getLastByRuleMainAndActionTypeForm($rule,Entity_Rule_Item::TYPE_SCAN,$scan);
            $return['scan_'.$scan->getId()] = $scanRule ? $scanRule->getAmount() : 0;
            $return['limit_scan_'.$scan->getId()] = $scanRule ? ($scanRule->getMaxSpendAmount() != -1 ? $scanRule->getMaxSpendAmount() : '') : '';
        }
        return $return;
    }
    
    public function getDateFrom()
    {
        $date = $this->getElement('date_from')->getValue();
        return $date ? date('d.m.Y',strtotime($date)) : $date;
    }
    
    public function getDateTo()
    {
        $date = $this->getElement('date_to')->getValue();
        return $date ? date('d.m.Y',strtotime($date)) : $date;
    }
}
?>