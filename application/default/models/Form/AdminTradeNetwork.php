<?php
class Form_AdminTradeNetwork extends Form_Abstract
{
    protected $_id;
    protected $_currentUser;
    
    public function __construct($id, $options = array())
    {
        $this->_id = $id;
        $this->_currentUser = isset($options['tradeNetwork']) ? $options['tradeNetwork'] : null;
        $this->_specialChars = false;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setAction($this->_id ? '/admin/user-shop/edit/' : '/admin/user-shop/add/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);
        
        $title = new Zend_Form_Element_Text('title', array(
            'required'    => true,
            'label'       => 'Trade network name',
            'validators'  => array(
                array('StringLength', true, 1, 500)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($title);
        
        $description = new Zend_Form_Element_Textarea('description', array(
            'required'    => false,
            'label'       => 'Short description',
            'validators'  => array(
                array('StringLength', true, 0, 5000)
            ),
        ));
        $this->addElement($description);
        
        $phone = new Zend_Form_Element_Text('phone', array(
            'required'    => false,
            'label'       => 'Phone',
            'validators'  => array(
                array('StringLength', false, array(0, 30)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($phone);
        
        $pictureSide = Application::getTradeNetworkBigPictureSide();
        $file = new Zend_Form_Element_File('file', array(
            'required'    => false,
            'adapter'     => 'Http',
            'label'       => 'Square logo ('.$pictureSide.'x'.$pictureSide.')',
            'name'        => 'file',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);
        
        $rectangleFile = new Zend_Form_Element_File('rectangle', array(
            'required'    => $this->_id ? false : true,
            'adapter'     => 'Http',
            'label'       => 'Rectangle logo ('.Application::getTradeNetworkRectanglePictureWidth().'x'.Application::getTradeNetworkRectanglePictureHeight().')',
            'name'        => 'rectangle',
            'validators'  => array(
                array('StringLength', false, array(0, 400)),
                array('File_FilesSize',false,array(64*1024))
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($rectangleFile);
        
        $isRecommended = new Zend_Form_Element_Checkbox('is_recommended', array(
            'required'    => false,
            'label'       => 'Is recommended by Soukkick',
            'checkedValue' => 'checked',
        ));
        $this->addElement($isRecommended);
        

        $integrationType = new Zend_Form_Element_Select('integration_type', array(
            'required'    => false,
            'label'       => 'Intergration type',
            'filters'     => array('StringTrim'),
            'multiOptions' => Entity_Trade_Network::getIntegrationTypes()
        ));
        $this->addElement($integrationType);
        
        
        $integrationDescription = new Zend_Form_Element_Textarea('integration_description', array(
            'required'    => false,
            'label'       => 'Intergration description',
            'validators'  => array(
                array('StringLength', false, array(0, 5000)),
             ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($integrationDescription);
        
        
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);

        $this->addCancelButton();

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Add' : 'Save')
        ));
                
        $this->addElement($submit);

        parent::init();
    }
    
    public function setApprovedTradeNetwork(Entity_Trade_Network $user)
    {
        $this->setAction($this->getAction().'user_id/'.$user->getId().'/');
        $shopInfo = Entity_Trade_Network::unserializeShopInfo($user->getShopInfo());
        
        //so far number of shops is not stored anywhere
        $this->getElement('name')->setValue($user->getTitle());
        $this->getElement('description')->setValue($user->getDescription());
        $this->getElement('address')->setValue($shopInfo['city']);
    }
    
    public function getValuesArray(Entity_Trade_Network $tradeNetwork)
    {
        return array(
                        'title' => $tradeNetwork->getTitle(),
                        'description' => $tradeNetwork->getDescription(),
                        'phone' => $tradeNetwork->getPhone(),
                        'is_recommended' => $tradeNetwork->isRecommended() ? 'checked' : '',
                        'integration_description' => $tradeNetwork->getIntegrationDescription(),
                        'integration_type' => $tradeNetwork->getIntegrationType(),
                    );
    }

    protected function getCancelUrl()
    {
        return '/admin/user-shop/';
    }
}
?>