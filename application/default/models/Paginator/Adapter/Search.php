<?php 
require_once 'Zend/Paginator/Adapter/Null.php';

class Paginator_Adapter_Search extends Zend_Paginator_Adapter_Null
{
    public function getURL( $pageUrl, $pageNum )
    {
        $result = $pageUrl . (strpos($pageUrl, '?') === false ? '?' : '&') . "page=" . $pageNum;
        return $result;
    }
}
