<?php 
require_once 'Zend/Paginator/Adapter/Null.php';

class Paginator_Adapter_Null extends Zend_Paginator_Adapter_Null
{
    protected $query;

    public function getURL( $pageUrl, $pageNum )
    {
        $result = $pageUrl . "page/" . $pageNum."/";

        if ($this->query) {
            $result .= $this->query;
        }

        return $result;
    }

    public function setQueryString($query) {
        $this->query = $query;
    }
}
