<?php
class Acl
{
    public static function canAdmin(Entity_User_Admin $admin)
    {
        return $admin->getUserAdminTypeId() == Entity_User_Admin_Type::ID_TOTAL;
    }
    
    public static function canAdminReward(Entity_User_Admin $admin)
    {
        return $admin->getUserAdminTypeId() == Entity_User_Admin_Type::ID_REWARD || $admin->getUserAdminTypeId() == Entity_User_Admin_Type::ID_TOTAL;
    }
}