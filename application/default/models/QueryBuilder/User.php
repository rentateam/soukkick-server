<?php
class QueryBuilder_User extends Shopfee_QueryBuilder_Abstract
{
    protected $sql = "SELECT * FROM user";

    public function __construct(Criteria_User $criteria)
    {
        $conditions = array();

        if ($dateFrom = $criteria->getDateFrom()) {
            $conditions[] = "date_registration >= :date_from";
            $this->params['date_from'] = $dateFrom;
        }

        if ($dateTo = $criteria->getDateTo()) {
            $conditions[] = "date_registration <= :date_to";
            $this->params['date_to'] = $dateTo;
        }

        if ($email = $criteria->getEmailSearch()) {
            $conditions[] = "email LIKE '%" . $email . "%'";
        }

        if ($name = $criteria->getNameSearch()) {
            $conditions[] = "name LIKE '%" . $name . "%' OR surname LIKE '%" . $name . "%'";
        }

        if (true === $criteria->isAuthorized()) {
            $conditions[] = "udid IS NULL";
        } else if (false === $criteria->isAuthorized()) {
            $conditions[] = "udid IS NOT NULL";
        }

        if ($conditions) {
            $this->sql .= " WHERE " . implode(" AND ", $conditions);
        }

        $orderBy = NULL;
        switch ($criteria->getSort()) {
            case Criteria_User::ORDER_DATE_REGISTERED_DESC:
                $orderBy = "date_registration DESC";
                break;
            case Criteria_User::ORDER_DATE_REGISTERED_ASC:
                $orderBy = "date_registration ASC";
                break;
            case Criteria_User::ORDER_AMOUNT_DESC:
                $orderBy = "amount DESC";
                break;
            case Criteria_User::ORDER_AMOUNT_ASC:
                $orderBy = "amount ASC";
                break;
            case Criteria_User::ORDER_CHECKIN_DESC:
                $orderBy = "number_checkin DESC";
                break;
            case Criteria_User::ORDER_CHECKIN_ASC:
                $orderBy = "number_checkin ASC";
                break;
            case Criteria_User::ORDER_SCAN_DESC:
                $orderBy = "number_scan DESC";
                break;
            case Criteria_User::ORDER_SCAN_ASC:
                $orderBy = "number_scan ASC";
                break;
            case Criteria_User::ORDER_INVITED_DESC:
                $orderBy = "number_invited DESC";
                break;
            case Criteria_User::ORDER_INVITED_ASC:
                $orderBy = "number_invited ASC";
                break;
        }

        if (false === is_null($orderBy)) {
            $this->sql .= " ORDER BY " . $orderBy;
        }

        parent::__construct($criteria);
    }
}