<?php
class QueryBuilder_Trm_Main extends Shopfee_QueryBuilder_Abstract
{
    protected $sql = "SELECT trm_main.*
                     FROM trm_main
                     LEFT JOIN rule_item ON trm_main.rule_item_id = rule_item.id
                     LEFT JOIN rule_main ON rule_item.rule_main_id = rule_main.id";

    public function __construct(Criteria_Trm_Main $criteria)
    {

        if ($tradeNetwork = $criteria->getTradeNetwork()){
            $this->params['bpa_main_id'] = $tradeNetwork->getBpaMainId();
            $conditions[] = "bpa_main_id = :bpa_main_id";
        }

        if ($criteria->getDateFrom()) {
            $conditions[] = "action_date >= :date_from";
            $this->params['date_from'] = $criteria->getDateFrom();
        }
        if ($criteria->getDateTo()) {
            $conditions[] = "action_date <= :date_to";
            $this->params['date_to'] = $criteria->getDateTo();
        }
        if ($criteria->getTimeFrom()) {
            $conditions[] = "action_time >= :time_from";
            $this->params['time_from'] = $criteria->getTimeFrom();
        }
        if ($criteria->getTimeTo()) {
            $conditions[] = "action_time <= :time_to";
            $this->params['time_to'] = $criteria->getTimeTo();
        }
        if ($shop = $criteria->getShop()) {
            $conditions[] = "shop_id = :shop_id";
            $this->params['shop_id'] = $shop->getId();
        }
        if ($user = $criteria->getUser()) {
            $conditions[] = "user_id = :user_id";
            $this->params['user_id'] = $user->getId();
        }
        if ($city = $criteria->getCity()) {
            $this->sql.= ' LEFT JOIN shop ON trm_main.shop_id = shop.id';
            $conditions[] = "city_id = :city_id";
            $this->params['city_id'] = $city->getId();
        }
        if ($this->sql && $conditions) {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }

        $this->sql .= ' ORDER BY id ASC';

        if ($criteria->getNumberFrom() && $criteria->getNumberTo()) {
            $this->sql.= ' LIMIT '.$criteria->getNumberFrom().','.($criteria->getNumberTo() - $criteria->getNumberFrom());
        } else if ($criteria->getNumberTo()) {
            $this->sql.= ' LIMIT '.$criteria->getNumberTo();
        } else if ($criteria->getNumberFrom()) {
            $this->sql.= ' LIMIT '.$criteria->getNumberTo().', 1000';
        }

        parent::__construct($criteria);
    }
}