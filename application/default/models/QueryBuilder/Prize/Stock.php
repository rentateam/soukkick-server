<?php
class QueryBuilder_Prize_Stock extends Shopfee_QueryBuilder_Abstract
{
    const SORT_PRIZE_NAME = 'prize_name';
    const SORT_NUMBER_AVAILABLE = 'number_available';
    const SORT_NUMBER_DELIVERED = 'number_delivered';
    
    public function __construct(Criteria_Prize_Stock $criteria)
    {
        $this->sql = "SELECT prize_stock.id".($criteria->getSort() == self::SORT_PRIZE_NAME ? ', prize.name as prize_name' : '')." FROM prize_stock";
        if($criteria->getSort() == self::SORT_PRIZE_NAME)
        {
            $this->sql .= ' LEFT JOIN prize ON prize_stock.prize_id = prize.id ';
        }
        
        $conditions = array();
        if ($this->sql && $conditions) 
        {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }
        
        
        $sortField = '';
        $this->sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getDirection().', prize_stock.id DESC';
        
        parent::__construct($criteria);
    }
}