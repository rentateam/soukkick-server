<?php
class QueryBuilder_Prize_Order extends Shopfee_QueryBuilder_Abstract
{
    const SORT_DATETIME_CREATED = 'datetime_created';
    const SORT_PRIZE_STATUS_ID = 'prize_order_status_id';
    const SORT_PRIZE_NAME = 'prize_name';
    const SORT_ADDRESS = 'address';
    const SORT_USER_NAME = 'user_name';
    const SORT_PHONE = 'phone';
    
    public function __construct(Criteria_Prize_Order $criteria)
    {
        $this->sql = "SELECT prize_order.id".($criteria->getSort() == self::SORT_PRIZE_NAME ? ', prize.name as prize_name' : '')." FROM prize_order";
        if($criteria->getSort() == self::SORT_PRIZE_NAME)
        {
            $this->sql .= ' LEFT JOIN prize ON prize_order.prize_id = prize.id ';
        }
        
        $conditions = array();
        $statusIds = $criteria->getPrizeStatusIds();
        if(!empty($statusIds))
        {
            $conditions[] = "prize_order_status_id IN (".implode(',',$statusIds).")";
        }
        if ($this->sql && $conditions) 
        {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }
        
        
        $sortField = '';
        $this->sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getDirection().', prize_order.id DESC';
        
        parent::__construct($criteria);
    }
}