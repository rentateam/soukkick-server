<?php
class QueryBuilder_Prize_Order_History extends Shopfee_QueryBuilder_Abstract
{
    public function __construct(Criteria_Prize_Order_History $criteria)
    {
        $this->sql = "SELECT * 
                        FROM prize_order_history";
        $conditions = array();
        $order = $criteria->getPrizeOrder();
        if($order)
        {
            $conditions[] = "prize_order_id = :prize_order_id";
            $this->params['prize_order_id'] = $order->getId();
        }
        if ($this->sql && $conditions) 
        {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }
        
        
        $this->sql .= ' ORDER BY id DESC';
        
        parent::__construct($criteria);
    }
}