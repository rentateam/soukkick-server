<?php
class QueryBuilder_Prize_Stock_History extends Shopfee_QueryBuilder_Abstract
{
    public function __construct(Criteria_Prize_Stock_History $criteria)
    {
        $this->sql = "SELECT * 
                        FROM prize_stock_history";
        $conditions = array();
        $stock = $criteria->getPrizeStock();
        if($stock)
        {
            $conditions[] = "prize_stock_id = :prize_stock_id";
            $this->params['prize_stock_id'] = $stock->getId();
        }
        if ($this->sql && $conditions) 
        {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }
        
        
        $this->sql .= ' ORDER BY id DESC';
        
        parent::__construct($criteria);
    }
}