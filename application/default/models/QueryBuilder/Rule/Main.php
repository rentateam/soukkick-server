<?php
class QueryBuilder_Rule_Main extends Shopfee_QueryBuilder_Abstract
{
    protected $sql = "SELECT DISTINCT(rule_main.id)
                     FROM rule_main";

    public function __construct(Criteria_Rule_Main $criteria)
    {
        $conditions[] = "bpa_main_id = :bpa_main_id";
        $this->params['bpa_main_id'] = $criteria->getBpaMain()->getId();

        $sortOrder = 'DESC';
        switch($criteria->getDateType())
        {
            case Criteria_Rule_Main::TYPE_CURRENT:
                $conditions[] = "(
                                    date_from <= :date_current
                                    AND
                                    is_active = 1
                                 )";
                $this->params['date_current'] = date('Y-m-d');
                $sortOrder = 'DESC';
                break;
            case Criteria_Rule_Main::TYPE_FUTURE:
                $conditions[] = "(
                                    date_from > :date_current
                                    OR
                                    (
                                        date_from <= :date_current
                                        AND date_to >= :date_current
                                        AND is_active = 0
                                    )
                                  )";
                $this->params['date_current'] = date('Y-m-d');
                $sortOrder = 'ASC';
                break;
            case Criteria_Rule_Main::TYPE_OLD:
                $conditions[] = "(
                                    date_from < :date_current
                                    AND date_to < :date_current
                                  )";
                $this->params['date_current'] = date('Y-m-d');
                $sortOrder = 'ASC';
                break;
        }
        if ($shop = $criteria->getShop())
        {
            $this->sql.= $this->getShopJoinSqlPart();
            $conditions[] = "shop_id = :shop_id";
            $this->params['shop_id'] = $shop->getId();
        }
        if ($city = $criteria->getCity())
        {
            if($shop === null)
            {
                $this->sql.= $this->getShopJoinSqlPart();
            }
            $this->sql.= ' LEFT JOIN shop ON rule_main2shop.shop_id = shop.id';
            $conditions[] = "city_id = :city_id";
            $this->params['city_id'] = $city->getId();
        }
        if ($criteria->isDeleted() !== null)
        {
            $conditions[] = "rule_main.is_deleted = :is_deleted";
            $this->params['is_deleted'] = $criteria->isDeleted() ? '1' : '0';
        }
        if ($this->sql && $conditions) {
            $this->sql .= ' WHERE ' . implode(' AND ', $conditions);
        }

        $this->sql .= ' ORDER BY date_from '.$sortOrder.', id DESC';

        parent::__construct($criteria);
    }

    protected function getShopJoinSqlPart()
    {
        return ' LEFT JOIN rule_main2shop ON rule_main.id = rule_main2shop.rule_main_id';
    }
}