<?php
return array (
                Zend_Validate_NotEmpty::IS_EMPTY      => 'Поле обязательно для заполнения',
                Zend_Validate_StringLength::TOO_SHORT => 'Длина введённого значения меньше %min% символов',   
                Zend_Validate_StringLength::TOO_LONG  => 'Длина введённого значения больше %max% символов',
                Zend_Validate_EmailAddress::INVALID            => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::INVALID_HOSTNAME   => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::INVALID_MX_RECORD  => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::DOT_ATOM           => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::QUOTED_STRING      => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::INVALID_LOCAL_PART => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_EmailAddress::LENGTH_EXCEEDED    => 'Введенное поле не явлется корректным e-mail адресом',
                Zend_Validate_Hostname::INVALID                => 'Неверное имя сервера',
                Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED                => 'Неверное имя сервера',
                Zend_Validate_Hostname::UNKNOWN_TLD                => 'Неверное имя сервера',
                Zend_Validate_Hostname::INVALID_DASH                => 'Неверное имя сервера',
                Zend_Validate_Hostname::INVALID_HOSTNAME_SCHEMA                => 'Неверное имя сервера',
                Zend_Validate_Hostname::UNDECIPHERABLE_TLD                => 'Неверное имя сервера',
                Zend_Validate_Hostname::INVALID_HOSTNAME                => 'Неверное имя сервера',
                Zend_Validate_Hostname::INVALID_LOCAL_NAME                => 'Неверное имя сервера',
                Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED                => 'Неверное имя сервера',
                Zend_Validate_Hostname::CANNOT_DECODE_PUNYCODE                => 'Неверное имя сервера',
                Zend_Captcha_Word::BAD_CAPTCHA        => 'Неправильно введено слово на картинке',
                Zend_Validate_Alnum::NOT_ALNUM        => "Поле должно содержать только цифры и буквы",
                
                Zend_Validate_File_Size::TOO_BIG => "Максимальный размер файла составляет '%max%' байт, попытка закачать '%size%' байт",
                Zend_Validate_File_Size::TOO_SMALL => "Минимальный размер файла составляет '%min%' байт, попытка закачать '%size%' байт",
                Zend_Validate_File_Size::NOT_FOUND => "Файл не найден",
        
                Zend_Validate_File_MimeType::FALSE_TYPE => "Неверный mime-type",
                Zend_Validate_File_MimeType::NOT_DETECTED => "Mime-type не определен",
                Zend_Validate_File_MimeType::NOT_READABLE => "Файл не найден",
                
                Zend_Validate_File_Extension::FALSE_EXTENSION => "Неверное расширение",
                Zend_Validate_File_Extension::NOT_FOUND       => "Файл не найден",     

                Zend_Validate_File_Upload::INI_SIZE       => "Файл превышает допустимый размер, указанный в форме",
                Zend_Validate_File_Upload::FORM_SIZE      => "Форма превышает допустимый размер",
                Zend_Validate_File_Upload::PARTIAL        => "Файл был загружен не полностью",
                Zend_Validate_File_Upload::NO_FILE        => "Файл не был загружен",
                Zend_Validate_File_Upload::NO_TMP_DIR     => "Временная папка для сохранения не найдена",
                Zend_Validate_File_Upload::CANT_WRITE     => "Ошибка записи",
                Zend_Validate_File_Upload::EXTENSION      => "Ошибка PHP-расширения",
                Zend_Validate_File_Upload::ATTACK         => "Возможная атака",
                Zend_Validate_File_Upload::FILE_NOT_FOUND => "Файл не найден",
                Zend_Validate_File_Upload::UNKNOWN        => "Неизвестная ошибка сервера",
                
                Zend_Validate_Identical::NOT_SAME        => "Число не совпадает",
                Zend_Validate_Identical::MISSING_TOKEN        => "Отсутствует число для сравнения",
                
                Zend_Validate_Int::INVALID => "Значение должно быть целым числом",
                Zend_Validate_Int::NOT_INT => "Значение должно быть целым числом",
            ); 
?>