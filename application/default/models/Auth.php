<?php

require_once 'Mapper/User.Mapper.php';
require_once 'Entity/User.Entity.php';
require_once 'Mapper/User/Admin.Mapper.php';
require_once 'Entity/User/Admin.Entity.php';
require_once 'Mapper/Trade/Network.Mapper.php';
require_once 'Entity/Trade/Network.Entity.php';
require_once 'Mapper/Bpa/Manager.Mapper.php';
require_once 'Entity/Bpa/Manager.Entity.php';
class Auth
{
    const ERROR_UNDEFINED_ACCOUNT_TYPE = 1;

    const TYPE_SITE = 'site';
    const TYPE_SITE_FACEBOOK = 'site_facebook';
    const TYPE_SITE_CONTACT = 'site_contact';
    const TYPE_SITE_UDID = 'site_udid';
    const TYPE_ADMIN = 'admin';
    const TYPE_SHOP = 'shop';

    const COOKIE_LOGIN = 'shopfeeLogin';
    const COOKIE_PASSWORD = 'shopfeeHash';
    const COOKIE_REMEMBER = 'shopfeeRemember';
    const COOKIE_ADMIN_LOGIN = 'shopfeeAdminLogin';
    const COOKIE_ADMIN_PASSWORD = 'shopfeeAdminHash';
    const COOKIE_ADMIN_REMEMBER = 'shopfeeAdminRemember';
    const COOKIE_SHOP_LOGIN = 'shopfeeShopLogin';
    const COOKIE_SHOP_PASSWORD = 'shopfeeShopHash';
    const COOKIE_SHOP_REMEMBER = 'shopfeeShopRemember';

    /**
     * Login the user
     * 
     * @return Zend_Auth_Result
     */
    protected static function loginByType($type, $email, $passwd, $saveToCookie = false, $isEncrypted = false, $facebookId = 0, $contactId = 0)
    {
        //if nothing supplied as login or password return null
        if ($email === NULL || $passwd === NULL)
            return null;

        if (!$isEncrypted) {
            //first try to extract such user
            $neededUser = self::getAuthInstanceMapperByType($type)->getEntityByEmail($email);
            if ($neededUser !== null) {
                if (Password::validateHash($passwd, $neededUser->getInfo('password'))) {
                    $passwd = $neededUser->getInfo('password');
                }
            } else {
                //else this user can not authentificate - there is no such user.
                //So trim password - he would be rejected
                $passwd = '';
            }
        }
        
        $auth = Shopfee_Auth::get($type);
        switch ($type) 
        {
            case self::TYPE_SITE:
                $className = 'Shopfee_Auth_Adapter_DbTable';
                break;
            case self::TYPE_ADMIN:
                $className = 'Shopfee_Auth_Adapter_DbTable_Admin';
                break;
            case self::TYPE_SHOP:
                $className = 'Shopfee_Auth_Adapter_DbTable_Shop';
                break;
            default:
                $className = 'Shopfee_Auth_Adapter_DbTable';
                break;
        }

        $adapterClassName = self::getAuthAdapterClassByType($type); 
        $authAdapter = $adapterClassName::getInstance($className);
        $authAdapter->setIdentity($email);
        $authAdapter->setCredential($passwd);

        //if the password is not encrypted already
        if ($isEncrypted)
            $authAdapter->setCredentialTreatment('?');

        $result = $auth->authenticate($authAdapter);

        if ($result->getCode() == Zend_Auth_Result::SUCCESS) {

            $storage = $auth->getStorage();
            $storageEntity = self::getAuthInstanceMapperByType($type)->getEntityById($authAdapter->getResultRowObject("id")->id);
            $storage->write($storageEntity);

            if ($saveToCookie) {
                $hostName = Application::getHostName();
                $savePasswd = $isEncrypted ? $passwd : Password::encryptPassword($passwd);
                setCookie(self::getLoginCookieNameByType($type), $email, time() + 3600 * 24, "/", '.' . $hostName);
                setCookie(self::getPasswordCookieNameByType($type), $savePasswd, time() + 3600 * 24, "/", '.' . $hostName);
                setCookie(self::getRememberCookieNameByType($type), true, time() + 3600 * 24, "/", '.' . $hostName);
            }
        }
        return $result;
    }
    
    public static function login($email, $passwd, $saveToCookie = false, $isEncrypted = false)
    {
        return self::loginByType(self::TYPE_SITE,$email, $passwd, $saveToCookie, $isEncrypted);
    }
    
    public static function loginFacebook($facebookId)
    {
        $type = self::TYPE_SITE_FACEBOOK;
        $auth = Shopfee_Auth::get(self::TYPE_SITE);
        $adapterClassName = self::getAuthAdapterClassByType($type); 
        $authAdapter = $adapterClassName::getInstance('Shopfee_Auth_Adapter_DbTable');
        $authAdapter->setIdentity($facebookId);
        $authAdapter->setCredential($facebookId);

        $result = $auth->authenticate($authAdapter);

        if ($result->getCode() == Zend_Auth_Result::SUCCESS) {
            $storage = $auth->getStorage();
            $storageEntity = self::getAuthInstanceMapperByType($type)->getEntityById($authAdapter->getResultRowObject("id")->id);
            $storage->write($storageEntity);
        }
        return $result;
    }
    
    public static function loginContact($contactId)
    {
        $type = self::TYPE_SITE_CONTACT;
        $auth = Shopfee_Auth::get(self::TYPE_SITE);
        $adapterClassName = self::getAuthAdapterClassByType($type); 
        $authAdapter = $adapterClassName::getInstance('Shopfee_Auth_Adapter_DbTable');
        $authAdapter->setIdentity($contactId);
        $authAdapter->setCredential($contactId);

        $result = $auth->authenticate($authAdapter);

        if ($result->getCode() == Zend_Auth_Result::SUCCESS) {
            $storage = $auth->getStorage();
            $storageEntity = self::getAuthInstanceMapperByType($type)->getEntityById($authAdapter->getResultRowObject("id")->id);
            $storage->write($storageEntity);
        }
        return $result;
    }
    
    public static function loginUdid($udid)
    {
        $type = self::TYPE_SITE_UDID;
        $auth = Shopfee_Auth::get(self::TYPE_SITE);
        $adapterClassName = self::getAuthAdapterClassByType($type); 
        $authAdapter = $adapterClassName::getInstance('Shopfee_Auth_Adapter_DbTable');
        $authAdapter->setIdentity($udid);
        $authAdapter->setCredential($udid);

        $result = $auth->authenticate($authAdapter);

        if ($result->getCode() == Zend_Auth_Result::SUCCESS) {
            $storage = $auth->getStorage();
            $storageEntity = self::getAuthInstanceMapperByType($type)->getEntityById($authAdapter->getResultRowObject("id")->id);
            $storage->write($storageEntity);
        }
        return $result;
    }
    
    public static function loginShop($email, $passwd, $saveToCookie = false, $isEncrypted = false)
    {
        return self::loginByType(self::TYPE_SHOP,$email, $passwd, $saveToCookie, $isEncrypted);
    }
    
    public static function loginAdmin($email, $passwd, $saveToCookie = false, $isEncrypted = false)
    {
        return self::loginByType(self::TYPE_ADMIN,$email, $passwd, $saveToCookie, $isEncrypted);
    }
    
    public static function logoff()
    {
        return self::logoffByType(self::TYPE_SITE);
    }
    
    public static function logoffAdmin()
    {
        return self::logoffByType(self::TYPE_ADMIN);
    }
    
    public static function logoffShop()
    {
        return self::logoffByType(self::TYPE_SHOP);
    }

    protected static function logoffByType($type)
    {
        $loginCookie = self::getLoginCookieNameByType($type);
        $passwdCookie = self::getPasswordCookieNameByType($type);

        $hostName = Application::getHostName();
        setCookie($loginCookie, "", false, "/", '.' . $hostName);
        setCookie($passwdCookie, "", false, "/", '.' . $hostName);
        unset($_COOKIE[$loginCookie]);
        unset($_COOKIE[$passwdCookie]);

        Shopfee_Auth::get($type)->clearIdentity();
    }

    public static function getCurrentUser()
    {
        return self::getCurrentUserByType(self::TYPE_SITE);
    }

    public static function getCurrentBpaManager()
    {
        return self::getCurrentUserByType(self::TYPE_SHOP);
    }
    
    public static function getCurrentTradeNetwork()
    {
        $manager = self::getCurrentBpaManager();
        if($manager)
        {
            //Костыль. Здесь должна быть явно указана сеть, а берется первая попавшаяся у манагера
            return Mapper_Trade_Network::getInstance()->getListByBpaMain($manager->getBpaMain())->current();
        }
        else
        {
            return null;
        }
    }
    
    public static function getCurrentAdmin()
    {
        return self::getCurrentUserByType(self::TYPE_ADMIN);
    }
    
    protected static function getCurrentUserByType($type)
    {
        $auth = Shopfee_Auth::get($type);
        
        if ($auth->hasIdentity()) 
        {
            $entity = $auth->getStorage()->read();
            $mapper = self::getAuthInstanceMapperByType($type);
            return $mapper->getEntityById($entity->getId());
        }
        else
        {
            return null;
        }
    }

    /**
     * send email about password to a user that forgot his password
     * @return bool   
     */
    public static function sendUserPassword($email)
    {
        $userMapper = Mapper_Account::getInstance();
        $userEntity = $userMapper->getByEmail($email);
        //if there is no user with such email
        if (null === $userEntity)
            return false;

        $checkWord = self::setCheckWord($userEntity);

        //composing a letter
        $params = array('checkWord' => $checkWord);
        $subject = 'Сброс пароля на ' . Application::getHostName();
        Shopfee_Mailer::getInstance()->sendLetter($params, 'changePassword.phtml', Application::getDefaultEmailAddress(), $email, Application::getDefaultEmailTitle(), '', $subject);
        return true;
    }

    /**
     *  @param Entity_Account $userEntity
     *  @return string
     */
    public static function setCheckWord($accountEntity)
    {
        $generatedCheckWord = md5(uniqid(mt_rand(), true));

        if (false === $accountEntity->update(array('checkword' => $generatedCheckWord)))
            throw new Pbup_Exception('checkword not updated');
        return $generatedCheckWord;
    }
    
    public static function changePassword($checkWord, $newPassword)
    {
        $userMapper = Mapper_Account::getInstance();
        $aggregate = $userMapper->getAggregate(array('checkword' => $checkWord));
        $aggregateSize = $aggregate->count();

        if ($aggregateSize == 0)
            return false;
        else if ($aggregateSize == 1) {
            $userEntity = $aggregate->current();

            return $userEntity->update(array(
                'password' => Password::encryptPassword($newPassword),
                'checkword' => ''
            ));
        }
    }
    
    protected static function getAuthInstanceMapperByType($type)
    {
        switch($type)
        {
            case self::TYPE_SITE:
            case self::TYPE_SITE_FACEBOOK:
            case self::TYPE_SITE_CONTACT:
            case self::TYPE_SITE_UDID:
                return Mapper_User::getInstance();
                break;
            case self::TYPE_ADMIN:
                return Mapper_User_Admin::getInstance();
            case self::TYPE_SHOP:
                return Mapper_Bpa_Manager::getInstance();
                break;
        }
    }
    
    protected static function getLoginCookieNameByType($type)
    {
        switch($type)
        {
            case self::TYPE_SITE:
                $loginCookie = self::COOKIE_LOGIN;
                break;
            case self::TYPE_ADMIN:
                $loginCookie = self::COOKIE_ADMIN_LOGIN;
                break;
            case self::TYPE_SHOP:
                $loginCookie = self::COOKIE_SHOP_LOGIN;
                break;
            default:
                $loginCookie = '';
        }
        return $loginCookie;
    }
    
    protected static function getPasswordCookieNameByType($type)
    {
        switch($type)
        {
            case self::TYPE_SITE:
                $passwdCookie = self::COOKIE_PASSWORD;
                break;
            case self::TYPE_ADMIN:
                $passwdCookie = self::COOKIE_ADMIN_PASSWORD;
                break;
            case self::TYPE_SHOP:
                $passwdCookie = self::COOKIE_SHOP_PASSWORD;
                break;
            default:
                $passwdCookie = '';
        }
        return $passwdCookie;
    }
    
    protected static function getRememberCookieNameByType($type)
    {
        switch($type)
        {
            case self::TYPE_SITE:
                $rememberCookie = self::COOKIE_REMEMBER;
                break;
            case self::TYPE_ADMIN:
                $rememberCookie = self::COOKIE_ADMIN_REMEMBER;
                break;
            case self::TYPE_SHOP:
                $rememberCookie = self::COOKIE_SHOP_REMEMBER;
                break;
            default:
                $rememberCookie = '';
        }
        return $rememberCookie;
    }
    
    protected static function getAuthAdapterClassByType($type)
    {
        switch($type)
        {
            case self::TYPE_SITE:
                return 'Shopfee_Auth_Adapter';
                break;
            case self::TYPE_ADMIN:
                return 'Shopfee_Auth_Adapter_Admin';
            case self::TYPE_SHOP:
                return 'Shopfee_Auth_Adapter_Shop';
                break;
            case self::TYPE_SITE_FACEBOOK:
                return 'Shopfee_Auth_Adapter_Facebook';
                break;
            case self::TYPE_SITE_CONTACT:
                return 'Shopfee_Auth_Adapter_Contact';
                break;
            case self::TYPE_SITE_UDID:
                return 'Shopfee_Auth_Adapter_Udid';
                break;
        }
    }
}