<?php
class Mapper_Event_Shop extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Event_Shop
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'event_shop';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Event_Shop
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Event_Shop
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function lazyload($business, $fieldName)
    {
        switch($fieldName)
        {
            case 'object_id':
                $objectId = $business->getObjectId();
                if($objectId !== null)
                {
                    switch($business->getType())
                    {
                        case Entity_Event_Shop::TYPE_ENTER:
                            $result = Mapper_Action_Enter::getInstance()->getEntityById($objectId);
                            break;
                         case Entity_Event_Shop::TYPE_EXIT:
                            $result = Mapper_Action_Exit::getInstance()->getEntityById($objectId);
                            break;
                        case Entity_Event_Shop::TYPE_SCAN:
                            $result = Mapper_Action_Scan::getInstance()->getEntityById($objectId);
                            break;
                        case Entity_Event_Shop::TYPE_CHECKIN:
                            $result = Mapper_Action_Checkin::getInstance()->getEntityById($objectId);
                            break;
                        default:
                            $result = null;
                    }
                }
                else
                    $result = null;
                break;
            default:
                $result = parent::lazyload($business, $fieldName);
        }
        return $result;
    }

    public function getListByTradeNetwork(Entity_Trade_Network $tradeNetwork, Entity_Shop $shop = null)
    {
        $shopIdListString = $this->getShopIdsString($tradeNetwork,$shop);

        $excludeTypes = array(Entity_Event_Shop::TYPE_ENTER, Entity_Event_Shop::TYPE_EXIT);
        if($shopIdListString)
        {
            $sql = "SELECT *
                    FROM event_shop
                    WHERE shop_id IN (".$shopIdListString.")
                    AND type NOT IN (".implode(',',$excludeTypes).")";
            $params = array();
            $sql.=" ORDER BY date_created DESC";
            $cacheKey = $this->getAggregateCacheKeyByParentEntity($tradeNetwork,$shopIdListString);
            $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        }
        else
        {
            $aggregate = $this->createAggregate(array());
        }
        return $aggregate;
    }

    public function getListByShop(Entity_Shop $shop)
    {
        $sql = "SELECT *
                FROM event_shop
                WHERE shop_id = :shop_id";
        $params = array('shop_id' => $shop->getId());
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }

    public function getListByObjectAndType(SFM_Entity $object, $type)
    {
        $sql = "SELECT *
                FROM event_shop
                WHERE object_id = :object_id
                AND type = :type";
        $params = array('object_id' => $object->getId(),'type' => $type);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }

    public function add(Entity_Shop $shop,$type,$object = null, $dateCreated = null,$timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'shop_id' => $shop->getId(),
                            'type' => $type,
                            'object_id' => $object ? $object->getId() : null,
                            'date_created' => $dateCreated.' '.$timeCreated
                      );
        return $this->insertEntity($proto);
    }

    public function clearCacheByTradeNetwork(Entity_Trade_Network $tradeNetwork, Entity_Shop $shop = null)
    {
        $shopIdListString = $this->getShopIdsString($tradeNetwork,$shop);
        $shopIdListStringFullShops = $this->getShopIdsString($tradeNetwork,null);
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($tradeNetwork,$shopIdListString));
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($tradeNetwork,$shopIdListStringFullShops));
    }

    protected function getShopIdsString(Entity_Trade_Network $tradeNetwork, Entity_Shop $shop = null)
    {
        if($shop === null)
        {
            $shopIdList = $tradeNetwork->getShops(null)->getListEntitiesId();
            sort($shopIdList);
        }
        else
        {
            $shopIdList = array($shop->getId());
        }

        if(!empty($shopIdList))
            return implode(',',$shopIdList);
        else
            return '';
    }
}