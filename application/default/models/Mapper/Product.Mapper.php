<?php 

class Mapper_Product extends Mapper_ShopsBound implements SFM_Interface_Singleton
{
    const FAVOURITE_LIST = 'Favourite';
    
    /**
     * @var Mapper_Product
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'product';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Product
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Product
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function add($name,Entity_Trade_Network $tradeNetwork,$shopIds,$description,$price,$discount)
    {
        $proto = array(
                            'name' => $name, 
                            'description' => $description,
                            'price' => $price,
                            'discount' => $discount,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d'),
                            'trade_network_id' => $tradeNetwork->getId()
                        );
        $entity = $this->insertEntity($proto);
        $this->insertShopIds($entity,$shopIds);
        return $entity;
    }
    
    protected function getShopsBoundTable()
    {
        return 'product2shop';
    }
    
    protected function getShopsBoundObjectFieldName()
    {
        return 'product_id';
    }
    
    public function getShopIdsByProduct(Entity_Product $product)
    {
        return $this->getShopIdsByObject($product);
    }
    
    public function getListByShop(Entity_Shop $shop,$pageNumber = 0)
    {
        $sql = "SELECT id 
                FROM product
                LEFT JOIN product2shop ON product.id=product2shop.product_id
                WHERE product2shop.shop_id=:shop_id
                ORDER BY name ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($shop);
        $aggregate = $this->getAggregateBySQL($sql, array('shop_id' => $shop->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }
    
    public function getListFavouriteByTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        $sql = "SELECT * 
                FROM product
                WHERE trade_network_id=:trade_network_id
                ORDER BY name ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($tradeNetwork,self::FAVOURITE_LIST);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('trade_network_id' => $tradeNetwork->getId()), $cacheKey);
        return $aggregate;
    }
    
    public function clearCacheByShop(Entity_Shop $shop)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($shop));
    }
    
    public function clearCacheFavouriteByTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($tradeNetwork,self::FAVOURITE_LIST));
    }
}