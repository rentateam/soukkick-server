<?php 
class Mapper_Transmitter extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Transmitter
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'transmitter';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Transmitter
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Transmitter
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function getListByShopAndType(Entity_Shop $shop, $typeId = null, $isHidden = false)
    {
        $sql = "SELECT transmitter.* 
                FROM transmitter";
        if($isHidden !== null)
        {
            $sql.= " LEFT JOIN shop ON shop_id = shop.id";
        }
        $sql.= " WHERE shop_id=:shop_id";
        $params = array('shop_id' => $shop->getId());
        if($typeId !== null)
        {
            $sql.= " AND transmitter_type_id = :transmitter_type_id";
            $params['transmitter_type_id'] = $typeId;
        }
        if($isHidden !== null)
        {
            $sql.= ' AND is_hidden=:is_hidden';
            $params['is_hidden'] = $isHidden ? '1' : '0';
        }
        $cacheKey = $this->getCacheKeyByShopAndType($shop,$typeId, $isHidden);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate;
    }
    
    protected function getCacheKeyByShopAndType(Entity_Shop $shop, $typeId, $isHidden)
    {
        $postfix = '';
        if($typeId !== null)
        {
            $postfix = $typeId;
        }
        if($isHidden !== null)
        {
            $postfix.= ($isHidden ? '1' : '0');
        }
        return $this->getAggregateCacheKeyByParentEntity($shop,$postfix);
    }
    
    //@TODO. Добавить алгоритм квадратов
    public function getListByCoordinates($lat,$long)
    {
        $pk = 180/3.14169;
        $cosLat = cos($lat/$pk);
        $cosLong = cos($long/$pk);
        $sinLat = sin($lat/$pk);
        $sinLong = sin($long/$pk);
        $sql = "SELECT transmitter.* 
                FROM transmitter
                LEFT JOIN shop ON shop_id = shop.id
                WHERE shop.is_hidden = :is_hidden
                AND 6366000*acos(".($cosLat*$cosLong)."*cos(transmitter.lat/$pk)*cos(transmitter.longtitude/$pk) + ".($cosLat*$sinLong)."*cos(transmitter.lat/$pk)*sin(transmitter.longtitude/$pk) + ".$sinLat."*sin(transmitter.lat/$pk)) <= ".(Entity_Transmitter::GPS_ACCURACY + Entity_Transmitter::TRANSMITTER_CHANGE_DISTANCE);
        $params = array('is_hidden' => '0');
        $aggregate = $this->getLoadedAggregateBySQL($sql,$params, null);
        return $aggregate;
    }
    
    /**
     * @param Entity_Transmitter $transmitter
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Transmitter $transmitter, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $activeRuleItemId = $transmitter->getActiveRuleItemId();
                $result = $activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($activeRuleItemId) : null;
                break;
            default:
                $result = parent::lazyload($transmitter, $fieldName);
        }
        return $result;
    }
    
    public function add(Entity_Shop $shop, $typeId, $frequencyString, $amountEnter, $lat, $long)
    {
        $proto = array(
                            'shop_id' => $shop->getId(),
                            'transmitter_type_id' => $typeId,
                            'frequency' => $frequencyString,
                            'amount_enter' => $amountEnter,
                            'lat' => $lat,
                            'longtitude' => $long,
                            'last_modified' => date('Y-m-d'),
                            'active_rule_item_id' => null,
                      );
        return $this->insertEntity($proto);
    } 
    
    public function clearCacheByShopAndType(Entity_Shop $shop, $typeId = null)
    {
        $typeIds = array('',$typeId);
        $hiddens = array(null,true,false);
        
        foreach($typeIds as $thisTypeId)
        {
            foreach($hiddens as $hidden)
            {
                SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByShopAndType($shop,$thisTypeId,$hidden));
            }
        }
    }
}