<?php
/**
 * @method Entity_City getEntityById() getEntityById(int $id)
 */
class Mapper_City extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_City
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'city';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_City
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    
    public function add($name)
    {
        $proto = array(
                        'name' => $name,
                    );
        $entity = $this->insertEntity($proto);

        return $entity;
    }
    
    /** 
     * @return Aggregate_City
     */
    public function getListPlain()
    {
        $sql = "SELECT *
                FROM city";
        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        $aggregate->loadEntities();
        return $aggregate;
    }
}