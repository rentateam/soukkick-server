<?php
class Mapper_Mall2Shop extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Mall2Shop
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'mall2shop';
        $this->uniqueFields = array(array('mall_id','tag_id'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Mall2Shop
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Mall2Shop
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * Wrapper
     * @param Entity_Mall $mall
     * @param Entity_Tag $tag
     * @return Entity_Mall2Shop
     */
    public function getEntityByMallAndTag(Entity_Mall $mall,Entity_Tag $tag)
    {
        return parent::getEntityByUniqueFields(array('mall_id' => $mall->getId(),'tag_id' => $tag->getId()));
    }

    public function add(Entity_Mall $mall, Entity_Tag $tag, Entity_Shop $shop = null, Entity_Mall_Map $map = null)
    {
        $proto = array(
                            'mall_id' => $mall->getId(),
                            'shop_id' => $shop ? $shop->getId() : null,
                            'tag_id' => $tag->getId(),
                            'mall_map_id' => $map->getId()
                      );
        return $this->insertEntity($proto);
    }

    /**
     * @param Entity_Shop $shop
     * @return Entity_Mall2Shop|null
     */
    public function getEntityByShop(Entity_Shop $shop)
    {
        $sql = "SELECT *
                FROM mall2shop
                WHERE shop_id=:shop_id";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('shop_id' => $shop->getId()), null);
        return $aggregate->count() ? $aggregate->current() : null;
    }

    public function getListByMall(Entity_Mall $mall)
    {
        $sql = "SELECT *
                FROM mall2shop
                WHERE mall_id=:mall_id";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_id' => $mall->getId()), null);
        return $aggregate;
    }

    /**
     * Список тэгов ТЦ, привязанных к магазинам
     * @param Entity_Mall $mall
     * @return Aggregate_Mall2Shop
     */
    public function getListBoundByMall(Entity_Mall $mall)
    {
        $sql = "SELECT mall2shop.*
                FROM mall2shop
                INNER JOIN tag ON tag.id = mall2shop.tag_id
                WHERE mall_id = :mall_id
                AND shop_id IS NOT NULL
                ORDER BY tag_id ASC";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_id' => $mall->getId()), null);
        return $aggregate;
    }

    /**
     * Список тэгов ТЦ, не привязанных к магазинам
     * @param Entity_Mall $mall
     * @return Aggregate_Tag
     */
    public function getListUnboundByMall(Entity_Mall $mall)
    {
        $sql = "SELECT mall2shop.*
                FROM mall2shop
                INNER JOIN tag ON tag.id = mall2shop.tag_id
                WHERE mall_id = :mall_id
                AND shop_id IS NULL
                ORDER BY tag.name ASC";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_id' => $mall->getId()), null);
        return $aggregate;
    }

    public function deleteMallTagIds(Entity_Mall $mall,$tagIds)
    {
        $sql = "DELETE
                FROM mall2shop
                WHERE mall_id=?
                AND tag_ids IN (?)";
        $params = array($mall->getId(),$tagIds);
        SFM_DB::getInstance()->delete($sql,$params);
    }

    public function getListByTag(Entity_Tag $tag)
    {
    	$sql = "SELECT *
                FROM mall2shop
                WHERE tag_id = :tag_id";
    	$aggregate = $this->getLoadedAggregateBySQL($sql, array('tag_id' => $tag->getId()), null);
    	return $aggregate;
    }
}