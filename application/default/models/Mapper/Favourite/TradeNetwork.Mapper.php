<?php 

class Mapper_Favourite_TradeNetwork extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Favourite_TradeNetwork
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'favourite_trade_network';
        $this->uniqueFields = array(array('trade_network_id','user_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Favourite_TradeNetwork
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Favourite_TradeNetwork
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    /**
     * @param Entity_User $user
     * @param Entity_Trade_Network $tradeNetwork
     * @return Entity_Favourite_TradeNetwork
     */
    public function getEntityByUserAndTradeNetwork(Entity_User $user, Entity_Trade_Network $tradeNetwork)
    {
        return parent::getEntityByUniqueFields(array('trade_network_id' => $tradeNetwork->getId(),'user_id' => $user->getId()));
    } 
    
    public function add(Entity_User $user, Entity_Trade_Network $tradeNetwork, $sortOrder)
    {
        $proto = array(
                        'user_id' => $user->getId(),
                        'trade_network_id' => $tradeNetwork->getId(), 
                        'sort_order' => $sortOrder, 
                    );
        return $this->insertEntity($proto);
    }
    
    public function getListByUser(Entity_User $user)
    {
        $sql = "SELECT favourite_trade_network.* 
                FROM favourite_trade_network
                LEFT JOIN trade_network ON trade_network_id = trade_network.id
                WHERE user_id=:user_id 
                AND trade_network.is_hidden = 0
                ORDER BY sort_order ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($user);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('user_id' => $user->getId()), $cacheKey);
        return $aggregate;
    }
    
    public function getListByTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        $sql = "SELECT favourite_trade_network.* 
                FROM favourite_trade_network
                LEFT JOIN trade_network ON trade_network_id = trade_network.id
                WHERE trade_network_id=:trade_network_id 
                AND trade_network.is_hidden = 0
                ORDER BY sort_order ASC";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('trade_network_id' => $tradeNetwork->getId()), null);
        return $aggregate;
    }

    public function clearCacheByUser(Entity_User $user)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($user));
    } 
}