<?php

class Mapper_Bpa_Manager extends Mapper_User_Abstract
{
    /**
     * @var Mapper_Bpa_Manager
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'bpa_manager';
        $this->uniqueFields = array(array('email'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Bpa_Manager
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Bpa_Manager
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_Bpa_Main $bpaMain,$email,$password,$name,$surname,$salt)
    {
        $proto = array(
                        'bpa_main_id' => $bpaMain->getId(),
                        'salt'  => $salt,
                        'name' => $name,
                        'surname' => $surname,
                        'email' => $email,
                        'reg_date' => date('Y-m-d H:i:s'),
                        'password' => $password,
                        'is_active' => '1'
                    );
        return $this->insertEntity($proto);
    }

    public function getListByTradeNetwork(Entity_Trade_Network $network)
    {
        $sql = "SELECT bpa_manager.*
                FROM bpa_manager
                LEFT JOIN bpa_main ON bpa_manager.bpa_main_id = bpa_main.id
                LEFT JOIN trade_network ON bpa_main.id = trade_network.bpa_main_id
                WHERE trade_network.id = :trade_network_id";
        $aggregate = $this->getAggregateBySQL($sql, array('trade_network_id' => $network->getId()), null);
        $aggregate->loadEntities();
        return $aggregate;
    }

    public function getListByBpaMain(Entity_Bpa_Main $bpaMain)
    {
        $sql = "SELECT *
                FROM bpa_manager
                WHERE bpa_main_id = :bpa_main_id";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('bpa_main_id' => $bpaMain->getId()), null);
        return $aggregate;
    }
}