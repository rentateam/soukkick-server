<?php 

class Mapper_Bpa_Main extends SFM_Mapper
{
    /**
     * @var Mapper_Bpa_Main
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'bpa_main';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Bpa_Main
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Bpa_Main
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function add($name,$description,$conversionRateToFishki,$conversionRateFromFishki)
    {
        $proto = array(
                        'name' => $name,
                        'description' => $description,
                        'conversion_rate_to_fishki' => $conversionRateToFishki,
                        'conversion_rate_from_fishki' => $conversionRateFromFishki,
                        'fishki_amount' => '0'
                    );
        return $this->insertEntity($proto);
    }
}