<?php 

class Mapper_ScanProduct extends Mapper_ShopsBound implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_ScanProduct
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'scan_product';
        $this->uniqueFields = array(array('barcode','trade_network_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_ScanProduct
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_ScanProduct
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    /**
     * Wrapper
     * 
     * @param string $barcode
     * @param Entity_Trade_Network $tradeNetwork
     * @return Entity_ScanProduct
     */
    public function getEntityByBarcodeAndTradeNetwork($barcode,Entity_Trade_Network $tradeNetwork)
    {
        return parent::getEntityByUniqueFields(array('barcode' => $barcode,'trade_network_id' => $tradeNetwork->getId()));
    }
    
    /**
     * @param Entity_ScanProduct $scanProduct
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_ScanProduct $scanProduct, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $activeRuleItemId = $scanProduct->getActiveRuleItemId();
                $result = $activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($activeRuleItemId) : null;
                break;
            default:
                $result = parent::lazyload($scanProduct, $fieldName);
        }
        return $result;
    }
    
    public function add($name,$description,Entity_Trade_Network $tradeNetwork,$barcode,$price)
    {
        $proto = array(
                            'name' => $name, 
                            'description' => $description,
                            'trade_network_id' => $tradeNetwork->getId(),
                            'barcode'   => $barcode,
                            'price' => $price,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d'),
                            'active_rule_item_id' => null
                        );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    protected function getShopsBoundTable()
    {
        return 'scan_product2shop';
    }
    
    protected function getShopsBoundObjectFieldName()
    {
        return 'scan_product_id';
    }
    
    public function getShopIdsByScanProduct(Entity_ScanProduct $product)
    {
        return $this->getShopIdsByObject($product);
    }
    
    public function getListByShop(Entity_Shop $shop,$pageNumber = 0)
    {
        $sql = "SELECT id 
                FROM scan_product
                LEFT JOIN scan_product2shop ON scan_product.id=scan_product2shop.scan_product_id
                WHERE scan_product2shop.shop_id=:shop_id
                ORDER BY name ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($shop);
        $aggregate = $this->getAggregateBySQL($sql, array('shop_id' => $shop->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }
    
    public function getListByShopIds(array $shopIds)
    {
        $sql = "SELECT id 
                FROM scan_product
                LEFT JOIN scan_product2shop ON scan_product.id=scan_product2shop.scan_product_id
                WHERE scan_product2shop.shop_id IN (".implode(',',$shopIds).")
                ORDER BY name ASC";
        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        $aggregate->loadEntities();
        return $aggregate;
    }
    
    public function clearCacheByShop(Entity_Shop $shop)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($shop));
    }
}