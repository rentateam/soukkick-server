<?php
class Mapper_Mall_Map extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Mall_Map
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'mall_map';
        $this->uniqueFields = array(array('mall_id','floor'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Mall_Map
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Mall_Map
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param Entity_Mall $mall
     * @param int $floor
     * @return Entity_Mall_Map
     */
    public function getEntityByMallAndFloor(Entity_Mall $mall, $floor)
    {
    	return parent::getEntityByUniqueFields(array('mall_id' => $mall->getId(),'floor' => $floor));
    }

    public function add(Entity_Mall $mall,$floor)
    {
        $proto = array(
                            'mall_id' => $mall->getId(),
                            'floor' => $floor,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d H:i:s')
                      );
        return $this->insertEntity($proto);
    }

    public function getListByMall(Entity_Mall $mall)
    {
        $sql = "SELECT *
                FROM mall_map
                WHERE mall_id = :mall_id";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_id' => $mall->getId()), null);
        return $aggregate;
    }
}