<?php 

abstract class Mapper_ShopsBound extends SFM_Mapper
{
    abstract protected function getShopsBoundTable();
    abstract protected function getShopsBoundObjectFieldName();
    
    protected function insertShopIds(SFM_Entity $object,$shopIds)
    {
        $db = SFM_DB::getInstance();
        foreach($shopIds as $shopId)
        { 
            $sql = "INSERT INTO ".$this->getShopsBoundTable()." (".$this->getShopsBoundObjectFieldName().",shop_id) VALUES (:object_id,:shop_id)";
            $params = array('object_id' => $object->getId(),'shop_id' => $shopId);
            $db->insert($sql, $params);
        }
    }
    
    public function replaceShopIds(SFM_Entity $object,$shopIds)
    {
        SFM_DB::getInstance()->delete("DELETE FROM ".$this->getShopsBoundTable()." WHERE ".$this->getShopsBoundObjectFieldName()."=:object_id",array('object_id' => $object->getId()));
        $this->insertShopIds($object,$shopIds);
    } 
    
    protected function insertObjectIds(Entity_Shop $shop, $objectIds)
    {
        $db = SFM_DB::getInstance();
        foreach($objectIds as $objectId)
        { 
            $sql = "INSERT INTO ".$this->getShopsBoundTable()." (".$this->getShopsBoundObjectFieldName().",shop_id) VALUES (:object_id,:shop_id)";
            $params = array('object_id' => $objectId,'shop_id' => $shop->getId());
            $db->insert($sql, $params);
        }
    }
    
    public function replaceObjectIds(Entity_Shop $shop, $objectIds)
    {
        SFM_DB::getInstance()->delete("DELETE FROM ".$this->getShopsBoundTable()." WHERE shop_id=:shop_id",array('shop_id' => $shop->getId()));
        $this->insertObjectIds($shop,$objectIds);
    } 
    
    public function getShopIdsByObject(SFM_Entity $object)
    {
        $db = SFM_DB::getInstance();
        $sql = "SELECT shop_id 
                FROM ".$this->getShopsBoundTable()."
                WHERE ".$this->getShopsBoundObjectFieldName()."=:object_id";
        $params = array('object_id' => $object->getId());
        $result = array(); 
        foreach($db->fetchAll($sql, $params) as $item)
        {
            $result[] = $item['shop_id'];
        }
        return $result;
    }
    
    public function deleteByShop(Entity_Shop $shop)
    {
        SFM_DB::getInstance()->delete("DELETE FROM ".$this->getShopsBoundTable()." WHERE shop_id=:shop_id",array('shop_id' => $shop->getId()));
    } 
    
    public function getListByTradeNetwork(Entity_Trade_Network $tradeNetwork,$pageNumber = 0)
    {
        $sql = "SELECT DISTINCT(".$this->tableName.".id) 
                FROM ".$this->tableName."
                WHERE trade_network_id=:trade_network_id
                ORDER BY ".$this->tableName.".name ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($tradeNetwork);
        $aggregate = $this->getAggregateBySQL($sql, array('trade_network_id' => $tradeNetwork->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }
    
    public function clearCacheByTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($tradeNetwork));
    }
}