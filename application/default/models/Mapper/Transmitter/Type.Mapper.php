<?php 
class Mapper_Transmitter_Type extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Transmitter_Type
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'transmitter_type';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Transmitter_Type
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Transmitter_Type
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
}