<?php 

class Mapper_User_Admin_Type extends Mapper_User_Abstract
{
    /**
     * @var Mapper_User_Admin_Type
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'user_admin_type';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_User_Admin_Type
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_User_Admin_Type
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
}