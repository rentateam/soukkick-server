<?php 

class Mapper_User_Push extends Mapper_User_Abstract
{
    /**
     * @var Mapper_User_Push
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'user_push';
        $this->uniqueFields = array(array('user_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_User_Push
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_User_Push
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function getEntityByUser(Entity_User $user)
    {
        return $this->getEntityByUniqueFileds(array('user_id' => $user->getId()));
    }
    
    public function add(Entity_User $user, $token, $type)
    {
        $proto = array(
                        'user_id' => $user->getId(),
                        'token' => $token,
                        'type' => $type, 
                    );
        return $this->insertEntity($proto);
    }
}