<?php 

abstract class Mapper_User_Abstract extends SFM_Mapper implements SFM_Interface_Singleton
{
    public function getByEmail($email)
    {
        return $this->getEntityByUniqueFileds(array('email' => $email));
    }
    
    public function getEntityByEmail($email)
    {
        return $this->getByEmail($email);
    }
}