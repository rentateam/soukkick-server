<?php 

class Mapper_User_Verify extends Mapper_User_Abstract
{
    /**
     * @var Mapper_User_Verify
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'user_verify';
        $this->uniqueFields = array(array('user_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_User_Verify
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_User_Verify
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function getEntityByUser(Entity_User $user)
    {
        return $this->getEntityByUniqueFileds(array('user_id' => $user->getId()));
    }
    
    public function add(Entity_User $user, $code)
    {
        $proto = array(
                        'user_id' => $user->getId(),
                        'code' => $code,
                    );
        return $this->insertEntity($proto);
    }
}