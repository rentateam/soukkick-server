<?php 

class Mapper_User_Admin extends Mapper_User_Abstract
{
    /**
     * @var Mapper_User_Admin
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'user_admin';
        $this->uniqueFields = array(array('email'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_User_Admin
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_User_Admin
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function getEntityByEmail($email)
    {
        return $this->getEntityByUniqueFileds(array('email' => $email ));
    }
}