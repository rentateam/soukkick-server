<?php 

class Mapper_User_Address extends Mapper_User_Abstract
{
    /**
     * @var Mapper_User_Address
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'user_address';
        $this->uniqueFields = array(array('user_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_User_Address
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_User_Address
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function getEntityByUser(Entity_User $user)
    {
        return $this->getEntityByUniqueFileds(array('user_id' => $user->getId()));
    }
    
    public function add(Entity_User $user, $country, $city, $index, $street, $house, $flat, $phone)
    {
        $proto = array(
                        'user_id' => $user->getId(),
                        'country' => $country,
                        'city' => $city, 
                        'post_index' => $index, 
                        'street' => $street,
                        'house' => $house,
                        'flat' => $flat,
                        'phone' => $phone,
                    );
        return $this->insertEntity($proto);
    }
}