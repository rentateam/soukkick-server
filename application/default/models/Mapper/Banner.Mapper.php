<?php 
class Mapper_Banner extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Banner
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'banner';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Banner
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Banner
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function add($dateExpire)
    {
        $proto = array(
                            'date_expire' => $dateExpire,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d H:i:s')
                      );
        return $this->insertEntity($proto);
    } 
    
    public function getList($pageNum = 0)
    {
        $sql = "SELECT * 
                FROM banner";
        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNum);
        return $aggregate;
    }
}