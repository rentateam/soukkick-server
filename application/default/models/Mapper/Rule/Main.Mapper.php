<?php 
class Mapper_Rule_Main extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Rule_Main
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'rule_main';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Rule_Main
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * @param int $id
     * @return Entity_Rule_Main
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function add(Entity_Bpa_Manager $manager, $name, $dateFrom, $dateTo, $maxSpendAmount)
    {
        $proto = array(
                            'created_bpa_manager_id' => $manager->getId(),
                            'bpa_main_id' => $manager->getBpaMainId(),
                            'name' => $name,
                            'date_from' => $dateFrom,
                            'date_to' => $dateTo,
                            'max_spend_amount' => $maxSpendAmount,
                            'spent_amount' => '0',
                            'is_active' => '0',
                            'is_deleted' => '0',
                            'deleted_bpa_manager_id' => null,
                            'datetime_deleted' => null,
                            'datetime_created' => date('Y-m-d H:i:s'),
                            'is_changed' => 0
                      );
        return $this->insertEntity($proto);
    }
    
    public function clearShopIds(Entity_Rule_Main $rule)
    {
        $sql = "DELETE FROM rule_main2shop WHERE rule_main_id = :rule_main_id";
        return SFM_DB::getInstance()->delete($sql,array('rule_main_id' => $rule->getId()));
    }
    
    public function clearMainRuleIds(Entity_Shop $shop)
    {
        $sql = "DELETE FROM rule_main2shop WHERE shop_id = :shop_id";
        return SFM_DB::getInstance()->delete($sql,array('shop_id' => $shop->getId()));
    }
    
    public function setShopIds(Entity_Rule_Main $rule, array $shopIds)
    {
        $this->clearShopIds($rule);
        foreach($shopIds as $shopId)
        {
            $sql = "INSERT INTO rule_main2shop (rule_main_id,shop_id) VALUES (:rule_main_id,:shop_id)";
            SFM_DB::getInstance()->insert($sql,array('rule_main_id' => $rule->getId(),'shop_id' => $shopId));
        }
    }
    
    public function getShopIds(Entity_Rule_Main $rule)
    {
        $sql = "SELECT shop_id FROM rule_main2shop WHERE rule_main_id = :rule_main_id";
        $shopIdItems = SFM_DB::getInstance()->fetchAll($sql,array('rule_main_id' => $rule->getId()));
        $shopIds = array();
        foreach($shopIdItems as $index => $shopIdItem)
        {
            $shopIds[] = $shopIdItem['shop_id'];
        }
        return $shopIds;
    }
    
    public function getListByBpaMain(Entity_Bpa_Main $bpaMain, $isActive = true)
    {
        $sql = "SELECT * 
                FROM rule_main
                WHERE bpa_main_id = :bpa_main_id";
        $params = array('bpa_main_id' => $bpaMain->getId());
        
        if($isActive !== null)
        {
            $sql.= " AND is_active = :is_active";
            $params['is_active'] = $isActive ? '1' : '0';
        }
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }
    
    public function getListByCriteria(Criteria_Rule_Main $criteria)
    {
        $queryBuilder = new QueryBuilder_Rule_Main($criteria);
        $aggregate = $this->getAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        $aggregate->loadEntities();
        return $aggregate;
    }
    
    public function getListByShop(Entity_Shop $shop)
    {
        $sql = "SELECT * 
                FROM rule_main
                LEFT JOIN rule_main2shop ON rule_main.id = rule_main2shop.rule_main_id
                WHERE shop_id = :shop_id
                AND rule_main.is_active = 1";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('shop_id' => $shop->getId()), null);
        return $aggregate;
    }
    
    public function getListPlain()
    {
        $sql = "SELECT * 
                FROM rule_main";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
        return $aggregate;
    }
}