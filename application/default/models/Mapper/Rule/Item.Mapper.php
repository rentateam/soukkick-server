<?php
class Mapper_Rule_Item extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Rule_Item
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'rule_item';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Rule_Item
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     * @param int $id
     * @return Entity_Rule_Item
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param Entity_Rule_Item $item
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Rule_Item $item, $fieldName)
    {
        $result = null;
        switch($fieldName)
        {
            case 'action_object_id':
                switch($item->getActionType())
                {
                    case Entity_Rule_Item::TYPE_CHECKIN:
                        $result = null;
                        break;
                    case Entity_Rule_Item::TYPE_SCAN:
                        $result = Mapper_ScanProduct::getInstance()->getEntityById($item->getActionObjectId());
                        break;
                    case Entity_Rule_Item::TYPE_SHOPPAGE:
                        $result = Mapper_Trade_Network::getInstance()->getEntityById($item->getActionObjectId());
                        break;
                    case Entity_Rule_Item::TYPE_PRIZE:
                        $result = Mapper_Prize::getInstance()->getEntityById($item->getActionObjectId());
                        break;
                    case Entity_Rule_Item::TYPE_ACHIEVEMENT:
                        $result = Mapper_Achievement::getInstance()->getEntityById($item->getActionObjectId());
                        break;
                    case Entity_Rule_Item::TYPE_SHOPFEE_PROMOCODE:
                        $result = Mapper_Promocode::getInstance()->getEntityById($item->getActionObjectId());
                        break;
                }
                break;
            default:
                $result = parent::lazyload($item, $fieldName);
        }
        return $result;
    }

    public function add(Entity_Rule_Main $rule, $amount, $type, $actionObject = null, $maxSpendAmount = -1)
    {
        $proto = array(
                            'rule_main_id' => $rule->getId(),
                            'amount' => $amount,
                            'action_type' => $type,
                            'action_object_id' => $actionObject ? $actionObject->getId() : null,
                            'is_active' => '1',
                            'is_deleted' => '0',
                            'max_spend_amount' => $maxSpendAmount,
                            'spent_amount' => 0,
                      );
        return $this->insertEntity($proto);
    }

    public function getStatListByRuleMain(Entity_Rule_Main $rule)
    {
        $sql = "SELECT rule_item.id as rule_item_id, SUM(trm_main.amount) as amount, COUNT(trm_main.id) as trm_number
                FROM rule_item
                LEFT JOIN trm_main ON rule_item.id = trm_main.rule_item_id
                WHERE rule_main_id = :rule_main_id
                GROUP BY rule_item.id";
        return SFM_DB::getInstance()->fetchAll($sql,array('rule_main_id' => $rule->getId()));
    }

    public function getListByRuleMain(Entity_Rule_Main $rule, $onlyActive = false)
    {
        $sql = "SELECT *
                FROM rule_item
                WHERE rule_main_id = :rule_main_id
                AND is_deleted = 0";
        if($onlyActive)
            $sql.= ' AND is_active = 1 ';
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('rule_main_id' => $rule->getId()), null);
        return $aggregate;
    }

    public function getListChangedByRuleMain(Entity_Rule_Main $rule)
    {
        $sql = "SELECT *
                FROM rule_item
                WHERE rule_main_id = :rule_main_id
                AND is_changed = 1
                AND is_deleted = 0";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('rule_main_id' => $rule->getId()), null);
        return $aggregate;
    }

    public function getListFullByRuleMainAndActionType(Entity_Rule_Main $rule, $actionType, $actionObject = null, $isDeleted = false)
    {
        $sql = "SELECT *
                FROM rule_item
                WHERE rule_main_id = :rule_main_id
                AND action_type = :action_type";
        $params = array('rule_main_id' => $rule->getId(),'action_type' => $actionType);
        if($isDeleted !== null)
        {
            $sql.= ' AND is_deleted = :is_deleted';
            $params['is_deleted'] = $isDeleted ? '1' : '0';
        }
        if($actionObject !== null)
        {
            $sql.= ' AND action_object_id = :action_object_id';
            $params['action_object_id'] = $actionObject->getId();
        }
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }

    /**
     * Получить последнее правило.
     * Если есть измененное правило - возвращается оно.
     * Если нет - возвращается активное.
     * @param Entity_Rule_Main $rule
     * @param integer $actionType
     * @param object $actionObject
     * @param boolean $isDeleted
     * @return Entity_Rule_Item|null
     */
    public function getLastByRuleMainAndActionType(Entity_Rule_Main $rule, $actionType, $actionObject = null, $isDeleted = false)
    {
        $lastActiveRuleItem = null;
        $itemList = $this->getListFullByRuleMainAndActionType($rule, $actionType, $actionObject, $isDeleted);
        foreach($itemList as $item)
        {
            if($item->isChanged())
                return $item;
            if($item->isActive())
                $lastActiveRuleItem = $item;
        }
        //Если не нашли измененных правил - возвращаем последнее активное.
        return $lastActiveRuleItem;
    }

    /**
     * Получить последнее правило для формы.
     * Если есть измененное правило - возвращается оно.
     * Если нет - ищется последнее (оно должно быть активным или неактивным. Но неактивных после активных не может быть).
     * @param Entity_Rule_Main $rule
     * @param integer $actionType
     * @param object $actionObject
     * @param boolean $isDeleted
     * @return Entity_Rule_Item|null
     */
    public function getLastByRuleMainAndActionTypeForm(Entity_Rule_Main $rule, $actionType, $actionObject = null, $isDeleted = false)
    {
        $item = null;
        $itemList = $this->getListFullByRuleMainAndActionType($rule, $actionType, $actionObject, $isDeleted);
        foreach($itemList as $item)
        {
            if($item->isChanged())
                return $item;
        }
        //Если не нашли измененных правил - возвращаем последнее активное.
        return $item;
    }

    public function getListByShopAndActionType(Entity_Shop $shop, $actionType)
    {
        $sql = "SELECT rule_item.id
                FROM rule_item
                LEFT JOIN rule_main ON rule_item.rule_main_id = rule_main.id
                LEFT JOIN rule_main2shop ON rule_main.id = rule_main2shop.rule_main_id
                WHERE shop_id = :shop_id
                AND action_type = :action_type
                AND rule_item.is_deleted = 0
                AND rule_item.is_active = 1";
        $params = array('shop_id' => $shop->getId(),'action_type' => $actionType);
        $aggregate = $this->getAggregateBySQL($sql, $params, null);
        $aggregate->loadEntities();
        return $aggregate;
    }
}