<?php 
class Mapper_BuyBarcode extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_BuyBarcode
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'buy_barcode';
        $this->uniqueFields = array(array('barcode'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_BuyBarcode
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_BuyBarcode
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    /**
     * @param Entity_BuyBarcode $barcode
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_BuyBarcode $barcode, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $activeRuleItemId = $barcode->getActiveRuleItemId();
                $result = $activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($activeRuleItemId) : null;
                break;
            default:
                $result = parent::lazyload($barcode, $fieldName);
        }
        return $result;
    }
    
    public function getEntityByBarcode($barcode)
    {
        return $this->getEntityByUniqueFields(array('barcode' => $barcode));
    }
    
    protected function getCacheKeyByShopWithActivity(Entity_Shop $shop, $isActive)
    {
        return $this->getAggregateCacheKeyByParentEntity($shop,$isActive ? '1' : '0');
    }
    
    /**
     * Список активных штрихкодов в магазине
     * @param Entity_Shop $shop
     * @param boolean $isActive
     * return Aggregate_BuyBarcode|null
     */
    protected function getListByShopWithActivity(Entity_Shop $shop, $isActive)
    {
        $sql = "SELECT * 
                FROM buy_barcode
                WHERE shop_id = :shop_id
                AND is_active = :is_active";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('shop_id' => $shop->getId(),'is_active' => $isActive ? '1' : '0'), $this->getCacheKeyByShopWithActivity($shop,$isActive));
        return $aggregate;
    }
    
    /**
     * Список неактивных штрихкодов в магазине
     * @param Entity_Shop $shop
     * return Aggregate_BuyBarcode|null
     */
    public function getListInactiveByShop(Entity_Shop $shop)
    {
        return $this->getListByShopWithActivity($shop,false);
    }
    
    /**
     * Список активных штрихкодов в магазине
     * @param Entity_Shop $shop
     * return Aggregate_BuyBarcode|null
     */
    public function getListActiveByShop(Entity_Shop $shop)
    {
        return $this->getListByShopWithActivity($shop,true);
    }
    
    public function getListByShop(Entity_Shop $shop)
    {
        $sql = "SELECT * 
                FROM buy_barcode
                WHERE shop_id = :shop_id";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('shop_id' => $shop->getId()), null);
        return $aggregate;
    }
    
    public function getListByShopIds(array $shopIds)
    {
        if(!empty($shopIds))
        {
            $sql = "SELECT * 
                    FROM buy_barcode
                    WHERE shop_id IN (".implode(',',$shopIds).")";
            $aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
        }
        else
        {
            $aggregate = $this->createAggregate(array());
        }
        return $aggregate;
    }
    
    public function add(Entity_Shop $shop, $barcode)
    {
        $proto = array(
                            'barcode' => $barcode,
                            'shop_id' => $shop->getId(),
                            'is_active' => 1,
                            'counter_date' => null
                      );
        return $this->insertEntity($proto);
    } 
    
    public function clearCacheActiveByShop(Entity_Shop $shop)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByShopWithActivity($shop,true));
        SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByShopWithActivity($shop,false));
    }
}