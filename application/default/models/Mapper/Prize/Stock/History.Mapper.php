<?php
class Mapper_Prize_Stock_History extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Stock_History
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_stock_history';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Stock_History
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function add(Entity_User_Admin $admin,Entity_Prize_Stock $stock,$difference,$comment)
    {
        $proto = array(
                        'user_admin_id' => $admin->getId(),
                        'prize_stock_id' => $stock->getId(),
                        'difference' => $difference,
                        'comment' => $comment,
                        'datetime_created' => date('Y-m-d H:i:s')
                    );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    public function getListByPrizeStock(Entity_Prize_Stock $stock)
    {
        $criteria = new Criteria_Prize_Stock_History();
        $criteria->setPrizeStock($stock);
        return $this->getListByCriteria($criteria);
    }
    
    protected function getListByCriteria(Criteria_Prize_Stock_History $criteria)
    {
        $queryBuilder = new QueryBuilder_Prize_Stock_History($criteria);
        $aggregate = $this->getAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        $aggregate->loadEntities();
        return $aggregate;
    }
}