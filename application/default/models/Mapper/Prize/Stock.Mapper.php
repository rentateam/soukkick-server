<?php
class Mapper_Prize_Stock extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Stock
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_stock';
        $this->uniqueFields = array(array('prize_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Stock
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * @return Entity_Prize_Stock
     * @param Entity_Prize $prize
     */
    public function getEntityByPrize(Entity_Prize $prize)
    {
        return $this->getEntityByUniqueFields(array('prize_id'=>$prize->getId()));
    }
    
    public function add(Entity_Prize $prize)
    {
        $proto = array(
                        'prize_id' => $prize->getId(),
                        'number_available' => 0,
                        'number_delivered' => 0,
                    );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    public function getListByCriteria(Criteria_Prize_Stock $criteria)
    {
        $queryBuilder = new QueryBuilder_Prize_Stock($criteria);
        $aggregate = $this->getAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        if(!$criteria->getPage())
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($criteria->getPage());
        return $aggregate;
    }
}