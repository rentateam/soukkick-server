<?php
class Mapper_Prize_Promo extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Promo
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_promo';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Promo
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function add(Entity_Prize $prize,$code)
    {
        $proto = array(
                        'prize_id' => $prize->getId(),
                        'code' => $code,
                        'is_active' => 1
                      );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    /**
     * @param Entity_Prize $prize
     * @return Entity_Prize_Promo|null
     */
    public function getAnyByPrize(Entity_Prize $prize)
    {
        $sql = "SELECT *
                FROM prize_promo
                WHERE prize_id=:prize_id
                AND is_active = :is_active
                LIMIT 1";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('prize_id' => $prize->getId(),'is_active' => 1), null);
        return $aggregate->totalCount() > 0 ? $aggregate->current() : null;
    }
}