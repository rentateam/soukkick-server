<?php
class Mapper_Prize_Order_History extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Order_History
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_order_history';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Order_History
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function add(Entity_User_Admin $admin,Entity_Prize_Order $order,$detail,$comment)
    {
        $proto = array(
                        'user_admin_id' => $admin->getId(),
                        'prize_order_id' => $order->getId(),
                        'detail' => $detail,
                        'comment' => $comment,
                        'datetime_created' => date('Y-m-d H:i:s')
                    );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    public function getListByPrizeOrder(Entity_Prize_Order $order)
    {
        $criteria = new Criteria_Prize_Order_History();
        $criteria->setPrizeOrder($order);
        return $this->getListByCriteria($criteria);
    }
    
    protected function getListByCriteria(Criteria_Prize_Order_History $criteria)
    {
        $queryBuilder = new QueryBuilder_Prize_Order_History($criteria);
        $aggregate = $this->getAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        $aggregate->loadEntities();
        return $aggregate;
    }
}