<?php
class Mapper_Prize_Order_Status extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Order_Status
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_order_status';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Order_Status
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function getListPlain()
    {
        $sql = "SELECT * 
                FROM prize_order_status";
        $aggregate = $this->getLoadedAggregateBySQL($sql,array(), null);
        return $aggregate;
    }
}