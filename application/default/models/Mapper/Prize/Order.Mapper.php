<?php
class Mapper_Prize_Order extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Prize_Order
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'prize_order';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Prize_Order
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function add(Entity_User $user, Entity_Prize $prize,$userName,$address,$phone,$postIndex,$country,$city)
    {
        $proto = array(
                        'user_id' => $user->getId(),
                        'prize_id' => $prize->getId(),
                        'prize_order_status_id' => Entity_Prize_Order_Status::ID_NEW,
                        'user_name' => $userName,
                        'address' => $address,
                        'phone' => $phone,
                        'post_index' => $postIndex,
                        'country' => $country,
                        'city' => $city,
                        'datetime_created' => date('Y-m-d H:i:s')
                    );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
    
    public function getListByCriteria(Criteria_Prize_Order $criteria)
    {
        $queryBuilder = new QueryBuilder_Prize_Order($criteria);
        $aggregate = $this->getAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        if(!$criteria->getPage())
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($criteria->getPage());
        return $aggregate;
    }
}