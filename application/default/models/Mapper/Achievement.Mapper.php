<?php 
class Mapper_Achievement extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Achievement
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'achievement';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Achievement
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Achievement
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    /**
     * @param Entity_Achievement $achievement
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Achievement $achievement, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $result = Mapper_Rule_Item::getInstance()->getEntityById($achievement->getActiveRuleItemId());
                break;
            default:
                $result = parent::lazyload($achievement, $fieldName);
        }
        return $result;
    }
    
    public function getListActiveByLevel($maxLevel)
    {
        $sql = "SELECT * 
                FROM achievement
                WHERE available_at_level <= :current_level
                AND is_active = 1";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('current_level' => $maxLevel), $cacheKey);
        return $aggregate;
    }
}