<?php
class Mapper_Promocode extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Promocode
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'promocode';
        $this->uniqueFields = array(array('code'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Promocode
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Promocode
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function getEntityByCode($code)
    {
        return parent::getEntityByUniqueFields(array('code' => mb_strtoupper($code)));
    }

    /**
     * @param Entity_Promocode $promocode
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Promocode $promocode, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $activeRuleItemId = $promocode->getActiveRuleItemId();
                $result = $activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($activeRuleItemId) : null;
                break;
            case 'bound_rule_item_id':
                $boundRuleItemId = $promocode->getBoundRuleItemId();
                $result = $boundRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($boundRuleItemId) : null;
                break;
            default:
                $result = parent::lazyload($promocode, $fieldName);
        }
        return $result;
    }

    public function add($name, $code, Entity_Rule_Item $item = null)
    {
        $proto = array(
                            'name' => $name,
                            'code' => $code,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d H:i:s'),
                            'bound_rule_item_id' => $item ? $item->getId() : null,
                            'active_rule_item_id' => null
                      );
        return $this->insertEntity($proto);
    }

    public function getList($pageNum = 0)
    {
        $sql = "SELECT id
                FROM promocode
                ORDER BY id DESC";
        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        if($pageNum)
            $aggregate->loadEntitiesForCurrentPage($pageNum);
        else
            $aggregate->loadEntities();
        return $aggregate;
    }
}