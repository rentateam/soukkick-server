<?php
class Mapper_Settings_Option extends SFM_Mapper implements SFM_Interface_Singleton
{
    const PER_PAGE = 20;

    /**
     * @var Mapper_Settings_Option
     */
    protected static $instance;
    protected $tableName = 'settings_option';
    protected $uniqueFields = array(
        array('slug')
    );

    /**
     * Singleton
     *
     * @return Mapper_Settings_Option
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return Entity_Settings_Option
     * @param string $slug
     */
    public function getEntityBySlug($slug)
    {
        return $this->getEntityByUniqueFields(array('slug' => $slug));
    }

    /**
     * @param null $page
     * @param int $perPage
     * @return Aggregate_Settings_Option
     */
    public function getList($page = NULL, $perPage = Mapper_Settings_Option::PER_PAGE)
    {
        $sql = "SELECT id FROM settings_option";
        $aggregate = $this->getAggregateBySQL($sql);

        is_null($page) ? $aggregate->loadEntities() : $aggregate->loadEntitiesForCurrentPage($page, $perPage);

        return $aggregate;
    }

    public function add($caption, $slug, $data)
    {
        $proto = array(
            'caption' => $caption,
            'slug' => $slug,
            'data' => $data,
        );

        $entity = $this->insertEntity($proto);
        return $entity;
    }
}