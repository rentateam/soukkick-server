<?php
class Mapper_Mall extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Mall
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'mall';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Mall
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Mall
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add($name,$address,$lat,$long)
    {
        $proto = array(
                            'name' => $name,
                            'address' => $address,
                            'lat' => $lat,
                            'longtitude' => $long,
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d H:i:s')
                      );
        return $this->insertEntity($proto);
    }

    public function getList($pageNum = 0)
    {
        $sql = "SELECT *
                FROM mall";
        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNum);
        return $aggregate;
    }
}