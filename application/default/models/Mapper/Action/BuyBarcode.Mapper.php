<?php

class Mapper_Action_BuyBarcode extends Mapper_Action_Transmitterred implements SFM_Interface_Singleton
{
    protected static function getFieldName()
    {
        return 'buy_barcode_id';
    }

    /**
     * @var Mapper_Action_BuyBarcode
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_buy_barcode';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_BuyBarcode
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_BuyBarcode
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_User $user,Entity_BuyBarcode $barcode,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'user_id' => $user->getId(),
                            'shop_id' => $barcode->getShopId(),
                            'trm_main_id' => $transaction->getId(),
                            'buy_barcode_id' => $barcode->getId(),
                            'date_created'  => $dateCreated,
                            'datetime_created'  => $dateCreated.' '.$timeCreated
                        );
        return $this->insertEntity($proto);
    }

    public function getListByTradeNetworkAndUserForDate(Entity_Trade_Network $tradeNetwork, Entity_User $user, $dateTime)
    {
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE shop_id IN
                (
                    SELECT shop.id
                    FROM shop
                    WHERE trade_network_id = :trade_network_id
                )
                AND user_id =:user_id
                AND date_created = :date_created
                ORDER BY id DESC";
        $params = array('trade_network_id' => $tradeNetwork->getId(),'user_id' => $user->getId(),'date_created' => $dateTime);
        $cacheKey = $this->getAggregateCacheKeyByParentAndChildEntity($tradeNetwork,$user,$dateTime);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate;
    }

    public function clearCacheByTradeNetworkAndUserForDate(Entity_Trade_Network $tradeNetwork,Entity_User $user, $dateTime)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentAndChildEntity($tradeNetwork,$user,$dateTime));
    }
}