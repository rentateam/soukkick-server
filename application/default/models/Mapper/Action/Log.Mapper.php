<?php

class Mapper_Action_Log extends SFM_Mapper
{
    /**
     * @var Mapper_Action_Log
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_log';
        $this->uniqueFields = array(/*array('user_id','adate','atime','action_type')*/);
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_Log
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_Log
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_User $user, $actionType, $lat, $long, $date, $time, $ip, Entity_Rule_Item $activeRuleItem = null, $objectId = null, $param1 = null, $param2 = null)
    {
        $proto = array(
                            'date_created' => date('Y-m-d H:i:s'),
                            'user_id' => $user->getId(),
                            'action_type' => $actionType,
                            'alat' => $lat,
                            'alon' => $long,
                            'adate' => $date,
                            'atime' => $time,
                            'active_rule_item_id' => $activeRuleItem ? $activeRuleItem->getId() : null,
                            'ip' => $ip,
                            'object_id' => $objectId,
                            'param1' => $param1,
                            'param2' => $param2,
                            'result_type' => null,
                            'result_code' => null,
                            'trm_main_id' => null,
                        );
        return $this->insertEntity($proto);
    }

    public function getEntityByTrmKey(Entity_User $user, $actionDate, $actionTime, $actionType)
    {
        /*$fields = array(
            'user_id' => $user->getId(),
            'adate' => $actionDate,
            'atime' => $actionTime,
            'action_type' => $actionType,
        );
        return $this->getEntityByUniqueFields($fields);*/

        $sql = "SELECT *
                FROM action_log
                WHERE user_id = :user_id
                AND adate = :adate
                AND atime = :atime
                AND action_type = :action_type";
        $params = array(
                        'user_id' => $user->getId(),
                        'adate' => $actionDate,
                        'atime' => $actionTime,
                        'action_type' => $actionType,
        );
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate->count() ? $aggregate->current() : null;
    }
}