<?php

class Mapper_Action_Shoppage extends Mapper_Action_Priced implements SFM_Interface_Singleton
{
    protected static function getFieldName()
    {
        return 'trade_network_id';
    }

    /**
     * @var Mapper_Action_Shoppage
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_shoppage';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_Shoppage
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_Shoppage
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_User $user,Entity_Trade_Network $tradeNetwork,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'user_id' => $user->getId(),
                            'trade_network_id' => $tradeNetwork->getId(),
                            'trm_main_id' => $transaction->getId(),
                            'date_created'  => $dateCreated,
                            'datetime_created'  => $dateCreated.' '.$timeCreated,
                        );
        return $this->insertEntity($proto);
    }
}