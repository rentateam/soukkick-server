<?php

abstract class Mapper_Action_Priced extends Mapper_Action_Abstract
{
    protected function __construct()
    {
        $this->uniqueFields = array_merge($this->uniqueFields,array(array('trm_main_id')));
        parent::__construct();
    }

    public function getEntityByTrmMain(Entity_Trm_Main $trmMain)
    {
        return $this->getEntityByUniqueFields(array('trm_main_id' => $trmMain->getId()));
    }
}