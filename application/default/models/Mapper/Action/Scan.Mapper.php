<?php

class Mapper_Action_Scan extends Mapper_Action_Priced implements SFM_Interface_Singleton
{
    protected static function getFieldName()
    {
        return 'scan_product_id';
    }

    /**
     * @var Mapper_Action_Scan
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_scan';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_Scan
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_Scan
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_User $user,Entity_ScanProduct $product,Entity_Shop $shop,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'user_id' => $user->getId(),
                            'scan_product_id' => $product->getId(),
                            'shop_id' => $shop->getId(),
                            'trm_main_id' => $transaction->getId(),
                            'date_created'  => $dateCreated,
                            'datetime_created'  => $dateCreated.' '.$timeCreated,
                        );
        return $this->insertEntity($proto);
    }
}