<?php

class Mapper_Action_Enter extends Mapper_Action_Transmitterred implements SFM_Interface_Singleton
{
    protected static function getFieldName()
    {
        return 'shop_id';
    }

    /**
     * @var Mapper_Action_Enter
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_enter';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_Enter
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_Enter
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_User $user,Entity_Shop $shop,$type, $dateCreated = null, $timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'user_id' => $user->getId(),
                            'shop_id' => $shop->getId(),
                            'transmitter_type_id' => $type,
                            'date_created'  => $dateCreated,
                            'datetime_created'  => $dateCreated.' '.$timeCreated,
                        );
        return $this->insertEntity($proto);
    }

    public function getLastEnter(Entity_User $user,Entity_Shop $shop,$type)
    {
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                AND user_id =:user_id
                AND transmitter_type_id >= :transmitter_type_id
                ORDER BY id DESC
                LIMIT 1";
        $params = array('object_id' => $shop->getId(),'user_id' => $user->getId(),'transmitter_type_id' => $type);
        $cacheKey = $this->getAggregateCacheKeyByParentAndChildEntity($shop,$user,$type);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate->current();
    }

    public function getAnyLastEnter(Entity_User $user)
    {
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE user_id =:user_id
                ORDER BY id DESC
                LIMIT 1";
        $params = array('user_id' => $user->getId());
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate->current();
    }

    public function clearCacheLastEnter(Entity_User $user,Entity_Shop $shop,$type)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentAndChildEntity($shop,$user,$type));
    }
}