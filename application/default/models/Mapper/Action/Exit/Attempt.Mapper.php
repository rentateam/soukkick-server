<?php 

class Mapper_Action_Exit_Attempt extends SFM_Mapper
{
    /**
     * @var Mapper_Action_Exit_Attempt
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'action_exit_attempt';
        $this->uniqueFields = array(array('shop_id','transmitter_type_id','user_id'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Action_Exit_Attempt
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Action_Exit_Attempt
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    /**
     * @param Entity_User $user
     * @param Entity_Shop $shop
     * @param integer $type
     * @return Entity_Action_Exit_Attempt
     */
    public function getByTradeNetworkAndTransmitterType(Entity_User $user,Entity_Shop $shop,$type)
    {
        return parent::getEntityByUniqueFields(array('user_id' => $user->getId(),'shop_id' => $shop->getId(),'transmitter_type_id' => $type));
    }
    
    public function add(Entity_User $user,Entity_Shop $shop,$type)
    {
        $proto = array(
                            'user_id' => $user->getId(),
                            'shop_id' => $shop->getId(),
                            'transmitter_type_id' => $type,
                            'datetime_created'  => date('Y-m-d H:i:s'),
                        );
        return $this->insertEntity($proto);
    }
    
    /**
     * Дата начала "окна" просмотра попыток.
     */
    protected static function getBeginDate($now)
    {
        return date('Y-m-d H:i:s',$now - Entity_Action_Exit_Attempt::TIME_CONVERT_ATTEMPT);
    }
    
    /**
     * Список попыток чекаутов, срок действия которых больше $beginDate-даты. Не кэшируется!
     * @param integer|null $beginTimestamp
     */
    public function getListOld($beginTimestamp = null)
    {
        if($beginTimestamp === null)
        {
            $beginTimestamp = time();
        }
        $sql = "SELECT shop_id,user_id, transmitter_type_id
                FROM ".$this->tableName."
                WHERE datetime_created <= :datetime_created
                GROUP BY shop_id, user_id, transmitter_type_id";
        $params = array('datetime_created' => self::getBeginDate($beginTimestamp));
        return SFM_DB::getInstance()->fetchAll($sql,$params);
    }
    
    public function clearOldByUserAndShop(Entity_User $user, Entity_Shop $shop,$type,$beginTimestamp = null)
    {
        return $this->clearByUserAndShop(true,$user,$shop,$type,$beginTimestamp);
    }
    
    public function clearNewByUserAndShop(Entity_User $user, Entity_Shop $shop,$type,$beginTimestamp = null)
    {
        return $this->clearByUserAndShop(false,$user,$shop,$type,$beginTimestamp);
    }
    
    protected function clearByUserAndShop($old,Entity_User $user, Entity_Shop $shop,$type,$beginTimestamp = null)
    {
        if($beginTimestamp === null)
        {
            $beginTimestamp = time();
        }
        $sql = "DELETE FROM ".$this->tableName." 
                WHERE user_id=:user_id
                AND shop_id=:shop_id
                AND transmitter_type_id=:transmitter_type_id
                AND datetime_created ".($old ? '<' : '>')."= :datetime_created";
        $params = array(
                            'user_id' => $user->getId(),
                            'shop_id' => $shop->getId(),
                            'transmitter_type_id' => $type,
                            'datetime_created' => self::getBeginDate($beginTimestamp),
                        );
        SFM_DB::getInstance()->delete($sql, $params);
    }
    
    public function clearByShop(Entity_Shop $shop)
    {
        $sql = "DELETE FROM ".$this->tableName." 
                WHERE shop_id=:shop_id";
        $params = array(
                            'shop_id' => $shop->getId(),
                        );
        SFM_DB::getInstance()->delete($sql, $params);
    }
}