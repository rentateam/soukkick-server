<?php

abstract class Mapper_Action_Transmitterred extends Mapper_Action_Priced
{
    public function getListByObjectAndUserAndTypeForDate(Interface_Actionable $object, Entity_User $user, $type = null, $dateTime = null)
    {
        if(!$dateTime)
            $dateTime = date('Y-m-d');
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                AND user_id =:user_id
                AND date_created = :date_created";
        $params = array('object_id' => $object->getId(),'user_id' => $user->getId(),'date_created' => $dateTime);
        $postfix = '';
        if($type !== null)
        {
            $sql.= " AND transmitter_type_id=:transmitter_type_id ";
            $params['transmitter_type_id'] = $type;
            $postfix = $type;
        }
        $sql.= " ORDER BY id DESC";
        $cacheKey = $this->getAggregateCacheKeyByParentAndChildEntity($object,$user,$dateTime.$postfix);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate;
    }

    public function clearCacheByObjectAndUserAndTypeForDate(Interface_Actionable $object,Entity_User $user,$type = null, $dateTime = null)
    {
        if(!$dateTime)
            $dateTime = date('Y-m-d');
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentAndChildEntity($object,$user,$dateTime.$type));
    }
}