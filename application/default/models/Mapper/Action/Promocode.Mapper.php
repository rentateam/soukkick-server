<?php

class Mapper_Action_Promocode extends Mapper_Action_Priced implements SFM_Interface_Singleton
{
    protected static function getFieldName()
    {
        return 'promocode_id';
    }

    /**
     * @var Mapper_Action_Promocode
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'action_promocode';
        $this->uniqueFields = array(array('user_id','promocode_id'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Action_Promocode
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Action_Promocode
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function getEntityByUserAndPromocode(Entity_User $user, Entity_Promocode $promocode)
    {
        return parent::getEntityByUniqueFields(array('user_id' => $user->getId(),'promocode_id' => $promocode->getId()));
    }

    public function add(Entity_User $user,Entity_Promocode $barcode,Entity_Trm_Main $transaction, $dateCreated = null, $timeCreated = null)
    {
        if(!$dateCreated)
            $dateCreated = date('Y-m-d');
        if(!$timeCreated)
            $timeCreated = date('H:i:s');
        $proto = array(
                            'user_id' => $user->getId(),
                            'trm_main_id' => $transaction->getId(),
                            'promocode_id' => $barcode->getId(),
                            'date_created'  => $dateCreated,
                            'datetime_created'  => $dateCreated.' '.$timeCreated
                        );
        return $this->insertEntity($proto);
    }
}