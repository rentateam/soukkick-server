<?php

abstract class Mapper_Action_Abstract extends SFM_Mapper
{
    protected static abstract function getFieldName();

    public function getListByUser(Entity_User $user, $pageNumber = 0)
    {
        $sql = "SELECT id
                FROM ".$this->tableName."
                WHERE user_id=:user_id
                ORDER BY id DESC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($user);
        $aggregate = $this->getAggregateBySQL($sql, array('user_id' => $user->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }

    public function getListByObject(Interface_Actionable $object, $pageNumber = 0)
    {
        $sql = "SELECT id
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                ORDER BY id DESC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($object);
        $aggregate = $this->getAggregateBySQL($sql, array('object_id' => $object->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }

    public function getListByObjectAndUserForDate(Interface_Actionable $object, Entity_User $user, $dateTime)
    {
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                AND user_id =:user_id
                AND date_created = :date_created
                ORDER BY id DESC";
        $params = array('object_id' => $object->getId(),'user_id' => $user->getId(),'date_created' => $dateTime);
        $cacheKey = $this->getAggregateCacheKeyByParentAndChildEntity($object,$user,$dateTime);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate;
    }

    /**
     * Статистика по действиям за последние $dayNumber дней. Не кэшируется.
     * @param array $objectIds - массив id объектов
     * @param string  $fromDate
     * @param string  $toDate
     *
     * @return array
     */
    public function getListByObjectsForDateInterval(array $objectIds, $fromDate, $toDate)
    {
        $sql = "SELECT COUNT(*) as number,DATE(date_created) as stat_date
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." IN (".implode(',',$objectIds).")
                AND date_created >= :date_created_from
                AND date_created <= :date_created_to
                GROUP BY DATE(date_created)
                ORDER BY DATE(date_created) ASC";
        $fromDateTimestamp = strtotime($fromDate);
        $toDateTimestamp = strtotime($toDate);
        $params = array('date_created_from' => date('Y-m-d',$fromDateTimestamp),'date_created_to' => date('Y-m-d',$toDateTimestamp));
        $db = SFM_DB::getInstance();
        $result = $db->fetchAll($sql,$params);
        $return = array();

        $dayNumber = ($toDateTimestamp - $fromDateTimestamp)/(24*60*60);
        for($i=0;$i<=$dayNumber;$i++)
        {
            $return[date('Y-m-d',$toDateTimestamp - $i*24*60*60)] = 0;
        }
        foreach($result as $item)
        {
            $return[$item['stat_date']] = $item['number'];
        }
        return $return;
    }

    /**
     * Количество начисленных баллов за последние $dayNumber дней. Не кэшируется.
     * @param array $shopIds - массив id магазинов
     * @param string  $fromDate
     * @param string  $toDate
     *
     * @return array
     */
    public function getPriceListByShopsForDateInterval(array $shopIds, $fromDate, $toDate)
    {
        $sql = "SELECT SUM(price) as price,DATE(date_created) as stat_date
                FROM ".$this->tableName."
                WHERE shop_id IN (".implode(',',$shopIds).")
                AND date_created >= :date_created_from
                AND date_created <= :date_created_to
                GROUP BY DATE(date_created)
                ORDER BY DATE(date_created) ASC";
        $fromDateTimestamp = strtotime($fromDate);
        $toDateTimestamp = strtotime($toDate);
        $params = array('date_created_from' => date('Y-m-d',$fromDateTimestamp),'date_created_to' => date('Y-m-d',$toDateTimestamp));
        $db = SFM_DB::getInstance();
        $result = $db->fetchAll($sql,$params);
        $return = array();

        $dayNumber = ($toDateTimestamp - $fromDateTimestamp)/(24*60*60);
        for($i=0;$i<=$dayNumber;$i++)
        {
            $return[date('Y-m-d',time() - $i*24*60*60)] = 0;
        }
        foreach($result as $item)
        {
            $return[$item['stat_date']] = $item['price'];
        }
        return $return;
    }

    /**
     * Кол-во разных пользователей за заданное время, которые сделали данное действие
     * @param array $objectIds array of Interface_Actionable ids
     * @param integer $minutesAgo
     * @param integer $minutesLength
     */
    public function getCountUsersByObjectAndMinutes(array $objectIds, $minutesAgo, $minutesLength)
    {
        $sql = "SELECT COUNT(DISTINCT user_id) as user_number
                FROM ".$this->tableName."
                WHERE date_created >= :date_created_begin
                AND date_created < :date_created_end
                ";
        if(!empty($objectIds))
        {
            $sql.= " AND ".static::getFieldName()."  IN (".implode(',',$objectIds).")";
        }

        $params = array(
                            'date_created_begin' => date('Y-m-d',time() - $minutesAgo*60*24*60*60),
                            'date_created_end' => date('Y-m-d',time() - $minutesAgo*60*24*60*60 + $minutesLength*60)
                        );
        $db = SFM_DB::getInstance();
        $result = $db->fetchValue($sql,$params);
        return $result['user_number'];
    }

    /**
     * Количество пользователей по массиву id объектов $objectIds за последние $daysAgo дней, которые сделали это действие впервые.
     * @param array $objectIds array of Interface_Actionable ids
     * @param integer $minutesAgo
     * @param integer $minutesLength
     */
    public function getCountNewUsersByObjectAndMinutes(array $objectIds,$minutesAgo, $minutesLength)
    {
        if(empty($objectIds))
            return 0;
        $sql = "SELECT COUNT(DISTINCT user_id) as user_number
                FROM ".$this->tableName." as first_table
                WHERE ".static::getFieldName()." IN (".implode(',',$objectIds).")
                AND datetime_created >= :date_created_begin
                AND datetime_created < :date_created_end
                AND user_id IN
                (
                    SELECT user_id
                    FROM ".$this->tableName."
                    WHERE ".static::getFieldName()." = first_table.".static::getFieldName()."
                    GROUP BY user_id
                    HAVING COUNT(*) = 1
                )";

        $params = array(
                            'date_created_begin' => date('Y-m-d',time() - $minutesAgo*60*24*60*60),
                            'date_created_end' => date('Y-m-d',time() - $minutesAgo*60*24*60*60 + $minutesLength*60)
                        );
        $db = SFM_DB::getInstance();
        $result = $db->fetchValue($sql,$params);
        return $result['user_number'];
    }

    /**
     * Разные пользователи за заданное время, которые сделали данное действие.
     * @param array $objectIds array of Interface_Actionable ids
     * @param integer $daysAgo
     */
    public function getUserIdsByObjectAndDays(array $objectIds,$daysAgo)
    {
        if(empty($objectIds))
            return array();
        $userIds = array();
        $sql = "SELECT DISTINCT user_id
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." IN (".implode(',',$objectIds).")
                AND date_created >= :date_created_begin
                AND date_created < :date_created_end";

        $params = array(
                            'date_created_begin' => date('Y-m-d 00:00:00',time() - $daysAgo*24*60*60),
                            'date_created_end' => date('Y-m-d 23:59:59',time() - $daysAgo*24*60*60)
                        );
        $db = SFM_DB::getInstance();
        foreach($db->fetchAll($sql,$params) as $item)
        {
            $userIds[] = $item['user_id'];
        }
        return $userIds;
    }

    public function getListByObjectAndDays(Interface_Actionable $object,$daysAgo)
    {
        $userIds = array();
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                AND date_created >= :date_created_begin
                AND date_created < :date_created_end";

        $params = array(
                            'object_id' => $object->getId(),
                            'date_created_begin' => date('Y-m-d 00:00:00',time() - $daysAgo*24*60*60),
                            'date_created_end' => date('Y-m-d 23:59:59',time() - $daysAgo*24*60*60)
                        );
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }

    public function getListByShopFromDate(Entity_Shop $shop,$fromDate)
    {
        $sql = "SELECT *
                FROM ".$this->tableName."
                WHERE ".static::getFieldName()." =:object_id
                AND datetime_created >= :datetime_created";
        $params = array('object_id' => $shop->getId(),'datetime_created' => $fromDate);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, null);
        return $aggregate;
    }

    public function clearCacheByUser(Entity_User $user)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($user));
    }

    public function clearCacheByObject(Interface_Actionable $object)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($object));
    }

    public function clearCacheByObjectAndUserForDate(Interface_Actionable $object,Entity_User $user, $dateTime)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentAndChildEntity($object,$user,$dateTime));
    }
}