<?php
class Mapper_Prize extends SFM_Mapper implements SFM_Interface_Singleton
{
    const LIST_STOCK = 'List_Stock';

    /**
     * @var Mapper_Prize
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'prize';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Prize
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Prize
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param Entity_Prize $prize
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Prize $prize, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $result = Mapper_Rule_Item::getInstance()->getEntityById($prize->getActiveRuleItemId());
                break;
            default:
                $result = parent::lazyload($prize, $fieldName);
        }
        return $result;
    }

    public function getList($pageNum = 0)
    {
        $sql = "SELECT *
                FROM prize
                ORDER BY price ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null);
        $aggregate = $this->getAggregateBySQL($sql, array(), $cacheKey);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNum);
        return $aggregate;
    }

    public function getListStock($pageNum = 0, $perPage = null)
    {
        $sql = "SELECT prize.id
                FROM prize
                LEFT JOIN prize_stock ON prize.id = prize_stock.prize_id
                WHERE (number_available > 0 AND type = :prize_type_own) OR type = :prize_type_promo
                ORDER BY price ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::LIST_STOCK);
        $aggregate = $this->getAggregateBySQL($sql, array('prize_type_own' => Entity_Prize::TYPE_OWN,'prize_type_promo' => Entity_Prize::TYPE_PROMO), $cacheKey);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
        {
            $perPage = $perPage ? $perPage : Aggregate_Prize::ITEMS_PER_PAGE;
            $aggregate->loadEntitiesForCurrentPage($pageNum,$perPage);
        }
        return $aggregate;
    }

    public function add($type, $name, $description, $price)
    {
        $proto = array(
                            'type' => $type,
                            'name' => $name,
                            'description' => $description,
                            'price' => $price,
                            'picture_path' => '',
                            'is_active' => 1,
                            'last_modified' => date('Y-m-d H:i:s')
                      );
        return $this->insertEntity($proto);
    }

    public function clearCacheGeneral()
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null));
    }

    public function clearCacheStock()
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::LIST_STOCK));
    }
}