<?php
class Mapper_Trm_Main extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Trm_Main
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'trm_main';
        $this->uniqueFields = array(/*array('user_id','action_date','action_time',)*/);
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Trm_Main
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     * @param int $id
     * @return Entity_Trm_Main
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add(Entity_Rule_Item $ruleItem, Entity_User $user, $amount, $actionDate, $actionTime, $actionLat, $actionLong, Entity_Shop $shop = null)
    {
        $proto = array(
                            'rule_item_id' => $ruleItem->getId(),
                            'user_id' => $user->getId(),
                            'amount' => $amount,
                            'shop_id' => $shop ? $shop->getId() : null,
                            'date_created' => date('Y-m-d H:i:s'),
                            'action_date' => $actionDate,
                            'action_time' => $actionTime,
                            'action_lat' => $actionLat,
                            'action_long' => $actionLong,
                            'trm_main_status_id' => Entity_Trm_Main_Status::ID_CONFIRMED
                      );
        return $this->insertEntity($proto);
    }

    public function getListByUser(Entity_User $user,$pageNum = 0,$perPage = 20)
    {
        $sql = "SELECT id
                FROM trm_main
                WHERE user_id = :user_id
                ORDER BY id DESC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($user);
        $aggregate = $this->getAggregateBySQL($sql, array('user_id' => $user->getId()), null);
        if($pageNum != 0)
        {
            $aggregate->loadEntitiesForCurrentPage($pageNum,$perPage);
        }
        else
        {
            $aggregate->loadEntities();
        }
        return $aggregate;
    }

    public function getListByCriteria(Criteria_Trm_Main $criteria)
    {
        $queryBuilder = new QueryBuilder_Trm_Main($criteria);
        $aggregate = $this->getLoadedAggregateBySQL($queryBuilder->getSQL(), $queryBuilder->getParams(), null);
        return $aggregate;
    }

    public function getNumberByRuleItem(Entity_Rule_Item $item)
    {
        $sql = "SELECT COUNT(*)
                FROM trm_main
                WHERE rule_item_id = :rule_item_id";
        return SFM_DB::getInstance()->fetchValue($sql,array('rule_item_id' => $item->getId()));
    }

    public function getTodayTotalAmount()
    {
        $dateFrom = new DateTime('now');
        $dateFrom->setTime(0, 0, 0);

        $dateTo = clone $dateFrom;
        $dateTo = $dateTo->modify("+1 day");

        $sql = "
          SELECT SUM(amount) as sum
          FROM trm_main
          WHERE amount > 0
            AND action_date >= '" . $dateFrom->format("Y-m-d") . "'
            AND action_date <  '" . $dateTo->format("Y-m-d") . "'
        ";

        return (int) SFM_DB::getInstance()->fetchValue($sql);
    }

    public function clearCacheByUser(Entity_User $user)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($user));
    }

}