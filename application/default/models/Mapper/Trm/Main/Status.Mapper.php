<?php
class Mapper_Trm_Main_Status extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Trm_Main_Status
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'trm_main_status';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Trm_Main_Status
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     * @param int $id
     * @return Entity_Trm_Main_Status
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
}