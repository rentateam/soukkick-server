<?php
class Mapper_Report extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Report
     */
    protected static $instance;
    protected $tableName = 'report';

    /**
     * Singleton
     *
     * @return Mapper_Report
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Report
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function add($caption, $query)
    {
        $proto = array(
            'caption' => $caption,
            'query' => $query
        );

        $entity = $this->insertEntity($proto);
        return $entity;
    }

    /**
     * @return Aggregate_Report
     */
    public function getList()
    {
        $sql = "SELECT id FROM report";
        $aggregate = $this->getAggregateBySQL($sql);
        return $aggregate;
    }
}