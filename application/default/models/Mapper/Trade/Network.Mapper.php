<?php

class Mapper_Trade_Network extends Mapper_User_Abstract
{
    const LIST_RECOMMENDED = 'Recommended';

    /**
     * @var Mapper_Trade_Network
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'trade_network';
        $this->uniqueFields = array(array('email'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Trade_Network
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Trade_Network
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Trade_Network
     */
    public function getEntityByEmail($email)
    {
        return parent::getEntityByUniqueFields(array('email' => $email));
    }

    /**
     * @param Entity_Trade_Network $tradeNetwork
     * @param string $fieldName
     * @return mixed
     * @throws SFM_Exception_LazyLoad
     */
    public function lazyload(Entity_Trade_Network $tradeNetwork, $fieldName)
    {
        $result = null;
        switch ($fieldName) {
            case 'active_rule_item_id':
                $activeRuleItemId = $tradeNetwork->getActiveRuleItemId();
                $result = $activeRuleItemId ? Mapper_Rule_Item::getInstance()->getEntityById($activeRuleItemId) : null;
                break;
            default:
                $result = parent::lazyload($tradeNetwork, $fieldName);
        }
        return $result;
    }

    public function add(Entity_Bpa_Main $bpaMain,$title,$description,$phone,$isActive,$shopInfo,$amountPage,$isRecommended)
    {
        $proto = array(
                        'bpa_main_id' => $bpaMain->getId(),
                        'title' => $title,
                        'description' => $description,
                        'picture_path' => '',
                        'rectangle_picture_path' => '',
                        'phone' => $phone,
                        'is_active' => $isActive ? '1' : '0',
                        'shop_info' => $shopInfo,
                        'amount_page' => $amountPage,
                        'is_recommended' => $isRecommended,
                        'reg_date' => date('Y-m-d H:i:s'),
                        'last_modified' => date('Y-m-d H:i:s'),
                        'active_rule_item_id' => null,
                        'is_deleted' => 0,
                        'is_hidden' => 0
                    );
        return $this->insertEntity($proto);
    }

    /**
     * Only for admin - no caching
     */
    public function getListForApprove()
    {
        $sql = "SELECT *
                FROM trade_network
                WHERE is_active = 0
                ORDER BY reg_date DESC";
        $cacheKey = null;
        $aggregate = $this->getLoadedAggregateBySQL($sql, array(), $cacheKey);
        return $aggregate;
    }

    public function getList($pageNum = 0, $name = NULL, $perPage = 30, $showHidden = false)
    {
        $sql = "SELECT *
                FROM trade_network
                WHERE is_deleted = 0";

        if(!$showHidden)
        {
            $sql.= " AND is_hidden = 0 ";
        }

        if ($name) {
            $name = SFM_DB::getInstance()->getAdapter()->quote("%{$name}%");
            $sql .= " AND title LIKE {$name}";
        }

        $aggregate = $this->getAggregateBySQL($sql, array(), null);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNum, $perPage);
        return $aggregate;
    }

    public function getListByBpaMain(Entity_Bpa_Main $bpaMain)
    {
        $sql = "SELECT *
                FROM trade_network
                WHERE bpa_main_id=:bpa_main_id
                AND is_deleted = 0";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('bpa_main_id' => $bpaMain->getId()), null);
        return $aggregate;
    }

    public function getListRecommended()
    {
       $sql = "SELECT *
               FROM trade_network
               WHERE is_recommended = 1
               AND trade_network.is_hidden = 0
               AND is_deleted = 0";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::LIST_RECOMMENDED);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array(), $cacheKey);
        return $aggregate;
    }

    public function getListByText($text)
    {
       $sql = "SELECT trade_network.*
               FROM trade_network
               LEFT JOIN shop ON trade_network.id = shop.trade_network_id
               WHERE title like :usershoptext
               AND trade_network.is_deleted = 0
               AND trade_network.is_hidden = 0
               GROUP BY trade_network_id
               HAVING COUNT(shop.id) > 0
               ORDER BY title";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('usershoptext' => '%'.$text.'%'), null);
        return $aggregate;
    }

     public function clearCacheRecommended()
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::LIST_RECOMMENDED));
    }
}