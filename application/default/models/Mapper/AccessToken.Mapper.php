<?php
class Mapper_AccessToken extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_AccessToken
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'access_token';
        $this->uniqueFields = array(array('token'));
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_AccessToken
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function getEntityByToken($token)
    {
        return $this->getEntityByUniqueFields(array('token' => $token));
    }
    
    public function add($token,Entity_Trade_Network $network, Entity_Shop $shop = null)
    {
        $proto = array(
                        'token' => $token,
                        'trade_network_id' => $network->getId(),
                        'shop_id' => $shop ? $shop->getId() : null,
                    );
        $entity = $this->insertEntity($proto);
        return $entity;
    }
}