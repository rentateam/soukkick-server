<?php
class Mapper_Shop extends SFM_Mapper implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Shop
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'shop';
        $this->uniqueFields = array();
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Shop
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_Shop
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    public function getList($pageNum = 0, $perPage = Aggregate_Shop::ITEMS_PER_PAGE)
    {
        $sql = "SELECT *
                FROM shop
                WHERE is_deleted = 0";
        $params = array();
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null);
        $aggregate = $this->getAggregateBySQL($sql, array(), $cacheKey);
        if($pageNum == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNum,$perPage);
        return $aggregate;
    }

    /**
     * Полный список магазинов, отдаваемых на клиент
     * @return Aggregate_Shop
     */
    public function getFullClientList()
    {
        $sql = "SELECT *
                FROM shop
                WHERE is_deleted = 0
                AND is_hidden = 0";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
        return $aggregate;
    }

    protected function getCacheKeyByTradeNetwork(Entity_Trade_Network $tradeNetwork, $isHidden)
    {
        $postfix = '';
        if($isHidden !== null)
        {
            $postfix = $isHidden ? '1' : '0';
        }
        return $this->getAggregateCacheKeyByParentEntity($tradeNetwork,$postfix);
    }

    public function getListByTradeNetwork(Entity_Trade_Network $tradeNetwork, $isHidden = false)
    {
        $sql = "SELECT *
                FROM shop
                WHERE trade_network_id=:trade_network_id
                AND is_deleted = 0";
        $params = array('trade_network_id' => $tradeNetwork->getId());
        if($isHidden !== null)
        {
            $sql.= ' AND is_hidden=:is_hidden';
            $params['is_hidden'] = $isHidden ? '1' : '0';
        }
        $cacheKey = $this->getCacheKeyByTradeNetwork($tradeNetwork,$isHidden);
        $aggregate = $this->getLoadedAggregateBySQL($sql, $params, $cacheKey);
        return $aggregate;
    }

    //@TODO. Добавить алгоритм умных квадратов
    public function getNearbyList($lat,$long,$allowTradeNetworks = 1)
    {
        $calculatedDistanceString = $this->getCalculatedDistanceString($lat,$long);

        //начальный радиус - 30 км.
        $radius = 30;
        $totalCountSql = "SELECT SUM(total_count_trade_network) as total_count
                            FROM
                            (
                                SELECT LEAST(COUNT(*),".$allowTradeNetworks.") as total_count_trade_network
                                FROM shop
                                WHERE is_deleted = 0
                                AND is_hidden = 0
                                GROUP BY trade_network_id
                            ) as groupped_by_trade_network_id";
        $totalCount = SFM_DB::getInstance()->fetchValue($totalCountSql);
        $needFindNumber = $totalCount > Entity_Shop::NEARBY_NUMBER ? Entity_Shop::NEARBY_NUMBER : $totalCount;
        $foundShops = 0;
        while($foundShops < $needFindNumber)
        {
            $sql = "SELECT shop_table.*
                    FROM
                    (
                        SELECT shop.*
                        FROM shop
                        LEFT JOIN trade_network ON trade_network_id = trade_network.id
                        WHERE shop.is_deleted = 0
                        AND shop.is_hidden = 0
                        AND ".$calculatedDistanceString." <= " .($radius*1000)."
                        ORDER BY ".$calculatedDistanceString." ASC, trade_network_id ASC
                    ) AS shop_table
                    GROUP BY trade_network_id
                    ORDER BY ".$calculatedDistanceString." ASC";
            $aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
            $foundShops = $aggregate->totalCount();
            if($foundShops > $needFindNumber)
                $aggregate = $this->createAggregate(array_slice($aggregate->getListEntitiesId(),0,$needFindNumber),null,true);
            $radius = $radius*2;
        }
        return $aggregate;
    }

    /**
     * Список магазинов в радиусе $radius от точки $lat,$long
     * @param double $lat
     * @param double $long
     * @param double $radius
     * @return Aggregate_Shop
     */
    public function getListByCoordinates($lat,$long,$radius)
    {
        $calculatedDistanceString = $this->getCalculatedDistanceString($lat,$long);

		$sql = "SELECT shop.*
                FROM shop
                WHERE shop.is_deleted = 0
                AND shop.is_hidden = 0
                AND ".$calculatedDistanceString." <= " .($radius*1000)."
                ORDER BY ".$calculatedDistanceString." ASC";
		$aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
    	return $aggregate;
    }

    protected function getCalculatedDistanceString($lat,$long)
    {
        $pk = 180/3.14169;
        $cosLat = cos($lat/$pk);
        $cosLong = cos($long/$pk);
        $sinLat = sin($lat/$pk);
        $sinLong = sin($long/$pk);
        return "6366000*acos(".($cosLat*$cosLong)."*cos(lat/$pk)*cos(longtitude/$pk) + ".($cosLat*$sinLong)."*cos(lat/$pk)*sin(longtitude/$pk) + ".$sinLat."*sin(lat/$pk))";
    }

    /**
     * Список магазинов ТЦ
     * @param Entity_Mall $mall
     * @return Aggregate_Shop
     */
    public function getListByMall(Entity_Mall $mall)
    {
        $sql = "SELECT shop.*
                FROM shop
                INNER JOIN mall2shop ON shop.id = mall2shop.shop_id
                WHERE shop.is_deleted = 0
                AND shop.is_hidden = 0
                AND mall_id = :mall_id
                ORDER BY shop.name ASC";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_id' => $mall->getId()), null);
        return $aggregate;
    }

    public function add(Entity_Trade_Network $tradeNetwork, $name, $lat,$long,$address, Entity_City $city = null)
    {
        $proto = array(
                            'trade_network_id' => $tradeNetwork->getId(),
                            'name' => $name,
                            'lat' => $lat,
                            'longtitude' => $long,
                            'address' => $address,
                            'city_id' => $city ? $city->getId() : null,
                            'last_modified' => date('Y-m-d'),
                            'is_deleted' => 0,
                            'is_hidden' => 0
                      );
        return $this->insertEntity($proto);
    }

    public function clearCacheGeneral()
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null));
    }

    public function clearCacheByByTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByTradeNetwork($tradeNetwork,null));
        SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByTradeNetwork($tradeNetwork,true));
        SFM_Cache_Memory::getInstance()->delete($this->getCacheKeyByTradeNetwork($tradeNetwork,false));
    }
}