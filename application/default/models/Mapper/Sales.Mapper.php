<?php 

class Mapper_Sales extends Mapper_ShopsBound implements SFM_Interface_Singleton
{
    /**
     * @var Mapper_Sales
     */
    protected static $instance;
    
    protected function __construct()
    {
        $this->tableName = 'sales';
        $this->uniqueFields = array();
        parent::__construct();
    }
    
    /**
     * Singleton
     * 
     * @return Mapper_Sales
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    /**
     * Wrapper
     * 
     * @param int $id
     * @return Entity_Sales
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }
    
    public function add($name,Entity_Trade_Network $tradeNetwork,$shopIds,$description,$shortDescription)
    {
        $proto = array(
                            'name' => $name, 
                            'description' => $description,
                            'short_description' => $shortDescription,
                            'trade_network_id' => $tradeNetwork->getId(),
                            'picture_path' => '',
                            'last_modified' => date('Y-m-d')
                        );
        $entity = $this->insertEntity($proto);
        $this->insertShopIds($entity,$shopIds);
        return $entity;
    }
    
    protected function getShopsBoundTable()
    {
        return 'sales2shop';
    }
    
    protected function getShopsBoundObjectFieldName()
    {
        return 'sales_id';
    }
    
    public function getShopIdsBySales(Entity_Sales $sales)
    {
        return $this->getShopIdsByObject($sales);
    }
    
    public function getListByShop(Entity_Shop $shop,$pageNumber = 0)
    {
        $sql = "SELECT id 
                FROM sales
                LEFT JOIN sales2shop ON sales.id=sales2shop.sales_id
                WHERE sales2shop.shop_id=:shop_id
                ORDER BY name ASC";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity($shop);
        $aggregate = $this->getAggregateBySQL($sql, array('shop_id' => $shop->getId()), $cacheKey);
        if($pageNumber == 0)
            $aggregate->loadEntities();
        else
            $aggregate->loadEntitiesForCurrentPage($pageNumber);
        return $aggregate;
    }
    
    public function clearCacheByShop(Entity_Shop $shop)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity($shop));
    }
}