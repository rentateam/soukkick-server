<?php
class Mapper_Tag extends SFM_Mapper implements SFM_Interface_Singleton
{
    const TABLE_TAG_SHOP = 'tag_shop';
    const TABLE_TAG_MALL = 'tag_mall';
    const FIELD_TAG_SHOP = 'shop_id';
    const FIELD_TAG_MALL = 'mall_id';

    /**
     * @var Mapper_Tag
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'tag';
        $this->uniqueFields = array(array('name'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_Tag
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     * @param int $id
     * @return Entity_Tag
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * Wrapper
     * @param string $name
     * @return Entity_Tag
     */
    public function getEntityByName($name)
    {
    	return parent::getEntityByUniqueFields(array('name' => $name));
    }

    public function add($name)
    {
        $proto = array(
                            'name' => $name,
                      );
        return $this->insertEntity($proto);
    }

    public function getTagListByMap(Entity_Mall_Map $mallMap)
    {
    	$sql = "SELECT tag.*
                FROM tag
                LEFT JOIN mall2shop ON tag.id = mall2shop.tag_id
                WHERE mall_map_id = :mall_map_id";
    	$aggregate = $this->getLoadedAggregateBySQL($sql, array('mall_map_id' => $mallMap->getId()), null);
    	return $aggregate;
    }
}