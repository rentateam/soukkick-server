<?php

class Mapper_User extends Mapper_User_Abstract
{
    const EXISTING_VISITORS = 'Existing_Visitors';
    const UDID_PREFIX = 'Udid_Prefix';
    const FACEBOOK_PREFIX = 'Facebook_Prefix';
    const CONTACT_PREFIX = 'Contact_Prefix';
    const EMAIL_PREFIX = 'Email_Prefix';

    /**
     * @var Mapper_User
     */
    protected static $instance;

    protected function __construct()
    {
        $this->tableName = 'user';
        $this->uniqueFields = array(array('promo_code'));
        parent::__construct();
    }

    /**
     * Singleton
     *
     * @return Mapper_User
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Wrapper
     *
     * @param int $id
     * @return Entity_User
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @return Entity_User
     * @param string $promoCode
     */
    public function getEntityByPromoCode($promoCode)
    {
        return $this->getEntityByUniqueFields(array('promo_code'=>$promoCode));
    }

    /**
     * @param integer $facebookId
     * @return Entity_User
     */
    public function getEntityByFacebookId($facebookId)
    {
        $list = $this->getListByFacebookId($facebookId);
        return $list->current() ? $list->current() : null;
    }

    /**
     * @param integer $contactId
     * @return Entity_User
     */
    public function getEntityByContactId($contactId)
    {
        $list = $this->getListByContactId($contactId);
        return $list->current() ? $list->current() : null;
    }

    /**
     * @param string $udid
     * @return Entity_User
     */
    public function getEntityByUdid($udid)
    {
        $list = $this->getListByUdid($udid);
        return $list->current() ? $list->current() : null;
    }

    /**
     * @param string $email
     * @return Entity_User
     */
    public function getEntityByEmail($email)
    {
        $list = $this->getListByEmail($email);
        return $list->current() ? $list->current() : null;
    }

    /**
     * @param string $email
     * @return Entity_User
     */
    public function getByEmail($email)
    {
        return $this->getEntityByEmail($email);
    }

    public function add($udid,$cleanUdid,$salt)
    {
        do
        {
            $promoCode = Entity_User::generatePromoCode($udid);
        }
        while($this->getEntityByPromoCode($promoCode) !== null);

        $proto = array(
                        'udid' => $udid,
                        'clean_udid' => $cleanUdid,
                        'salt'  => $salt,
                        'name' => Entity_User::DEFAULT_NAME,
                        'surname' => Entity_User::DEFAULT_SURNAME,
                        'reg_date' => date('Y-m-d H:i:s'),
                        'amount' => 0,
                        'total_amount' => 0,
                        'number_prize' => 0,
                        'number_checkin' => 0,
                        'number_scan' => 0,
                        'last_modified' => date('Y-m-d H:i:s'),
                        'promo_code' => $promoCode,
                        'date_registration' => null,
                        'float_time' => null
                    );
        return $this->insertEntity($proto);
    }

    /**
     * @param integer $facebookId
     * @return Aggregate_User
     */
    public function getListByFacebookId($facebookId)
    {
        $sql = "SELECT *
                FROM user
                WHERE facebook_id=:facebook_id";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::FACEBOOK_PREFIX.$facebookId);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('facebook_id' => $facebookId), $cacheKey);
        return $aggregate;
    }

    /**
     * @param integer $contactId
     * @return Aggregate_User
     */
    public function getListByContactId($contactId)
    {
        $sql = "SELECT *
                FROM user
                WHERE contact_id=:contact_id";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::CONTACT_PREFIX.$contactId);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('contact_id' => $contactId), $cacheKey);
        return $aggregate;
    }

    /**
     * @param string $udid
     * @return Aggregate_User
     */
    public function getListByUdid($udid)
    {
        $sql = "SELECT *
                FROM user
                WHERE udid=:udid";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::UDID_PREFIX.$udid);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('udid' => $udid), $cacheKey);
        return $aggregate;
    }

    /**
     * @param string $email
     * @return Aggregate_User
     */
    public function getListByEmail($email)
    {
        $sql = "SELECT *
                FROM user
                WHERE email=:email";
        $cacheKey = $this->getAggregateCacheKeyByParentEntity(null,self::EMAIL_PREFIX.$email);
        $aggregate = $this->getLoadedAggregateBySQL($sql, array('email' => $email), $cacheKey);
        return $aggregate;
    }

    public function clearCacheByFacebookId($facebookId)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::FACEBOOK_PREFIX.$facebookId));
    }

    public function clearCacheByContactId($contactId)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::CONTACT_PREFIX.$contactId));
    }

    public function clearCacheByUdid($udid)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::UDID_PREFIX.$udid));
    }

    public function clearCacheByEmail($email)
    {
        SFM_Cache_Memory::getInstance()->delete($this->getAggregateCacheKeyByParentEntity(null,self::EMAIL_PREFIX.$email));
    }

    public function getListPlain()
    {
        $sql = "SELECT *
                FROM user";
        $aggregate = $this->getLoadedAggregateBySQL($sql, array(), null);
        return $aggregate;
    }

    /**
     * Получить страницу полного списка пользователей
     * @param $page int
     * @param $perPage int
     * @return Aggregate_User
     */
    public function getListPaginated($page, $perPage)
    {
        $sql = "SELECT * FROM user";

        $aggregate = $this->getAggregateBySQL($sql);
        $aggregate->loadEntitiesForCurrentPage($page, $perPage);

        return $aggregate;
    }

    public function getExistingVisitorsAggregate(Entity_Shop $shop, $makeIfNotExists = true)
    {
        $cacheKey = null;//$this->getAggregateCacheKeyByParentEntity($shop, self::EXISTING_VISITORS);
        $aggregate = SFM_Cache_Memory::getInstance()->get($cacheKey);
        if($makeIfNotExists)
        {
            if (null === $aggregate)
            {
                $aggregate = $this->createAggregate($shop->getExistingVisitorIdsFromBase(),$cacheKey);
                $this->saveCached($aggregate);
            }
            $aggregate->loadEntities();
        }
        return $aggregate;
    }

    /**
     * РџРµСЂРµСЃР±РѕСЂРєР° Р°РіСЂРµРіР°С‚Р° С‚РµРєСѓС‰РёС… РїРѕСЃРµС‚РёС‚РµР»РµР№ - РґРѕР±Р°РІР»РµРЅРёРµ РІ РЅРµРіРѕ РІРѕС€РµРґС€РµРіРѕ РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ
     * @param Entity_Action_Enter $enter
     */
    public function addToExistingVisitorsAggregate(Entity_Action_Abstract $enter)
    {
        $aggregate = $this->getExistingVisitorsAggregate($enter->getShop(),false);
        if(null !== $aggregate)
        {
            $user = $enter->getUser();
            $aggregate->push($user);
        }
    }

    /**
     * РџРµСЂРµСЃР±РѕСЂРєР° Р°РіСЂРµРіР°С‚Р° С‚РµРєСѓС‰РёС… РїРѕСЃРµС‚РёС‚РµР»РµР№ - СѓРґР°Р»РµРЅРёРµ РёР· РЅРµРіРѕ РІС‹С€РµРґС€РµРіРѕ РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ
     * @param Entity_Action_Exit $exit
     */
    public function removeFromExistingVisitorsAggregate(Entity_Action_Abstract $exit)
    {
        $aggregate = $this->getExistingVisitorsAggregate($exit->getShop(),false);
        if(null !== $aggregate)
        {
            $user = $exit->getUser();
            $aggregate->remove($user);
        }
    }

    public function usePromoCode(Entity_User $fromUser, Entity_User $toUser)
    {
       $db = SFM_DB::getInstance();

       $fromUser->increaseInvited();

       $sql = "INSERT INTO user_promocode (from_user_id, to_user_id) VALUES (:from_user_id,:to_user_id)";
       $params = array('from_user_id' => $fromUser->getId(),'to_user_id' => $toUser->getId());
       $db->insert($sql, $params);

    }

    public function isUsedAnyPromoCode(Entity_User $user)
    {
        $db = SFM_DB::getInstance();

        $sql = "SELECT from_user_id FROM user_promocode
                WHERE to_user_id=:to_user_id";
        $params = array('to_user_id' => $user->getId());

        if($db->fetchValue($sql, $params)){
            return true;
        } else {
            return false;
        }
    }

    public function getRegisterDateMin()
    {
        $sql = "
        SELECT MIN(reg_date)
        FROM user
        ";

        $date = SFM_DB::getInstance()->fetchValue($sql);
        $date = DateTime::createFromFormat('Y-m-d G:i:s', $date);

        return $date ? $date->format('Y-m-d') : NULL;
    }

    public function getRegisterDateMax()
    {
        $sql = "
        SELECT MAX(reg_date)
        FROM user
        ";

        $date = SFM_DB::getInstance()->fetchValue($sql);
        $date = DateTime::createFromFormat('Y-m-d G:i:s', $date);

        return $date ? $date->format('Y-m-d') : NULL;
    }

    public function getListByCriteria(Criteria_User $criteria, $page, $perPage)
    {
        $builder = new QueryBuilder_User($criteria);

        $aggregate = $this->getAggregateBySQL($builder->getSQL(), $builder->getParams());
        if ($page) {
            $aggregate->loadEntitiesForCurrentPage($page, $perPage);
        } else {
            $aggregate->loadEntities();
        }

        return $aggregate;
    }

    public function getCount(Criteria_User $criteria)
    {
        $builder = new QueryBuilder_UserCount($criteria);
        $value = SFM_DB::getInstance()->fetchValue($builder->getSQL(), $builder->getParams());
        if (!$value) {
            $value = 0;
        }
        return $value;
    }

    public function getAuthorizedUsersTotalNumber()
    {
        $criteria = new Criteria_User();
        $criteria->setAuthorized(true);
        return $this->getCount($criteria);
    }

    public function getUsersTotalNumber()
    {
        $criteria = new Criteria_User();
        return $this->getCount($criteria);
    }

    public function getNumberShows(Entity_User $user)
    {
        $sql = "
            SELECT count(tm.id) as num
            FROM trm_main tm
            LEFT JOIN rule_item AS ri ON ri.id = tm.rule_item_id
            WHERE tm.user_id = :user_id AND ri.action_type = " . Entity_Rule_Item::TYPE_SHOPPAGE . "
        ";

        $value = SFM_DB::getInstance()->fetchValue($sql, array('user_id' => $user->getId()));
        if (!$value) {
            $value = 0;
        }
        return $value;
    }

    public function getInvitedUsers(Entity_User $user)
    {
        $sql = "
            SELECT to_user_id AS id
            FROM user_promocode
            WHERE from_user_id = :user_id
        ";

        $aggregate = $this->getAggregateBySQL($sql, array('user_id' => $user->getId()));
        $aggregate->loadEntities();
        return $aggregate;
    }

    public function getInvitedBy(Entity_User $user)
    {
        $sql = "
            SELECT from_user_id AS id
            FROM user_promocode
            WHERE to_user_id = :user_id
        ";

        $aggregate = $this->getAggregateBySQL($sql, array('user_id' => $user->getId()));
        $aggregate->loadEntities();
        return $aggregate->current();
    }

    public function ban(Entity_User $user, $reason)
    {
        if (empty($reason)) {
            $reason = "—";
        }

        $sql = "INSERT INTO user_banned (user_id, reason, date_created) VALUES (:user_id, :reason, NOW())";
        $params = array('user_id' => $user->getId(),'reason' => $reason);
        SFM_DB::getInstance()->insert($sql, $params);
    }

    public function unban(Entity_User $user)
    {
        $sql = "DELETE FROM user_banned WHERE user_id = :user_id";
        $params = array('user_id' => $user->getId());
        SFM_DB::getInstance()->delete($sql, $params);
    }

    public function getBannedReason(Entity_User $user)
    {
        $sql = "
            SELECT reason
            FROM user_banned
            WHERE user_id = :user_id
        ";

        $value = SFM_DB::getInstance()->fetchValue($sql, array('user_id' => $user->getId()));
        if (!$value) {
            $value = NULL;
        }
        return $value;
    }

    public function getCountByUdidToday($cleanUdid)
    {
        $now = time();
        $db = SFM_DB::getInstance();
        $sql = 'SELECT count(*) as cnt
                FROM user
                WHERE clean_udid=:clean_udid
                AND date_registration >= :date_created_start
                AND date_registration <= :date_created_end';
        $params = array(
                            'clean_udid' => $cleanUdid,
                            'date_created_start' => date('Y-m-d H:i:s',$now - 24*60*60),
                            'date_created_end' => date('Y-m-d H:i:s',$now)
                        );
        return $db->fetchValue($sql,$params);
    }
}