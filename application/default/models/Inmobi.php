<?
class Inmobi
{
    const APPID_IPHONE = 'IPhone';
    const APPID_ANDROID = 'Android';
    
    const INMOBI_APPID_ANDROID = 'ba18756f-7bf3-4939-ab8f-d8eac66068f4';
    const INMOBI_APPID_IPHONE = '02632ec4-79a2-450d-b9a6-abb2d2279fea';
    const PHOENIX_APPID_ANDROID = '33abffcedac43a654ac7f501856bf70069642353';
    const PHOENIX_APPID_IPHONE = '5ec829debe54b19a5f78d9a65b900a3951731851';
    
    
    protected $_appId;
    protected $_odin1;
    protected $_udid;
    protected $_goalName;
    protected $_mode;
    
    public function __construct($appId,$odin1,$udid,$goalName)
    {
        switch($appId)
        {
            case self::APPID_IPHONE:
                $this->_appId = self::INMOBI_APPID_IPHONE;
                $this->_odin1 = null;
                $this->_udid = $udid;
                $this->_mode = self::APPID_IPHONE;
                break;
            case self::APPID_ANDROID:
                $this->_appId = self::INMOBI_APPID_ANDROID;
                $this->_odin1 = self::salt($odin1);
                $this->_udid = self::salt($udid);
                $this->_mode = self::APPID_ANDROID;
                break;
            default:
                $this->_mode = null;
                //throw new Shopfee_Exception('Unknown app id to inmobi: '.$appId);
        }
        
        $this->_goalName = 'download';
    }
    
    protected static function salt($string)
    {
        return md5($string);
    }
    
    public function makeRequest()
    {
        if($this->_mode !== null)
        {
            $odinString = $this->_odin1 ? '&odin1='.$this->_odin1 : '';
            $phoenixAppId = $this->_mode == self::APPID_ANDROID ? self::PHOENIX_APPID_ANDROID : self::PHOENIX_APPID_IPHONE;
            $this->sendRequest('http://dserv.appsdt.com/download/tracker','appId='.$this->_appId.$odinString.'&udid='.$this->_udid.'&goalName='.$this->_goalName);
            //$this->sendRequest('https://cserv.appsdt.com/click/phoenix/'.$phoenixAppId.'/?',$odinString.'&udid='.$this->_udid.'&goalName='.$this->_goalName);
        }
    }
    
    protected function sendRequest($url,$postfields)
    {
        $ch = curl_init();  // инициализируем сессию curl
        curl_setopt($ch, CURLOPT_URL,$url); // указываем URL, куда отправлять POST-запрос
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);// разрешаем перенаправление
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // указываем, что результат запроса следует передать в переменную, а не вывести на экран
        curl_setopt($ch, CURLOPT_TIMEOUT, 2); // таймаут соединения
        curl_setopt($ch, CURLOPT_POST, 1); // указываем, что данные надо передать именно методом POST
        curl_exec($ch); // выполняем запрос
        curl_close($ch); // завершаем сессию
    }
}