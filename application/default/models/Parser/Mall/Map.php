<?php
class Parser_Mall_Map
{
    protected $_fileName;
    /**
     * @var Entity_Mall_Map
     */
    protected $_mallMap;

    protected static $_reservedTags = array(
                                                'atm',
                                                'toilet([_0-9]*)',
                                                'stairs([_0-9]*)',
                                                'escalator([_0-9]*)',
                                                'elevator([_0-9]*)',
                                                'empty_x0020_spot',
                                                'entrance',
                                                'mall'
                                            );

    public function __construct($fileName, Entity_Mall_Map $map)
    {
        $this->_fileName = $fileName;
        $this->_mallMap = $map;
    }

    public function extractAndSaveTagList()
    {
        $ext = SFM_Util_File::getExt(basename($this->_fileName));
        if($ext != 'svg')
        {
            throw new Shopfee_Exception('Cannot parse '.$ext.' files!');
        }

        $tagList = array();
        $contents = file_get_contents($this->_fileName);
        $tagList = $this->parseTags($contents);
        $this->saveTags($tagList);
        return $tagList;
    }

    protected function parseTags($contents)
    {
        $tags = array();
        $xml =  simplexml_load_string($contents);
        foreach($xml->children() as $g)
        {
            $id = $this->getShopTag((string)$g['id']);
            if($id)
            {
                $tags[] = $id;
            }
        }
        sort($tags);
        return $tags;
    }

    protected function saveTags($tagList)
    {
        $mall = $this->_mallMap->getMall();
        foreach($tagList as $tag)
        {
            Entity_Mall2Shop::add($mall, $tag, null, $this->_mallMap);
        }
    }

    protected function getShopTag($idString)
    {
        foreach(self::$_reservedTags as $reservedTag)
        {
            //Если слово зарезервировано
            if(preg_match('/'.$reservedTag.'/',$idString,$matches))
            {
                return '';
            }
        }

        //Если не зарезервировано - возвращаем тэг
        if(preg_match_all('/(_x[0-9A-Fa-f]{4}_)/',$idString,$matches))
        {
            unset($matches[0]);
            $matches = array_values($matches);
            foreach($matches[0] as $asciiSymbol)
            {
                $replaceAsciiSymbol = chr(hexdec('0'.str_replace('_','',$asciiSymbol)));
                $idString = str_replace($asciiSymbol,$replaceAsciiSymbol,$idString);
            }
        }
        return $idString;
    }

    protected static function hex2bin($h)
    {
        if (!is_string($h))
            return null;
        $r = '';
        for ($a = 0; $a < strlen($h); $a += 2)
        {
            $r .= chr(hexdec($h{$a}.$h{($a + 1)}));
        }
        return $r;
    }
}