<?
abstract class C1_Abstract
{
    protected $_accessToken;
    protected $_tokenEntity;
    protected $_errorCode;
    
    public function __construct($accessToken)
    {
        $this->_accessToken = $accessToken;
        $this->_tokenEntity = Mapper_AccessToken::getInstance()->getEntityByToken($this->_accessToken);
    }
    
    protected function isValidAccessToken()
    {
        return $this->_tokenEntity !== null;
    }
    
    public function getShopList()
    {
        if($this->isValidAccessToken())
        {
            $shop = $this->_tokenEntity->getShop();
            if($shop !== null)
            {
                return Mapper_Shop::getInstance()->createAggregate(array($shop->getId()),null,true);
            }
            else
            {
                return $this->_tokenEntity->getTradeNetwork()->getShops();
            }
            
        }
        else 
        {
            return Mapper_Shop::getInstance()->createAggregate(array());
        }
    }
}