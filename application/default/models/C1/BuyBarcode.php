<?
class C1_BuyBarcode extends C1_Abstract
{
    const ERROR_NO_ERROR = 0;
    const ERROR_NOT_AUTHORIZED = 1;
    const ERROR_INVALID_XML = 2;
    
    const OPERATION_TYPE_ORDER = 'Order';
    const OPERATION_TYPE_RETURN = 'Return';
    
    protected static $_errorMessages = array(
                                        self::ERROR_NO_ERROR => 'Success',
                                        self::ERROR_NOT_AUTHORIZED => 'Not Authorized',
                                        self::ERROR_INVALID_XML => 'Invalid XML',
                                     );
    
    public function in()
    {
        $barcodeList = array();
        $shopList = $this->getShopList();
        $this->_errorCode = self::ERROR_NO_ERROR;
        if($this->isValidAccessToken())
        {
            //generate barcodes
            foreach($shopList as $shop)
            {
                $thisShopBarcodeList = array();
                for($i=0;$i<100;$i++)
                {
                    $barcode = Entity_BuyBarcode::add($shop);
                    $thisShopBarcodeList[] = $barcode->getBarcode();
                }
                $barcodeList[$shop->getId()] = $thisShopBarcodeList;
            }
            
            $this->_errorCode = self::ERROR_NO_ERROR;
        }
        else 
        {
            $this->_errorCode = self::ERROR_NOT_AUTHORIZED;
        }
        
        return $barcodeList;
    }
    
    public function out($xmlString)
    {
        $this->_errorCode = self::ERROR_NO_ERROR;
        if($this->isValidAccessToken())
        {
            if($xmlString !== null)
            {
                $tran = simplexml_load_string($xmlString); 
                if($tran !== false)
                {
                    if($tran->attributes()->ShopsCount != count($tran->Shop))
                    {
                        $this->_errorCode = self::ERROR_INVALID_XML;
                        return;
                    }
                    else
                    {
                        $shopList = is_array($tran->Shop) ? $tran->Shop : array($tran->Shop);
                        foreach($shopList as $shop)
                        {
                            if($shop->attributes()->ItemsCount != count($shop->Item))
                            {
                                $this->_errorCode = self::ERROR_INVALID_XML;
                                return;
                            }
                            else 
                            {
                                $operations = is_array($shop->Item) ? $shop->Item : array($shop->Item);
                                foreach($operations as $operation)
                                {
                                    switch($operation->attributes()->type)
                                    {
                                        case self::OPERATION_TYPE_ORDER:
                                            $barcode = Mapper_BuyBarcode::getInstance()->getEntityByBarcode($operation->attributes()->id);
                                            if($barcode === NULL || $barcode->isActive() || $barcode->getShopId() != $shop->attributes()->id)
                                            {
                                                $this->_errorCode = self::ERROR_INVALID_XML;
                                                return;
                                            }
                                            $date = $operation->attributes()->OperationDate.' '.$operation->attributes()->OperationTime;
                                            $barcode->setCounterDate($date);
                                            break;
                                        case self::OPERATION_TYPE_RETURN:
                                            
                                            break;
                                        default:
                                            $this->_errorCode = self::ERROR_INVALID_XML;
                                            return;
                                    }
                                }
                            }
                        }
                    }
                    $this->_errorCode = self::ERROR_NO_ERROR;
                }
                else 
                {
                    $this->_errorCode = self::ERROR_INVALID_XML;
                    return;
                }
            }
            else
            {
                $this->_errorCode = self::ERROR_INVALID_XML;
                return;
            }
        }
        else 
        {
            $this->_errorCode = self::ERROR_NOT_AUTHORIZED;
        }
    }
    
    public function getErrorMessage()
    {
        return self::$_errorMessages[$this->_errorCode];
    }
    
    public function getErrorCode()
    {
        return $this->_errorCode;
    }
}