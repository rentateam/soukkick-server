<?php
class Criteria_Trm_Main extends Shopfee_Criteria_Abstract
{
    protected $_tradeNetwork;
    protected $_dateFrom;
    protected $_dateTo;
    protected $_timeFrom;
    protected $_timeTo;
    protected $_shop;
    protected $_city;
    protected $_user;
    protected $_numberFrom;
    protected $_numberTo;

    public function setDateFrom($dateFrom)
    {
        $this->_dateFrom = $dateFrom;
        return $this;
    }

    public function getDateFrom()
    {
        return $this->_dateFrom;
    }

    public function setDateTo($dateTo)
    {
        $this->_dateTo = $dateTo;
        return $this;
    }

    public function getDateTo()
    {
        return $this->_dateTo;
    }

    public function setTimeFrom($timeFrom)
    {
        $this->_timeFrom = $timeFrom;
        return $this;
    }

    public function getTimeFrom()
    {
        return $this->_timeFrom;
    }

    public function setTimeTo($timeTo)
    {
        $this->_timeTo = $timeTo;
        return $this;
    }

    public function getTimeTo()
    {
        return $this->_timeTo;
    }

    public function setShop(Entity_Shop $shop = null)
    {
        $this->_shop = $shop;
        return $this;
    }

    public function setUser(Entity_User $user = null)
    {
        $this->_user = $user;
        return $this;
    }

    public function getShop()
    {
        return $this->_shop;
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function setCity(Entity_City $city = null)
    {
        $this->_city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->_city;
    }

    public function setTradeNetwork(Entity_Trade_Network $tradeNetwork)
    {
        $this->_tradeNetwork = $tradeNetwork;
        return $this;
    }

    public function getTradeNetwork()
    {
        return $this->_tradeNetwork;
    }

    public function setNumberFrom($numberFrom)
    {
        $this->_numberFrom = $numberFrom;
        return $this;
    }

    public function getNumberFrom()
    {
        return $this->_numberFrom;
    }

    public function setNumberTo($numberTo)
    {
        $this->_numberTo = $numberTo;
        return $this;
    }

    public function getNumberTo()
    {
        return $this->_numberTo;
    }
}
