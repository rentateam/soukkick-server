<?php
class Criteria_Prize_Order_History extends Shopfee_Criteria_Abstract
{
    /**
     * @var Entity_Prize_Order
     */
    protected $_prizeOrder;
    
    public function setPrizeOrder(Entity_Prize_Order $prizeOrder)
    {
        $this->_prizeOrder = $prizeOrder;
        return $this;
    }

    public function getPrizeOrder()
    {
        return $this->_prizeOrder;
    }
}