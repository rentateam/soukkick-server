<?php
class Criteria_Prize_Stock extends Shopfee_Criteria_Abstract
{
    public function __construct()
    {
        $this->_sort = QueryBuilder_Prize_Stock::SORT_NUMBER_AVAILABLE;
        $this->_direction = 'DESC';
    }
}