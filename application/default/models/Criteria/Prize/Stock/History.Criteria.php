<?php
class Criteria_Prize_Stock_History extends Shopfee_Criteria_Abstract
{
    /**
     * @var Entity_Prize_Stock
     */
    protected $_prizeStock;
    
    public function setPrizeStock(Entity_Prize_Stock $prizeStock)
    {
        $this->_prizeStock = $prizeStock;
        return $this;
    }

    public function getPrizeStock()
    {
        return $this->_prizeStock;
    }
}