<?php
class Criteria_Prize_Order extends Shopfee_Criteria_Abstract
{
    protected $_prizeStatusIds;
    
    public function __construct()
    {
        $this->_sort = QueryBuilder_Prize_Order::SORT_DATETIME_CREATED;
        $this->_direction = 'DESC';
    }

    public function setPrizeStatusIds($prizeStatusIds)
    {
        $this->_prizeStatusIds = $prizeStatusIds;
        return $this;
    }

    public function getPrizeStatusIds()
    {
        return $this->_prizeStatusIds;
    }
}