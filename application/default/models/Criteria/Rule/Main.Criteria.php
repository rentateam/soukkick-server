<?php
class Criteria_Rule_Main extends Shopfee_Criteria_Abstract
{
    const TYPE_CURRENT = 1;
    const TYPE_FUTURE = 2;
    const TYPE_OLD = 3;

    protected $_bpaMain;
    protected $_dateType;
    protected $_shop;
    protected $_city;
    protected $_isDeleted;

    public function setDateType($dateType)
    {
        $this->_dateType = $dateType;
        return $this;
    }

    public function getDateType()
    {
        return $this->_dateType;
    }

    public function setBpaMain(Entity_Bpa_Main $bpa)
    {
        $this->_bpaMain = $bpa;
        return $this;
    }

    public function getBpaMain()
    {
        return $this->_bpaMain;
    }

    public function setShop(Entity_Shop $shop = null)
    {
        $this->_shop = $shop;
        return $this;
    }

    public function getShop()
    {
        return $this->_shop;
    }

    public function setCity(Entity_City $city = null)
    {
        $this->_city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->_city;
    }

    public function setDeleted($isDeleted)
    {
        $this->_isDeleted = $isDeleted;
        return $this;
    }

    public function isDeleted()
    {
        return $this->_isDeleted;
    }
}