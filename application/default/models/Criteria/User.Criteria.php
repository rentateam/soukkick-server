<?php
class Criteria_User extends Shopfee_Criteria_Abstract
{
    const EMAIL = 1;
    const NAME = 2;

    const ORDER_DATE_REGISTERED_DESC = 1;
    const ORDER_DATE_REGISTERED_ASC = 2;
    const ORDER_AMOUNT_DESC = 3;
    const ORDER_AMOUNT_ASC = 4;
    const ORDER_CHECKIN_DESC = 5;
    const ORDER_CHECKIN_ASC = 6;
    const ORDER_SCAN_DESC = 7;
    const ORDER_SCAN_ASC = 8;
    const ORDER_INVITED_DESC = 9;
    const ORDER_INVITED_ASC = 10;

    protected $dateFrom;
    protected $dateTo;
    protected $isAuthorized;
    protected $emailSearch;
    protected $nameSearch;

    public function setDateFrom($date)
    {
        $this->dateFrom = $date;
    }

    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    public function setDateTo($date)
    {
        $this->dateTo = $date;
    }

    public function getDateTo()
    {
        return $this->dateTo;
    }

    public function setAuthorized($state)
    {
        $this->isAuthorized = $state;
    }

    public function isAuthorized()
    {
        return $this->isAuthorized;
    }

    public function setSearch($type, $text)
    {
        switch ($type) {
            case self::EMAIL:
                $this->emailSearch = $text;
                break;
            case self::NAME:
                $this->nameSearch = $text;
                break;
        }
    }

    public function getEmailSearch()
    {
        return $this->emailSearch;
    }

    public function getNameSearch()
    {
        return $this->nameSearch;
    }

}
