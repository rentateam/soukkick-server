<?
class Stat_Rule_Item
{
    protected $date;
    protected $spentAmount = 0;
    protected $checkinNumber = 0;
    protected $checkinAmount = 0;
    protected $buyNumber = 0;
    protected $buyAmount = 0;
    protected $scanNumber = 0;
    protected $scanAmount = 0;
    protected $shoppageNumber = 0;
    protected $shoppageAmount = 0;
    
    public function __construct($date)
    {
        $this->date = $date;
    }
    
    public function __set($name,$value)
    {
        $parameter = lcfirst($name);
        $this->$parameter = $value;
        return $this;
    }
    
    public function __call($method, $args)
    {
        if(strpos($method,'add') === 0)
        {
            $parameter = lcfirst(substr($method,3));
            $this->$parameter += $args[0];
            return $this;
        }
        else if(strpos($method,'get') === 0)
        {
            $parameter = lcfirst(substr($method,3));
            return $this->$parameter;
        }
    }
}