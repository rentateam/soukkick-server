<?
class Stat_Rule
{
    protected $_dateFrom;
    protected $_dateTo;
    protected $_shop;
    protected $_city;
    protected $_tradeNetwork;

    protected $_statItems = array();
    protected $_overallAmount = 0;
    protected $_checkinNumber = 0;
    protected $_checkinAmount = 0;
    protected $_scanNumber = 0;
    protected $_scanAmount = 0;
    protected $_shoppageNumber = 0;
    protected $_shoppageAmount = 0;
    protected $_buyNumber = 0;
    protected $_buyAmount = 0;

    public function __construct(Entity_Trade_Network $tradeNetwork,$dateFrom,$dateTo,Entity_Shop $shop = null,Entity_City $city = null)
    {
        $this->_dateFrom = $dateFrom;
        $this->_dateTo = $dateTo;
        $this->_shop = $shop;
        $this->_city = $city;
        $this->_tradeNetwork = $tradeNetwork;
    }

    public function calculate()
    {
        $this->_overallAmount = 0;
        $this->_checkinNumber = 0;
        $this->_checkinAmount = 0;
        $this->_scanNumber = 0;
        $this->_scanAmount = 0;
        $this->_shoppageNumber = 0;
        $this->_shoppageAmount = 0;
        $this->_buyNumber = 0;
        $this->_buyAmount = 0;

        $criteria = new Criteria_Trm_Main();
        $criteria->setDateFrom($this->_dateFrom)
                 ->setDateTo($this->_dateTo)
                 ->setShop($this->_shop)
                 ->setCity($this->_city)
                 ->setTradeNetwork($this->_tradeNetwork);
        $transactionList = Mapper_Trm_Main::getInstance()->getListByCriteria($criteria);

        $statItems = array();
        foreach($transactionList as $transaction)
        {
            $date = date('d.m.Y',strtotime($transaction->getDateCreated()));
            if(!isset($statItems[$date]))
                $statItems[$date] = new Stat_Rule_Item($date);

            $statRuleItem = $statItems[$date];

            $this->_overallAmount += $transaction->getAmount();
            $statRuleItem->addSpentAmount($transaction->getAmount());
            switch($transaction->getRuleItem()->getActionType())
            {
                case Entity_Rule_Item::TYPE_CHECKIN:
                    $statRuleItem->addCheckinAmount($transaction->getAmount());
                    $statRuleItem->addCheckinNumber(1);
                    $this->_checkinAmount += $transaction->getAmount();
                    $this->_checkinNumber++;
                    break;
                case Entity_Rule_Item::TYPE_SHOPPAGE:
                    $statRuleItem->addShoppageAmount($transaction->getAmount());
                    $statRuleItem->addShoppageNumber(1);
                    $this->_shoppageAmount += $transaction->getAmount();
                    $this->_shoppageNumber++;
                    break;
                case Entity_Rule_Item::TYPE_SCAN:
                    $statRuleItem->addScanAmount($transaction->getAmount());
                    $statRuleItem->addScanNumber(1);
                    $this->_scanAmount += $transaction->getAmount();
                    $this->_scanNumber++;
                    break;
                case Entity_Rule_Item::TYPE_BUY_BARCODE:
                    $statRuleItem->addBuyAmount($transaction->getAmount());
                    $statRuleItem->addBuyNumber(1);
                    $this->_buyAmount += $transaction->getAmount();
                    $this->_buyNumber++;
                    break;
            }
        }
        $this->_statItems = $statItems;
    }

    public function getStat()
    {
        return $this->_statItems;
    }

    public function getOverallAmount()
    {
        return $this->_overallAmount;
    }

    public function getCheckinNumber()
    {
        return $this->_checkinNumber;
    }

    public function getCheckinAmount()
    {
        return $this->_checkinAmount;
    }

    public function getScanNumber()
    {
        return $this->_scanNumber;
    }

    public function getScanAmount()
    {
        return $this->_scanAmount;
    }

    public function getShoppageNumber()
    {
        return $this->_shoppageNumber;
    }

    public function getShoppageAmount()
    {
        return $this->_shoppageAmount;
    }

    public function getBuyNumber()
    {
        return $this->_buyNumber;
    }

    public function getBuyAmount()
    {
        return $this->_buyAmount;
    }
}