<?php

class TransmitterController extends Shopfee_Controller_Action_Authorized 
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $lat = $this->getQuotedParam('lat');
        $long = $this->getQuotedParam('long');
        $transmitterList = Mapper_Transmitter::getInstance()->getListByCoordinates($lat,$long);
        if(!$this->isJson)
        {
            $this->_redirect('/');  
        }
        else 
        {
            $this->view->transmitterList = array();
            foreach($transmitterList as $transmitter)
            {
                $this->view->transmitterList[] = $transmitter->getJsonObject($this->currentUser);
            }
        }
    }
}