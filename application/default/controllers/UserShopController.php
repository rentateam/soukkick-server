<?php

class UserShopController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('inner', 'json')
                ->addActionContext('recommended', 'json')
                ->addActionContext('text', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function innerAction()
    {
        $id = $this->getQuotedParam('user_shop_id');
        if($id === null)
            $id = $this->getQuotedParam('id');

        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);

        if($this->isJson)
        {
            $object = new Memcached_Object_TradeNetwork_Info($this->currentUser,$tradeNetwork);
            if($object->exists() && $this->isSetMobile304())
            {
                //nothing - empty body
            }
            else
            {
                $productList = $tradeNetwork->getProducts();
                $salesList = $tradeNetwork->getSales();
                $scanProductList = $tradeNetwork->getScanProducts();

                $this->view->productList = array();
                foreach($productList as $product)
                {
                    $this->view->productList[] = $product->getJsonObject();
                }
                $this->view->salesList = array();
                foreach($salesList as $sales)
                {
                    $this->view->salesList[] = $sales->getJsonObject();
                }
                $this->view->scanList = array();
                foreach($scanProductList as $scanProduct)
                {
                    $this->view->scanList[] = $scanProduct->getJsonObject($this->currentUser);
                }

                $object->set();
                $this->allowMobileCaching();
            }
        }
    }

    public function recommendedAction()
    {
        $object = new Memcached_Object_RecommendedTradeNetworks($this->currentUser);
        $cachingTime = 24*60*60;
        if($object->exists() && $this->isSetMobile304($cachingTime))
        {
            //nothing - empty body
        }
        else
        {
            $shopList = Mapper_Trade_Network::getInstance()->getListRecommended();
            $this->view->userShopList = array();
            foreach($shopList as $tradeNetwork)
            {
                $this->view->userShopList[] = $tradeNetwork->getJsonObject($this->currentUser);
            }
            $object->set();
            $this->allowMobileCaching();
            $response = $this->getResponse();
            $response->setHeader('Cache-Control', 'private', true);
            $response->setHeader('Cache-Control', 'max-age='.$cachingTime);
            $response->setHeader('Cache-Control', 'must-revalidate');
        }
    }

    public function textAction()
    {
        $includeInner = $this->getQuotedParam('with_details',false) == '1';
        $tradeNetworkList = Mapper_Trade_Network::getInstance()->getListByText($this->getQuotedParam('q'));
        $this->view->userShopList = array();
        foreach($tradeNetworkList as $tradeNetwork)
        {
            $this->view->userShopList[] = $tradeNetwork->getJsonObject($this->currentUser,$includeInner);
        }
    }
}