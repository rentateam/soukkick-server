<?php
class Form_Log extends Form_Abstract
{
    public function init()
    {
        $this->setAction('/index/log/');
        $this->setMethod(self::METHOD_POST);
        $this->setEnctype(self::ENCTYPE_MULTIPART);

        $file = new Zend_Form_Element_File('info', array(
            'required'    => false,
            'adapter'     => 'Http',
            'name'        => 'info',
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($file);

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ''
        ));

        $this->addElement($submit);

        parent::init();
    }

    public function getValuesArray(Entity_Trade_Network $user)
    {
        return array();
    }
}

class IndexController extends Shopfee_Controller_Action
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('info', 'json')
                ->addActionContext('delete-data', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function flushCacheAction()
    {
        SFM_Cache_Memory::getInstance()->flush();
        die();
    }

    public function logAction()
    {

        $form = new Form_Log();
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $file = $form->uploadFile('info');

            $mailer = new Zend_Mail('windows-1251');
            $mailer->createAttachment(file_get_contents($file),
                                         Zend_Mime::TYPE_TEXT,
                                         Zend_Mime::DISPOSITION_ATTACHMENT,
                                         Zend_Mime::ENCODING_BASE64,
                                         'log.txt');
            $mailer->setFrom(Application::getDefaultEmailAddress(), iconv('utf-8','windows-1251',''));
            $mailer->setBodyText('this is a test');
            $mailer->addTo('alex.korolkov@gmail.com', iconv('utf-8','windows-1251',''));
            $mailer->setSubject(iconv('utf-8','windows-1251','Лог от '.date('Y-m-d H:i:s')));
            $mailer->send();
            die('');
        }
        $this->view->form = $form;
    }

    public function feedbackAction()
    {
        if($this->getRequest()->isPost())
        {
            $message = $_POST['message'];
            if($message)
            {
                $emailFrom = Application::getDefaultEmailAddress();
                $subject = 'Обратная связь на shopfee.ru';
                $params = array('email' => $_POST['email'],'name' => $_POST['name'],'message' => $message);
                Shopfee_Mailer::getInstance()->sendLetter($params, 'feedback.phtml', $emailFrom, 'info@shopfee.ru', Application::getDefaultEmailTitle(), '', $subject);
            }
        }
    }

    public function testAction()
    {
        var_dump(Mapper_Shop::getInstance()->getEntityById(1)->getJsonReplicationObject());
        die();
    }
}