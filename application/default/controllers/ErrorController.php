<?php

/**
 * ErrorController - The default error controller class
 * 
 * @author
 * @version 
 */

class ErrorController extends Shopfee_Controller_Action
{

    /**
     * This action handles  
     *    - Application errors
     *    - Errors in the controller chain arising from missing 
     *      controller classes and/or action methods
     */
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found                
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Страница не найдена';
                break;
            default:
                $this->view->message = 'Извините, произошла ошибка.';
                break;
        }
        if($this->_getParam("format") == "json") {
            /**
             * Common error status field for all json requests 
             * json_succesful=1 if no errors
             * json_succesful=-1 else
             * @see Letsee_Controller_Action postDispatch()
             */ 
            $this->view->json_succesful = -1;
        }
        $env = Zend_Registry::get(Application::ENVIROMENT_NAME);
        if($env != Application::ENVIROMENT_PRODUCTION) {
            $this->view->message = $errors->exception;
        }
        else 
        {
            error_log($errors->exception."\n");
        }
    }
}
