<?php

abstract class ShopsBoundController extends Shopfee_Controller_Action_AuthorizedShop 
{
    protected function extractShopIds()
    {
        $shopIds = isset($_REQUEST['shop_id']) ? $_REQUEST['shop_id'] : array();
        foreach($shopIds as &$shopId)
        {
            $shopId = intval($shopId);
        }
        $shopIds = array_unique($shopIds);
        return $shopIds;
    }
}