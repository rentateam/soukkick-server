<?php

class SalesController extends ShopsBoundController
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        $pageNum = $this->getQuotedParam("page");
        if( empty($pageNum) ) {
            $pageNum = 1;
        }

        $shop = $this->extractShop();

        $salesList = $shop->getSales($pageNum);
        $salesCount = $salesList->totalCount();

        if($this->getQuotedParam("format") != "json")
        {

            $paginator = new Zend_Paginator( new Paginator_Adapter_Null($salesCount) );
            $paginator->setItemCountPerPage(Aggregate_Sales::ITEMS_PER_PAGE);
            $paginator->setCurrentPageNumber( $pageNum );
            if($salesCount > Aggregate_Sales::ITEMS_PER_PAGE)
                $this->view->paginator = $paginator;

            $this->view->pageUrl = '/sales/index/shop_id/'.$shop->getId().'/';
            $this->view->pageNum = $pageNum;
            $this->view->salesList = $salesList;
        }
        else
        {
            $this->view->salesList = array();
            foreach($salesList as $sales)
            {
                $salesListItem = array(
                                            'id' => $sales->getId(),
                                            'picture' => $sales->getBigPicturePathFull(false),
                                        );
                $this->view->salesList[] = $salesListItem;
            }
        }
    }

    public function formAction()
    {
        $id = $this->getQuotedParam('id');
        $form = new Form_Sales($id);
        if($id)
        {
            $sales = Mapper_Sales::getInstance()->getEntityById($id);
            $this->checkForNullEntity($sales);
            $selectedShopIds = $sales->getShopIds();
            $form->setValues($sales);
        }
        else
        {
            $sales = null;
            $selectedShopIds = array();
        }

        $this->view->sales = $sales;
        $this->view->form = $form;

        $view = new Zend_View();
        $view->setScriptPath(realpath(Application::EDIT_VIEWS_PATH));
        $view->assign(array('form' => $form,'sales' => $sales,'currentTradeNetwork' => Auth::getCurrentTradeNetwork(),'selectedShopIds' => $selectedShopIds));
        $this->view->html = $view->render('sales/form.phtml');
    }

    public function addAction()
    {
        $form = new Form_Sales(0);
        $this->view->selectedShopIds = array();

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $uploadedFileName = $form->uploadFile('file');
                $tradeNetwork = Auth::getCurrentTradeNetwork();
                $shopIds = $tradeNetwork->getShops(null)->getListEntitiesId();
                Entity_Sales::add($values['name'],$tradeNetwork,$shopIds,$values['description'],$values['short_description'],$uploadedFileName);
                $db->commit();
                $this->_redirect('/edit/shopcase/#sales_caption');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $sales = Mapper_Sales::getInstance()->getEntityById($id);
        $this->checkForNullEntity($sales);

        $form = new Form_Sales($id);
        $this->view->selectedShopIds = $sales->getShopIds();
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $tradeNetwork = Auth::getCurrentTradeNetwork();
                $shopIds = $tradeNetwork->getShops(null)->getListEntitiesId();
                $params = array(
                                'name' => $values['name'],
                                'description' => $values['description'],
                                'short_description' => $values['short_description'],
                                'shop_ids' => $shopIds
                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $sales->update($params);
                $db->commit();
                $this->_redirect('/edit/shopcase/#sales_caption');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($sales);

        $this->view->sales = $sales;
        $this->view->form = $form;
        $this->view->shop = $sales->getShop();
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
    }

    public function deleteAction()
    {
        $id = $this->getQuotedParam('id');
        $sales = Mapper_Sales::getInstance()->getEntityById($id);
        $this->checkForNullEntity($sales);

        $sales->delete();
        $this->_redirect('/edit/shopcase/#sales_caption');
    }

    /**
    *   @return Entity_Shop|null
    */
    protected function extractShop()
    {
        $shopId = $this->getQuotedParam('shop_id');
        $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
        $this->checkForNullEntity($shop);

        $this->view->shop = $shop;

        return $shop;
    }

    public function detailAction()
    {
        $id = $this->getQuotedParam('id');
        $sales = Mapper_Sales::getInstance()->getEntityById($id);
        if($sales !== null)
        {
            $this->view->sales = $sales;
            $this->view->tradeNetwork = $sales->getTradeNetwork();
        }
        else
        {
            $this->set404();
        }
    }
}