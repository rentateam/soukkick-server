<?php

class ShopController extends Shopfee_Controller_Action_Authorized 
{
    //protected $_authAdminActions = array('add','edit','delete');
    
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('inner', 'json')
                ->addActionContext('full', 'json')
                ->addActionContext('form', 'json')
                ->addActionContext('list-by-user-shop', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page");
        if( empty($pageNum)) {
            $pageNum = $this->getQuotedParam('format') == 'json' ? 1 : 0;
        }
        
        $lat = $this->getQuotedParam('lat');
        $long = $this->getQuotedParam('long');
        $shopList = Mapper_Shop::getInstance()->getNearbyList($lat,$long);
        $shopList = Entity_Shop::sortListByDistance($shopList,$lat, $long);
        $shopCount = $shopList->totalCount();  
        if(!$this->isJson)
        {
            $this->_redirect('/admin/shop/');   
        }
        else 
        {
            $this->view->shopList = array();
            $includeInner = $this->getQuotedParam('with_details',false);
            $includeTransmitters = $this->getQuotedParam('with_transmitters',false);
            foreach($shopList as $shop)
            {
                $this->view->shopList[] = $shop->getJsonObject($this->currentUser,$includeInner,$includeTransmitters);
            }
            $this->view->totalCount = $shopCount;
        }
        
        $this->view->pageNum = $pageNum;
    }
    
    public function innerAction()
    {
        $id = $this->getQuotedParam('shop_id');
        if($id === null)
            $id = $this->getQuotedParam('id');
        $shop = Mapper_Shop::getInstance()->getEntityById($id);
        $this->checkForNullEntity($shop);
        
        if($this->isJson)
        {
            $object = new Memcached_Object_Shop_Info($this->currentUser,$shop);
            if($object->exists() && $this->isSetMobile304())
            {
                //nothing - empty body
            }
            else
            {
                $productList = $shop->getProducts();
                $salesList = $shop->getSales();
                $scanProductList = $shop->getScanProducts();
                
                $this->view->productList = array();
                foreach($productList as $product)
                {
                    $this->view->productList[] = $product->getJsonObject();
                }
                $this->view->salesList = array();
                foreach($salesList as $sales)
                {
                    $this->view->salesList[] = $sales->getJsonObject();
                }
                $this->view->scanList = array();
                foreach($scanProductList as $scanProduct)
                {
                    $this->view->scanList[] = $scanProduct->getJsonObject($this->currentUser);
                }
                
                $this->view->shopList = array($shop->getJsonObject($this->currentUser));
                $object->set();
                $this->allowMobileCaching();
            }
        }
    }
    
    public function listByUserShopAction()
    {
        $id = $this->getQuotedParam('user_shop_id');
        if($id === null)
            $id = $this->getQuotedParam('id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);
        $shopList = Mapper_Shop::getInstance()->getListByTradeNetwork($tradeNetwork);
        $this->view->shopList = array();
        foreach($shopList as $shop)
        {
            $this->view->shopList[] = $shop->getJsonObject($this->currentUser);
        }
    }
    
    public function fullAction()
    {
        $shopList = Mapper_Shop::getInstance()->getFullClientList();
        $this->view->shopList = array();
        $includeInner = $this->getQuotedParam('with_details',false);
        $includeTransmitters = $this->getQuotedParam('with_transmitters',false);
        foreach($shopList as $shop)
        {
            $this->view->shopList[] = $shop->getJsonObject($this->currentUser,true,true);
        }
    }
}