<?php

class VerifyController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('request', 'json')
                ->addActionContext('code', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function requestAction()
    {
        $phone = $this->getRequest()->getParam('phone');
        $messageSender = new Message_LittleSms(false);
        $verifyEntity = Mapper_User_Verify::getInstance()->getEntityByUser($this->currentUser);
        if($verifyEntity === null)
        {
            $verifyEntity = Entity_User_Verify::add($this->currentUser);
        }
        else
        {
            $verifyEntity->regenerateCode();
        }
        $this->view->success = $messageSender->sendSms($phone,'Код потверждения: '.$verifyEntity->getCode());
        $this->currentUser->getAddress()->setPhone($phone);
    }

    public function codeAction()
    {
        $code = strtoupper($this->getRequest()->getParam('code'));
        $actionMaker = new Action_Single_Achievement_Verify($this->currentUser);
        $actionMaker->setCode($code);
        $action = $actionMaker->make();
        $this->view->success = $action['trm_main'] != null;
    }
}