<?php

class LoginController extends Shopfee_Controller_Action
{
    public function init()
    {
        parent::init();
        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('udid', 'json')
                ->addActionContext('facebook', 'json')
                ->addActionContext('contact', 'json')
                ->addActionContext('email', 'json')
                ->addActionContext('has-facebook-user', 'json')
                ->addActionContext('has-contact-user', 'json')
                ->addActionContext('not-authorized-phone', 'json')
                ->initContext();
    }

    /**
     * @deprecated. Используется только для REST-API в браузере, а на клиентах уже нет.
     */
    public function indexAction()
    {
        $cookieLogin = isset($_COOKIE[Auth::COOKIE_LOGIN]) ? $_COOKIE[Auth::COOKIE_LOGIN] : null;
        $cookiePassword = isset($_COOKIE[Auth::COOKIE_PASSWORD]) ? $_COOKIE[Auth::COOKIE_PASSWORD] : null;

        $authSuccess = false;
        $remember = isset($_COOKIE[Auth::COOKIE_REMEMBER]) ? $_COOKIE[Auth::COOKIE_REMEMBER] : false;
        $backUrl = $this->getBackUrl();

        $form = new Form_Login($backUrl);

        //Сохраняем это поле, чтобы в случае неудачного логина залогинить пользователя под ним обратно
        $currentUser = Auth::getCurrentUser();
        $udid = $currentUser ? $currentUser->getUdid() : '';


        //if the user is already logged in
        if ($currentUser && !$udid)
        {
            //Из iphone сейчас может прийти несколько запросов на логин (из-за костыля в серверных запросах - событийная и не событийная модель)
            //Потому, если пользователь уже залогинен, надо отвечать корректно - его информацией, а не редиректом
            if($this->isJson)
            {
                $authSuccess = true;
            }
            else
            {
                $this->_redirect($backUrl);
            }
        }
        //else check the cookie
        else if ($cookieLogin && $cookiePassword)
        {
            $authResult = Auth::login($cookieLogin, $cookiePassword, false, true);
            if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                $authSuccess = true;
        }
        //else it is just form processing or form rendering
        else
        {
            //form processing
            if ($this->getRequest()->isPost() && $form->isValid($_POST))
            {
                $values = $form->getValues();
                if($values['udid'])
                {
                    $authResult = Auth::loginUdid($values['udid']);
                    if ((null === $authResult) || (Zend_Auth_Result::SUCCESS !== $authResult->getCode()))
                    {
                        //Значит, не зарегистрирован. Регистрируем его.
                        $user = Entity_User::add($values['udid'],$values['clean_udid']);
                        $authResult = Auth::loginUdid($values['udid']);
                        $this->inmobi();
                    }
                }
                else if($values['facebook_id'] != 0)
                {
                    $authResult = Auth::loginFacebook($values['facebook_id']);
                }
                else
                {
                    $authResult = Auth::login($values['email'], $values['password'], ($values['remember'] == 'checked' || $values['remember'] == 1));
                }

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                {
                    $authSuccess = true;
                }
                else
                {
                    if(!$this->isJson)
                    {
                        $this->view->message = 'Неправильный логин или пароль';
                    }
                    else
                    {
                        //Если не вышло - логиним пользователя обратно
                        if($udid)
                            Auth::loginUdid($udid);
                    }
                }
            }
        }


        if($authSuccess && isset($values['clean_udid']))
        {
            Auth::getCurrentUser()->setCleanUdid($values['clean_udid']);
        }

        if(!$this->isJson)
        {
            if ($authSuccess)
            {
                //if the last page was from authorize controller
                //refirect to main page to avoid loops
                if (false === strpos($backUrl, '/login/'))
                    $this->_redirect($backUrl);
                else
                    $this->_redirect('/');
            }
            else {
                $this->view->form = $form;
            }
        }
        else
        {
            $this->setupViewWithUserInfo($authSuccess);
        }

        $this->view->backUrl = urlencode($backUrl);
    }

    public function forbiddenAction()
    {

    }

    public function notAuthorizedPhoneAction()
    {
        $this->view->noPhoneAuthorization = true;
    }

    public function udidAction()
    {
        $authSuccess = false;

        //Сохраняем это поле, чтобы в случае неудачного логина залогинить пользователя под ним обратно
        $currentUser = Auth::getCurrentUser();
        $udid = $currentUser ? $currentUser->getUdid() : '';


        //if the user is already logged in
        if ($currentUser && !$udid)
        {
            //Из iphone сейчас может прийти несколько запросов на логин (из-за костыля в серверных запросах - событийная и не событийная модель)
            //Потому, если пользователь уже залогинен, надо отвечать корректно - его информацией, а не редиректом
            $authSuccess = true;
        }
        //else it is just form processing or form rendering
        else
        {
            //form processing
            if ($this->getRequest()->isPost())
            {
                $udIdValue = $this->getQuotedParam('udid');
                $cleanUdidValue = $this->getQuotedParam('clean_udid');
                $authResult = Auth::loginUdid($udIdValue);
                if ((null === $authResult) || (Zend_Auth_Result::SUCCESS !== $authResult->getCode()))
                {
                    //Значит, не зарегистрирован. Регистрируем его.

                    $user = Entity_User::add($udIdValue,$cleanUdidValue);
                    $authResult = Auth::loginUdid($udIdValue);
                    $this->inmobi();
                }

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                {
                    $authSuccess = true;
                    Auth::getCurrentUser()->setCleanUdid($cleanUdidValue);
                }
            }
        }

        $this->setupViewWithUserInfo($authSuccess);
    }

    public function emailAction()
    {
        $authSuccess = false;
        $backUrl = $this->getBackUrl();

        $form = new Form_Login($backUrl);

        //Сохраняем это поле, чтобы в случае неудачного логина залогинить пользователя под ним обратно
        $currentUser = Auth::getCurrentUser();
        $udid = $currentUser ? $currentUser->getUdid() : '';


        //if the user is already logged in
        if ($currentUser && !$udid)
        {
            //Из iphone сейчас может прийти несколько запросов на логин (из-за костыля в серверных запросах - событийная и не событийная модель)
            //Потому, если пользователь уже залогинен, надо отвечать корректно - его информацией, а не редиректом
            $authSuccess = true;
        }
        //else it is just form processing or form rendering
        else
        {
            //form processing
            if ($this->getRequest()->isPost() && $form->isValid($_POST))
            {
                $values = $form->getValues();
                $authResult = null;
                if(Mapper_User::getInstance()->getEntityByEmail($values['email']))
                {
                    $authResult = Auth::login($values['email'], $values['password'], ($values['remember'] == 'checked' || $values['remember'] == 1));
                }

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                {
                    $authSuccess = true;
                    Auth::getCurrentUser()->setCleanUdid($values['clean_udid']);
                }
                else
                {
                    //Если не вышло - логиним пользователя обратно. Т.к. теоретически у него может быть нужный email, но не совпадать пароль.
                    if($udid)
                        Auth::loginUdid($udid);
                }
            }
        }

        $this->setupViewWithUserInfo($authSuccess);
    }

    public function facebookAction()
    {
        $authSuccess = false;
        //Сохраняем это поле, чтобы в случае неудачного логина залогинить пользователя под ним обратно
        $currentUser = Auth::getCurrentUser();
        $udid = $currentUser ? $currentUser->getUdid() : '';


        //if the user is already logged in
        if($currentUser && !$udid)
        {
            //Из iphone сейчас может прийти несколько запросов на логин (из-за костыля в серверных запросах - событийная и не событийная модель)
            //Потому, если пользователь уже залогинен, надо отвечать корректно - его информацией, а не редиректом
            $authSuccess = true;
        }
        //else it is just form processing or form rendering
        else
        {
            //form processing
            if ($this->getRequest()->isPost())
            {
                $facebookId = intval($this->getRequest()->getParam('facebook_id'));
                $authResult = null;
                if(Mapper_User::getInstance()->getEntityByFacebookId($facebookId))
                {
                    $authResult = Auth::loginFacebook($facebookId);
                }

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                {
                    $authSuccess = true;
                    if(isset($values['clean_udid']))
                        Auth::getCurrentUser()->setCleanUdid($values['clean_udid']);
                }
                else
                {
                    $this->view->message = 'Неправильный логин или пароль';
                }
            }
        }

        $this->setupViewWithUserInfo($authSuccess);
    }

    public function contactAction()
    {
        $authSuccess = false;
        //Сохраняем это поле, чтобы в случае неудачного логина залогинить пользователя под ним обратно
        $currentUser = Auth::getCurrentUser();
        $udid = $currentUser ? $currentUser->getUdid() : '';


        //if the user is already logged in
        if($currentUser && !$udid)
        {
            //Из iphone сейчас может прийти несколько запросов на логин (из-за костыля в серверных запросах - событийная и не событийная модель)
            //Потому, если пользователь уже залогинен, надо отвечать корректно - его информацией, а не редиректом
            $authSuccess = true;
        }
        //else it is just form processing or form rendering
        else
        {
            //form processing
            if ($this->getRequest()->isPost())
            {
                $contactId = intval($this->getRequest()->getParam('contact_id'));
                $authResult = null;
                if(Mapper_User::getInstance()->getEntityByContactId($contactId))
                {
                    $authResult = Auth::loginContact($contactId);
                }

                if ((null !== $authResult) && (Zend_Auth_Result::SUCCESS === $authResult->getCode()))
                {
                    $authSuccess = true;
                    Auth::getCurrentUser()->setCleanUdid($values['clean_udid']);
                }
                else
                {
                    $this->view->message = 'Неправильный логин или пароль';
                }
            }
        }

        $this->setupViewWithUserInfo($authSuccess);
    }

    public function hasFacebookUserAction()
    {
        $fbUser = Mapper_User::getInstance()->getEntityByFacebookId(intval($this->getRequest()->getParam('facebook_id')));
        $this->view->exists = $fbUser !== null;
    }

    public function hasContactUserAction()
    {
        $cUser = Mapper_User::getInstance()->getEntityByContactId(intval($this->getRequest()->getParam('contact_id')));
        $this->view->exists = $cUser !== null;
    }

    protected function inmobi()
    {
        $request = $this->getRequest();

        $appId = $request->getParam('appid');
        $inmobi = new Inmobi($appId,$request->getParam('odin1'),$request->getParam('udid'),$request->getParam('goalName'));
        $inmobi->makeRequest();
    }

    protected function setupViewWithUserInfo($authSuccess)
    {
        /** @var $user Entity_User */
        $user = Auth::getCurrentUser();

        $this->view->success = $authSuccess;
        $this->view->user = $authSuccess ? $user->getJsonObject() : $user->getNullJsonObject();
    }
}