<?php

class ProductController extends ShopsBoundController
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        $pageNum = $this->getQuotedParam("page");
        if( empty($pageNum) ) {
            $pageNum = 1;
        }

        $shop = $this->extractShop();

        $productList = $shop->getProducts($pageNum);
        $productCount = $productList->totalCount();

        $this->view->productList = array();
        foreach($productList as $product)
        {
            $this->view->productList[] = $product->getJsonObject();
        }
        $this->view->totalCount = $productCount;
        $this->view->pageNum = $pageNum;
    }

    public function addAction()
    {
        $form = new Form_Product(0);
        $this->view->selectedShopIds = array();

        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $uploadedFileName = $form->uploadFile('file');
                $price = isset($values['price']) ? $values['price'] : 0;
                $discount = isset($values['discount']) ? $values['discount'] : 0;
                $tradeNetwork = Auth::getCurrentTradeNetwork();
                $shopIds = $tradeNetwork->getShops(null)->getListEntitiesId();

                Entity_Product::add($values['name'],$tradeNetwork,$shopIds,$values['description'],$price,$discount,$uploadedFileName);
                $db->commit();
                $this->_redirect('/edit/shopcase/#product_caption');

            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getQuotedParam('id');
        $product = Mapper_Product::getInstance()->getEntityById($id);
        $this->checkForNullEntity($product);

        $form = new Form_Product($id);
        $this->view->selectedShopIds = $product->getShopIds();
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try
            {
                $tradeNetwork = Auth::getCurrentTradeNetwork();
                $shopIds = $tradeNetwork->getShops(null)->getListEntitiesId();
                $params = array(
                                'name' => $values['name'],
                                'description' => $values['description'],
                                'price' => $values['price'],
                                'discount' => $values['discount'],
                                'shop_ids' => $shopIds
                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $product->update($params);
                $db->commit();
                $this->_redirect('/edit/shopcase/#product_caption');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($product);

        $this->view->product = $product;
        $this->view->form = $form;
        $this->view->shop = $product->getShop();
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
    }

    public function formAction()
    {
        $id = $this->getQuotedParam('id');
        $form = new Form_Product($id);
        if($id)
        {
            $product = Mapper_Product::getInstance()->getEntityById($id);
            $this->checkForNullEntity($product);
            $form->setValues($product);
            $selectedShopIds = $product->getShopIds();
        }
        else
        {
            $product = null;
            $selectedShopIds = array();
        }

        $this->view->product = $product;
        $this->view->form = $form;
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');

        $view = new Zend_View();
        $view->setScriptPath(realpath(Application::EDIT_VIEWS_PATH));
        $view->assign(array('form' => $form,'product' => $product,'currentTradeNetwork' => Auth::getCurrentTradeNetwork(),'selectedShopIds' => $selectedShopIds));
        $this->view->html = $view->render('product/form.phtml');
    }

    public function deleteAction()
    {
        $id = $this->getQuotedParam('id');
        $product = Mapper_Product::getInstance()->getEntityById($id);
        $this->checkForNullEntity($product);

        $product->delete();
        $this->_redirect('/edit/shopcase/#product_caption');
    }

    public function detailAction()
    {
        $id = $this->getQuotedParam('id');
        $product = Mapper_Product::getInstance()->getEntityById($id);
        if($product !== null)
        {
            $this->view->product = $product;
            $this->view->tradeNetwork = $product->getTradeNetwork();
        }
        else
        {
            $this->set404();
        }
    }

    /**
    *   @return Entity_Shop|null
    */
    protected function extractShop()
    {
        $shopId = $this->getQuotedParam('shop_id');
        $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
        $this->checkForNullEntity($shop);

        $this->view->shop = $shop;

        return $shop;
    }
}