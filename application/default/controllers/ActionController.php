<?php
class ActionController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('enter', 'json')
                ->addActionContext('exit', 'json')
                ->addActionContext('exit-cancel', 'json')
                ->addActionContext('scan', 'json')
                ->addActionContext('prize', 'json')
                ->addActionContext('buy-barcode', 'json')
                ->addActionContext('list', 'json')
                ->addActionContext('checkin-list', 'json')
                ->addActionContext('prize-list', 'json')
                ->addActionContext('scan-list', 'json')
                ->addActionContext('shoppage', 'json')
                ->addActionContext('promo', 'json')
                ->addActionContext('pack', 'json')
                ->addActionContext('test', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function enterAction()
    {
        $db = SFM_DB::getInstance();

        $id = $this->getQuotedParam('transmitter_id');
        $activeRuleItemId = $this->getQuotedParam('active_rule_item_id');

        $this->view->actionAmount = 0;
        $this->view->wasCheckin = false;
        $db->beginTransaction();
        try
        {
            $transmitter = Mapper_Transmitter::getInstance()->getEntityById($id);
            $this->checkForNullEntity($transmitter);
            $frqList = isset($_REQUEST['frq']) ? $_REQUEST['frq'] : array();
            $action = new Action_Single_Checkin($this->currentUser);
            $action->setTransmitter($transmitter)
                    ->setFrqList($frqList)
                    ->setUseEncoding($this->getQuotedParam('use_encoding') == 1 ? true : false)
                    ->setActiveRuleItemId($activeRuleItemId)
                    ->setOldApi(true);

            $actionResult = $action->make();
            $db->commit();
            $this->view->success = true;
            $this->view->actionAmount = $actionResult['amount'];
            $this->view->wasCheckin = isset($actionResult['wasCheckin']) ? $actionResult['wasCheckin'] : false;
            switch($action->getError())
            {
                case Action_Single_Abstract::WARNING_FRQ_LIST_WRONG:
                    $this->view->success = false;
                    $this->view->wrongFrequency = true;
                    break;
                case Action_Single_Abstract::ERROR_DUPLICATE:
                    $this->view->success = false;
                    $this->view->duplicate = true;
                    break;
                case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
                    $this->view->success = false;
                    $this->view->limitReached = true;
                    break;
            }
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function exitAction()
    {

        $db = SFM_DB::getInstance();

        $id = $this->getQuotedParam('transmitter_id');
        //Пока все чекауты одного типа
        $db->beginTransaction();
        try
        {
            $transmitter = Mapper_Transmitter::getInstance()->getEntityById($id);
            $this->checkForNullEntity($transmitter);
            Entity_Action_Exit_Attempt::add($this->currentUser, $transmitter->getShop(),$transmitter->getTransmitterTypeId());
            $db->commit();
            $this->view->success = true;
        }
        catch(Action_Exception_Duplicate $e)
        {
            $db->rollback();
            $this->view->success = false;
            $this->view->duplicate = true;
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function exitCancelAction()
    {

        $id = $this->getQuotedParam('transmitter_id');
        $db = SFM_DB::getInstance();
        $db->beginTransaction();
        try
        {
            $transmitter = Mapper_Transmitter::getInstance()->getEntityById($id);
            $this->checkForNullEntity($transmitter);
            Mapper_Action_Exit_Attempt::getInstance()->clearNewByUserAndShop($this->currentUser, $transmitter->getShop(),$transmitter->getTransmitterTypeId());
            $db->commit();
            $this->view->success = true;
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function shoppageAction()
    {

        $db = SFM_DB::getInstance();

        $id = $this->getQuotedParam('user_shop_id');
        $activeRuleItemId = $this->getQuotedParam('active_rule_item_id');
        $db->beginTransaction();
        try
        {
            $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
            $this->checkForNullEntity($tradeNetwork);
            $actionMaker = new Action_Single_TradeNetworkPage($this->currentUser);
            $actionMaker->setTradeNetwork($tradeNetwork)
                        ->setActiveRuleItemId($activeRuleItemId)
                        ->setOldApi(true);
            $action = $actionMaker->make();
            $db->commit();
            $this->view->success = true;
            $this->view->actionAmount = $action['amount'];
            switch($actionMaker->getError())
            {
                case Action_Single_Abstract::ERROR_DUPLICATE:
                    $this->view->success = false;
                    $this->view->duplicate = true;
                    break;
                case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
                    $this->view->success = false;
                    $this->view->limitReached = true;
                    break;
            }
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function scanAction()
    {

        $db = SFM_DB::getInstance();

        $barcode = $this->getQuotedParam('barcode');
        $shopId = $this->getQuotedParam('shop_id');
        $lat = $this->getQuotedParam('lat');
        $long = $this->getQuotedParam('long');
        $activeRuleItemId = $this->getQuotedParam('active_rule_item_id');
        $db->beginTransaction();
        try
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            $this->checkForNullEntity($shop);

            $actionMaker = new Action_Single_Scan($this->currentUser);
            $actionMaker->setBarcode($barcode)
                        ->setShop($shop)
                        ->setLat($lat)
                        ->setLong($long)
                        ->setActiveRuleItemId($activeRuleItemId)
                        ->setOldApi(true);
            $action = $actionMaker->make();
            $db->commit();
            $this->view->success = true;
            $this->view->actionAmount = $action['amount'];
            switch($actionMaker->getError())
            {
                case Action_Single_Abstract::WARNING_NOT_IN_SHOP:
                    $this->view->notInShop = true;
                    $this->view->success = false;
                    $actionMaker = new Action_Single_Scan($this->currentUser);
                    $actionMaker->setBarcode($barcode)
                                ->setShop($shop)
                                ->setLat($lat)
                                ->setLong($long);
                    $actionMaker->logHackScan();
                    break;
                case Action_Single_Abstract::ERROR_NO_SCAN_PRODUCT:
                    $this->view->success = false;
                    $this->view->noProduct = true;
                    break;
                case Action_Single_Abstract::ERROR_DUPLICATE:
                    $this->view->success = false;
                    $this->view->duplicate = true;
                    break;
                case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
                    $this->view->success = false;
                    $this->view->limitReached = true;
                    break;
            }
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function prizeAction()
    {

        $db = SFM_DB::getInstance();

        $id = $this->getQuotedParam('prize_id');
        $prize = Mapper_Prize::getInstance()->getEntityById($id);
        $this->checkForNullEntity($prize);
        $db->beginTransaction();
        try
        {
            $actionMaker = new Action_Single_Prize($this->currentUser);
            $actionMaker->setPrize($prize)
                        ->setOldApi(true);
            $action = $actionMaker->make();
            $db->commit();
            $prize->deleteMobileCache();
            $this->view->success = true;
            $this->view->actionAmount = -$action['amount'];
            if($prize->getType() == Entity_Prize::TYPE_PROMO)
            {
                $this->view->code = $action['code'];
            }
            switch($actionMaker->getError())
            {
                case Action_Single_Abstract::ERROR_AMOUNT_INSUFFICIENT:
                    $this->view->success = false;
                    $this->view->insufficient = true;
                    break;
                case Action_Single_Abstract::ERROR_NO_PRIZE_PROMOCODE:
                    $this->view->success = false;
                    $this->view->noPromocode = true;
                    break;
                case Action_Single_Abstract::ERROR_DUPLICATE:
                    $this->view->success = false;
                    $this->view->duplicate = true;
                    break;
                case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
                    $this->view->success = false;
                    $this->view->limitReached = true;
                    break;
            }
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function buyBarcodeAction()
    {

        $db = SFM_DB::getInstance();

        $barcode = $this->getQuotedParam('barcode');
        $shopId = $this->getQuotedParam('shop_id');
        $activeRuleItemId = $this->getQuotedParam('active_rule_item_id');
        if($shopId)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
            $this->checkForNullEntity($shop);
        }
        else
        {
            $shop = null;
        }
        $db->beginTransaction();
        try
        {
            $actionMaker = new Action_Single_BuyBarcode($this->currentUser);
            $actionMaker->setBarcode($barcode)
                        ->setShop($shop)
                        ->setActiveRuleItemId($activeRuleItemId)
                        ->setOldApi(true);
            $action = $actionMaker->make();
            $db->commit();
            $this->view->success = true;
            $this->view->actionAmount = $action['amount'];
            switch($actionMaker->getError())
            {
                case Action_Single_Abstract::ERROR_NO_BUYBARCODE:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_NO_BUYBARCODE;
                    $this->view->errorMessage = 'Неверный штрихкод';
                    break;
                case Action_Single_Abstract::ERROR_BUYBARCODE_INACTIVE:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_IS_INACTIVE;
                    $this->view->errorMessage = 'Неверный штрихкод';
                    break;
                /*case Action_Single_Abstract::INFO_NO_SHOP_ENTER:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_NO_SHOP_ENTER;
                    $this->view->errorMessage = 'Отсутствует вход в магазин';
                    break;*/
                case Action_Single_Abstract::WARNING_WRONG_SHOP:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_WRONG_SHOP;
                    $this->view->errorMessage = 'Штрихкод не соответствует магазину';
                    break;
                case Action_Single_Abstract::ERROR_DUPLICATE:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_DUPLICATE;
                    $this->view->errorMessage = 'Сегодня вас уже вознаградили за покупку';
                    break;
                case Action_Single_Abstract::WARNING_NO_RULE_ITEM:
                    $this->view->success = false;
                    $this->view->errorCode = Entity_Action_BuyBarcode::ERROR_INACTIVE_RULE;
                    $this->view->errorMessage = 'Акция закончилась';
                    break;
            }
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }

    public function listAction()
    {

        $showAll = $this->getQuotedParam('all') ? true : false;
        $from = $this->getQuotedParam('from');
        $to = $this->getQuotedParam('to');
        $fromLastUpdate = $this->getQuotedParam('today') ? true : false;

        $criteria = new Criteria_Trm_Main();
        $criteria->setUser($this->currentUser);
        if(!$showAll)
        {
             $criteria->setNumberFrom($from)
                      ->setNumberTo($to);
        }
        if($fromLastUpdate)
        {
            //История с момента предыдущего updater-a.
            $criteria->setDateFrom(date('Y-m-d'))
                     ->setTimeFrom(date('00:00:00'));
        }
        $this->view->list = array();
        foreach(Mapper_Trm_Main::getInstance()->getListByCriteria($criteria) as $trmMain)
        {
            $this->view->list[] = $trmMain->getJsonObject();
        }
    }

    public function promoAction()
    {
        $db = SFM_DB::getInstance();

        $code = $this->getQuotedParam('code');
        $db->beginTransaction();
        try
        {
            $actionMaker = new Action_Single_Promocode($this->currentUser);
            $actionMaker->setCode($code)
                        ->setLat($this->getRequest()->getParam('alat'))
                        ->setLong($this->getRequest()->getParam('alon'));
            $action = $actionMaker->make();
            $db->commit();
            $this->view->success = true;
            $this->view->actionAmount = $action['amount'];
            $this->view->promoError = $actionMaker->getError();
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
            throw $e;
        }
    }

    public function packAction()
    {
        $param = json_decode($this->getRequest()->getParam('param'),true);
        $myDateTime = $param && isset($param['cur_date_time']) ? $param['cur_date_time'] : null;
        $this->view->actionResults = array();
        if($param && $myDateTime)
        {
            $myDateTime = $param['cur_date_time'];
            $maker = new Action_Bulk($this->currentUser, $param['actions']);
            $maker->setIp($_SERVER['REMOTE_ADDR'])
                  ->setClientDateTime($myDateTime);
            $this->view->actionResults = $maker->make();
            $this->view->amount = $this->currentUser->getAmount();
            $this->view->totalAmount = $this->currentUser->getTotalAmount();
        }
        else
        {
            $this->view->actionError = Action_Single_Abstract::ERROR_WRONG_PARAMETER;
        }
    }

    public function testAction()
    {
        /*$udid = $this->getRequest()->getParam('udid');
        Auth::logoff();
        Auth::loginUdid($udid);*/

        /*
        $this->currentUser->setFloatTime(null);
        die();*/
        $actions = array();
        $actions[] = array(
                        'anum' => 0,
                        'type' => 1,
                        'alat' => '55.734831',
                        'alon' => '37.458345',
                        'adate' => date('Y-m-d'),//"2012-12-13",
                        'atime' => /*date('H:i:s'),/*/"11:02:21",
                        "active_rule_item_id" => 659,
                        'transmitter_id' => 62,
                        'frqList' => array(20746,20920,20661,20920,20576,20920,20920,20920,20833,20661,20833,20661)
        );
        /*$actions[] = array(
                        'anum' => 0,
                        'type' => Entity_Rule_Item::TYPE_CHECKIN,
                        'alat' => $alat,
                        'alon' => $along,
                        'adate' => $adate,
                        'atime' => $atime,
                        'transmitter_id' => 203,
                        'use_encoding' => false,
        );
        $actions[] = array(
                        'anum' => 1,
                        'type' => Entity_Rule_Item::TYPE_SHOPPAGE,
                        'alat' => $alat,
                        'alon' => $along,
                        'adate' => $adate,
                        'atime' => $atime,
                        'user_shop_id' => 1,
        );
        $actions[] = array(
                        'anum' => 2,
                        'type' => Entity_Rule_Item::TYPE_SCAN,
                        'alat' => $alat,
                        'alon' => $along,
                        'adate' => $adate,
                        'atime' => $atime,
                        'shop_id' => 1,
                        'barcode' => '24769780',
        );
        $actions[] = array(
                        'anum' => 3,
                        'type' => Entity_Rule_Item::TYPE_BUY_BARCODE,
                        'alat' => $alat,
                        'alon' => $along,
                        'adate' => $adate,
                        'atime' => $atime,
                        'shop_id' => 1,
                        'barcode' => '3056094812811',
        );*/



        /*Auth::getCurrentUser()->setFloatTime(null);*/
        $param = json_encode(array('actions' => $actions));
        $param = json_decode($param,true);
        $myDateTime = date('Y-m-d H:i:s',time() /*+ 1*60*60*/);//'2012-12-02 19:34:00';
        $this->view->actionResults = array();
        if($param && $myDateTime)
        {
            $maker = new Action_Bulk($this->currentUser, $param['actions']);
            $maker->setIp($_SERVER['REMOTE_ADDR'])
                  ->setClientDateTime($myDateTime);
            $this->view->actionResults = $maker->make();
            $this->view->amount = $this->currentUser->getAmount();
            $this->view->totalAmount = $this->currentUser->getTotalAmount();
        }
        else
        {
            $this->view->actionError = Action_Single_Abstract::ERROR_WRONG_PARAMETER;
        }
        /*var_dump($actionResults);
        die();*/
    }
}