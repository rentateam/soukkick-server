<?php

class ReplicationController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('get-data', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function getDataAction()
    {
        //100 объектов максимум
        $chunkSize = 100;
        $this->view->shopNet = $this->getArrayFromAggregate(Mapper_Trade_Network::getInstance()->getList(1,null,$chunkSize),$chunkSize);
        $this->view->prize = $this->getArrayFromAggregate(Mapper_Prize::getInstance()->getListStock(1,$chunkSize),$chunkSize);

        $banner = Entity_Banner::getFirstBanner();
        if($banner)
        {
            $this->view->banner = $banner->getJsonReplicationObject();
        }
    }

    protected function getArrayFromAggregate(SFM_Aggregate $aggregate, $chunkSize = 100)
    {
        $resultList = array();
        for($i=1;$i<=ceil($aggregate->totalCount()/$chunkSize);$i++){
            $aggregate->clearLoadedEntities();
            $aggregate->loadEntitiesForCurrentPage($i,$chunkSize);
            foreach($aggregate as $item){
                $resultList[] = $item->getJsonReplicationObject();
            }
        }
        return $resultList;
    }
}