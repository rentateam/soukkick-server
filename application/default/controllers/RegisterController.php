<?php
class RegisterController extends Shopfee_Controller_Action
{
    protected $_authActions = array('index');

    public function init()
    {
        parent::init();
        $this->_helper->contextSwitch()
                ->addActionContext('check-email', 'json')
                ->addActionContext('index', 'json')
                ->initContext();
    }

    public function indexAction()
    {
        $backUrl = $this->getBackUrl();
        $user = Auth::getCurrentUser();

        $form = new Form_Register($backUrl);
        $form->setAndroidCrutch($this->getQuotedParam('no_files'),0);
        $form->setValues($user);
        $db = SFM_DB::getInstance();
        if ($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            try
            {
                $db->beginTransaction();
                $values = $form->getValues();

                //Проверяем, надо ли регистрировать его или он уже сегодня регистрировался
                if(!$values['facebook_id'] && !$values['contact_id'] && !$user->canRegister())
                {
                    $this->view->wasRegToday = true;
                    throw new Shopfee_Exception('Such udid was registered already');
                }

                $picture = $form->uploadFile('photo');

                if($values['facebook_id'])
                {
                    $existingEntity = Mapper_User::getInstance()->getEntityByFacebookId($values['facebook_id']);
                    if($existingEntity !== null && $existingEntity->getId() != $this->currentUser->getId())
                    {
                        throw new Entity_User_Exception_Facebook();
                    }
                }
                if($values['contact_id'])
                {
                    $existingEntity = Mapper_User::getInstance()->getEntityByContactId($values['contact_id']);
                    if($existingEntity !== null && $existingEntity->getId() != $this->currentUser->getId())
                    {
                        throw new Entity_User_Exception_Contact();
                    }
                }
                if($values['email'] && !isset($values['facebook_id']) && !isset($values['contact_id']))
                {
                    $existingEntity = Mapper_User::getInstance()->getEntityByEmail($values['email']);
                    if($existingEntity !== null && $existingEntity->getId() != $this->currentUser->getId())
                    {
                        throw new Entity_User_Exception_Email();
                    }
                }

                //Тонкий момент. Меняем мыло пользователю только в том случае, если оно не установлено (это предусмотрено внутри setEmailAndPassword)
                //а пароль - только, если он непустой.
                $user->setEmailAndPassword($values['email'], $values['password']);
                $birthdate = $values['birthdate'] ? date('Y-m-d',strtotime($values['birthdate'])) : null;
                $user->setNames($values['name'], $values['surname'],$values['is_male'] == '1', $birthdate,true);
                if($values['facebook_id'])
                {
                    $this->view->amountFacebook = $user->updateFacebookId($values['facebook_id']);
                }
                if($values['contact_id'])
                {
                    $this->view->amountContact = $user->updateContactId($values['contact_id']);
                }
                if($picture)
                {
                    $user->setPicture($picture);
                }
                if($values['promo_code'])
                {
                    $user->usePromoCode($values['promo_code']);
                }

                //authorize the user
                $this->view->user = $user->getJsonObject();
                $this->view->success = true;
                $db->commit();

                if(!$this->isJson)
                    $this->redirectToBackurl($backUrl);
            }
            catch(Entity_User_Exception_Facebook $e)
            {
                $this->view->user = Entity_User::getNullJsonObject();
                $this->view->success = false;
                $this->view->duplicateFacebookId = true;
                $db->rollback();
            }
            catch(Entity_User_Exception_Contact $e)
            {
                $this->view->user = Entity_User::getNullJsonObject();
                $this->view->success = false;
                $this->view->duplicateContactId = true;
                $db->rollback();
            }
            catch(Entity_User_Exception_Email $e)
            {
                $this->view->user = Entity_User::getNullJsonObject();
                $this->view->success = false;
                $this->view->duplicateEmail = true;
                $db->rollback();
            }
            catch (Shopfee_Exception $e)
            {
                if(!$this->isJson)
                {
                    $this->_redirect('/register/');
                }
                else
                {
                    $this->view->success = false;
                    $this->view->user = Entity_User::getNullJsonObject();
                }
                $db->rollback();
            }
        }
        else
        {
            $this->view->success = false;
            $errorMessages = array();
            if($this->getRequest()->isPost())
            {
                foreach($form->getElements() as $element)
                {
                    $elementName = $element->getName();
                    if(!isset($errorMessages[$elementName]))
                        $errorMessages[$elementName] = array();

                    $errorMessages[$elementName] = array_merge($errorMessages[$elementName],array_values($element->getMessages()));
                }
                foreach($errorMessages as $elementName => $elementContents)
                {
                    $errorMessages[$elementName] = array_unique($elementContents);
                }

                $this->view->errorMessage = $errorMessages;
            }
            $this->view->user = Entity_User::getNullJsonObject();
        }

        if(!$this->isJson)
        {
            $this->view->form = $form;
            $this->view->backUrl = urlencode($backUrl);
        }
    }

    public function shopAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $backUrl = $this->getBackUrl();

        //if the user is already logged in
        if (Auth::getCurrentBpaManager())
            $this->redirectToBackurl($backUrl,'/edit/');

        $form = new Form_RegisterShop($backUrl);
        try
        {
            if ($this->getRequest()->isPost())
            {
                if($form->isValid($_POST))
                {
                    $values = $form->getValues();

                    $picture = $form->uploadFile('photo');

                    $db = SFM_DB::getInstance();
                    $db->beginTransaction();
                    try
                    {
                        $bpaMain = Entity_Bpa_Main::add($values['shop_name'],Entity_Shop::getType($values['type']),Entity_Bpa_Main::DEFAULT_CONVERSION_RATE_TO_FISHKI,Entity_Bpa_Main::DEFAULT_CONVERSION_RATE_FROM_FISHKI);
                        $manager = Entity_Bpa_Manager::add($bpaMain,$values['email'],$values['password'],'',$values['surname']);
                        $shopInfo = Entity_Trade_Network::serializeShopInfo($values['city'],$values['size']);
                        $network = Entity_Trade_Network::add($bpaMain,$values['shop_name'],Entity_Shop::getType($values['type']),$picture,null,$values['phone'],false,$shopInfo);
                        $db->commit();
                        $this->_redirect('/register/success/');
                    }
                    catch(Exception $e)
                    {
                        $db->rollback();
                        $this->error = true;
                        throw $e;
                    }
                }
            }
            else
            {
                $form->setValuesFromArray($_GET);
            }
        }
        catch (Shopfee_Exception $e)
        {
                //$this->_redirect('/register/');
            throw $e;
        }

        $this->view->registerForm = $form;
        $this->view->backUrl = urlencode($backUrl);
    }

    protected function redirectToBackurl($backUrl, $defaultUrl = '/')
    {
        //redirect him to the page he came from
        //but only if it not the login page
        //otherwise we'll receive a loop
        if (false === strpos($backUrl, '/login') && false === strpos($backUrl, '/register'))
            $this->_redirect($backUrl);
        else
            $this->_redirect($defaultUrl);
    }

    public function checkEmailAction()
    {
        $facebookId = $this->getQuotedParam('facebook_id');
        $contactId = $this->getQuotedParam('contact_id');
        if(!$facebookId && !$contactId)
        {
            $user = Mapper_User::getInstance()->getEntityByEmail(strtolower($this->getQuotedParam('email')));
            $result = ($user===null) ? true : false;
            if($this->getQuotedParam('except') && !$result)
            {
                $result = ($this->currentUser && $this->currentUser->getId() == $user->getId()) ? true : false;
            }
        }
        else
        {
            //Если регистрация идет с соцсеток - не проверяем почту на дубль.
            $result = true;
        }
        $this->view->success = $result;
    }

    public function successAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    }
}