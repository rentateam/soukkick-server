<?php
class ErpController extends Shopfee_Controller_Action
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('in', 'xml')
                ->addActionContext('out', 'xml');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function inAction()
    {
        $c1 = new C1_BuyBarcode($this->getQuotedParam('access_token'));
        $this->view->shopList = $c1->getShopList();
        $this->view->barcodeList = $c1->in();
        $this->view->code = $c1->getErrorCode();
        $this->view->message = $c1->getErrorMessage();
        
        $view = new Zend_View();
        $view->setScriptPath(realpath(Application::VIEWS_PATH));
        $viewParams = array(
                                'shopList' => $this->view->shopList,
                                'barcodeList' => $this->view->barcodeList,
                                'code' => $this->view->code,
                                'message' => $this->view->message,
                            );
        $view->assign($viewParams);
        $xmlContents = $view->render('erp/in.xml.phtml');
        
        $filename = 'in'.date('Ymd').'.xml';
        $f = fopen($_SERVER['DOCUMENT_ROOT'].'../1c/'.$filename,'w+');
        fwrite($f,$xmlContents);
        fclose($f);
    }
    
    public function outAction() 
    {
        $c1 = new C1_BuyBarcode($this->getQuotedParam('access_token'));
        if(isset($_FILES['xml']))
        {
            $mailer = new Zend_Mail('windows-1251');
            $mailer->createAttachment(file_get_contents($_FILES['xml']['tmp_name']),
                                         Zend_Mime::TYPE_TEXT,
                                         Zend_Mime::DISPOSITION_ATTACHMENT,
                                         Zend_Mime::ENCODING_BASE64,
                                         'out.xml');
            $mailer->setFrom(Application::getDefaultEmailAddress(), iconv('utf-8','windows-1251',''));
            $mailer->setBodyText('');
            $mailer->addTo('alex.korolkov@gmail.com', iconv('utf-8','windows-1251',''));
            $mailer->setSubject(iconv('utf-8','windows-1251','Out.xml '.date('Y-m-d H:i:s')));
            $mailer->send();
            
            
            $xmlString = file_get_contents($_FILES['xml']['tmp_name']);
        }
        else
        {
            $xmlString = null;
        }
        $c1->out($xmlString);
        $this->view->code = $c1->getErrorCode();
        $this->view->message = $c1->getErrorMessage();
    }
    
    public function testAction()
    {
        
    }
}