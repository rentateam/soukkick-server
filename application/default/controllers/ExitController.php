<?php

class ExitController extends Shopfee_Controller_Action_Authorized 
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        Auth::logoff();
        if(!$this->isJson)
        {
            $this->_redirect("/");
        }
    }
}