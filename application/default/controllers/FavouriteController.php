<?php

class FavouriteController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('set', 'json')
                ->addActionContext('sync', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $includeInner = $this->getQuotedParam('with_details',false) == '1';
        $this->view->userShopList = array();
        foreach($this->currentUser->getFavouriteTradeNetworks() as $favourite)
        {
            $this->view->userShopList[] = $favourite->getTradeNetwork()->getJsonObject($this->currentUser,$includeInner);
        }
        $this->allowMobileCaching();
    }
    
    public function setAction() 
    {
        $id = $this->getQuotedParam('user_shop_id');
        $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($id);
        $this->checkForNullEntity($tradeNetwork);

        $isFavourite = intval($this->getQuotedParam('is_favourite')) > 0;

        SFM_DB::getInstance()->beginTransaction();

        try {
            $tradeNetwork->setFavourite($this->currentUser, $isFavourite);
            SFM_DB::getInstance()->commit();
            $this->view->success = true;

        } catch(Entity_Favourite_Exception $e) {

            SFM_DB::getInstance()->rollback();
            $this->view->success = false;
            if ($isFavourite) {
                $this->view->duplicateFavourite = true;
            } else {
                $this->view->favouriteNotExists = true;
            }

        } catch(Exception $e) {

            SFM_DB::getInstance()->rollback();
            $this->view->success = false;

        }
    }

    public function syncAction()
    {
        $userShopIds = $this->getRequest()->getParam('user_shop_id', array());

        SFM_DB::getInstance()->beginTransaction();
        try {

            $favouriteTradeNetworks = Mapper_Favourite_TradeNetwork::getInstance()->getListByUser($this->currentUser);

            /** @var $favouriteTradeNetwork Entity_Favourite_TradeNetwork */
            foreach ($favouriteTradeNetworks as $favouriteTradeNetwork) {
                $tradeNetwork = $favouriteTradeNetwork->getTradeNetwork();
                $isFavourite = array_search($tradeNetwork->getId(), $userShopIds);
                if (false === $isFavourite) {
                    $tradeNetwork->setFavourite($this->currentUser, false);
                } else {
                    unset($userShopIds[$isFavourite]);
                }
            }

            foreach ($userShopIds as $userShopId) {
                $tradeNetwork = Mapper_Trade_Network::getInstance()->getEntityById($userShopId);
                $tradeNetwork->setFavourite($this->currentUser, true);
            }
            SFM_DB::getInstance()->commit();
            $this->view->success = true;

        } catch (Exception $e) {

            SFM_DB::getInstance()->rollback();
            $this->view->success = false;

        }
    }
}