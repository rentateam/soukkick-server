<?php

class PrizeController extends Shopfee_Controller_Action_Authorized 
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page");
        if( empty($pageNum)) {
            $pageNum = 0;//$this->isJson ? 1 : 0;
        }
        
        $prizeList = Mapper_Prize::getInstance()->getListStock($pageNum);
        $prizeCount = $prizeList->totalCount();  
        if(!$this->isJson)
        {
            $this->view->prizeList = $prizeList;
            $paginator = new Zend_Paginator( new Paginator_Adapter_Null($prizeCount) );
            $paginator->setItemCountPerPage(Aggregate_Prize::ITEMS_PER_PAGE);
            $paginator->setCurrentPageNumber( $pageNum );
            if($prizeCount > Aggregate_Prize::ITEMS_PER_PAGE)
                $this->view->paginator = $paginator;
                
            $this->view->pageUrl = '/prize/index/';
            $this->view->pageNum = $pageNum;    
        }
        else 
        {
            $object = new Memcached_Object_PrizeList($this->currentUser);
            $cachingTime = 24*60*60;
            if($object->exists() && $this->isSetMobile304($cachingTime))
            {
                //nothing - empty body
            }
            else
            {
                $this->view->prizeList = array();
                foreach($prizeList as $prize)
                {
                    $this->view->prizeList[] = $prize->getJsonObject();
                }
                $this->view->totalCount = $prizeCount;
                $object->set();
                $this->allowMobileCaching();
                $response = $this->getResponse(); 
                $response->setHeader('Cache-Control', 'private', true);
                $response->setHeader('Cache-Control', 'max-age='.$cachingTime);
                $response->setHeader('Cache-Control', 'must-revalidate');
            }
        }
    }
}