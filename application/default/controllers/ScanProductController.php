<?php

class ScanProductController extends ShopsBoundController 
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json')
                ->addActionContext('form', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page");
        if( empty($pageNum) ) {
            $pageNum = 1;
        }
        
        $shop = $this->extractShop();
        
        $scanProductList = $shop->getScanProducts($pageNum);
        $scanProductCount = $scanProductList->totalCount();
        
        if($this->getQuotedParam("format") != "json")
        {
            $this->view->scanProductList = $scanProductList;
            $paginator = new Zend_Paginator( new Paginator_Adapter_Null($scanProductCount) );
            $paginator->setItemCountPerPage(Aggregate_ScanProduct::ITEMS_PER_PAGE);
            $paginator->setCurrentPageNumber( $pageNum );
            if($scanProductCount > Aggregate_ScanProduct::ITEMS_PER_PAGE)
                $this->view->paginator = $paginator;
                
            $this->view->pageUrl = '/scan-product/index/shop_id/'.$shop->getId().'/';
        }
        else
        {
            $this->view->scanProductList = array();
            foreach($scanProductList as $scanProduct)
            {
                $this->view->scanProductList[] = $scanProduct->getJsonObject($this->currentUser);
            }
            $this->view->totalCount = $scanProductCount;
        }       
        $this->view->pageNum = $pageNum;
    }
    
    public function addAction() 
    {
        $form = new Form_ScanProduct(0);
        $this->view->selectedShopIds = array();
        
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $uploadedFileName = $form->uploadFile('file');
                Entity_ScanProduct::add($values['name'],$values['description'],Auth::getCurrentTradeNetwork(),$values['barcode'],$uploadedFileName);
                $db->commit();
                $this->_redirect('/edit/shopcase/#scan_caption');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $this->view->form = $form;
    }
    
    public function formAction() 
    {
        $id = $this->getQuotedParam('id');
        $form = new Form_ScanProduct($id);
        if($id)
        {
            $scanProduct = Mapper_ScanProduct::getInstance()->getEntityById($id);
            $this->checkForNullEntity($scanProduct);
            $selectedShopIds = $scanProduct->getShopIds();
            $form->setValues($scanProduct);
        }
        else
        {
            $scanProduct = null;
            $selectedShopIds = array();
        }
        
        $this->view->scanProduct = $scanProduct;
        $this->view->form = $form;
        $fckDecorator = new Form_Decorator_FCK(array('value' => $form->getValue('description'), 'id' => 'description'));
        $this->view->fck = $fckDecorator->render('');
        
        $view = new Zend_View();
        $view->setScriptPath(realpath(Application::EDIT_VIEWS_PATH));
        $view->assign(array('form' => $form,'scanProduct' => $scanProduct,'currentTradeNetwork' => Auth::getCurrentTradeNetwork(),'selectedShopIds' => $selectedShopIds));
        $this->view->html = $view->render('scan-product/form.phtml');
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $scanProduct = Mapper_ScanProduct::getInstance()->getEntityById($id);
        $this->checkForNullEntity($scanProduct);
        
        $form = new Form_ScanProduct($id);
        $this->view->selectedShopIds = $scanProduct->getShopIds();
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $values = $form->getValues();
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $params = array(
                                'name' => $values['name'],
                                'description' => $values['description'],
                                'barcode' => $values['barcode'],
                            );
                $uploadedFileName = $form->uploadFile('file');
                if($uploadedFileName)
                {
                    $params['picture_path'] = $uploadedFileName;
                }
                $scanProduct->update($params);
                $db->commit();
                $this->_redirect('/edit/shopcase/#scan_caption');
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        $form->setValues($scanProduct);
        
        $this->view->scanProduct = $scanProduct;
        $this->view->form = $form;
        $this->view->shop = $scanProduct->getShop();
    }
    
    public function deleteAction() 
    {
        $id = $this->getQuotedParam('id');
        $scanProduct = Mapper_ScanProduct::getInstance()->getEntityById($id);
        $this->checkForNullEntity($scanProduct);
        
        $scanProduct->delete();
        $this->_redirect('/edit/shopcase/#scan_caption');       
    }
    
    /**
    *   @return Entity_Shop|null
    */
    protected function extractShop()
    {
        $shopId = $this->getQuotedParam('shop_id');
        $shop = Mapper_Shop::getInstance()->getEntityById($shopId);
        $this->checkForNullEntity($shop);
        
        $this->view->shop = $shop;

        return $shop;
    }
}