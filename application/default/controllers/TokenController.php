<?php

class TokenController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('set', 'json')
                ->addActionContext('iphone', 'json')
                ->addActionContext('android', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function iphoneAction()
    {
        $this->_forward('set','token','default',array('type' => Entity_User_Push::ENUM_TYPE_IPHONE));
    }
    
    public function androidAction()
    {
        $this->_forward('set','token','default',array('type' => Entity_User_Push::ENUM_TYPE_IPHONE));
    }
    
    public function setAction() 
    {
        $db = SFM_DB::getInstance();
        
        $token = $this->getQuotedParam('token');
        $type = $this->getQuotedParam('type');
        
        $db->beginTransaction();
        try
        {
            Entity_User_Push::add($this->currentUser, $token, $type);
            $db->commit();
            $this->view->success = true;
        }
        catch(Exception $e)
        {
            $db->rollback();
            $this->view->success = false;
        }
    }
}