<?php
class ProfileController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();
        $this->_helper->contextSwitch()
                ->addActionContext('photo', 'json')
                ->addActionContext('address', 'json')
                ->addActionContext('password', 'json')
                ->addActionContext('name', 'json')
                ->addActionContext('use-promo-code', 'json')
                ->addActionContext('achievement', 'json')
                ->addActionContext('send-contact-invite', 'json')
                ->initContext();
    }

    public function indexAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $cachingTime = 24*60*60;
        if($this->isSetMobile304($cachingTime))
        {
        }
        else
        {
            $this->allowMobileCaching($cachingTime);
        }
    }

    public function promoAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $cachingTime = 24*60*60;
        if($this->isSetMobile304($cachingTime))
        {
        }
        else
        {
            $this->allowMobileCaching($cachingTime);
        }
    }

    public function inviteAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $cachingTime = 24*60*60;
        if($this->isSetMobile304($cachingTime))
        {
        }
        else
        {
            $this->allowMobileCaching($cachingTime);
        }
    }


    public function photoAction()
    {
        $form = new Form_Profile_Photo();
        if ($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $picture = $form->uploadFile('photo');
            $this->currentUser->setPicture($picture);
            $this->view->success = true;
            $this->view->picture = $this->currentUser->getPicturePathFull(false);
        }
        else
        {
            $this->view->success = false;
        }
    }

    public function addressAction()
    {
        $address = $this->currentUser->getAddress();
        $form = new Form_Profile_Address();
        $form->setValues($address);
        if ($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $values = $form->getValues();
                $params = array(
                                    'country' => $values['country'],
                                    'city' => $values['city'],
                                    'post_index' => $values['post_index'],
                                    'street' => $values['street'],
                                    'house' => $values['house'],
                                    'flat' => $values['flat'],
                                    'phone' => $values['phone']
                                );
                $address->update($params);
                $this->view->success = true;
            }
            else
            {
                $this->view->success = false;
            }
        }

        if($this->isJson)
        {
            if(!isset($this->view->success) || $this->view->success)
                $this->view->address = $address->getJsonObject();
            else
                $this->view->address = Entity_User_Address::getNullJsonObject();
        }
    }

    public function passwordAction()
    {
        $form = new Form_Profile_Password();

        if ($this->getRequest()->isPost())
        {
            $dataToValidate = $_POST;
            $dataToValidate['old_password_template'] = $this->currentUser->getPassword();
            if($form->isValid($dataToValidate))
            {
                $values = $form->getValues();
                $this->currentUser->setPassword($values['password']);
                $this->view->errorMessage = '';
            }
            else
            {
                $errorMessages = array();
                foreach($form->getElements() as $element)
                {
                    $errorMessages = array_merge($errorMessages,$element->getErrorMessages());
                }
                $this->view->errorMessage = implode("\n",$errorMessages);
            }
        }
    }

    public function nameAction()
    {
        $form = new Form_Profile_Name();

        if ($this->getRequest()->isPost())
        {
            if($form->isValid($_POST))
            {
                $values = $form->getValues();
                $this->currentUser->setNames($values['name'],$values['surname'],$values['is_male'],$values['birthdate'],false);
                $this->view->errorMessage = '';
            }
            else
            {
                $errorMessages = array();
                foreach($form->getElements() as $element)
                {
                    $errorMessages = array_merge($errorMessages,$element->getErrorMessages());
                }
                $this->view->errorMessage = implode("\n",$errorMessages);
            }
        }
    }

    public function usePromoCodeAction()
    {
        try
        {
            $actionMaker = new Action_Single_Achievement_Promocode($this->currentUser);
            $actionMaker->setPromocode(strtoupper($this->getQuotedParam('promo_code')));
            $action = $actionMaker->make();
            $this->view->success = true;
        }
        catch (Exception $e)
        {
            error_log($e->getMessage());
            $this->view->success = false;
        }
    }

    public function getFacebookFriendsAction()
    {
        $this->view->friendList = array();
        if(isset($_REQUEST['friend']))
        {
            foreach($_REQUEST['friend'] as $facebookId => $name)
            {
                $this->view->friendList[$facebookId] = array('name' => $name,'photo' => 'http://graph.facebook.com/'.$facebookId.'/picture?type=square');
            }
        }
        $this->renderScript('profile/get-social-friends.phtml');
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function getContactFriendsAction()
    {
        $this->view->friendList = array();
        if(isset($_REQUEST['friend']))
        {
            foreach($_REQUEST['friend'] as $contactId => $name)
            {
                $this->view->friendList[$contactId] = array('name' => $name,'photo' => $_REQUEST['photo'][$contactId]);
            }
        }
        $this->renderScript('profile/get-social-friends.phtml');
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function sendContactInviteAction()
    {
        $contact = OAuth_Vkontakte::getInstance(2998160, 'vpORhk6CyuG5OwdLm51c');
        $contact->setAccessToken($this->getQuotedParam('access_token'));
        $this->view->resp = $contact->sendMessage(array(33191198,45926103),urlencode('test message'));
    }

    public function achievementAction()
    {
        $this->view->achievementList = array();
        foreach(Mapper_Achievement::getInstance()->getListActiveByLevel($this->currentUser->getLevel()) as $achievement)
        {
            $this->view->achievementList[] = $achievement->getJsonObject();
        }
    }
}