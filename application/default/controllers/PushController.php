<?php

class PushController extends Shopfee_Controller_Action/*_Authorized*/ 
{
    /*public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }*/
    
    public function indexAction() 
    {
        if($this->getRequest()->isPost())
        {
            $type = $this->getRequest()->getParam('type');
            switch($type)
            {
                case Shopfee_Push_Factory::TYPE_IPHONE:
                    $action = 'iphone';
                    break;
                case Shopfee_Push_Factory::TYPE_ANDROID:
                    $action = 'android';
                    break;
                default:
                    throw new Shopfee_Exception('Wrong type '.$type);
            }
            
            $this->_forward($action,'push');
        }
    }
    
    public function androidAction()
    {
        $push = Shopfee_Push_Factory::create(Shopfee_Push_Factory::TYPE_ANDROID,Zend_Registry::get(Application::ENVIROMENT_NAME));
        $push->setApiKey($this->getRequest()->getParam('api_key'));
        $push->setRegistrationId($this->getRequest()->getParam('registration_id'));
        $result = false;
        try 
        {
            $result = $push->sendMessage($this->getRequest()->getParam('message'),$this->getRequest()->getParam('code'));
            if(!$result)
            {
                echo "Errors!\r\n";
                print_r($push->getLastErrors());
                die();
            }
        }
        catch(Exception $e)
        {
            error_log($e->getMessage());
            echo "Exception!\r\n";
            var_dump($e->getMessage());
            die();
        }
        die('Success!');
    }
    
    public function iphoneAction()
    {
        $push = Shopfee_Push_Factory::create(Shopfee_Push_Factory::TYPE_IPHONE,/*Zend_Registry::get(Application::ENVIROMENT_NAME)*/Application::ENVIROMENT_DEVELOPMENT);
        $push->setDeviceToken($this->getRequest()->getParam('device_token'));
        $push->setIdentifier($this->getRequest()->getParam('identifier'));
        $push->setBadgeNumber(intval($this->getRequest()->getParam('badge_number')));
        $result = $push->sendMessage($this->getRequest()->getParam('message'),$this->getRequest()->getParam('code'));
        if(!$result)
        {
            echo "Errors!\r\n";
            print_r($push->getLastErrors());
            die();
        }
        die('Success!');
    }
}