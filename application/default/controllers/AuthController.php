<?php 

class AuthController extends Shopfee_Controller_Action
{
    public function forgotAction()
    {
        $form = new Form_ForgotPassword();
        
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $email = mysql_escape_string($this->getQuotedParam('email'));
            try
            {
                $this->view->success = Auth::sendUserPassword($email);
                if($this->view->success === false)
                    $this->view->errorMessage = 'Пользователь с таким e-mail не найден.';
            }
            catch(Zend_Mail_Transport_Exception $e)
            {
                $this->view->success = false;
                $this->view->errorMessage = 'Произошла ошибка при отправке письма. Попробуйте повторить попытку позже.';
            }
        }
        
        $this->view->form = $form;
    }
    
    public function changeAction()
    {
        $form = new Form_ChangePassword();
        $checkWord = mysql_escape_string($this->getQuotedParam('checkword'));
        
        //if the checkWord is empty then someone is trying to hack us. Send him out 
        if(empty($checkWord))
            $this->_redirect('/');
            
        $form->setCheckWord($checkWord);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $newPassword = mysql_escape_string($this->getQuotedParam('password'));
            $this->view->success = Auth::changePassword($checkWord,$newPassword);
        }
        
        $this->view->form = $form;
    }
}