<?php
class SettingsController extends Shopfee_Controller_Action_Authorized
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }

    public function indexAction()
    {
        $options = Mapper_Settings_Option::getInstance()->getList();
        foreach ($options as $option) {
            $this->view->{$option->getSlug()} = $option->getData();
        }
    }
}