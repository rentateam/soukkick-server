<?php

class BannerController extends Shopfee_Controller_Action_Authorized 
{
    public function init()
    {
        parent::init();

        $this->_helper->contextSwitch()
                ->addActionContext('index', 'json');
        $this->_helper->contextSwitch()->initContext();
    }
    
    public function indexAction() 
    {
        $banner = Entity_Banner::getFirstBanner();
        if($banner)
        {
            $this->view->banner = $banner->getJsonObject();
        }
        /*else
        {
            $this->view->banner = array();
        }*/
    }
}