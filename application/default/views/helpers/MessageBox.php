<?php 
require_once 'Zend/View/Helper/Partial.php';

class Helper_MessageBox extends Zend_View_Helper_Partial
{
    public function messageBox($message,$header = '')
    {
        return $this->partial('include/message.phtml',array('message' => $message,'header' => $header));
    }
}