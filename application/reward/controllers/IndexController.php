<?php
//require_once 'AbstractController.php';
class Reward_IndexController extends Shopfee_Controller_Action
{
    //protected $_authShopActions = array('index','exit');

    public function indexAction()
    {
        $this->_redirect('/reward/order/');
    }

    public function exitAction() 
    {
        Auth::logoffReward();
        $this->_redirect("/reward/");
    }
}