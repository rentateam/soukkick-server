<?php
require_once 'AbstractController.php';
class Reward_StockController extends Reward_AbstractController
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->menuStock = true;
    }
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page",1);
        $criteria = new Criteria_Prize_Stock();
        
        $sort = $this->getRequest()->getParam('sort',QueryBuilder_Prize_Stock::SORT_NUMBER_AVAILABLE);
        $dir = $this->getRequest()->getParam('dir','DESC');
        $criteria->setSort($sort);
        $criteria->setDirection($dir);
        $criteria->setPage($pageNum);
                 
        $stockList = Mapper_Prize_Stock::getInstance()->getListByCriteria($criteria);
        
        $stockCount = $stockList->totalCount();
        $paginator = new Zend_Paginator( new Paginator_Adapter_Null($stockCount) );
        $paginator->setItemCountPerPage(Aggregate_Prize_Stock::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber( $pageNum );
        if($stockCount > Aggregate_Prize_Stock::ITEMS_PER_PAGE)
            $this->view->paginator = $paginator;
         
        $this->view->pageUrl = '/reward/stock/index/sort/'.$sort.'/dir/'.$dir.'/';
        $this->view->pageNum = $pageNum;
        $this->view->stockList = $stockList;
        $this->view->sortField = $sort;
        $this->view->sortDir = $dir;
    }
    
    public function viewAction()
    {
        $id = $this->getQuotedParam('id');
        $stock = Mapper_Prize_Stock::getInstance()->getEntityById($id);
        $this->checkForNullEntity($stock);
        
        $this->view->stock = new Decorator_Prize_Stock($stock);
        $this->view->history = Mapper_Prize_Stock_History::getInstance()->getListByPrizeStock($stock);
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $stock = Mapper_Prize_Stock::getInstance()->getEntityById($id);
        $this->checkForNullEntity($stock);
        
        $form = new Form_Prize_Stock(array('prizeStock' => $stock));
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $editor = new Prize_Stock_Editor($this->currentAdmin,$stock);
                $editor->save($form);
                $db->commit();
                $this->_redirect($stock->getAdminUrl());
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        
        $this->view->form = $form;
        $this->view->stock = new Decorator_Prize_Stock($stock);
    }
}