<?php
require_once 'AbstractController.php';
class Reward_OrderController extends Reward_AbstractController
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->menuOrder = true;
    }
    
    public function indexAction() 
    {
        $pageNum = $this->getQuotedParam("page",1);
        $criteria = new Criteria_Prize_Order();
        
        $sort = $this->getRequest()->getParam('sort',QueryBuilder_Prize_Order::SORT_DATETIME_CREATED);
        $dir = $this->getRequest()->getParam('dir','DESC');
        $criteria->setSort($sort);
        $criteria->setDirection($dir);
        $criteria->setPrizeStatusIds($this->extractPrizeOrderStatusIds());
        $criteria->setPage($pageNum);
                 
        $orderList = Mapper_Prize_Order::getInstance()->getListByCriteria($criteria);
        $this->view->statusList = Mapper_Prize_Order_Status::getInstance()->getListPlain();
        $this->view->selectedStatusIds = $this->extractRawPrizeOrderStatusIds();
        
        $orderCount = $orderList->totalCount();
        $paginator = new Zend_Paginator( new Paginator_Adapter_Null($orderCount) );
        $paginator->setItemCountPerPage(Aggregate_Prize_Order::ITEMS_PER_PAGE);
        $paginator->setCurrentPageNumber( $pageNum );
        if($orderCount > Aggregate_Prize_Order::ITEMS_PER_PAGE)
            $this->view->paginator = $paginator;
         
        $this->view->pageUrl = '/reward/order/index/sort/'.$sort.'/dir/'.$dir.'/';
        $this->view->pageNum = $pageNum;
        $this->view->orderList = $orderList;
        $this->view->sortField = $sort;
        $this->view->sortDir = $dir;
    }
    
    public function viewAction()
    {
        $id = $this->getQuotedParam('id');
        $order = Mapper_Prize_Order::getInstance()->getEntityById($id);
        $this->checkForNullEntity($order);
        
        $this->view->order = new Decorator_Prize_Order($order);
        $this->view->history = Mapper_Prize_Order_History::getInstance()->getListByPrizeOrder($order);
    }
    
    public function editAction() 
    {
        $id = $this->getQuotedParam('id');
        $order = Mapper_Prize_Order::getInstance()->getEntityById($id);
        $this->checkForNullEntity($order);
        
        $form = new Form_Prize_Order();
        $form->setValues($order);
        if($this->getRequest()->isPost() && $form->isValid($_POST))
        {
            $db = SFM_DB::getInstance();
            $db->beginTransaction();
            try 
            {
                $editor = new Prize_Order_Editor($this->currentAdmin,$order);
                $editor->save($form);
                $db->commit();
                $this->_redirect($order->getAdminUrl());
            }
            catch(Exception $e)
            {
                $db->rollback();
                throw $e;
            }
        }
        
        $this->view->form = $form;
        $this->view->order = new Decorator_Prize_Order($order);
    }
    
    public function changeStatusAction()
    {
        $db = SFM_DB::getInstance();
        $db->beginTransaction();
        try
        {
            $statusId = $this->getQuotedParam('status_id');
            $status = Mapper_Prize_Order_Status::getInstance()->getEntityById($statusId);
            $this->checkForNullEntity($status);
            
            foreach($_REQUEST['order_id'] as $orderId)
            {
                $orderId = intval($orderId);
                $order = Mapper_Prize_Order::getInstance()->getEntityById($orderId);
                $this->checkForNullEntity($order);
                $order->updateStatus($statusId);
            }
            $db->commit();
            $this->_redirect('/reward/order/');
        }
        catch(Exception $e)
        {
            $db->rollback();
            throw $e;
        }
        
    }
    
    public function printAction()
    {
        $id = $this->getQuotedParam('id');
        $order = Mapper_Prize_Order::getInstance()->getEntityById($id);
        $this->checkForNullEntity($order);
        
        $report = new Report_Prize_Order();
        $report
            ->setPrizeOrder($order);

        $writer = Report_WriterFactory::create('pdf');
        $writer->setReport($report);

        echo $writer->output();
        die();
    }
    
    protected function extractPrizeOrderStatusIds()
    {
        $resultStatusIds = $this->extractRawPrizeOrderStatusIds();
        if(empty($resultStatusIds) || (count($resultStatusIds) == 1 && $resultStatusIds[0] == 0))
        {
            $resultStatusIds = Entity_Prize_Order_Status::getActiveIds();
        }
        
        return $resultStatusIds;
    }
    
    protected function extractRawPrizeOrderStatusIds()
    {
        $resultStatusIds = array();
        $statusIds = $this->getRequest()->getParam('prize_order_status_id',array());
        foreach($statusIds as $index => $statusId)
        {
            $resultStatusIds[$index] = intval($statusId);
        }
        array_unique($resultStatusIds);
        return $resultStatusIds;
    }
}