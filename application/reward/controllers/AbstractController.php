<?php

abstract class Reward_AbstractController extends Shopfee_Controller_Action_Admin
{
    public function init()
    {
        parent::init();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('reward');
        $layout->setLayoutPath('../application/reward/layouts');
    }
    
    public function preDispatch()
    {
        parent::preDispatch();
        if (!Acl::canAdminReward($this->currentAdmin)){
            $this->_redirect('/');
        }
    }
}