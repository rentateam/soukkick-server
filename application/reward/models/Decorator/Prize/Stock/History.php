<?
class Decorator_Prize_Stock_History extends Decorator_Abstract
{
    public function __construct(Entity_Prize_Stock_History $item)
    {
        $this->_item = $item;
    }
    
    public function getDatetimeCreated()
    {
        $datetimeCreated = $this->_item->getDatetimeCreated();
        return date('d.m.Y',strtotime($datetimeCreated)).' в '.date('H:i',strtotime($datetimeCreated));
    }
    
    public function getUsername()
    {
        return $this->getUserAdmin()->getName();
    }
    
    public function getDifference()
    {
        $difference = $this->_item->getDifference();
        return ($difference > 0 ? '+' : '-').abs($difference);
    }
}