<?
class Decorator_Prize_Order extends Decorator_Abstract
{
    public function __construct(Entity_Prize_Order $item)
    {
        $this->_item = $item;
    }
    
    public function getDatetimeCreated()
    {
        return date('d.m.Y',strtotime($this->_item->getDatetimeCreated()));
    }
}