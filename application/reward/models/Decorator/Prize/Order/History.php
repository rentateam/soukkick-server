<?
class Decorator_Prize_Order_History extends Decorator_Abstract
{
    public function __construct(Entity_Prize_Order_History $item)
    {
        $this->_item = $item;
    }
    
    public function getDatetimeCreated()
    {
        $datetimeCreated = $this->_item->getDatetimeCreated();
        return date('d.m.Y',strtotime($datetimeCreated)).' в '.date('H:i',strtotime($datetimeCreated));
    }
    
    public function getUsername()
    {
        return '';
    }
    
    public function getDetail()
    {
        return nl2br($this->_item->getDetail());
    }
}