<?
class Decorator_Prize_Stock extends Decorator_Abstract
{
    public function __construct(Entity_Prize_Stock $item)
    {
        $this->_item = $item;
    }
    
    public function getNumberClass()
    {
        $numberAvailable = $this->_item->getNumberAvailable();
        if($numberAvailable > 10)
        {
            return 'many';
        }
        else if($numberAvailable > 5)
        {
            return 'normal';
        }
        else
        {
            return 'low';
        }
    }
    
    public function getPrizeName()
    {
        return $this->_item->getPrize()->getName();
    }
}