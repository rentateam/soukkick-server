<?
class Prize_Stock_Editor
{
    /**
     * @var Entity_Prize_Stock
     **/
    protected $_prizeStock;
    /**
     * @var Entity_User_Admin
     **/
    protected $_admin;
    
    public function __construct(Entity_User_Admin $admin,Entity_Prize_Stock $prizeStock)
    {
        $this->_prizeStock = $prizeStock;
        $this->_admin = $admin;
    }
    
    public function save(Form_Prize_Stock $form)
    {
        $values = $form->getValues();
        $newValue = $values['add'] ? $this->_prizeStock->getNumberAvailable() + $values['number'] : $this->_prizeStock->getNumberAvailable() - $values['number'];
        $this->_prizeStock->updateNumberAvailable($this->_admin, $newValue, $values['comment']);
    }
}