<?
class Prize_Order_Editor
{
    /**
     * @var Entity_Prize_Order
     **/
    protected $_prizeOrder;
    /**
     * @var Entity_User_Admin
     **/
    protected $_admin;
    
    public function __construct(Entity_User_Admin $admin, Entity_Prize_Order $prizeOrder)
    {
        $this->_prizeOrder = $prizeOrder;
        $this->_admin = $admin;
    }
    
    public function save(Form_Prize_Order $form)
    {
        $values = $form->getValues();
        
        $this->makeHistory($values);
        
        
        $comment = $values['comment'];
        unset($values['comment']);
        $statusId = $values['prize_order_status_id'];
        unset($values['prize_order_status_id']);
        
        if($statusId != $this->_prizeOrder->getPrizeOrderStatusId())
        {
            $this->_prizeOrder->updateStatus($statusId);
        }
        
        $this->_prizeOrder->update($values);
        
        
    }
    
    protected function makeHistory($values)
    {
        $simpleFieldNames = array(
                                        'user_name' => 'Имя пользователя',
                                        'address' => 'Адрес',
                                        'post_index' => 'Индекс',
                                        'country' => 'Страна',
                                        'city' => 'Город',
                                        'phone' => 'Телефон',
                                        'datetime_created' => 'Дата доставки',
                                        'prize_order_status_id' => 'Статус'
                                 );
        $changes = array();
        
        foreach($simpleFieldNames as $field => $title)
        {
            $oldValue = $this->_prizeOrder->getInfo($field);
            $newValue = $values[$field];
            if($oldValue != $newValue)
            {
                $changes[$field] = array();
                $changes[$field]['old'] = $oldValue;
                $changes[$field]['new'] = $newValue;
            }
        }
        
        if(isset($changes['prize_order_status_id']))
        {
            $changes['prize_order_status_id']['old'] = Mapper_Prize_Order_Status::getInstance()->getEntityById($changes['prize_order_status_id']['old'])->getName();
            $changes['prize_order_status_id']['new'] = Mapper_Prize_Order_Status::getInstance()->getEntityById($changes['prize_order_status_id']['new'])->getName();
        }
        
        if(!empty($changes))
        {
            $detail = '';
            foreach($changes as $field => $content)
            {
                $detail.= 'Поле <strong>"'.$simpleFieldNames[$field].'"</strong> изменено с <strong>'.$content['old'].'</strong> на <strong>'.$content['new']."</strong>\r\n";
            }
            
            Entity_Prize_Order_History::add($this->_admin,$this->_prizeOrder, $detail, $values['comment']);
        }
    }
}