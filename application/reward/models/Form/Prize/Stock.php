<?php
class Form_Prize_Stock extends Form_Abstract
{
    /**
     * @var Entity_Prize_Stock
     */
    protected $_prizeStock;
    
    public function __construct($options = array())
    {
        $this->_prizeStock = $options['prizeStock'];
        unset($options['prizeStock']);
        parent::__construct($options);
    }
    
    public function init()
    {
        $add = new Zend_Form_Element_Select('add', array(
            'required'    => true,
            'multiOptions'  => array('1' => 'Add','0' => 'Remove'),
            'value'        => 1
        ));
        $this->addElement($add);
        
        $number = new Zend_Form_Element_Text('number', array(
            'required'    => true,
            'validators'  => array(
                array('Int')
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($number);
        
        $comment = new Zend_Form_Element_Text('comment', array(
            'required'    => true,
            'validators'  => array(
                array('StringLength', true, 0, 10000)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($comment);
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Add' : 'Save')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function setPrizeStock(Entity_Prize_Stock $_prizeStock)
    {
        $this->_prizeStock = $_prizeStock;
    }
    
    public function isValid($values)
    {
        $this->getElement('number')->addValidator(new Form_Validate_StockQuantity(array('prizeStock' => $this->_prizeStock,'isSubtract' => $values['add'] == '0')));
        return parent::isValid($values);
    }
}
?>