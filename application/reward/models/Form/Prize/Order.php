<?php
class Form_Prize_Order extends Form_Abstract
{
    public function init()
    {
        $datetimeCreated = new Zend_Form_Element_Text('datetime_created', array(
            'required'    => true,
            'validators'  => array(
                array('StringLength', true, 1, 50)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($datetimeCreated);
        
        $status = new Zend_Form_Element_Select('prize_order_status_id', array(
            'required'    => true,
            'multiOptions'  => Form_Helper_Select::aggregateToList(Mapper_Prize_Order_Status::getInstance()->getListPlain(), 'name'),
        ));
        $this->addElement($status);
        
        $username = new Zend_Form_Element_Text('user_name', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', true, 0, 200)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($username);
        
        $address = new Zend_Form_Element_Text('address', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', true, 0, 1000)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($address);
        
        $phone = new Zend_Form_Element_Text('phone', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', true, 0, 50)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($phone);
        
        $index = new Zend_Form_Element_Text('post_index', array(
                        'required'    => false,
                        'validators'  => array(
                                        new Form_Validate_StringLength(array(0, 6)),
                        ),
                        'filters'     => array('StringTrim'),
        ));
        
        $this->addElement($index);
        
        $country = new Zend_Form_Element_Text('country', array(
                        'required'    => false,
                        'validators'  => array(
                                        new Form_Validate_StringLength(array(0, 50)),
                        ),
                        'filters'     => array('StringTrim'),
        ));
        
        $this->addElement($country);
        
        $city = new Zend_Form_Element_Text('city', array(
                        'required'    => false,
                        'validators'  => array(
                                        new Form_Validate_StringLength(array(0, 100)),
                        ),
                        'filters'     => array('StringTrim'),
        ));
        
        $this->addElement($city);
        
        $comment = new Zend_Form_Element_Text('comment', array(
            'required'    => false,
            'validators'  => array(
                array('StringLength', true, 0, 10000)
            ),
            'filters'     => array('StringTrim'),
        ));
        $this->addElement($comment);
        
        $id = new Zend_Form_Element_Hidden('id', array(
            'value' => $this->_id
        ));
        $this->addElement($id);
        
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label'       => ($this->_id == 0 ? 'Добавить' : 'Редактировать')
        ));
                
        $this->addElement($submit);
        
        parent::init();
    }
    
    public function getValuesArray(Entity_Prize_Order $order)
    {
        return array(
                        'datetime_created' => date('Y-m-d',strtotime($order->getDatetimeCreated())),
                        'prize_order_status_id' => $order->getPrizeOrderStatusId(),
                        'user_name' => $order->getUsername(),
                        'address' => $order->getAddress(),
                        'phone' => $order->getPhone(),
                        'country' => $order->getCountry(),
                        'city' => $order->getCity(),
                        'post_index' => $order->getPostIndex()
                    );
    }
}
?>