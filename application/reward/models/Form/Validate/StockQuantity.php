<?
class Form_Validate_StockQuantity extends Zend_Validate_Abstract
{
    const INVALID            = 'stockQuantityInvalid';
    
    /**
     * @var Entity_Prize_Stock
     */
    protected $_prizeStock;
    
    /**
     * @var boolean
     */
    protected $_isSubtract;
    
    public function __construct($options = array())
    {
        $this->_prizeStock = $options['prizeStock'];
        unset($options['prizeStock']);
        $this->_isSubtract = $options['isSubtract'];
        unset($options['isSubtract']);
    }
    
    
    
    public function setMessageTemplates()
    {
        $this->_messageTemplates = array(
                        self::INVALID => 'На складе осталось всего '.$this->_prizeStock->getNumberAvailable().'шт.'
        );
    }
    
    public function isValid($slug)
    {
        $this->setMessageTemplates();
        if($this->_isSubtract && $this->_prizeStock->getNumberAvailable() - $slug < 0)
        {
            $this->_error(self::INVALID);
            return false;
        }
        else
            return true;
    }
}