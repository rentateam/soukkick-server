<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM shop";
        $test = 1;
        foreach($db->fetchAll($sql) as $item)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($item['id']);
            Entity_Trade_Network::add($shop, $test.'@mail.ru', '123456', '', '', null);
            $test++;
        }
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }