<?php
require_once 'default.php';

function copyFileToS3($s3,$filePath)
{
    $file = realpath($_SERVER['DOCUMENT_ROOT'].'..').'/public/'.$filePath;
    if(file_exists($file))
    {
        echo "putting $file to ".$filePath."\r\n";
        $s3->put($file,$filePath);
    }
    else
    {
        echo "$file does not exist\r\n";
    }
}

set_time_limit(0);
$db = SFM_DB::getInstance();

$tableNames = array(
                        'product' => array(
                                                'big_path' => Application::getProductBigPicturePath(true),
                                                'small_path' => Application::getProductSmallPicturePath(true),
                                            ),
                        'sales' => array(
                                                'big_path' => Application::getSalesBigPicturePath(true),
                                                'small_path' => Application::getSalesSmallPicturePath(true),
                                            ),
                        'scan_product' => array(
                                                'big_path' => Application::getScanProductBigPicturePath(true),
                                                'small_path' => Application::getScanProductSmallPicturePath(true),
                                            ),
                        'user' => array(
                                                'big_path' => Application::getUserPicturePath(true),
                                            ),
                        'banner' => array(
                                                'big_path' => Application::getBannerPicturePath(true),
                                            ),
                        'prize' => array(
                                                'big_path' => Application::getPrizeBigPicturePath(true),
                                                'small_path' => Application::getPrizeSmallPicturePath(true),
                                            ),
                        'trade_network' => array(
                                                'big_path' => Application::getTradeNetworkBigPicturePath(true),
                                                'small_path' => Application::getTradeNetworkPicturePath(true),
                                            ),
                    );

$s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);
$db->beginTransaction();

foreach($tableNames as $tableName => $content)
{
    $pictureField = 'picture_path';
    $sql = "SELECT id,".$pictureField." FROM ".$tableName." WHERE ".$pictureField." IS NOT NULL AND ".$pictureField." != ''";
    $list = $db->fetchAll($sql);
    
    foreach($list as $data)
    {
        foreach(array('big_path','small_path') as $path)
        {
            if(isset($content[$path]))
            {
                copyFileToS3($s3,$content[$path].$data[$pictureField]);
            }
        }
        
        if($tableName == 'trade_network')
        {
            copyFileToS3($s3,Application::getTradeNetworkRectanglePicturePath(true).$data[$pictureField]);
        }
    }
}
copyFileToS3($s3,'media/image/user/default.png');