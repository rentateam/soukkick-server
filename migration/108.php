<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM shop";
        foreach($db->fetchAll($sql) as $item)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($item['id']);
            $shop->regenerateFrequency();
        }
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }