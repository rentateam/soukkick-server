<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();

        //Пересчет счетчиков по rule-itemам
        $ruleList = $db->fetchAll('SELECT id FROM `rule_main` where is_active = 1');
        $actionTypes = array(
                                    Entity_Rule_Item::TYPE_CHECKIN,
                                    Entity_Rule_Item::TYPE_SHOPPAGE,
                                    Entity_Rule_Item::TYPE_SCAN,
                                    Entity_Rule_Item::TYPE_BUY_BARCODE,
                              );
        foreach($ruleList as $rule)
        {
            foreach($actionTypes as $actionType)
            {
                $ruleItemIds = array();
                $activeRuleItemIds = array();
                foreach($db->fetchAll('SELECT id, is_deleted, action_object_id FROM rule_item WHERE action_type=:action_type AND rule_main_id = :rule_main_id',array('action_type' => $actionType,'rule_main_id' => $rule['id'])) as $item)
                {
                    $actionObjectId = $item['action_object_id'] ? $item['action_object_id'] : 0;
                    if(!isset($ruleItemIds[$actionObjectId]))
                        $ruleItemIds[$actionObjectId] = array();
                    
                    $ruleItemIds[$actionObjectId][] = $item['id'];
                    if(!$item['is_deleted'])
                    {
                        $activeRuleItemIds[$actionObjectId] = $item['id'];
                    }
                }
                if(!empty($ruleItemIds))
                {
                    foreach($ruleItemIds as $actionObjectId => $itemList)
                    {
                        $transactionSum = $db->fetchValue('SELECT SUM(amount) FROM trm_main WHERE rule_item_id in ('.implode(',',$itemList).')');
                        if(isset($activeRuleItemIds[$actionObjectId]))
                        {
                            $db->update('UPDATE rule_item set spent_amount = :spent_amount WHERE id=:id',array('spent_amount' => $transactionSum,'id' => $activeRuleItemIds[$actionObjectId]));
                        }
                    }
                }
            }
        }
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        SFM_Cache_Memory::getInstance()->flush();
        throw $e;
    }
