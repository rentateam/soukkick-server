<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM transmitter";
        foreach($db->fetchAll($sql) as $item)
        {
            $transmitter = Mapper_Transmitter::getInstance()->getEntityById($item['id']);
            $transmitter->regenerateFrequency();
        }
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }