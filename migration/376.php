<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        //insert Shopfee user
        $db->insert("INSERT INTO bpa_main VALUES (100,'Shopfee','Shopfee',0,1,1)",array());
        SFM_Cache_Memory::getInstance()->flush();
        $bpaMain = Mapper_Bpa_Main::getInstance()->getEntityById(Entity_Bpa_Main::ID_SHOPFEE);
        $manager = Entity_Bpa_Manager::add($bpaMain, 'admin@shopfee.ru', 'shopfee.ru_1024', 'Shopfee', 'Shopfee');
        
        //insert Shopfee rules
        $rule = Entity_Rule_Main::add($manager, 'Shopfee', date('Y-m-d H:i:s'), '2025-01-01 00:00:00');
        $rule->setActive(true);
        
        $ruleItem = Entity_Rule_Item::add($rule, 20, Entity_Rule_Item::TYPE_FACEBOOK);
        $ruleItem = Entity_Rule_Item::add($rule, 20, Entity_Rule_Item::TYPE_CONTACT);
        $ruleItem = Entity_Rule_Item::add($rule, 50, Entity_Rule_Item::TYPE_PROMOCODE);
        
        
        //generate rule_mains for shops
        $sql = "SELECT *
                FROM trade_network";
        $networkList = $db->fetchAll($sql); 
        foreach($networkList as $item)
        {
            $network = Mapper_Trade_Network::getInstance()->getEntityById($item['id']);
            $manager = $network->getAnyBpaManager();
            $bpaMain = $network->getBpaMain();
            $rule = Entity_Rule_Main::add($manager, 'Shopfee', date('Y-m-d H:i:s'), '2025-01-01 00:00:00');
            //shoppage
            $ruleItem = Entity_Rule_Item::add($rule, $network->getAmountPage(), Entity_Rule_Item::TYPE_SHOPPAGE, $network);
            $shopList = $network->getShops(); 
            foreach($shopList as $shop)
            {
                //checkin
                $ruleItem = Entity_Rule_Item::add($rule, $shop->getAmountEnter(), Entity_Rule_Item::TYPE_CHECKIN, $shop->getMainTransmitter());
            }
            //scans
            foreach($network->getScanProducts() as $scan)
            {
                $ruleItem = Entity_Rule_Item::add($rule, $scan->getPrice(), Entity_Rule_Item::TYPE_SCAN, $scan);
            }
            $rule->setActive(true);
            Mapper_Rule_Main::getInstance()->setShopIds($rule, $shopList->getListEntitiesId());
        }
        
        
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }