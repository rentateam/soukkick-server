<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        $db->update('UPDATE bpa_main set fishki_amount = 0',array());
        $rule = Entity_Bpa_Main::getShopfeeRule();
        $db->update('UPDATE rule_main set max_spend_amount = -1 where id='.$rule->getId(),array());
        $ruleItem = Entity_Rule_Item::add($rule, 1, Entity_Rule_Item::TYPE_SHOPFEE_GIFT, null, -1);

        //Пересчет счетчиков по rule-itemам
        $amountList = $db->fetchAll('SELECT id, amount FROM user WHERE amount > 0');
        foreach($amountList as $amountData)
        {
            $user = Mapper_User::getInstance()->getEntityById($amountData['id']);
            $transaction = Entity_Trm_Main::add($ruleItem, $user, $amountData['amount'], null);
        }
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        SFM_Cache_Memory::getInstance()->flush();
        throw $e;
    }