<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        $ruleList = $db->fetchAll('SELECT id from rule_main');
        foreach($ruleList as $rule)
        {
            //удаляем дубли для чекинов и дописываем недостающие правила
            $ruleItemList = $db->fetchAll('SELECT id from rule_item WHERE rule_main_id = '.$rule['id'].' AND action_type = 1');
            
            if(count($ruleItemList) > 1)
            {
                $i=0;
                foreach($ruleItemList as $ruleItem)
                {
                    //Первое правило оставляем
                    if($i == 0)
                    {
                        //Выкидываем id передатчика
                        $db->update("UPDATE rule_item SET action_object_id = NULL WHERE id=".$ruleItem['id'],array());
                    }
                    else 
                    {
                        $db->delete("DELETE FROM rule_item WHERE id=".$ruleItem['id'],array());
                    }
                    $i++;
                }
            }
            else if(count($ruleItemList) == 0)
            {
                $db->insert("INSERT INTO rule_item (rule_main_id,action_object_id,amount,max_spend_amount,spent_amount,is_active,action_type,is_deleted) VALUES (".$rule['id'].",NULL,0,-1,0,1,1,0)",array());
            }
            
        }
        
        $db->update('UPDATE rule_item set max_spend_amount = 10000',array());
        $db->update('UPDATE rule_main set max_spend_amount = 10000',array());
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }