<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id, description,name,picture_path,rectangle_picture_path
                FROM shop";
        foreach($db->fetchAll($sql) as $item)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($item['id']);
            $tradeNetwork = $shop->getTradeNetwork();
            $params = array(
                                'description' => $item['description'],
                                'title' => $item['name'],
                                'picture_path' => ($item['picture_path'] && $item['picture_path'] != 'NULL' ? $_SERVER['DOCUMENT_ROOT'].'/media/image/shop/big/'.$item['picture_path'] : ''),
                                'rectangle_picture_path' => ($item['rectangle_picture_path'] && $item['rectangle_picture_path'] != 'NULL' ? $_SERVER['DOCUMENT_ROOT'].'/media/image/shop/rectangle/'.$item['rectangle_picture_path'] : ''),
                            );
            if(!file_exists($params['picture_path']))
                unset($params['picture_path']);
            if(!file_exists($params['rectangle_picture_path']))
                unset($params['rectangle_picture_path']);
            $tradeNetwork->update($params);
        }
        $db->delete("ALTER TABLE `shop` DROP `description`",array());
        $db->delete("ALTER TABLE `shop` DROP `picture_path`",array());
        $db->delete("ALTER TABLE `shop` DROP `rectangle_picture_path`",array());
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }