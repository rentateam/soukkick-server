<?php
require_once 'default.php';

function convertPicture($s3,$entity,$s3Path,$fieldName)
{
    $destinationDir = '/tmp';
    //echo $s3Path."\n";
    if(strpos($s3Path,'.jpg') !== false)
    {
        $destinationPict = $destinationDir.'/'.basename($s3Path);
        if($s3->get($s3Path,$destinationPict))
        {
            $entity->update(array($fieldName => $destinationPict));
            echo "Updated file ".$s3Path."\n";
        }
        else
        {
            echo "File ".$s3Path.' does not exist!'."\n";
        }
    }
}

set_time_limit(0);
$db = SFM_DB::getInstance();


$s3 = new Shopfee_Aws_S3(Application::STATIC_IMAGE);

foreach($db->fetchAll('SELECT id from scan_product',array()) as $item)
{
    $scan = Mapper_ScanProduct::getInstance()->getEntityByid($item['id']);
    convertPicture($s3,$scan,$scan->getBigPicturePathFull(true),'picture_path');
}

foreach($db->fetchAll('SELECT id from product',array()) as $item)
{
    $product = Mapper_Product::getInstance()->getEntityByid($item['id']);
    convertPicture($s3,$product,$scan->getBigPicturePathFull(true),'picture_path');
}

foreach($db->fetchAll('SELECT id from prize',array()) as $item)
{
    $prize = Mapper_Prize::getInstance()->getEntityByid($item['id']);
    convertPicture($s3,$prize,$scan->getBigPicturePathFull(true),'picture_path');
}

foreach($db->fetchAll('SELECT id from trade_network',array()) as $item)
{
    $network = Mapper_Trade_Network::getInstance()->getEntityByid($item['id']);
    convertPicture($s3,$network,$scan->getBigPicturePathFull(true),'picture_path');
    convertPicture($s3,$network,$network->getRectanglePicturePathFull(true),'rectangle_picture_path');
}

foreach($db->fetchAll('SELECT id from sales',array()) as $item)
{
    $sales = Mapper_Sales::getInstance()->getEntityByid($item['id']);
    convertPicture($s3,$sales,$scan->getBigPicturePathFull(true),'picture_path');
}