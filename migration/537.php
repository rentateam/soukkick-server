<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    exec('cp -rf '.$_SERVER['DOCUMENT_ROOT'].'/media/image /tmp');
    $folder = '/tmp/image/';
    
    
    
    foreach($db->fetchAll('SELECT id from scan_product',array()) as $item)
    {
        $scan = Mapper_ScanProduct::getInstance()->getEntityByid($item['id']);
        $pict = $folder.'/scan-product/big/'.$scan->getPicturePath();
        if(file_exists($pict))
        $scan->update(array('picture_path' => $pict));
    }
    
    foreach($db->fetchAll('SELECT id from product',array()) as $item)
    {
        $product = Mapper_Product::getInstance()->getEntityByid($item['id']);
        $pict = $folder.'/product/big/'.$product->getPicturePath();
        if(file_exists($pict))
            $product->update(array('picture_path' => $pict));
    }
    
    foreach($db->fetchAll('SELECT id from prize',array()) as $item)
    {
        $prize = Mapper_Prize::getInstance()->getEntityByid($item['id']);
        $pict = $folder.'/prize/big/'.$prize->getPicturePath();
        if(file_exists($pict))
            $prize->update(array('picture_path' => $pict));
    }
    
    foreach($db->fetchAll('SELECT id from trade_network',array()) as $item)
    {
        $network = Mapper_Trade_Network::getInstance()->getEntityByid($item['id']);
        $pict = $folder.'/usershop/big/'.$network->getPicturePath();
        if(file_exists($pict))
            $network->update(array('picture_path' => $pict));
        $pict = $folder.'/usershop/rectangle/'.$network->getRectanglePicturePath();
        if(file_exists($pict))
            $network->update(array('rectangle_picture_path' => $pict));
    }
    
    foreach($db->fetchAll('SELECT id from sales',array()) as $item)
    {
        $sales = Mapper_Sales::getInstance()->getEntityByid($item['id']);
        $pict = $folder.'/sales/big/'.$sales->getPicturePath();
        if(file_exists($pict))
            $sales->update(array('picture_path' => $pict));
    }