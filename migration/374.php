<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        //generate bpa_managers and bpa_mains
        $sql = "SELECT *
                FROM trade_network";
        $networkList = $db->fetchAll($sql); 
        $i=0;
        foreach($networkList as $item)
        {
            $network = Mapper_Trade_Network::getInstance()->getEntityById($item['id']);
            $bpaMain = Entity_Bpa_Main::add($item['title'], $item['description'], Entity_Bpa_Main::DEFAULT_CONVERSION_RATE_TO_FISHKI, Entity_Bpa_Main::DEFAULT_CONVERSION_RATE_FROM_FISHKI);
            $bpaManager = Entity_Bpa_Manager::add($bpaMain, $item['email'], $item['password'], $item['name'], $item['surname']);
            $bpaManager->setPassword($item['password']);
            $bpaManager->update(array('salt' => $item['salt'],'reg_date' => $item['reg_date']));
            $network->update(array('bpa_main_id' => $bpaMain->getId())); 
        }
        $db->delete('ALTER TABLE trade_network DROP `email`',array());
        $db->delete('ALTER TABLE trade_network DROP `password`',array());
        $db->delete('ALTER TABLE trade_network DROP `name`',array());
        $db->delete('ALTER TABLE trade_network DROP `surname`',array());
        $db->delete('ALTER TABLE trade_network DROP `checkword`',array());
        $db->delete('ALTER TABLE trade_network DROP `salt`',array());
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }