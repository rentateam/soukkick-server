<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM user";
        foreach($db->fetchAll($sql) as $item)
        {
            $user = Mapper_User::getInstance()->getEntityById($item['id']);
            Entity_User_Address::add($user,'','','','','','','');
        }
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }