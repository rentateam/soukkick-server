<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM user";
        $userList = $db->fetchAll($sql); 
        $i=0;
        foreach($userList as $item)
        {
            $db->update("UPDATE user set promo_code=:promo_code WHERE id=:id",array('id' => $item['id'],'promo_code' => Entity_User::generatePromoCode('test'.$i)));
            $i++; 
        }
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }