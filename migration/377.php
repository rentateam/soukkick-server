<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();
        
        //insert Shopfee prize rules
        $rule = Entity_Bpa_Main::getShopfeeRule();
        
        foreach(Mapper_Prize::getInstance()->getList() as $prize)
        {
            $ruleItem = Entity_Rule_Item::add($rule, $prize->getPrice(), Entity_Rule_Item::TYPE_PRIZE, $prize);
            echo "added rule item ".$ruleItem->getId()."\r\n";
        }
        
        /*$updater = new Rule_AmountUpdater();
        $updater->update();*/
        
        //insert transactions - now it is not needed
        $tableNames = array(
                                'action_checkin' => Entity_Rule_Item::TYPE_CHECKIN,
                                'action_shoppage' => Entity_Rule_Item::TYPE_SHOPPAGE,
                                'action_scan' => Entity_Rule_Item::TYPE_SCAN,
                                'action_prize' => Entity_Rule_Item::TYPE_PRIZE,
                            );
        foreach($tableNames as $tableName => $type)
        {
            /*$sql = 'SELECT * FROM '.$tableName;
            foreach($db->fetchAll($sql) as $item)
            {
                switch($type)
                {
                    case Entity_Rule_Item::TYPE_CHECKIN:
                        $shopId = $item['shop_id'];
                        $object = Mapper_Shop::getInstance()->getEntityById($shopId)->getMainTransmitter();
                        break;
                    case Entity_Rule_Item::TYPE_SHOPPAGE:
                        $object = Mapper_Trade_Network::getInstance()->getEntityById($item['trade_network_id']);
                        $shopId = null;
                        break;
                    case Entity_Rule_Item::TYPE_SCAN:
                        $object = Mapper_ScanProduct::getInstance()->getEntityById($item['scan_product_id']);
                        $shopId = $item['shop_id'];
                        break;
                    case Entity_Rule_Item::TYPE_PRIZE:
                        $object = Mapper_Prize::getInstance()->getEntityById($item['prize_id']);
                        $shopId = null;
                        break;
                }
                $shop = $shopId ? Mapper_Shop::getInstance()->getEntityById($shopId) : null;
                $user = Mapper_User::getInstance()->getEntityById($item['user_id']);
                $transaction = Rule_Trm_Factory::create($user, $type,$object,$shop);
                $db->update('UPDATE '.$tableName.' set trm_main_id=:trm_id WHERE id=:id',array('trm_id' => $transaction->getId(),'id' => $item['id']));
            }*/
            $db->delete('ALTER TABLE '.$tableName.' DROP price',array());
        }
        
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }