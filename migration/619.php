<?php
require_once 'default.php';

$db = SFM_DB::getInstance();

/** @var Entity_User $user */
foreach (Mapper_User::getInstance()->getListPlain() as $user) {
    $user->update(array('number_invited' => $user->getInvitedUsers()->count()));
}