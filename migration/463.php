<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    
    try
    {
        $db->beginTransaction();

        //Магазины без передатчиков        
        $shopList = $db->fetchAll('SELECT id
FROM `shop`
WHERE id NOT
IN (

SELECT shop_id
FROM transmitter
)');
        foreach($shopList as $shopItem)
        {
    $shop = Mapper_Shop::getInstance()->getEntityById($shopItem['id']);
            Entity_Transmitter::add($shop,Entity_Transmitter_Type::ID_DEFAULT, 0, $shop->getLat(), $shop->getLongtitude());
        }
        
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }
