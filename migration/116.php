<?php
    require_once 'default.php';
    
    $db = SFM_DB::getInstance();
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id, amount_enter
                FROM shop";
        foreach($db->fetchAll($sql) as $item)
        {
            $shop = Mapper_Shop::getInstance()->getEntityById($item['id']);
            Entity_Transmitter::add($shop, Entity_Transmitter_Type::ID_DEFAULT, $item['amount_enter'], $shop->getLat(), $shop->getLongtitude());
        }
        $db->delete("ALTER TABLE `shop` DROP `amount_enter`");
        $db->delete("ALTER TABLE `shop` DROP `frequency`");
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }
    
    try
    {
        $db->beginTransaction();
        
        $sql = "SELECT id
                FROM transmitter";
        foreach($db->fetchAll($sql) as $item)
        {
            $transmitter = Mapper_Transmitter::getInstance()->getEntityById($item['id']);
            $transmitter->regenerateFrequency();
        }
        $db->commit();
        SFM_Cache_Memory::getInstance()->flush();
    } 
    catch(Exception $e)
    {
        $db->rollBack();
        throw $e;
    }