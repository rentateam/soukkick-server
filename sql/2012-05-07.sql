ALTER TABLE  `user` ADD  `promo_code` VARCHAR( 10 ),
ADD UNIQUE (  `promo_code` )

CREATE TABLE  `shopfee`.`user_promocode` (
`from_user_id` INT(10) UNSIGNED NOT NULL COMMENT  'от кого',
`to_user_id` INT(10) UNSIGNED NOT NULL COMMENT  'кому',
 KEY `fk_user_promocode_from_user_id` (`from_user_id`),
 KEY `fk_user_promocode_to_user_id` (`to_user_id`),
 CONSTRAINT `fk_user_promocode_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_user_promocode_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = INNODB COMMENT =  'Использованные промо коды';
