ALTER TABLE `buy_barcode` ADD `counter_date` DATETIME NULL;
CREATE TABLE `access_token` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `token` varchar(50) NOT NULL,
 `trade_network_id` int(10) unsigned NOT NULL,
 `shop_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `index_access_token_trade_network_id` (`trade_network_id`),
 KEY `index_access_token_shop_id` (`shop_id`),
 CONSTRAINT `access_token_ibfk_1` FOREIGN KEY (`trade_network_id`) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `access_token_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;
ALTER TABLE `access_token` CHANGE `shop_id` `shop_id` INT( 11 ) NULL;