CREATE TABLE `action_checkout` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_created` datetime NOT NULL,
 `datetime_created` datetime NOT NULL,
 `user_id` int(11) unsigned NOT NULL,
 `shop_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `action_checkout_shop_ibfk_1` (`shop_id`),
 KEY `fk_action_checkout_user_id` (`user_id`),
 CONSTRAINT `action_checkout_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_action_checkout_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `action_checkout_attempt` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `datetime_created` datetime NOT NULL,
 `user_id` int(11) unsigned NOT NULL,
 `shop_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `action_checkout_attempt_shop_ibfk_1` (`shop_id`),
 KEY `fk_action_checkout_attempt_user_id` (`user_id`),
 CONSTRAINT `action_checkout_attempt_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_action_checkout_attempt_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `action_checkout_attempt` ADD INDEX `index_action_checkout_attempt_user_id_shop_id_datetime_created` ( `user_id` , `shop_id` , `datetime_created` );
ALTER TABLE `action_checkout_attempt` ADD INDEX `index_action_checkout_attempt_datetime_created` ( `datetime_created` );