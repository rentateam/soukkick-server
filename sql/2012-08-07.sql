ALTER TABLE `rule_main` ADD `is_changed` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `sales` ADD `short_description` VARCHAR( 3000 ) NOT NULL AFTER `description` ;
ALTER TABLE `rule_main` CHANGE `date_from` `date_from` DATE NOT NULL ,
CHANGE `date_to` `date_to` DATE NOT NULL;