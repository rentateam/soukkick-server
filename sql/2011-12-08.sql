ALTER TABLE `user` ADD `number_scan` INT NOT NULL ,
ADD `number_prize` INT NOT NULL ,
ADD `number_checkin` INT NOT NULL;
ALTER TABLE `user_shop` RENAME TO favourite_shop;

CREATE TABLE `user_shop` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `email` varchar(50) NOT NULL,
 `password` varchar(65) NOT NULL,
 `name` varchar(100) NOT NULL,
 `surname` varchar(50) DEFAULT NULL,
 `picture_path` varchar(20) DEFAULT NULL,
 `reg_date` datetime NOT NULL,
 `checkword` varchar(32) DEFAULT NULL,
 `salt` varchar(32) NOT NULL DEFAULT '',
 `shop_id` int NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `unique_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `shop` ADD `is_recommended` BOOLEAN NOT NULL DEFAULT '0';