CREATE TABLE `settings_option` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`slug` VARCHAR(255) NOT NULL,
	`data` VARCHAR(65535) NULL,
	`caption` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `key` (`slug`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;