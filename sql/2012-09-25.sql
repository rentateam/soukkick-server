CREATE TABLE `prize_order_status`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40),
  PRIMARY KEY (`id`)
);

CREATE TABLE `prize_order`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `prize_id` INT(11) NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `datetime_created` DATETIME NOT NULL,
  `prize_order_status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`prize_id`) REFERENCES `prize`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`prize_order_status_id`) REFERENCES `prize_order_status`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

insert into prize_order_status values (1,'New');
insert into prize_order_status values (2,'Approved');
insert into prize_order_status values (3,'In progress');
insert into prize_order_status values (4,'Delivered');

ALTER TABLE `prize_order`
  ADD COLUMN `user_name` VARCHAR(200) NULL AFTER `prize_order_status_id`,
  ADD COLUMN `address` VARCHAR(1000) NULL AFTER `user_name`,
  ADD COLUMN `phone` VARCHAR(50) NULL AFTER `address`;
  
  
CREATE TABLE `prize_order_history`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `prize_order_id` INT NOT NULL,
  `datetime_created` DATETIME NOT NULL,
  `comment` VARCHAR(10000),
  `detail` VARCHAR(10000),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`prize_order_id`) REFERENCES `prize_order`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `user_admin` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(50) DEFAULT NULL,
  `password` VARCHAR(65) DEFAULT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `checkword` VARCHAR(32) DEFAULT NULL,
  `salt` VARCHAR(32) NOT NULL DEFAULT '',
  `user_admin_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `user_admin_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
  
  
insert into user_admin_type values (1,'Администратор');
insert into user_admin_type values (2,'Администратор наград');
INSERT INTO user_admin (email,`password`,NAME,checkword,salt,user_admin_type_id)
VALUES ('admin@shopfee.ru',(SELECT `password` FROM `user` WHERE email = 'admin@shopfee.ru'),'Shopfee',NULL,(SELECT salt FROM `user` WHERE email = 'admin@shopfee.ru'),1);

ALTER TABLE `user_admin`  
  ADD FOREIGN KEY (`user_admin_type_id`) REFERENCES `user_admin_type`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `stock`   
  CHANGE `prize_count` `number_available` INT(11) DEFAULT 0  NOT NULL,
  ADD COLUMN `number_delivered` INT(11) DEFAULT 0  NOT NULL AFTER `number_available`;

RENAME TABLE `stock` TO `prize_stock`;

insert into `prize_stock` (prize_id,number_available,number_delivered) select id,0,0 from prize;
ALTER TABLE `prize_stock`   
  DROP INDEX `prize_id`,
  ADD  UNIQUE INDEX `prize_id` (`prize_id`);
  
CREATE TABLE `prize_stock_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `prize_stock_id` INT(11) NOT NULL,
  `datetime_created` DATETIME NOT NULL,
  `comment` VARCHAR(10000) DEFAULT NULL,
  `difference` VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prize_stock_id` (`prize_stock_id`),
  CONSTRAINT `prize_stock_history_ibfk_1` FOREIGN KEY (`prize_stock_id`) REFERENCES `prize_stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8;


ALTER TABLE `prize_stock_history`   
  ADD COLUMN `user_admin_id` INT(10) UNSIGNED NOT NULL AFTER `difference`,
  ADD FOREIGN KEY (`user_admin_id`) REFERENCES `user_admin`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

  ALTER TABLE `prize_order_history`   
  ADD COLUMN `user_admin_id` INT(10) UNSIGNED NOT NULL,
  ADD FOREIGN KEY (`user_admin_id`) REFERENCES `user_admin`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
  
ALTER TABLE `prize` ADD `is_active` TINYINT( 1 ) NOT NULL DEFAULT '1';
update prize_stock set number_available = 10;
drop table `prize2user`;