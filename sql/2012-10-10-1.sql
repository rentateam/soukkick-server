ALTER TABLE `trade_network` ADD COLUMN `is_hidden` TINYINT(1) DEFAULT 0  NOT NULL AFTER `is_deleted`;
ALTER TABLE `shop` ADD COLUMN `is_hidden` TINYINT(1) DEFAULT 0  NOT NULL;
ALTER TABLE `shop` ADD  INDEX `index_shop_is_hidden` (`is_hidden`);
ALTER TABLE `trade_network` ADD  INDEX `index_trade_network_is_hidden` (`is_hidden`);
