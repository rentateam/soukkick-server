ALTER TABLE `user` ADD `number_buy_barcode` INT NOT NULL;
CREATE TABLE `buy_barcode` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `barcode` varchar(11) NOT NULL,
 `price` int(11) NOT NULL DEFAULT '0',
 `active_rule_item_id` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `active_rule_item_id` (`active_rule_item_id`),
 CONSTRAINT `buy_barcode_ibfk_1` FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `action_buy_barcode` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_created` datetime NOT NULL,
 `datetime_created` datetime NOT NULL,
 `user_id` int(11) unsigned NOT NULL,
 `shop_id` int(11) NOT NULL,
 `trm_main_id` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_action_buy_barcode_user_id` (`user_id`),
 KEY `action_buy_barcode_shop_ibfk_1` (`shop_id`,`datetime_created`),
 CONSTRAINT `action_buy_barcode_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_action_buy_barcode_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `buy_barcode` ADD `shop_id` INT NOT NULL;
ALTER TABLE `buy_barcode`  ADD FOREIGN KEY (`shop_id`) REFERENCES `shop`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `buy_barcode` ADD `is_active` TINYINT NOT NULL DEFAULT '1';
ALTER TABLE `action_buy_barcode` ADD `buy_barcode_id` INT NOT NULL;
ALTER TABLE `action_buy_barcode`  
  ADD FOREIGN KEY (`buy_barcode_id`) REFERENCES `buy_barcode`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `buy_barcode` CHANGE `barcode` `barcode` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;