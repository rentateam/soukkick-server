ALTER TABLE `trm_main` ADD `action_date` DATE NOT NULL ,
ADD `action_time` TIME NOT NULL ,
ADD `action_lat` DOUBLE NULL DEFAULT NULL ,
ADD `action_long` DOUBLE NULL DEFAULT NULL;

update trm_main set action_date = DATE(date_created);
update trm_main set action_time = TIME(date_created);