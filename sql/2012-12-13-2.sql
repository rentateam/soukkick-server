CREATE TABLE `promocode` (                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
   `id` INT(11) NOT NULL AUTO_INCREMENT,
   `picture_path` VARCHAR(500) DEFAULT NULL,
   `name` VARCHAR(500),
   `code` VARCHAR(20),
   `amount` INT(11) DEFAULT '0',
   `active_rule_item_id` INT(11) DEFAULT NULL,
   `bound_rule_item_id` INT(11) DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `active_rule_item_id` (`active_rule_item_id`),
   KEY `bound_rule_item_id` (`bound_rule_item_id`),
   CONSTRAINT `promocode_ibfk_1` FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `promocode_ibfk_2` FOREIGN KEY (`bound_rule_item_id`) REFERENCES `rule_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) ENGINE=INNODB DEFAULT CHARSET=utf8;   
 
 ALTER TABLE `promocode`   
  ADD COLUMN `last_modified` DATETIME NOT NULL AFTER `bound_rule_item_id`;
  
CREATE TABLE `action_promocode` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_created` datetime NOT NULL,
 `datetime_created` datetime NOT NULL,
 `user_id` int(11) unsigned NOT NULL,
 `trm_main_id` int(11) DEFAULT NULL,
 `promocode_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_action_promocode_user_id` (`user_id`),
 KEY `promocode_id` (`promocode_id`),
 CONSTRAINT `action_promocode_ibfk_2` FOREIGN KEY (`promocode_id`) REFERENCES `promocode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_action_promocode_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `action_promocode` ADD UNIQUE `index_action_promocode_user_id_promocode_id` ( `user_id` , `promocode_id` );
ALTER TABLE `promocode` ADD UNIQUE `index_promocode_code` ( `code` ( 20 ) ) ;
ALTER TABLE `promocode` DROP `amount`;
