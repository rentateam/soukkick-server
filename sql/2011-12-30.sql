ALTER TABLE `user_shop` ADD `phone` VARCHAR( 30 ) NULL;
ALTER TABLE `user_shop` ADD `is_active` BOOLEAN NOT NULL DEFAULT '0';
ALTER TABLE `user_shop` CHANGE `shop_id` `shop_id` INT( 11 ) NULL ;
ALTER TABLE `user_shop` ADD `shop_info` VARCHAR( 1000 ) NULL;
update user_shop set is_active = 1;