ALTER TABLE `action_checkin` ADD `datetime_created` DATETIME NOT NULL AFTER `date_created` ;
ALTER TABLE `action_prize` ADD `datetime_created` DATETIME NOT NULL AFTER `date_created` ;
ALTER TABLE `action_scan` ADD `datetime_created` DATETIME NOT NULL AFTER `date_created` ;
ALTER TABLE `action_shoppage` ADD `datetime_created` DATETIME NOT NULL AFTER `date_created` ;


update `action_checkin` set `datetime_created` = `date_created` ;
update `action_prize` set `datetime_created` = `date_created` ;
update `action_scan` set `datetime_created` = `date_created` ;
update `action_shoppage` set `datetime_created` = `date_created` ;