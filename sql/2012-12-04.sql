ALTER TABLE `user` ADD `float_time` DATETIME NULL DEFAULT NULL;
ALTER TABLE `user` DROP `is_admin`;

ALTER TABLE `action_log` ADD INDEX `index_action_log_user_id_action_type_adate_atime` ( `user_id` , `action_type` , `adate` , `atime` );