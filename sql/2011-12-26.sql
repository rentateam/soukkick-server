CREATE TABLE `event_shop` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date_created` DATETIME NOT NULL ,
`type` INT NOT NULL ,
`object_id` INT NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `event_shop` CHANGE `object_id` `object_id` INT( 11 ) NULL ;
ALTER TABLE `event_shop` ADD `shop_id` INT NOT NULL ;