ALTER TABLE `user`   ADD COLUMN `total_amount` DOUBLE NOT NULL;
update `user` set total_amount = amount;

CREATE TABLE `achievement`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `picture` VARCHAR(50),
  `name` VARCHAR(30),
  `available_at_level` INT,
  `amount` INT DEFAULT 0,
  `is_active` TINYINT(1) DEFAULT 0,
  PRIMARY KEY (`id`)
);
ALTER TABLE `achievement` ADD `active_rule_item_id` INT NULL;
ALTER TABLE `achievement` ADD FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


insert into achievement (name,picture,available_at_level,amount,is_active) VALUES ('Подключи Facebook','/imgs/Profile_bage_FB.png',0,20,1);
insert into achievement (name,picture,available_at_level,amount,is_active) VALUES ('Подключи ВКонтакте','/imgs/Profile_bage_VK.png',0,20,1);
insert into achievement (name,picture,available_at_level,amount,is_active) VALUES ('Введи промо-код','/imgs/Profile_bage_promo.png',0,50,1);

update rule_item set action_object_id = 1 where action_type = 4;
update rule_item set action_object_id = 2 where action_type = 5;
update rule_item set action_object_id = 3 where action_type = 6;