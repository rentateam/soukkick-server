CREATE TABLE `mall` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `picture_path` varchar(20) DEFAULT NULL,
 `last_modified` datetime NOT NULL,
 `name` varchar(500) NOT NULL,
 `address` varchar(300) NOT NULL,
 `lat` double DEFAULT NULL,
 `longtitude` double DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tag` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(1000) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tag` ADD UNIQUE `index_tag_name` ( `name` ( 10 ) ) ;

CREATE TABLE `mall2shop` (
 `tag_id` int(11) NOT NULL,
 `mall_id` int(11) NOT NULL,
 `shop_id` int(11) NOT NULL,
 UNIQUE KEY `index_tag_mall_mall_id_tag_id` (`mall_id`,`tag_id`),
 UNIQUE KEY `index_tag_mall_mall_id_shop_id` (`mall_id`,`shop_id`),
 CONSTRAINT `mall2shop_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `mall2shop_ibfk_2` FOREIGN KEY (`mall_id`) REFERENCES `mall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mall_map` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `floor` int(11) NOT NULL,
 `mall_id` int(11) NOT NULL,
 `picture_path` varchar(20) NOT NULL DEFAULT '',
 `description` varchar(5000) NOT NULL,
 `last_modified` datetime NOT NULL,
 PRIMARY KEY (`id`),
 KEY `index_mall_map_mall_id` (`mall_id`),
 CONSTRAINT `mall_map_ibfk_1` FOREIGN KEY (`mall_id`) REFERENCES `mall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `mall2shop` CHANGE `shop_id` `shop_id` INT( 11 ) NULL;
ALTER TABLE `mall2shop` ADD `id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;

ALTER TABLE `mall_map` CHANGE `floor` `floor` VARCHAR( 10 ) NOT NULL;

ALTER TABLE `tag` DROP INDEX `index_tag_name` ,
ADD INDEX `index_tag_name` ( `name` ( 100 ) );