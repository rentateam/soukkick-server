CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_id` int(11) NOT NULL,
  `prize_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `prize_id` (`prize_id`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`prize_id`) REFERENCES `prize` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `prize2user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_id` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `status` enum('new','done') COLLATE utf8_unicode_ci DEFAULT 'new',
  `post_ticket` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prize2user_1` (`prize_id`),
  KEY `fk_prize2user_2` (`user_id`),
  CONSTRAINT `fk_prize2user_1` FOREIGN KEY (`prize_id`) REFERENCES `prize` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_prize2user_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;