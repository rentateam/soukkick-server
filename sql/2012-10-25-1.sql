CREATE TABLE `action_log`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `alat` DOUBLE,
  `alon` DOUBLE,
  `adate` DATE,
  `atime` TIME,
  `action_type` INT NOT NULL,
  `active_rule_item_id` INT,
  PRIMARY KEY (`id`)
);
ALTER TABLE `action_log`   
  ADD COLUMN `date_created` DATETIME NOT NULL AFTER `active_rule_item_id`;
  
ALTER TABLE `action_log`   
  ADD COLUMN `ip` VARCHAR(30) NULL AFTER `date_created`;

