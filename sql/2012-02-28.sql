CREATE TABLE `product2shop` (
`product_id` INT NOT NULL ,
`shop_id` INT NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `product2shop` ADD INDEX `index_product2shop_shop_id` ( `shop_id` ) ;
ALTER TABLE `product2shop` ADD UNIQUE `index_product2shop_product_id_shop_id` ( `product_id` , `shop_id` ); 
insert into product2shop (product_id,shop_id) select id,shop_id from product;
ALTER TABLE `product` DROP FOREIGN KEY `fk_product_shop` ;
ALTER TABLE product DROP INDEX fk_product_shop;
ALTER TABLE `product` ADD `user_shop_id` INT NOT NULL;
ALTER TABLE `product` ADD INDEX `index_product_user_shop_id` ( `user_shop_id` );
ALTER TABLE `product` CHANGE `user_shop_id` `user_shop_id` INT( 10 ) UNSIGNED NOT NULL ;
update product set user_shop_id = (select user_shop_id from shop where shop.id=shop_id);
ALTER TABLE `product` ADD FOREIGN KEY ( `user_shop_id` ) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `product` DROP `shop_id`;

ALTER TABLE `product2shop` ADD FOREIGN KEY ( `product_id` ) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `product2shop` ADD FOREIGN KEY ( `shop_id` ) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;


CREATE TABLE `sales2shop` (
`sales_id` INT NOT NULL ,
`shop_id` INT NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `sales2shop` ADD INDEX `index_sales2shop_shop_id` ( `shop_id` ) ;
ALTER TABLE `sales2shop` ADD UNIQUE `index_sales2shop_sales_id_shop_id` ( `sales_id` , `shop_id` ); 
insert into sales2shop (sales_id,shop_id) select id,shop_id from sales;
ALTER TABLE `sales` DROP FOREIGN KEY `fk_sales_shop` ;
ALTER TABLE sales DROP INDEX fk_sales_shop;
ALTER TABLE `sales` ADD `user_shop_id` INT NOT NULL;
ALTER TABLE `sales` ADD INDEX `index_sales_user_shop_id` ( `user_shop_id` );
ALTER TABLE `sales` CHANGE `user_shop_id` `user_shop_id` INT( 10 ) UNSIGNED NOT NULL ;
update sales set user_shop_id = (select user_shop_id from shop where shop.id=shop_id);
ALTER TABLE `sales` ADD FOREIGN KEY ( `user_shop_id` ) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `sales` DROP `shop_id`;
ALTER TABLE `sales2shop` ADD FOREIGN KEY ( `sales_id` ) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `sales2shop` ADD FOREIGN KEY ( `shop_id` ) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;

CREATE TABLE `scan_product2shop` (
`scan_product_id` INT NOT NULL ,
`shop_id` INT NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `scan_product2shop` ADD INDEX `index_scan_product2shop_shop_id` ( `shop_id` ) ;
ALTER TABLE `scan_product2shop` ADD UNIQUE `index_scan_product2shop_scan_product_id_shop_id` ( `scan_product_id` , `shop_id` ); 
insert into scan_product2shop (scan_product_id,shop_id) select id,shop_id from scan_product;
ALTER TABLE `scan_product` DROP FOREIGN KEY `fk_scan_product_shop` ;
ALTER TABLE scan_product DROP INDEX fk_scan_product_shop;
ALTER TABLE `scan_product` ADD `user_shop_id` INT NOT NULL;
ALTER TABLE `scan_product` ADD INDEX `index_scan_product_user_shop_id` ( `user_shop_id` );
ALTER TABLE `scan_product` CHANGE `user_shop_id` `user_shop_id` INT( 10 ) UNSIGNED NOT NULL ;
update scan_product set user_shop_id = (select user_shop_id from shop where shop.id=shop_id);
ALTER TABLE `scan_product` ADD FOREIGN KEY ( `user_shop_id` ) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `scan_product` DROP FOREIGN KEY `fk_scan_product_shop_id` ;
ALTER TABLE `scan_product` DROP `shop_id`;
ALTER TABLE `scan_product2shop` ADD FOREIGN KEY ( `scan_product_id` ) REFERENCES `scan_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `scan_product2shop` ADD FOREIGN KEY ( `shop_id` ) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;