ALTER TABLE `prize_order`   
  ADD COLUMN `country` VARCHAR(50) NULL AFTER `post_index`,
  ADD COLUMN `city` VARCHAR(100) NULL AFTER `country`;
