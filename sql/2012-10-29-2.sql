ALTER TABLE `action_log`   
  ADD COLUMN `object_id` INT NULL AFTER `trm_main_id`,
  ADD COLUMN `param1` VARCHAR(50) NULL AFTER `object_id`,
  ADD COLUMN `param2` VARCHAR(50) NULL AFTER `param1`;
  
ALTER TABLE `action_log`   
  ADD COLUMN `result_code` INT NULL AFTER `param2`,
  ADD COLUMN `result_type` INT NULL AFTER `result_code`;

