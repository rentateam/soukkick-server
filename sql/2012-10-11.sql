CREATE TABLE `user_banned` (
	`user_id` INT(10) UNSIGNED NOT NULL COMMENT 'Пользователь',
	`reason` TEXT NOT NULL COMMENT 'Причина бана',
	`date_created` DATETIME NOT NULL COMMENT 'Время бана',
	PRIMARY KEY (`user_id`),
	UNIQUE INDEX `user_id` (`user_id`),
	CONSTRAINT `FK_user_banned_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;