ALTER TABLE `shop` ADD `user_shop_id` INT NOT NULL ;
update shop set user_shop_id = (SELECT id FROM `user_shop` WHERE shop_id=shop.id);
alter table `user_shop` drop Foreign KEY `fk_user_shop_shop_id`;
alter table `user_shop` drop KEY `fk_user_shop_shop_id`;
ALTER TABLE `user_shop` DROP `shop_id`;
ALTER TABLE `shop` CHANGE `user_shop_id` `user_shop_id` INT( 10 ) UNSIGNED NOT NULL;
alter table `shop` add FOREIGN KEY (`user_shop_id`) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE TABLE `transmitter` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `frequency` varchar(100) NOT NULL,
 `amount_enter` int(11) NOT NULL DEFAULT '0',
 `lat` double NOT NULL DEFAULT '0',
 `longtitude` double NOT NULL DEFAULT '0',
 `last_modified` datetime NOT NULL,
 `shop_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `shop_id` (`shop_id`),
 CONSTRAINT `transmitter_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `transmitter_type` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL
) ENGINE = InnoDB;

ALTER TABLE `transmitter` ADD `transmitter_type_id` INT NOT NULL ;
alter table `transmitter` add FOREIGN KEY (`transmitter_type_id`) REFERENCES `transmitter_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
INSERT INTO `transmitter_type` (`id`, `name`) VALUES (NULL, 'Обычный');


ALTER TABLE transmitter DROP INDEX shop_id;
ALTER TABLE transmitter DROP INDEX transmitter_type_id;
ALTER TABLE `transmitter` ADD INDEX `index_transmitter_shop_id_transmitter_type_id` ( `shop_id` , `transmitter_type_id` ) ;

ALTER TABLE `action_checkout_attempt` ADD `transmitter_type_id` INT NOT NULL ;
update action_checkout_attempt set transmitter_type_id = 1;
alter table `action_checkout_attempt` add foreign key (transmitter_type_id) references `transmitter_type` (`id`) on update no action on delete no action;
ALTER TABLE `action_checkin` ADD `transmitter_type_id` INT NOT NULL ;
update action_checkin set transmitter_type_id = 1;
alter table `action_checkin` add foreign key (transmitter_type_id) references `transmitter_type` (`id`) on update no action on delete no action;
ALTER TABLE `action_checkout` ADD `transmitter_type_id` INT NOT NULL ;
update action_checkout set transmitter_type_id = 1;
alter table `action_checkout` add foreign key (transmitter_type_id) references `transmitter_type` (`id`) on update no action on delete no action;


ALTER TABLE `user` ADD `is_male` BOOLEAN NOT NULL ,
ADD `birthdate` DATE NOT NULL;

update user set is_male = true, birthdate = '2012-01-01';
