ALTER TABLE `action_scan` ADD `shop_id` INT NOT NULL ;
ALTER TABLE `action_scan` ADD INDEX `action_scan_shop_id` ( `shop_id` ) ;
TRUNCATE table  action_scan;
ALTER TABLE `action_scan` ADD FOREIGN KEY ( `shop_id` ) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;