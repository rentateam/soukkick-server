CREATE TABLE `user_push`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `token` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
ALTER TABLE `user_push` ADD COLUMN `type` ENUM('iphone','android') NOT NULL AFTER `token`;
ALTER TABLE `user_push`   
  DROP INDEX `user_id`,
  ADD  UNIQUE INDEX `user_id` (`user_id`);