truncate table `action_shoppage`;
ALTER TABLE `action_shoppage` DROP FOREIGN KEY `action_shoppage_ibfk_1` ;
ALTER TABLE `action_shoppage` CHANGE `shop_id` `user_shop_id` INT( 10 ) UNSIGNED NOT NULL;
ALTER TABLE `action_shoppage` ADD FOREIGN KEY ( `user_shop_id` ) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `action_shoppage` DROP INDEX `action_checkin_shop_ibfk_1` ,ADD INDEX `action_checkin_user_shop_ibfk_1` ( `user_shop_id` ) ;
ALTER TABLE `user_shop` ADD `amount_page` INT( 11 ) NOT NULL ;
update `user_shop` set amount_page = (select amount_page from shop where user_shop_id = user_shop.id limit 1);
ALTER TABLE `shop` DROP `amount_page`;