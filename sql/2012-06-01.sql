ALTER TABLE `user` ADD `contact_id` BIGINT NOT NULL;
ALTER TABLE `user` CHANGE `contact_id` `contact_id` BIGINT( 11 ) NULL ;
ALTER TABLE `user` ADD INDEX `index_user_contact_id` ( `contact_id` ) ;
update `user` set contact_id = null;