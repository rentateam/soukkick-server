CREATE TABLE `action_enter` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_created` datetime NOT NULL,
 `datetime_created` datetime NOT NULL,
 `user_id` int(11) unsigned NOT NULL,
 `shop_id` int(11) NOT NULL,
 `transmitter_type_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_action_enter_user_id` (`user_id`),
 KEY `action_enter_shop_ibfk_1` (`shop_id`,`datetime_created`),
 KEY `transmitter_type_id` (`transmitter_type_id`),
 CONSTRAINT `action_enter_ibfk_2` FOREIGN KEY (`transmitter_type_id`) REFERENCES `transmitter_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `action_enter_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_action_enter_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
RENAME TABLE `action_checkout` TO `action_exit` ;
RENAME TABLE `action_checkout_attempt` TO `action_exit_attempt` ;
truncate table action_checkin;
truncate table action_enter;
truncate table action_exit;
truncate table action_exit_attempt;
truncate table event_shop;
ALTER TABLE `action_exit_attempt` ADD UNIQUE `index_action_exit_attempt_user_id_shop_id_transmitter_type_id` ( `user_id` , `shop_id` , `transmitter_type_id` ) ;
ALTER TABLE action_exit_attempt DROP INDEX index_action_checkout_attempt_user_id_shop_id_datetime_created;