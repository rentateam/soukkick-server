CREATE TABLE `favourite_user_shop` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) unsigned NOT NULL,
 `user_shop_id` int(10) unsigned NOT NULL,
 `sort_order` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 KEY `fk_favourite_user_shop_user_shop_id` (`user_shop_id`),
 KEY `fk_favourite_user_shop_user_id` (`user_id`),
 CONSTRAINT `favourite_user_shop_ibfk_1` FOREIGN KEY (`user_shop_id`) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `fk_favourite_user_shop_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
INSERT INTO favourite_user_shop (user_id,user_shop_id,sort_order) 
SELECT user_id,shop.user_shop_id,sort_order
        FROM favourite_shop 
left join shop on favourite_shop.shop_id = shop.id;
DROP TABLE `favourite_shop`;
ALTER TABLE `user_shop` ADD `is_recommended` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `shop` DROP `is_recommended`;
