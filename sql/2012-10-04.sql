ALTER TABLE `prize`  ADD COLUMN `type` INT DEFAULT 1  NOT NULL AFTER `is_active`;
CREATE TABLE `prize_promo` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `prize_id` INT(11) NOT NULL,
 `code` VARCHAR(255) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_prize_promo_prize_id` (`prize_id`),
 CONSTRAINT `prize_promo_ibfk_1` FOREIGN KEY (`prize_id`) REFERENCES `prize` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  )ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `action_prize_promo` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `date_created` DATETIME NOT NULL,
 `datetime_created` DATETIME NOT NULL,
 `user_id` INT(11) UNSIGNED NOT NULL,
 `prize_promo_id` INT(11) NOT NULL,
 `trm_main_id` INT(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_action_prize_prize_promo_id` (`prize_promo_id`),
 KEY `fk_action_prize_promo_user_id` (`user_id`),
 CONSTRAINT `action_prize_promo_ibfk_1` FOREIGN KEY (`prize_promo_id`) REFERENCES `prize_promo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `action_prize_promo_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8;

ALTER TABLE `prize_promo` ADD COLUMN `is_active` TINYINT(1) DEFAULT 1  NOT NULL AFTER `code`;