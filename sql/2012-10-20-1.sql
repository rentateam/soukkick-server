CREATE TABLE `report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `query` TEXT NOT NULL,
    `caption` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8; 