ALTER TABLE `user` ADD `last_modified` DATETIME NOT NULL;
ALTER TABLE `user_shop` ADD `last_modified` DATETIME NOT NULL;
CREATE TABLE `user_address` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT NOT NULL ,
`country` VARCHAR( 100 ) NOT NULL ,
`city` VARCHAR( 100 ) NOT NULL ,
`index` VARCHAR( 6 ) NOT NULL ,
`street` VARCHAR( 200 ) NOT NULL ,
`house` VARCHAR( 10 ) NOT NULL ,
`flat` VARCHAR( 10 ) NOT NULL
) ENGINE = InnoDB;
ALTER TABLE `user_address` ADD `phone` VARCHAR( 30 ) NOT NULL;
ALTER TABLE `user_address` CHANGE `country` `country` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `city` `city` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `index` `index` VARCHAR( 6 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `street` `street` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `house` `house` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `flat` `flat` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `phone` `phone` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `user_address` CHANGE `index` `post_index` VARCHAR( 6 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;