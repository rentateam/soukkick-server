CREATE TABLE `city`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `shop` ADD COLUMN `city_id` INT NULL AFTER `transmitter_info`;
ALTER TABLE `shop`  ADD FOREIGN KEY (`city_id`) REFERENCES `city`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
INSERT INTO `city` (
`id` ,
`name`
)
VALUES (
'1', 'Москва'
);

update shop set city_id = 1;
