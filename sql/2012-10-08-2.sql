ALTER TABLE `trade_network` ADD COLUMN `is_deleted` TINYINT(1) DEFAULT 0  NOT NULL AFTER `integration_description`;
ALTER TABLE `shop` ADD COLUMN `is_deleted` TINYINT(1) DEFAULT 0  NOT NULL;
ALTER TABLE `shop`   ADD  INDEX `index_shop_is_deleted` (`is_deleted`);
ALTER TABLE `trade_network`   ADD  INDEX `index_trade_network_is_deleted` (`is_deleted`);
