CREATE TABLE `banner` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_expire` date NOT NULL,
 `picture_path` varchar(100) NOT NULL,
 `last_modified` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;