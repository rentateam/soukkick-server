CREATE TABLE `rule_main` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `datetime_created` datetime NOT NULL,
 `name` varchar(500) NOT NULL,
 `user_shop_id` int(10) unsigned NOT NULL,
 `is_active` tinyint(1) NOT NULL DEFAULT '0',
 `date_from` datetime NOT NULL,
 `date_to` datetime NOT NULL,
 `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
 `deleted_user_shop_id` int(10) unsigned NULL,
 `datetime_deleted` datetime NOT NULL,
 PRIMARY KEY (`id`),
 KEY `user_shop_id` (`user_shop_id`),
 CONSTRAINT `rule_main_ibfk_1` FOREIGN KEY (`user_shop_id`) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `rule_main_ibfk_2` FOREIGN KEY (`deleted_user_shop_id`) REFERENCES `user_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `rule_item`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `rule_main_id` INT(11),
  `action_object_id` INT,
  `amount` INT,
  `max_spend_amount` INT DEFAULT -1,
  `spent_amount` INT DEFAULT 0,
  `is_active` TINYINT DEFAULT 0,
  `action_type` INT,
  `is_deleted` TINYINT DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`rule_main_id`) REFERENCES `rule_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `trm_main`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_created` DATETIME NOT NULL,
  `rule_item_id` INT NOT NULL,
  `amount` INT NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `shop_id` INT(11),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`shop_id`) REFERENCES `shop`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `rule_main2shop`(  
  `rule_main_id` INT NOT NULL,
  `shop_id` INT(11) NOT NULL,
  FOREIGN KEY (`rule_main_id`) REFERENCES `rule_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`shop_id`) REFERENCES `shop`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);


RENAME TABLE `user_shop` TO `trade_network`;
ALTER TABLE `action_shoppage` DROP FOREIGN KEY `action_shoppage_ibfk_1`;
ALTER TABLE `action_shoppage` CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `action_shoppage` ADD CONSTRAINT `action_shoppage_ibfk_4` FOREIGN KEY (`trade_network_id`) REFERENCES `trade_network`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE `favourite_user_shop` DROP FOREIGN KEY `favourite_user_shop_ibfk_1` ;
ALTER TABLE `favourite_user_shop`  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `favourite_user_shop` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;






RENAME TABLE `favourite_user_shop` TO `favourite_trade_network`;



ALTER TABLE `product` DROP FOREIGN KEY `product_ibfk_1` ;
ALTER TABLE `product`  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `product` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;



ALTER TABLE `rule_main` DROP FOREIGN KEY `rule_main_ibfk_1` ;
ALTER TABLE `rule_main` DROP FOREIGN KEY `rule_main_ibfk_2` ;
ALTER TABLE `rule_main`   
  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL,
  CHANGE `deleted_user_shop_id` `deleted_trade_network_id` INT(10) UNSIGNED NULL;
ALTER TABLE `rule_main` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
ALTER TABLE `rule_main` ADD FOREIGN KEY ( `deleted_trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
  

ALTER TABLE `sales` DROP FOREIGN KEY `sales_ibfk_1` ;
ALTER TABLE `sales`   
  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
  ALTER TABLE `sales` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;

ALTER TABLE `scan_product` DROP FOREIGN KEY `scan_product_ibfk_1` ;
  ALTER TABLE `scan_product`   
  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
  ALTER TABLE `scan_product` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
  
ALTER TABLE `shop` DROP FOREIGN KEY `shop_ibfk_1` ;
ALTER TABLE `shop`   
  CHANGE `user_shop_id` `trade_network_id` INT(10) UNSIGNED NOT NULL;
  ALTER TABLE `shop` ADD FOREIGN KEY ( `trade_network_id` ) REFERENCES `trade_network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;



  
CREATE TABLE `bpa_main`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(300) NOT NULL,
  `description` VARCHAR(5000) NOT NULL,
  `fishki_amount` INT NOT NULL DEFAULT 0,
  `conversion_rate_to_fishki` INT NOT NULL,
  `conversion_rate_from_fishki` INT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `bpa_manager`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `bpa_main_id` INT NOT NULL,
  `name` VARCHAR(100),
  `surname` VARCHAR(50),
  `email` VARCHAR(50) NOT NULL,
  `reg_date` DATETIME NOT NULL,
  `picture_path` VARCHAR(20),
  `salt` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`bpa_main_id`) REFERENCES `bpa_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
ALTER TABLE `bpa_manager`   
  ADD COLUMN `password` VARCHAR(65) NULL AFTER `email`;


ALTER TABLE `trade_network`   
  ADD COLUMN `bpa_main_id` INT NULL AFTER `is_recommended`,
  ADD FOREIGN KEY (`bpa_main_id`) REFERENCES `bpa_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
  

  

ALTER TABLE `rule_main` DROP FOREIGN KEY `rule_main_ibfk_1`;
ALTER TABLE `rule_main` DROP FOREIGN KEY `rule_main_ibfk_2`;
ALTER TABLE `rule_main`   
  CHANGE `trade_network_id` `created_bpa_manager_id` INT(10) UNSIGNED NOT NULL,
  CHANGE `deleted_trade_network_id` `deleted_bpa_manager_id` INT(10) UNSIGNED NULL,
  ADD COLUMN `bpa_main_id` INT NULL AFTER `datetime_deleted`;
ALTER TABLE `rule_main` CHANGE `created_bpa_manager_id` `created_bpa_manager_id` INT( 11 ) NOT NULL ,
CHANGE `deleted_bpa_manager_id` `deleted_bpa_manager_id` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE `rule_main` ADD CONSTRAINT `rule_main_ibfk_3` FOREIGN KEY (`deleted_bpa_manager_id`) REFERENCES `bpa_manager`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE `rule_main` ADD CONSTRAINT `rule_main_ibfk_1` FOREIGN KEY (`created_bpa_manager_id`) REFERENCES `bpa_manager`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `rule_main` ADD CONSTRAINT `rule_main_ibfk_2` FOREIGN KEY (`bpa_main_id`) REFERENCES `bpa_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `bpa_manager`   ADD COLUMN `is_active` TINYINT DEFAULT 1  NOT NULL AFTER `salt`;

ALTER TABLE `rule_main`   
  ADD COLUMN `max_spend_amount` INT(11) DEFAULT -1  NULL AFTER `bpa_main_id`,
  ADD COLUMN `spent_amount` INT(11) DEFAULT 0  NULL AFTER `max_spend_amount`;
  
  
  
  
ALTER TABLE  `rule_main2shop` DROP FOREIGN KEY `rule_main2shop_ibfk_1`;
  
  ALTER TABLE `rule_main2shop`   
  CHANGE `rule_main_id` `rule_item_id` INT(11) NOT NULL; 
  


ALTER TABLE `rule_main2shop` ADD CONSTRAINT `rule_item2shop_ibfk_1` FOREIGN KEY (`rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
RENAME TABLE `rule_main2shop` TO `rule_item2shop`;





CREATE TABLE `rule_main2shop`(  
  `rule_main_id` INT NOT NULL,
  `shop_id` INT(11) NOT NULL,
  FOREIGN KEY (`rule_main_id`) REFERENCES `rule_main`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (`shop_id`) REFERENCES `shop`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE `rule_main`   
  CHANGE `datetime_deleted` `datetime_deleted` DATETIME NULL;

  
ALTER TABLE `transmitter`   
  ADD COLUMN `active_rule_item_id` INT(11) NULL AFTER `transmitter_type_id`,
  ADD FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `trade_network`   
  ADD COLUMN `active_rule_item_id` INT(11) NULL,
  ADD FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
  
ALTER TABLE `scan_product`   
  ADD COLUMN `active_rule_item_id` INT(11) NULL,
  ADD FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
  
ALTER TABLE `prize`   
  ADD COLUMN `active_rule_item_id` INT(11) NULL,
  ADD FOREIGN KEY (`active_rule_item_id`) REFERENCES `rule_item`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
  
  
ALTER TABLE `action_checkin` ADD `trm_main_id` INT NULL DEFAULT NULL;
ALTER TABLE `action_shoppage` ADD `trm_main_id` INT NULL DEFAULT NULL;
ALTER TABLE `action_scan` ADD `trm_main_id` INT NULL DEFAULT NULL;
ALTER TABLE `action_prize` ADD `trm_main_id` INT NULL DEFAULT NULL;

delete from action_checkin where price = 0;
