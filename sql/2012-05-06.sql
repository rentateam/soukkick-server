ALTER TABLE `user` ADD `udid` VARCHAR( 40 ) NULL ;
ALTER TABLE `user` CHANGE `email` `email` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `password` `password` VARCHAR( 65 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
CHANGE `is_male` `is_male` TINYINT( 1 ) NULL ,
CHANGE `birthdate` `birthdate` DATE NULL;