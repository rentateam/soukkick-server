CREATE TABLE `user_verify` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(10) unsigned NOT NULL,
 `code` varchar(10) NOT NULL,
 PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
 CONSTRAINT `user_verify_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;