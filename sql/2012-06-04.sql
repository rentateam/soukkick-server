CREATE TABLE `user_hack_scan` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `lat` double NOT NULL DEFAULT '0',
 `longtitude` double NOT NULL DEFAULT '0',
 `date_created` datetime NOT NULL,
 `shop_id` int(11) NOT NULL,
 `user_id` int(10) unsigned NOT NULL,
 PRIMARY KEY (`id`),
 KEY `shop_id` (`shop_id`),
 KEY `user_id` (`user_id`),
 CONSTRAINT `user_hack_scan_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
 CONSTRAINT `user_hack_scan_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `user_hack_scan` ADD `barcode` VARCHAR( 30 ) NOT NULL ;