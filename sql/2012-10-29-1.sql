CREATE TABLE `trm_main_status`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20),
  PRIMARY KEY (`id`)
);

ALTER TABLE `trm_main`   
  ADD COLUMN `trm_main_status_id` INT NOT NULL AFTER `action_long`, 
  ADD  INDEX `trm_main_status_id` (`trm_main_status_id`);

INSERT INTO trm_main_status VALUES (1,'new');
INSERT INTO trm_main_status VALUES (2,'confirmed');
INSERT INTO trm_main_status VALUES (3,'rejected');

UPDATE `trm_main`   SET trm_main_status_id = 2;
