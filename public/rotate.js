﻿var ie = /MSIE (\d+\.\d+);/.test(navigator.userAgent) && !window.opera;

function SetOpacity(elem, val) {
    if (ie) {
        if (!elem.alpha$prepared) {
            elem.alpha$prepared = true;
            var oldStyle = elem.style.cssText;
            if (oldStyle)
                if (oldStyle.charAt(oldStyle.length - 1) != ";")
                oldStyle += ";";
            else
                oldStyle = "";
            elem.style.cssText = oldStyle + " filter: alpha(opacity=100);";
        }
        try { elem.filters.alpha.opacity = val; } catch (ex) { }
    } else {
        elem.style.opacity = val / 100;
        elem.style.MozOpacity = val / 100;
    }
}

var imgs = ["imgs/pic1.jpg", "imgs/pic2.jpg", "imgs/pic3.jpg", "imgs/pic4.jpg", "imgs/pic5.jpg", "imgs/pic6.jpg"];

var rotateHolder = document.getElementById("rotateHolder");
var preload = document.createElement("div");
preload.style.cssText = "overflow: hidden; width: 100px; height: 100px; position: absolute; top: -200px";
window.setTimeout(function() { document.body.appendChild(preload); }, 1);

imgs[0] = document.getElementById("imgRotate");
for (var i = 1; i < imgs.length; i++) {
    var img = document.createElement("img");
    img.src = imgs[i];
    preload.appendChild(imgs[i] = img);
}

var currentImg = 0;
window.setInterval(function() {
    var prevImg = currentImg;
    currentImg++;
    if (currentImg >= imgs.length)
        currentImg = 0;

    var imgRotate = imgs[currentImg]
    imgRotate.style.visibility = "hidden";
    SetOpacity(imgRotate, 0);
    rotateHolder.appendChild(imgRotate);

    var step = 0;
    var anim = window.setInterval(function() {
        if (!step)
            imgRotate.style.visibility = "visible";
        step += 5 /* скорость анимации */;
        if (step >= 100)
            step = 100;

        SetOpacity(imgRotate, step);

        if (step === 100) {
            window.clearInterval(anim);
            rotateHolder.removeChild(imgs[prevImg]);
        }
    }, 30);
}, 3000 /* интервал, в мс между картинками */);
