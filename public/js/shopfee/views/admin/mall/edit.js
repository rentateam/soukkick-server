formRules.form_mall_map = {
                            floor: {
                                required: true,
                                maxlength: 10
                            },
                            file: {
                                required: true,
                                accept:'svg'
                            }
                        };
formMessages.form_mall_map = {
                            floor: {
                                    required: 'Этаж обязателен для заполнения',
                                    number: 'Максимальная длина обозначения этажа 10 символов'
                                },
                                file: {
                                    required: 'Карта не была загружена',
                                    accept: 'Карта должна быть в формате svg'
                                }
                           };

$(function(){
    var options = {
                        $link:$('#edit_mall'),
                        formId:'form_mall',
                        entityId:registry.mallEdit.mallId,
                        entityController:'admin/mall',
                        afterCallback:function(data){
                            var $form = $(this);
                            formRules.form_mall_map.file.required = true;
                            addValidators($form,formRules,formMessages);
                            initCloseForm($form);
                            initSubmitForm($form);
                        }
                  }; 
    new Editor(options);
    
    var mapOptions = {
                        $link:$('#add_mall_map'),
                        formId:'form_mall_map',
                        entityId:0,
                        entityController:'admin/mall-map',
                        extraParams:{
                                        mall_id:registry.mallEdit.mallId
                                    },
                        afterCallback:function(data){
                            var $form = $(this);
                            formRules.form_mall_map.file.required = false;
                            addValidators($form,formRules,formMessages,true);
                            initCloseForm($form);
                            initSubmitForm($form);
                        }
                  };
    new Editor(mapOptions);
    
    $('[data-mall-map-id]').each(function(){
        var mapId = $(this).data('mall-map-id');
        var options = {
                        $link:$('[data-edit-mall-map-id='+mapId+']'),
                        formId:'form_mall_map',
                        entityId:mapId,
                        entityController:'admin/mall-map',
                        afterCallback:function(data){
                            var $form = $(this);
                            addValidators($form,formRules,formMessages);
                            initCloseForm($form);
                            initSubmitForm($form);
                        }
                  }; 
        new Editor(options);
    });
    
    
    initSubmitForm($(document));
    initCloseForm($(document));
    
    $('#mall-tag-bind').click(function(){
        var mall2shopId = $('#mall2shop-unbound').val();
        var shopId = $('#mall-candidate').val();
        if(mall2shopId && shopId)
        {
            location.href = '/admin/mall/bind-shop/mall2shop_id/'+mall2shopId+'/shop_id/'+shopId+'/';
        }
    });
});
