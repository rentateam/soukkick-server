var formRules = {};
var formMessages = {};
var logoRequired = false;//typeof(registry.mallEdit) == 'undefined' || typeof(registry.mallEdit.mallId) == 'undefined';
formRules.form_mall = {
                            name: {
                                required: true,
                                maxlength: 500
                            },
                            address: {
                                required: true
                            },
                            lat: {
                                required: true,
                                number:true
                            },
                            long: {
                                required: true,
                                number:true
                            },
                            logo: {
                                required: logoRequired
                            }
                        };
formMessages.form_mall = {
                                name: {
                                    required: 'Название обязательно для заполнения',
                                    maxlength: 'Максимальная длина названия 500 символов'
                                },
                                address: {
                                    required: 'Адрес обязателен для заполнения'
                                },
                                lat:{
                                    required: 'Широта обязательна для заполнения',
                                    number: 'Широта должна быть числом'
                                },
                                long:{
                                    required: 'Долгота обязательна для заполнения',
                                    number: 'Долгота должна быть числом'
                                },
                                logo: {
                                    required: 'Логотип не был загружен'
                                }
                           };