$(function(){
    var options = {
                        $link:$('#add_mall'),
                        formId:'form_mall',
                        entityId:0,
                        entityController:'admin/mall',
                        afterCallback:function(data){
                            var $form = $(this);
                            addValidators($form,formRules,formMessages,true);
                            initCloseForm($form);
                            initSubmitForm($form);
                        }
                  };
    new Editor(options);
    
    initSubmitForm($(document));
    initCloseForm($(document));
});