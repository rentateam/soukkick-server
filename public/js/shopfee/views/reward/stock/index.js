$(function(){
    var tableSorter = new TableSorter();
    tableSorter.setCurrentSortField(registry.stock.sortField);
    tableSorter.setCurrentSortDir(registry.stock.sortDir);
    $(document).bind(TableSorter.prototype.EVENT_SORT_CHANGED, function(event, field, direction) {
        location.href = '/reward/stock/index/page/'+registry.stock.page+'/?sort='+field+'&dir='+direction;
    });
});