function checkAllCheckboxes($checkAll)
{
	var checked = $checkAll.is(':checked');
	$('[data-order-id]').each(function(){
		checked ? $(this).attr('checked','checked') : $(this).removeAttr('checked');
	});
}

$(function(){
    $('#prize_order_status_id').change(function(){
        $('#form_reward_order').submit();
    });
    
    $('#order-change-status-submit').click(function(){
    	orderIds = [];
    	$('[data-order-id]:checked').each(function(){
    		orderIds.push('order_id[]='+$(this).data('order-id'));
    	});
    	if(orderIds.length > 0){
    		location.href = '/reward/order/change-status/?status_id='+$('#order-change-status').val()+'&'+orderIds.join('&');
    	}
    });
    
    var $checkAll = $('#check-all-order');
    if (jQuery.browser.msie) {
    	$checkAll.click(function(){checkAllCheckboxes($(this))});
    } else {
    	$checkAll.change(function(){checkAllCheckboxes($(this))});
    }
    
    var tableSorter = new TableSorter();
    tableSorter.setCurrentSortField(registry.order.sortField);
    tableSorter.setCurrentSortDir(registry.order.sortDir);
    $(document).bind(TableSorter.prototype.EVENT_SORT_CHANGED, function(event, field, direction) {
        location.href = '/reward/order/index/page/'+registry.order.page+'/?sort='+field+'&dir='+direction;
    });
});