$(function(){
    //Календарик
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $.datepicker.setDefaults(
    {
        showOn: 'both',
        buttonImageOnly: true,
        buttonImage: '/img/admin/calendar.gif',
        buttonText: 'Calendar',
        minDate: '-100Y',
        maxDate: '+10Y',
        firstDay: 1
    } );
    $('.date_element').each(function(){
        $(this).datepicker({dateFormat: 'yy-mm-dd'});
    });
});