function ShopModalForm(options)
{
    this.options = options;
    this.init();
}

ShopModalForm.prototype = {
        shopForm:               null,
        shopMarker:             null,
        
        init:                   function()
                                {
                                    var formOptions = {
                                                            template:this.options.template,
                                                            objectId:this.options.editObjectId,
                                                            objectType: 'shop',
                                                            top: '0%'
                                                      };
                                    this.shopForm = new ModalForm(formOptions);
                                    
                                    this._bindAfterShow();
                                    this._bindAddForm();
                                },
                                
        getForm:                function()
                                {
                                    return this.shopForm;
                                },
                                
        _bindAfterShow:         function()
                                {
                                    var thisObj = this;
                                    $(document).bind(ModalForm.prototype.EVENT_AFTER_SHOW,{},function(invokeObject,shopModalForm){
                                        
                                        if(shopModalForm == thisObj.shopForm)
                                        {
                                            //Переключатель ТЦ/адрес+координаты
                                            thisObj._initMallSwitch();
                                            
                                            //Закрытие формы
                                            var thisModalForm = shopModalForm;
                                            shopModalForm.getCancelButton().click(function(){
                                                thisModalForm.hide();
                                            });
                                            
                                            thisObj._addValidators($(shopModalForm.getForm()));
                                            
                                            //Карты
                                            thisObj._initializeGoogleMap();
                                            
                                            //Режим редактирования
                                            if(thisObj.options.mapShopList.length == 1)
                                            {
                                                var lat = thisObj.options.mapShopList[0].lat;
                                                var longtitude = thisObj.options.mapShopList[0].long;
                                                var defaultPoint;
                                                if(lat && longtitude)
                                                {
                                                    defaultPoint = new google.maps.LatLng(lat,longtitude);
                                                }
                                                else
                                                {
                                                    defaultPoint = thisObj._getDefaultPoint();
                                                }
                                                thisObj._synchronizeCoordinates({latLng: defaultPoint});
                                            }
                                        }
                                    });
                                },
                                
        _bindAddForm:           function()
                                {
                                    var thisObj = this;
                                    this.options.$showObject.click(function(){
                                        thisObj.shopForm.show();
                                        return false;
                                    });
                                },
                                
        _initMallSwitch:        function()
                                {
                                    var switchElementIds = ['address','lat','long'];
                                    var thisObj = this;
                                    $('#mall_id').change(function(){
                                        var $this = $(this);
                                        var noMall = $this.val() == '0';
                                        for(var i = 0; i < switchElementIds.length; i++)
                                        {
                                            var $element = $('#'+switchElementIds[i]);
                                            //noMall ? $element.removeAttr('readonly') : $element.attr('readonly','readonly');
                                        }
                                        if(!noMall)
                                        {
                                            var $selectedOption = $this.find('option:selected');
                                            var lat = $selectedOption.data('lat');
                                            var longtitude = $selectedOption.data('long');
                                            $('#address').val($selectedOption.data('address'));
                                            $('#lat').val(lat);
                                            $('#long').val(longtitude);
                                            
                                            var mallPoint = new google.maps.LatLng(lat,longtitude);
                                            thisObj._synchronizeCoordinates({latLng: mallPoint});
                                            thisObj._setMarker(mallPoint,null);
                                        }
                                    });
                                },
                                
        _addValidators:         function()
                                {
                                    var $shopForm = $(this.shopForm.getForm());
                                    var rules = {
                                                    address: {
                                                        required: true
                                                    },
                                                    lat: {
                                                        required: true,
                                                        number:true
                                                    },
                                                    long: {
                                                        required: true,
                                                        number:true
                                                    }
                                                };
                                    var messages = {
                                                        address: {
                                                            required: 'Адрес обязателен для заполнения'
                                                        },
                                                        lat:{
                                                            required: 'Широта обязательна для заполнения',
                                                            number: 'Широта должна быть числом'
                                                        },
                                                        long:{
                                                            required: 'Долгота обязательна для заполнения',
                                                            number: 'Долгота должна быть числом'
                                                        }
                                                   };
                                                   
                                    var $thisShopForm = $shopForm;
                                    $shopForm.validate({
                                        onkeyup: false,
                                        errorClass: "error",
                                        errorPlacement: function(error, element) {
                                            //$currentElement.parent().append("<div class='js-error-container'></div>");
                                            //error.appendTo(element.next(".js-error-container"));
                                            error.insertAfter(element);
                                        },
                                        rules:rules,
                                        messages:messages
                                    });
                                },
                                
        _getDefaultPoint:       function()
                                {
                                    return new google.maps.LatLng(55.832951,37.482516);
                                },
                                
        _initializeGoogleMap:   function() 
                                {
                                    this.map = new google.maps.Map(
                                                        document.getElementById(this.options.googleMapId),
                                                        {
                                                            zoom: 12,
                                                            center: this._getDefaultPoint(),
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                            zoomControl: true,
                                                            mapTypeControl: true,
                                                            scaleControl: true
                                                        }
                                                    );
                                    this._initializeShopMarkers();
                                },
                                
        _initializeShopMarkers: function()
                                {
                                    var thisObj = this;
                                    for(var i=0;i<this.options.mapShopList.length;i++)
                                    {
                                        var shop = this.options.mapShopList[i];
                                        //default coordinates
                                        if((shop.lat == 0) && (shop.long == 0) /*&& (shop.cityName != '' || shop.address != '')*/)
                                        {
                                            var geocoder = new google.maps.Geocoder();
                                            if (geocoder) 
                                            {
                                                geocoder.geocode( 
                                                                    { 
                                                                        address: shop.cityName+' '+shop.address
                                                                    }, 
                                                                    function(results, status) {
                                                                        if (status == google.maps.GeocoderStatus.OK) 
                                                                        {
                                                                            thisObj._setMarker(results[0].geometry.location,'');
                                                                        }
                                                                        else
                                                                        {
                                                                            thisObj._setMarker(thisObj._getDefaultPoint(),'');
                                                                        }
                                                                      }
                                                                );
                                            }
                                        }
                                        //else, if coordinates are set
                                        else
                                        {
                                            thisObj._setMarker(new google.maps.LatLng(shop.lat,shop.long),shop.address);
                                        }
                                    }
                                },
                                
        _setMarker:             function(point,title)
                                {
                                    this.map.setCenter(point);
                                    
                                    if(this.shopMarker === null)
                                    {
                                        this.shopMarker = new google.maps.Marker(
                                                                                    {
                                                                                        animation:google.maps.Animation.DROP,
                                                                                        map: this.map,
                                                                                        position: point,
                                                                                        title:title,
                                                                                        draggable:true
                                                                                    }
                                                                                );
        
                                        var thisObj = this;
                                        google.maps.event.addListener(this.shopMarker, 'dragend', function(mouseEvent){
                                            thisObj._synchronizeCoordinates(mouseEvent);
                                        });
                                    }
                                    else
                                    {
                                        this.shopMarker.setPosition(point);
                                        if(title !== null)
                                            this.shopMarker.setTitle(title);
                                    }
                                },

        _synchronizeCoordinates:    function(mouseEvent)
                                    {
                                        var coordinate = mouseEvent.latLng;
                                        $('#lat').val(this._roundFloat(coordinate.lat()));
                                        $('#long').val(this._roundFloat(coordinate.lng()));
                                    },

        _roundFloat:            function(float)
                                {
                                    return float.toFixed(6);
                                }
};
