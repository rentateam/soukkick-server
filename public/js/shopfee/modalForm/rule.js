$.validator.addMethod(
    "limitTooLow",
    function(value, element, params) {
        var $priceElement = $(element).closest('form').find('input[name="'+params+'"]');
        return parseFloat($priceElement.val()) <= parseFloat(value);
    },
    'Лимит должен быть не меньше ставки'
);


function RuleModalForm(options)
{
    this.options = options;
    this.init();
}

RuleModalForm.prototype = {
        ruleForm:               null,

        init:                   function()
                                {
                                    var formOptions = {
                                                            template:this.options.template,
                                                            objectId:this.options.editObjectId,
                                                            objectType: 'rule',
                                                            top: '0%'
                                                      };
                                    this.ruleForm = new ModalForm(formOptions);

                                    this._bindAfterShow();
                                    this._bindAddForm();
                                },

        getForm:                function()
                                {
                                    return this.ruleForm;
                                },

        _bindAfterShow:         function()
                                {
                                    var thisObj = this;
                                    $(document).bind(ModalForm.prototype.EVENT_AFTER_SHOW,{},function(invokeObject,ruleModalForm){
                                        if(ruleModalForm.options.objectId == thisObj.options.editObjectId)
                                        {
                                            //Закрытие формы
                                            var thisModalForm = ruleModalForm;
                                            ruleModalForm.getCancelButton().click(function(){
                                                thisModalForm.hide();
                                            });

                                            var dateFrom = $('[name="date_from"]');
                                            if (dateFrom.val() == "") {
                                                dateFrom.val(moment().format('DD.MM.YYYY'));
                                            }

                                            var $form = $(ruleModalForm.getForm());
                                            thisObj._addValidators(thisObj.options.scanIds);
                                            thisObj._initCalendar();
                                            thisObj._initRecursiveCheckboxes();
                                            thisObj._initAlertEmptyTransmitters($form);

                                            //Удаление
                                            var $deleteButton = $('[data-delete-object-id='+thisObj.options.editObjectId+']');
                                            $deleteButton.unbind();
                                            $deleteButton.bind('click',function(){
                                                thisObj._deleteRule();
                                                return false;
                                            });
                                        }
                                    });
                                },

        _bindAddForm:           function()
                                {
                                    var thisObj = this;
                                    this.options.$showObject.click(function(){
                                        thisObj.getForm().show();
                                        return false;
                                    });
                                },

        _addValidators:         function(scanIds)
                                {
                                    var $ruleForm = $(this.ruleForm.getForm());
                                    var rules = {
                                            'rule_shop_id[]': {
                                                required: true
                                            },
                                            checkin: {
                                                required: true,
                                                number:true,
                                                max:50,
                                                min:0
                                            },
                                            limit_checkin: {
                                                required: true,
                                                number:true,
                                                limitTooLow:'checkin'
                                            },
                                            shoppage: {
                                                required: true,
                                                number:true,
                                                max:3,
                                                min:0
                                            },
                                            limit_shoppage: {
                                                required: true,
                                                number:true,
                                                limitTooLow:'shoppage'
                                            },
                                            buy: {
                                                required: true,
                                                number:true,
                                                max:100,
                                                min:0
                                            },
                                            limit_buy: {
                                                required: true,
                                                number:true,
                                                limitTooLow:'buy'
                                            },
                                            date_from: {
                                                required: true
                                            },
                                            date_to: {
                                                dateAfterToday:'[name="date_from"]'
                                            }
                                        };
                                        $.validator.addMethod("dateAfterToday", function(value, element, params) {
                                            var compareDate = moment($(params).val(), "DD.MM.YYYY").toDate();
                                            var thisDate = moment(value,"DD.MM.YYYY").toDate();
                                            thisDate.setHours(23);
                                            thisDate.setMinutes(59);
                                            thisDate.setSeconds(59);
                                            var today = new Date();
                                            var maximum = compareDate.getTime() < today.getTime() ? today : compareDate;
                                            return thisDate.getTime() - maximum.getTime() > 0;
                                        }, 'Дата не может быть меньше даты начала и текущей даты');
                                        var messages = {
                                                            'rule_shop_id[]': {
                                                                required: 'Выберите хотя бы один магазин!'
                                                            },
                                                            checkin:{
                                                                required: 'Не задано вознаграждение за посещение',
                                                                number: 'Вознаграждение за посещение должно быть целым числом',
                                                                max: 'Вознаграждение за посещение ограничено 50 фишками',
                                                                min: 'Вознаграждение за посещение не должно быть отрицательным'
                                                            },
                                                            limit_checkin:{
                                                                required: 'Не задан лимит за посещение',
                                                                number: 'Лимит за посещение должен быть целым числом',
                                                                limitTooLow: 'Лимит должен быть не меньше ставки'
                                                            },
                                                            shoppage:{
                                                                required: 'Не задано вознаграждение за показ рекламы',
                                                                number: 'Вознаграждение за показ рекламы должно быть целым числом',
                                                                max: 'Вознаграждение за показ рекламы ограничено 3 фишками',
                                                                min: 'Вознаграждение за показ рекламы не должно быть отрицательным'

                                                            },
                                                            limit_shoppage:{
                                                                required: 'Не задан лимит за показ рекламы',
                                                                number: 'Лимит за показ рекламы должен быть целым числом',
                                                                limitTooLow: 'Лимит должен быть не меньше ставки'
                                                            },
                                                            buy:{
                                                                required: 'Не задано вознаграждение за покупки',
                                                                number: 'Вознаграждение за покупки должно быть целым числом',
                                                                max: 'Вознаграждение за покупки ограничено 100 фишками',
                                                                min: 'Вознаграждение за покупки не должно быть отрицательным'
                                                            },
                                                            limit_buy:{
                                                                required: 'Не задан лимит за покупки',
                                                                number: 'Лимит за покупки должен быть целым числом',
                                                                limitTooLow: 'Лимит должен быть не меньше ставки'
                                                            },
                                                            date_from: {
                                                                required: 'Выберите дату начала!'
                                                            }
                                                       };

                                        for(var i=0;i<scanIds.length;i++)
                                        {
                                            var scanElementPostfix = 'scan_'+scanIds[i];
                                            rules[scanElementPostfix] = {
                                                                                required: true,
                                                                                number:true,
                                                                                max:150,
                                                                                min:0
                                                                             };
                                            rules['limit_'+scanElementPostfix] = {
                                                                                        required: true,
                                                                                        number:true,
                                                                                        limitTooLow:scanElementPostfix
                                                                                   };

                                            messages[scanElementPostfix] = {
                                                                                required: 'Не задано вознаграждение за сканы',
                                                                                number: 'Вознаграждение за сканы должно быть целым числом',
                                                                                max: 'Вознаграждение за сканы ограничено 150 фишками',
                                                                                min: 'Вознаграждение за сканы не должно быть отрицательным'
                                                                             };
                                            messages['limit_'+scanElementPostfix] = {
                                                                                        required: 'Не задан лимит за сканы',
                                                                                        number: 'Лимит за сканы должен быть целым числом',
                                                                                        limitTooLow: 'Лимит должен быть не меньше ставки'
                                                                                    };
                                        }

                                        var $thisRuleForm = $ruleForm;
                                        $ruleForm.validate({
                                            onkeyup: false,
                                            errorClass: "error",
                                            errorPlacement: function(errorLabel, element) {
                                                var $errorContainer = $('[data-error-rule-id='+$thisRuleForm.attr('data-object-id')+']');
                                                var $element = $(element);

                                                var elementName = $element.attr('name');
                                                elementName = elementName.replace('[]','');
                                                var $p = $('p.error_for_'+elementName,$errorContainer);
                                                if($p.length == 0) {
                                                    $p = $('<p class="error_for_'+elementName+'" />').appendTo($errorContainer);
                                                }

                                                if(errorLabel.text().length > 0) {
                                                    $p.html(errorLabel);
                                                } else {
                                                    $p.remove();
                                                }

                                                if($errorContainer.find('label').length > 0) {
                                                    $errorContainer.show();
                                                } else {
                                                    $errorContainer.hide();
                                                }
                                            },
                                            rules:rules,
                                            messages:messages
                                        });
                                },

    _initCalendar:              function()
                                {
                                    //Календарик
                                    $.datepicker.setDefaults($.datepicker.regional['ru']);
                                    $.datepicker.setDefaults(
                                    {
                                        showOn: 'both',
                                        buttonImageOnly: true,
                                        buttonImage: '/img/admin/calendar.gif',
                                        buttonText: 'Calendar',
                                        minDate: '-100Y',
                                        maxDate: '+10Y',
                                        firstDay: 1
                                    } );
                                    $('.date_element').each(function(){
                                        $(this).datepicker({dateFormat: 'dd.mm.yy'});
                                    });
                                },

    _initRecursiveCheckboxes:   function()
                                {
                                    var thisObj = this;
                                    $('[data-checkbox-recursive]').each(function(){
                                        if (jQuery.browser.msie) {
                                            $(this).click(function(){thisObj._setCheckboxRecursive($(this))});
                                        } else {
                                            $(this).change(function(){thisObj._setCheckboxRecursive($(this))});
                                        }
                                    });
                                },

    _setCheckboxRecursive:      function($checkbox)
                                {
                                    $('input[type=checkbox]',$checkbox.parents('[data-checkbox-recursive-container]').first()).each(function(){
                                        var $this = $(this);
                                        if($this.get(0) != $checkbox.get(0))
                                        {
                                            if($checkbox.is(':checked'))
                                                $this.attr('checked','checked');
                                            else
                                                $this.removeAttr('checked');
                                        }
                                    });
                                },

    _checkAndAlertEmptyTransmitters:    function()
                                        {
                                            var hasEmptyTransmitters = false;
                                            $('[data-has-transmitter]').each(function(){
                                                var $this = $(this);
                                                if($this.is(':checked') && $this.data('has-transmitter') == '0')
                                                {
                                                    hasEmptyTransmitters = true;
                                                }
                                            });

                                            if(hasEmptyTransmitters)
                                            {
                                                alert("Внимание! В некоторых магазинах данной кампании не установлено оборудования для чекинов. \r\nДля установки оборудования свяжитесь с нами по телефону:"+registry.supportPhone+". \r\nПосле установки оборудования эта возможность автоматически появится в этих магазинах.");
                                            }
                                        },

    _initAlertEmptyTransmitters:        function($ruleForm)
                                        {
                                            var thisObj = this;
                                            $ruleForm.submit(function(){
                                                thisObj._checkAndAlertEmptyTransmitters();
                                            });
                                        },

    _deleteRule:                        function()
                                        {
                                            if(confirm('Действительно удалить кампанию со сроками '+this.options.dateFrom+'—'+this.options.dateTo+'?')){
                                                location.href = '/edit/control/rule-delete/id/'+this.options.editObjectId+'/';
                                            }
                                        }
};