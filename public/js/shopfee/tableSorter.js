function TableSorter()
{
    var thisObj = this;
    $(TableSorter.prototype.TABLE_SELECTOR).each(function(){
        thisObj.init($(this));
    });
    return this;
}

TableSorter.prototype = {
    
    EVENT_SORT_CHANGED:     'event_sort_changed',
    
    SORT_ASC:               'ASC',
    SORT_DESC:              'DESC',
    
    TABLE_SELECTOR:         'table[data-table-sort]',
    SORT_ASC_HTML:          '<span class="sort_up" data-sort-asc="1" style="display:none;">↑</span>',
    SORT_DESC_HTML:         '<span class="sort_down" data-sort-desc="1" style="display:none;">↓</span>',
    
    TH_SORT_FIELD:          'sort-field',
    //TH_SORT_DIR:            'sort-direction',
    
    
    init:                   function($table)
                            {
                                this.$table = $table;
                                
                                var thisObj = this;
                                this.$table.find('th').each(function(){
                                    thisObj.initTh($(this));
                                });
                            },
                            
    initTh:                 function($th)
                            {
                                var thisObj = this;
                                if(this._getThSortField($th))
                                {
                                    $th.css('cursor','pointer');
                                    $th.append(TableSorter.prototype.SORT_ASC_HTML+TableSorter.prototype.SORT_DESC_HTML);
                                    $th.click(function(){
                                        thisObj.clickTh($(this));
                                    });
                                }
                            },
                            
    clickTh:                function($th)
                            {
                                var thisSortField = this._getThSortField($th);
                                if(thisSortField)
                                {
                                    var direction;
                                    //Если сортировка была по другому полю - сортируем по убыванию
                                    if(thisSortField != this.getCurrentSortField())
                                    {
                                        direction = TableSorter.prototype.SORT_DESC;
                                    }
                                    else
                                    {
                                        //Иначе - сортируем в другую сторону
                                        direction = this.getCurrentSortDir() == TableSorter.prototype.SORT_DESC ? TableSorter.prototype.SORT_ASC : TableSorter.prototype.SORT_DESC;
                                    }
                                    this._setCurrentSortFieldForTh(thisSortField,$th);
                                    this._setCurrentSortDirForTh(direction,$th);
                                    /*this.$table*/$(document).trigger(TableSorter.prototype.EVENT_SORT_CHANGED,[thisSortField,direction]);
                                }
                            },
                            
    getTable:               function()
                            {
                                return this.$th;
                            },
                            
    setCurrentSortField:    function(field)
                            {
                                this._setCurrentSortFieldForTh(field,this._getThForField(field));
                            },
                            
    setCurrentSortDir:      function(direction)
                            {
                                this._setCurrentSortDirForTh(direction,this._getThForField(this.getCurrentSortField()));
                            },
                        
    _setCurrentSortFieldForTh:      function(field,$th)
                                    {
                                        this.$table.data('sort-field',field);
                                    },
                            
    _setCurrentSortDirForTh:        function(direction,$th)
                                    {
                                        this.$table.data('sort-direction',direction);
                                        var $activeSpan = $th.find('span[data-sort-'+(direction == TableSorter.prototype.SORT_DESC ? 'desc' : 'asc')+']');
                                        var $inactiveSpan = $th.find('span[data-sort-'+(direction == TableSorter.prototype.SORT_DESC ? 'asc' : 'desc')+']');
                                        $activeSpan.css('display','inline');
                                        $inactiveSpan.hide();
                                    },
                                    
    _getThForField:                 function(field)
                                    {
                                        return this.$table.find('th[data-'+TableSorter.prototype.TH_SORT_FIELD+'='+field+']');
                                    },
                                    
    _getThSortField:                function($th)
                                    {
                                        return $th.data(TableSorter.prototype.TH_SORT_FIELD);
                                    },
                            
    getCurrentSortField:    function(field)
                            {
                                return this.$table.data('sort-field');
                            },
                            
    getCurrentSortDir:      function(direction)
                            {
                                return this.$table.data('sort-direction');
                            }
};