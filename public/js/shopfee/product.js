function PriceOrDiscount()
{
    $('#SetPrice,#SetDiscount').each(function(){
        $(this).change(function(){
            var $radios = $('input:radio[name='+$(this).attr('name')+']');
            for(var i = 0; i < $radios.length;i++)
            {
                var $thisRadio = $($radios[i]);
                var $container = $thisRadio.parents('.price_or_discount_container').first();
                var $input = $('input[type=text]',$container);
                if($thisRadio.is(':checked'))
                {
                    $container.removeClass('unactive');
                    $input.attr('disabled',null);
                }
                else
                {
                    $container.addClass('unactive');
                    $input.attr('disabled','disabled');
                }
            }
        });
    });
}