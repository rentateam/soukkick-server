function ShopSelect(options)
{
    this.options = options;
    this.init();
}

ShopSelect.prototype = {
        
        init:           function()
                        {
                            this.hidden = true;
            
                            var $container = $('#'+this.options.id);
                            this.$currentChoice = $(this.options.currentChoiceSelector,$container);
                            
                            var thisObj = this;
                            this.$currentChoice.click(function(){
                                thisObj.toggle();
                            });
                            
                            this.$ul = $(this.options.ulSelector,$container);
                            
                            this.$liAll = $(this.options.liAllSelector,$container);
                            $('label',this.$liAll).click(function(){
                                thisObj.toggleAll();
                            });
                            this.$liAll.click(function(){
                                thisObj.close();
                            });
                            
                            $('input[type=checkbox]',this.$ul).each(function(){
                                if (jQuery.browser.msie) {
                                    $(this).click(function(){
                                        thisObj.checkValueChanged($(this));
                                    });
                                } else {
                                    $(this).change(function(){
                                        thisObj.checkValueChanged($(this));
                                    });
                                }
                            });
                            
                            this.fillCurrentChoice();
                        },
        
        toggle:         function()
                        {
                            this.hidden ? this.open() : this.close();
                        },
                        
        open:           function()
                        {
                            this.$ul.slideDown();
                            this.hidden = false;
                        },
                        
        close:          function()
                        {
                            this.$ul.slideUp();
                            this.hidden = true;
                            this.fillCurrentChoice();
                            
                        },
                        
        fillCurrentChoice:  function()
                            {
                                var selectedShopNumber = $(':checked',this.$ul).length - $(':checked',this.$liAll).length;
                                var totalShopNumber = $('input[type=checkbox]',this.$ul).length - 1;
                                var text = selectedShopNumber + ' ';
                                
                                var last = selectedShopNumber % 10;
                                if(last == 1) 
                                {
                                    if(selectedShopNumber == 11)
                                        text += 'магазинов';
                                    else
                                        text += 'магазин';
                                } 
                                else if(last == 2 || last == 3 || last == 4) 
                                {
                                    if(selectedShopNumber == 12 || selectedShopNumber == 13 || selectedShopNumber == 14)
                                        text += 'магазинов';
                                    else 
                                        text += 'магазина';
                                } 
                                else if(last >= 5 || last == 0) 
                                {
                                    text += 'магазинов';
                                }
                                text = text + ' из '+totalShopNumber;
                                this.$currentChoice.html(text);
                            },
                        
        toggleAll:          function()
                            {
                                var checkboxAllClass = this.options.checkboxAllClass;                
                                var checked = this.getCheckAllCheckbox().is(':checked');
                                var thisObj = this;
                                $('input[type=checkbox]',this.$ul).each(function(){
                                    var $this = $(this);
                                    var $li = $this.closest('li'); 
                                    if(!$this.hasClass(checkboxAllClass))
                                    {
                                        if(checked)
                                        {
                                            $this.attr('checked','checked');
                                            $li.addClass('active');
                                        }
                                        else
                                        {
                                            $this.removeAttr('checked');
                                            $li.removeClass('active');
                                        }
                                        
                                    }
                                });
                                this.close();
                            },
                        
        checkValueChanged:  function($checkbox)
                            {
                                var $li = $checkbox.closest('li'); 
                                if(!$checkbox.attr('checked'))
                                {
                                    this.getCheckAllCheckbox().removeAttr('checked');
                                    $li.removeClass('active');
                                }
                                else
                                {
                                    $li.addClass('active');
                                }
                            },
                            
        getCheckAllCheckbox:    function()
                                {
                                    return $('.'+this.options.checkboxAllClass,this.$liAll);
                                }
};

function initShopSelect(id)
{
    var options = {
                    id:id+'_shop_select',
                    currentChoiceSelector:'.current_choise',
                    ulSelector:'.all_options',
                    liAllSelector:'.all',
                    checkboxAllClass:'checkbox_all'
                  };
    new ShopSelect(options);
}