var curr_div_icon = undefined;

function datepickerSelect(dateText, inst)
{

  curr_div_icon.next().val(
      $.datepicker.formatDate('yy-mm-dd', $(this).datepicker('getDate'))
    );
  curr_div_icon.next().next().html(
      $.datepicker.formatDate('dd M yy', $(this).datepicker('getDate'))
    );
  $(this).datepicker('destroy');
}

function onLoadSummGraphData(data)
{
  var plotData = eval(data); //alert(data);
  /*var plotData = [[['2008-01-31', 2], ['2008-02-01', 5], ['2008-02-02', 7],
	       	['2008-02-03', 8],['2008-02-04', 7],['2008-02-05', 10]],
	     
		[['2008-01-31', 1], ['2008-02-01', 2], ['2008-02-02', 5],
		['2008-02-03', 3],['2008-02-04', 4],['2008-02-05', 1]]]; */
  
  
  $('#summary_graph').empty();
  
  $.jqplot("summary_graph",
	           plotData,
	           
		    {
		
	           	 title: "Сводка",
	           	 axes: {
	           	 	yaxis: {
						tickOptions : {
							formatString : '%d'
						},
						min: 0 
	           	 	 },
	           	 	 xaxis: {

						  renderer : $.jqplot.DateAxisRenderer,
						  //min:'2008-01-31',
						  //max:'2008-02-05',
						  tickOptions : {
								formatString : '%#d&nbsp;%b'
		           	 	 } 
	           	 	 }
	           	 },
	           	 
	           	 highlighter: {
	                 show: true,
	                 sizeAdjust: 7.5,
	                 tooltipAxes: 'y',
	                 useAxesFormatters: false,
	                 tooltipFormatString: '%d'
	             },
	             
	           	 series: [
	           	   { color:"#BCD2EE", label: 'Покупки'},
	           	   { color:'#90EE90', label: 'Продажи' }
	           	 ],
	           	 
	           	 seriesDefaults: {
	           	 	fill: true,
	           	 	fillAndStroke: true,
	           	 	showMarker: false,
	           	 	fillAlpha: 0.7
	             },
	             
	           	 legend: {
	           	 	show: true,
	           	 	location: "no",
	           	 	xoffset: 12,
	           	 	yoffset: 12 
	           	 }
					
	           }
			 
	          );
  
}

function onLoadScanGraphData(data)
{
	var scanDataPlain = eval(data);
  /*var scanDataPlain = eval("[[38,'2012-04-13',0],[38,'2012-04-12',0],[38,'2012-04-11',0],[38,'2012-04-10',0],"+
	"	[38,'2012-04-09',0],[38,'2012-04-08',0],[38,'2012-04-07',0],[38,'2012-04-06',0],"+
	"	[38,'2012-04-05',0],[38,'2012-04-04',0],[38,'2012-04-03',0],[38,'2012-04-02',0],"+
	"	[38,'2012-04-01',0],[38,'2012-03-31',0],[38,'2012-03-30',0],[38,'2012-03-29',0],"+
	"	[38,'2012-03-28',0],[38,'2012-03-27',0],[38,'2012-03-26',0],[38,'2012-03-25',0],"+
	"	[38,'2012-03-24',0],[38,'2012-03-23',0],[38,'2012-03-22',0],[38,'2012-03-21',0],"+
	"	[38,'2012-03-20',0],[38,'2012-03-19',0],[38,'2012-03-18',0],[38,'2012-03-17',0],"+
	"	[38,'2012-03-16',0],[38,'2012-03-15',0],[38,'2012-03-14',0],[38,'2012-03-13',0],"+
	"	[38,'2012-03-12',0],[38,'2012-03-11',0],[38,'2012-03-10',0],[38,'2012-03-09',0],"+
	"	[38,'2012-03-08',0],[38,'2012-03-07',0],[38,'2012-03-06',0],[38,'2012-03-05',0],"+
	"	[38,'2012-03-04',0],[38,'2012-03-03',0],[38,'2012-03-02',0],[38,'2012-03-01',0],"+
	"	[38,'2012-02-29',0],[38,'2012-02-28',0],[38,'2012-02-27',0],[38,'2012-02-26',0],"+
	"	[38,'2012-02-25',0],[38,'2012-02-24',0],[38,'2012-02-23',0],[38,'2012-02-22',0],"+
	"	[38,'2012-02-21',0],[38,'2012-02-20',0],[38,'2012-02-19',0],[38,'2012-02-18',0],"+
	"	[38,'2012-02-17',0],[38,'2012-02-16',0],[38,'2012-02-15',0],[38,'2012-02-14',0],"+
	"	[38,'2012-02-13',0],[40,'2012-04-13',0],[40,'2012-04-12',0],[40,'2012-04-11',0],"+
	"	[40,'2012-04-10',0],[40,'2012-04-09',0],[40,'2012-04-08',0],[40,'2012-04-07',0],"+
	"	[40,'2012-04-06',0],[40,'2012-04-05',0],[40,'2012-04-04',0],[40,'2012-04-03',0],"+
	"	[40,'2012-04-02',0],[40,'2012-04-01',0],[40,'2012-03-31',0],[40,'2012-03-30',0],"+
	"	[40,'2012-03-29',0],[40,'2012-03-28',0],[40,'2012-03-27',0],[40,'2012-03-26',0],"+
	"	[40,'2012-03-25',0],[40,'2012-03-24',0],[40,'2012-03-23',0],[40,'2012-03-22',0],"+
	"	[40,'2012-03-21',0],[40,'2012-03-20',0],[40,'2012-03-19',0],[40,'2012-03-18',0],"+
	"	[40,'2012-03-17',0],[40,'2012-03-16',0],[40,'2012-03-15',0],[40,'2012-03-14',0],"+
	"	[40,'2012-03-13',0],[40,'2012-03-12',0],[40,'2012-03-11',0],[40,'2012-03-10',0], "+
	"	[40,'2012-03-09',0],[40,'2012-03-08',0],[40,'2012-03-07',0],[40,'2012-03-06',0], "+
	"	[40,'2012-03-05',0],[40,'2012-03-04',0],[40,'2012-03-03',0],[40,'2012-03-02',0], "+
	"	[40,'2012-03-01',0],[40,'2012-02-29',0],[40,'2012-02-28',0],[40,'2012-02-27',0], "+
	"	[40,'2012-02-26',0],[40,'2012-02-25',0],[40,'2012-02-24',0],[40,'2012-02-23',0], "+
	"	[40,'2012-02-22',0],[40,'2012-02-21',0],[40,'2012-02-20',0],[40,'2012-02-19',0], "+
	"	[40,'2012-02-18',0],[40,'2012-02-17',0],[40,'2012-02-16',0],[40,'2012-02-15',0], "+
	"	[40,'2012-02-14',0],[40,'2012-02-13',0],[39,'2012-04-13',0],[39,'2012-04-12',0], "+
	"	[39,'2012-04-11',0],[39,'2012-04-10',0],[39,'2012-04-09',0],[39,'2012-04-08',0], "+
	"	[39,'2012-04-07',0],[39,'2012-04-06',0],[39,'2012-04-05',0],[39,'2012-04-04',0], "+
	"	[39,'2012-04-03',0],[39,'2012-04-02',0],[39,'2012-04-01',0],[39,'2012-03-31',0], "+
	"	[39,'2012-03-30',0],[39,'2012-03-29',0],[39,'2012-03-28',0],[39,'2012-03-27',0], "+
	"	[39,'2012-03-26',0],[39,'2012-03-25',0],[39,'2012-03-24',0],[39,'2012-03-23',0], "+
	"	[39,'2012-03-22',0],[39,'2012-03-21',0],[39,'2012-03-20',0],[39,'2012-03-19',0], "+
	"	[39,'2012-03-18',0],[39,'2012-03-17',0],[39,'2012-03-16',0],[39,'2012-03-15',0], "+
	"	[39,'2012-03-14',0],[39,'2012-03-13',0],[39,'2012-03-12',0],[39,'2012-03-11',0], "+
	"	[39,'2012-03-10',0],[39,'2012-03-09',0],[39,'2012-03-08',0],[39,'2012-03-07',0], "+
	"	[39,'2012-03-06',0],[39,'2012-03-05',0],[39,'2012-03-04',0],[39,'2012-03-03',0], "+
	"	[39,'2012-03-02',0],[39,'2012-03-01',0],[39,'2012-02-29',0],[39,'2012-02-28',0], "+
	"	[39,'2012-02-27',0],[39,'2012-02-26',0],[39,'2012-02-25',0],[39,'2012-02-24',0], "+
	"	[39,'2012-02-23',0],[39,'2012-02-22',0],[39,'2012-02-21',0],[39,'2012-02-20',0], "+
	"	[39,'2012-02-19',0],[39,'2012-02-18',0],[39,'2012-02-17',0],[39,'2012-02-16',0], "+
	"	[39,'2012-02-15',0],[39,'2012-02-14',0],[39,'2012-02-13',0]]");  */
	
		
	var scanData = new Object();
	
	$.each(scanDataPlain, function (index, value) {

		if(value.length == 3)
		{
			if(scanData[value[0]] !== undefined )
				scanData[value[0]].push(new Array(value[1], value[2]));
			else
				scanData[value[0]] = new Array();
		}			
	});
	
	var pattern = /(\d+).png/;
	
	$('div.scan_item').each(function (index) {
  	//Get item ID
		var matches = pattern.exec( $(this).find('img').attr('src') );
  	var div_graph_id = 'scan_graph_'+index; 
		$(this).children('div.scan_graph').attr('id', div_graph_id);
		
		$('#'+div_graph_id).empty();
		
		$.jqplot('scan_graph_'+index, [ scanData[matches[1]] ], 
		
				    {
		
	           	 title: "Скан_"+matches[1],
	           	 axes: {
	           	 	yaxis: {
									tickOptions : {
																	formatString : '%d'
															},
									min: 0 
	           	 	 },
	           	 	 xaxis: {

						  renderer : $.jqplot.DateAxisRenderer,
						  //min:'2008-01-31',
						  //max:'2008-02-05',
						  tickOptions : {
								formatString : '%#d&nbsp;%b'
		           	 	 } 
	           	 	 }
	           	 },
	           	 
	           	 highlighter: {
	                 show: true,
	                 sizeAdjust: 7.5,
	                 tooltipAxes: 'y',
	                 useAxesFormatters: false,
	                 tooltipFormatString: '%d'
	             },
	             
	           	 series: [
	           	   { color:"#BCD2EE"}
	           	 ],
	           	 
	           	 seriesDefaults: {
	           	 	fill: true,
	           	 	fillAndStroke: true,
	           	 	showMarker: false,
	           	 	fillAlpha: 0.7
	             },
	             
	           	 legend: {
	           	 	show: false,
	           	 	location: "no",
	           	 	xoffset: 12,
	           	 	yoffset: 12 
	           	 }
					
	           }

				);
	 
	}); 
	

}

function buildSummaryGraph()
{
    
    //$('#summary_graph').html('Загрузка..');
    var req_url = '/edit/stat-summary/?user_shop_id=1&date_from='+$('#date_from').val()+'&date_to='+$('#date_to').val();
		
    //onLoadSummGraphData();
    
		$.ajax({
      url:req_url, 
      type: 'GET',
      dataType: 'text',
      success: onLoadSummGraphData,
      error: function (xhr, errorText, errorThrown) { alert(errorThrown);}
    });

}

function buildScanGraphs()
{
  var req_url = '/edit/stat-scan/?user_shop_id=1&date_from='+$('#date_from').val()+'&date_to='+$('#date_to').val();
	//onLoadScanGraphData(data);
	
	$.ajax({
      url:req_url, 
      type: 'GET',
      dataType: 'text',
      success: onLoadScanGraphData,
      error: function (xhr, errorText, errorThrown) { alert(errorThrown);}
    });
}

$(document).ready(function () {
  
  //$('#calendar').datepicker();
  //$('#calendar').datepicker("hide");
    
  
  $('body').click(function () {
      $('#calendar').datepicker('destroy');
    
  });
  
  $('#calendar').click(function (evnt) { evnt.stopPropagation(); });
  
  
  $('.calendars div.icon').toggle(function (evnt) {
    
    curr_div_icon = $(this);

    $('#calendar').css('left', $(this).position().left);
    $('#calendar').datepicker('destroy');
    $('#calendar').datepicker( {onSelect: datepickerSelect});
    
    evnt.stopPropagation();
  
  },
   function (evnt)
   {
      $('#calendar').datepicker('destroy');
      evnt.stopPropagation();
  });
  
  
  $('.calendars input:submit').click(function (evnt) {
  	evnt.preventDefault();
		buildSummaryGraph();
  	buildScanGraphs();      
    
		
  });
  
  //build_graphs  by initial params
  buildSummaryGraph();
  buildScanGraphs();
    
});