function initCloseForm($container)
{
    $('.close_form',$container).each(function(){
        $(this).click(function(){
            var $form = $(this).parents('form').first();
            $form.slideUp();
            return false;
        });
    });
}

function initSubmitForm($container)
{
    $('.form_submit',$container).each(function(){
        $(this).click(function(){
            var $form = $(this).parents('form').first();
            $form.submit();
            return false;
        });
    });
}

function addValidators($form,formRules,formMessages,isNewForm)
{
    if(isNewForm == undefined)
        isNewForm = false;

    var formId = $form.attr('id');
    $form.validate({
        onkeyup: false,
        errorClass: "error",
        errorPlacement: function(error, element) {
            error.appendTo(element.next(".js-error-container"));
        },
    });
    var rulesObject;
    var $selection;
    for(var i in formRules[formId]) {
        rulesObject = formRules[formId][i];

        rulesObject.messages = formMessages[formId][i];

        if(i == 'file')
            rulesObject.required = isNewForm ? true : false;

        $selection = $(':input[name="'+i+'"]',$form);
        if($selection.length <= 1){
            $selection = [$selection];
        }


        for(var j = 0;j < $selection.length; j++)
        {
            var $currentElement = $($selection[j]);
            if($currentElement.length > 0)
            {
                $currentElement.parent().append("<div class='js-error-container'></div>");
                $currentElement.rules('add',rulesObject);
            }
        }
    }
}

function Editor(options)
{
    this.options = options;
    this.init(options);
}

Editor.prototype = {
        
        init:               function(options)
                            {
                                var theseOptions = options;
                                options.$link.click(function(){
                                    var $this = $(this);
                                    
                                    var formId = theseOptions.formId;
                                    var params = {id:theseOptions.entityId};
                                    if(theseOptions.shopId)
                                    {
                                        params.shop_id = theseOptions.shopId; 
                                    }
                                    if(typeof(theseOptions.extraParams) != 'undefined')
                                    {
                                        params = $.extend(params,theseOptions.extraParams);
                                    }
                                    $.getJSON(
                                                '/' + theseOptions.entityController + '/form/format/json/',
                                                params,
                                                function(data)
                                                {
                                                    $('#' + formId).replaceWith(data.html);
                                                    var $form = $('#' + formId); 
                                                    $form.slideDown();
                                                    
                                                    if(theseOptions.afterCallback)
                                                        theseOptions.afterCallback.call($form,data);
                                                }
                                            );
                                    return false;
                                });
                            }
}