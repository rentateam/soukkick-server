function ModalForm(options)
{
    this.options = options;
    this.init();
}

ModalForm.prototype = {
        options:                {},
        objectId:                 0,
        objectType:               '',
        
        EVENT_AFTER_SHOW:       'ModalForm_Event_After_Show',
        
        init:                   function()
                                {
                                    var progressBarCSS = {
                                            css:{
                                                  border: 'none',//'3px solid #00BFEF', 
                                                  backgroundColor: '#000',//'#007FCF',  
                                                  textAlign:  'left',
                                                  top: this.options.top ? this.options.top : '50%',
                                                  //opacity: '0.5',
                                                  cursor:'normal',
                                            },
                                            overlayCSS:{
                                                 backgroundColor:'#000',//'#AEDFEF', 
                                                 opacity:        '0.5' 
                                            }
                                        };
                                        var thisObj = this;
                                        this.blockOptions = { 
                                              message:this.options.template,
                                              fadeOut: 100,
                                              css: progressBarCSS.css,
                                              overlayCSS: progressBarCSS.overlayCSS,
                                              focusInput:false,
                                              onBlock:function(){
                                                  $(document).trigger(ModalForm.prototype.EVENT_AFTER_SHOW,[thisObj]);
                                              }
                                        };
                                        
                                        this.objectId = this.options.objectId;
                                        this.objectType = this.options.objectType;
                                },
                                
        show:                   function()
                                {
                                    jQuery.blockUI(this.blockOptions);
                                },
                                
        hide:                   function()
                                {
                                    jQuery.unblockUI();
                                },
                                
        getForm:                function()
                                {
                                    return $('form[data-object-id='+this.objectId+'][data-object-type='+this.objectType+']');
                                },
                                
        getCancelButton:        function()
                                {
                                    return $('[data-cancel-object-id]',this.getForm());
                                }
};