﻿//массив элементов блоков информации
var targetEls = [];
//высота для отрисовщика
//для закрываемого
var outHeight = 0;
//для открываемого
var inHeight = 0;
//префикс идентификаторов показываемых блоков
var targetElPrefix = null;
//массив элементов запускающих перерисовку
var triggerEls = [];
//индекс текущего элемента
var activeDivIndex = null;
//индекс старого элемента
var oldDivIndex = null;
//текущий элемент
var currentDiv = null;
//тот чем заменим текущий
var newDiv = null;
//массив высот и скоростей, порядок тот же что и улементов
var divHeights = [{ height: 72, speed: 100000 }, { height: 88, speed: 100000 }, { height: 72, speed: 100000 },
    { height: 137, speed: 100000 }, { height: 122, speed: 100000 }, { height: 55, speed: 100000 }, { height: 89, speed: 100000 }, 
    { height: 105, speed: 100000 }, { height: 88, speed: 100000 }, { height: 38, speed: 100000 },
    { height: 55, speed: 100000 }, { height: 55, speed: 100000 }, { height: 72, speed: 100000 },
    { height: 55, speed: 100000 }, { height: 72, speed: 100000 }, { height: 72, speed: 100000}];
//хэндлеры интервалов запускающих функции изменения высоты
//открыть
var inInterval = null;
//закрыть
var outInterval = null;

//изменяет размер на 1 пиксель, в случае если предельный размер достигнут отключает интервал
function raise(stopHeight) {
    if (outInterval > 0) {

        return;
    }
    inHeight = inHeight + 2;
    newDiv.style.height = inHeight + 'px';

    if (inHeight >= stopHeight) { clearInterval(inInterval); inInterval = null; currentDiv = newDiv; }
}

function hide() {
    if (currentDiv == null) { return; }
    outHeight = outHeight - 2;
    currentDiv.style.height = outHeight + 'px';

    if (outHeight <= 0.0) { clearInterval(outInterval); outInterval = null; currentDiv.style.display = 'none'; }
}

//убирает блок
function fadeOut(el) {

    //---------------------
    oldDivIndex = activeDivIndex;
    //---------------------
    outHeight = divHeights[targetEls.indexOf(el)].height;
    outInterval = setInterval("hide()", 1000 / divHeights[targetEls.indexOf(el)].speed);

}
//показывает блок
function fadeIn(el, id) {

    //if (curInterval>0) return;
    el.style.display = 'block';
    el.style.height = "0px";
    //---------------------
    activeDivIndex = id;
    newDiv = el;
    //---------------------
    inHeight = 0;
    inInterval = setInterval("raise(" + divHeights[targetEls.indexOf(el)].height + ")", 1000 / divHeights[targetEls.indexOf(el)].speed);

}

function changeAreaDiv(triggerEl //элемент ссылки
) {

    if (inInterval || outInterval) return; 
    var _tmp = triggerEl.id.split(triggerElPrefix);
    var id = parseInt(_tmp[1]);
    //если есть что убирать - убираем
    if (currentDiv != null && currentDiv.style.display != 'none') fadeOut(currentDiv);
    //
    if (currentDiv != null) {
        //если выбран новый блок
        if (targetElPrefix + id != currentDiv.id) fadeIn(document.getElementById(targetElPrefix + id), id);
        else {//если выбран тот же
            if (currentDiv.style.display == 'none') { fadeIn(document.getElementById(targetElPrefix + id), id); }
            //currentDiv = null;
            //activeDivIndex = null;
        }
        //если нечего убирать - показываем новый
    } else {
        fadeIn(document.getElementById(targetElPrefix + id), id);
    }
}

function init(targetPrefix,     //префикс целей
                triggerPrefix,  //префикс линков
                size,           //сколько индексов перебирать, должен быть больше или равен 1
                speed           //скорость изменения высоты, 1000 - 1 раз в секунду, 10000 - 10 раз в секунду
            ) {
    targetElPrefix = targetPrefix;
    triggerElPrefix = triggerPrefix;
    var i = 1;
    //обходим элементы
    for (i; i <= size; i++) {
        targetEls.push(document.getElementById(targetPrefix + i));
        var el = document.getElementById(triggerPrefix + i);
        //присваиваем обработчик клика
        el.onclick = function() { changeAreaDiv(this); };
        triggerEls.push(el);
        //если первый, то показываем его
        if (targetEls.length == 0) {
            targetEls[0].style.display = 'block';
            targetEls[0].style.height = divHeights[0].height;
            activeDivIndex = i;
            currentDiv = targetEls[0];
        }

    }

}