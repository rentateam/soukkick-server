function MobileBridge()
{
    this.init();
}

MobileBridge.prototype = {

        TYPE_IPHONE:                1,
        TYPE_ANDROID:               2,
        TYPE_PC:                    3,
        
        type:                       null,

        instance:                   null,

        adapter:                    null,

        data:                       {},

        getInstance:                function()
                                    {
                                        if(MobileBridge.prototype.instance == null)
                                            MobileBridge.prototype.instance = new MobileBridge();

                                        return MobileBridge.prototype.instance;
                                    },

        init:                       function()
                                    {
                                        this.$document = $(document);
                                        
                                        if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i))
                                            this.setType(MobileBridge.prototype.TYPE_IPHONE);
                                        else if( navigator.userAgent.match(/Android/i))
                                            this.setType(MobileBridge.prototype.TYPE_ANDROID);
                                        else
                                            this.setType(MobileBridge.prototype.TYPE_PC);
                                    },

        setType:                    function(type)
                                    {
                                        this.type = type;
                                        switch(type)
                                        {
                                            case MobileBridge.prototype.TYPE_IPHONE:
                                                this.adapter = new IphoneMobileAdapter();
                                                break;
                                            case MobileBridge.prototype.TYPE_ANDROID:
                                                this.adapter = new AndroidMobileAdapter();
                                                break;
                                            case MobileBridge.prototype.TYPE_PC:
                                                this.adapter = new PCAdapter();
                                                break;
                                        }
                                    },
                                    
        getType:                    function()
                                    {
                                        return this.type;
                                    },

        triggerEventFromMobile:     function(eventName,params)
                                    {
                                        this.$document.trigger(eventName,params);
                                    },

        triggerEventToMobile:       function(eventName,params)
                                    {
                                        this.adapter.triggerEventToMobile(eventName,params);
                                    },

        getData:                    function(namespace,item)
                                    {
                                        return data[namespace][item];
                                    },

        setData:                    function(namespace,item,value)
                                    {
                                        if(!data[namespace])
                                            data[namespace] = {};
                                        
                                        data[namespace][item] = value;
                                    }
};

function MobileAdapter()
{
    this.init();
}

MobileAdapter.prototype = {

        init:                   function()
                                {
                                    
                                },

        triggerEventToMobile:   function(eventName,params)
                                {
                        
                                }
};

function IphoneMobileAdapter()
{
    this.init();
}

$.extend(IphoneMobileAdapter.prototype, MobileAdapter.prototype, {
    
    triggerEventToMobile:       function(eventName,params)
                                {
                                    var paramString = '';
                                    var paramValue;
                                    for(var i in params)
                                    {
                                        paramValue = params[i];
                                        if(paramValue instanceof Object)
                                        {
                                            paramValue = JSON.stringify(paramValue, null, 2);
                                        }
                                        paramString += i+'='+encodeURIComponent(paramValue)+'&';
                                    }
                                    window.location ='defeni-hybrid://defeni.com/'+eventName+'?'+paramString;
                                }
});

function AndroidMobileAdapter()
{
    this.init();
}

$.extend(AndroidMobileAdapter.prototype, MobileAdapter.prototype, {
    
    triggerEventToMobile:       function(eventName,params)
                                {
                                    androidObj.Trigger(eventName, JSON.stringify(params, null, 2));
                                }
});

function PCAdapter()
{
    this.init();
}

$.extend(PCAdapter.prototype, MobileAdapter.prototype, {
    
    triggerEventToMobile:       function(eventName,params)
                                {
                                    console.log('event '+eventName+', params:');
                                    console.log(params);
                                }
});

function makeNativeHighlight($element,highlightClass)
{
    $element.bind('vmousedown',(function(){
        $(this).addClass(highlightClass);
    }));
    $element.bind('vmouseup',(function(){
        $(this).removeClass(highlightClass);
    }));
}