/**
 * flowplayer.controls 3.0.2. Flowplayer JavaScript plugin.
 * 
 * This file is part of Flowplayer, http://flowplayer.org
 *
 * Author: Tero Piirainen, <info@flowplayer.org>
 * Copyright (c) 2008 Flowplayer Ltd
 *
 * Dual licensed under MIT and GPL 2+ licenses
 * SEE: http://www.opensource.org/licenses
 * 
 * Date: 2008-11-25 11:29:03 -0500 (Tue, 25 Nov 2008)
 * Revision: 1424 
 */ 
$f.addPlugin("controls", function(wrap, options) {
		
    //{{{ private functions
    function fixE(e) {
        if (typeof e == 'undefined') {
            e = window.event;
        }
        if (typeof e.layerX == 'undefined') {
            e.layerX = e.offsetX;
        }
        if (typeof e.layerY == 'undefined') {
            e.layerY = e.offsetY;
        }
        return e;
    }
	
    function w(e) {
        return e.clientWidth;
    }
	
    function offset(e) {
        return e.offsetLeft;
    }
	
    /* a generic dragger utility for hoirzontal dragging */
    function Draggable(o, min, max, offset) {
		
        var dragging = false;
		
        function foo() { }
		
        o.onDragStart = o.onDragStart || foo;
        o.onDragEnd   = o.onDragEnd   || foo;
        o.onDrag      = o.onDrag      || foo;
		
        function move(x) {
            // must be withing [min, max]
            if (x > max) {
                return false;
            }
            if (x < min) {
                return false;
            }
            o.style.left = x + "px";
            return true;
        }
		
        function end() {
            document.onmousemove = null;
            document.onmouseup   = null;
            o.onDragEnd(parseInt(o.style.left, 10));
            dragging = false;
        }

        function drag(e) {
            e = fixE(e);
            var x = e.clientX - offset;
            if (move(x)) {
                dragging = true;
                o.onDrag(x);
            }
            return false;
        }
		
        o.onmousedown = function(e)  {
            e = fixE(e);
            o.onDragStart(parseInt(o.style.left, 10));
            document.onmousemove	= drag;
            document.onmouseup		= end;
            return false;
        };
		
        this.dragTo = function(x) {
            if (move(x)) {
                o.onDragStart(x);
                o.onDragEnd(x);
            }
        };
		
        this.setMax = function(val) {
            max = val;
        };
		
        this.isDragging = function() {
            return dragging;
        };

        return this;
    }

    function extend(to, from) {
        if (from) {
            for (key in from) {
                if (key) {
                    to[key] = from[key];
                }
            }
        }
    }

    function byId(id) {
        var els = wrap.getElementsByTagName("*");
        var re = new RegExp("(^|\\s)" + id + "(\\s|$)");
        for (var i = 0; i < els.length; i++) {
            if (re.test(els[i].id)) {
                return els[i];
            }
        }
    }

    function byClass(name) {
        var els = wrap.getElementsByTagName("*");
        var re = new RegExp("(^|\\s)" + name + "(\\s|$)");
        for (var i = 0; i < els.length; i++) {
            if (re.test(els[i].className)) {
                return els[i];
            }
        }
    }
	
    // prefix integer with zero when nessessary
    function pad(val) {
        val = parseInt(val, 10);
        return val >= 10 ? val : "0" + val;
    }
	
    // display seconds in hh:mm:ss format
    function toTime(sec) {
		
        var h = Math.floor(sec / 3600);
        var min = Math.floor(sec / 60);
        sec = sec - (min * 60);
		
        if (h >= 1) {
            min -= h * 60;
            return pad(h) + ":" + pad(min) + ":" + pad(sec);
        }
		
        return pad(min) + ":" + pad(sec);
    }
	
    function getTime(time, duration) {
        return "<span>" + toTime(time) + "</span>";
    }
	
    //}}}
	
	
    var self = this;
	
    var opts = {
        playHeadClass: 'playhead',
        trackClass: 'track',
        playClass: 'play',
        pauseClass: 'pause',
        bufferClass: 'buffer',
        progressClass: 'progress',
        cliptrackClass: 'cliptrack',
		
        timeClass: 'timer',
        duration: 0,
		
        templatePre: '<a class="play">play</a><div class="track">'+
        '<div class="buffer"></div>' +
        '<div class="progress"></div>' +
        '<div class="playhead"></div>',

        templatePost:'</div></div><div class="timer"></div>'
    };
	
    extend(opts, options);
	
    if (typeof wrap == 'string') {
        wrap = document.getElementById(wrap);
    }
	
    if (!wrap) {
        return;
    }
        
    var playlist = self.getPlaylist();
    //console.log(playlist);
    
    var trackWidth = 392;
    var trBeg = 32;
    var trackData = [];

    var durationSummary = 0;
    var durationLeft = 0;
    var widthLeft = 0;

    // inner HTML
    if (!wrap.innerHTML.replace(/\s/g, '')) {
        wrap.innerHTML = opts.templatePre;
        wrap.innerHTML += opts.templatePost;
    }
	
    // get elements
    var ball = byClass(opts.playHeadClass);
    var bufferBar = byClass(opts.bufferClass);
    var progressBar = byClass(opts.progressClass);
    var track = byClass(opts.trackClass);
    var time = byClass(opts.timeClass);
	
    // initial time

    time.innerHTML = getTime(0, opts.duration);
	
    // get dimensions
    trackWidth = w(track);
    var ballWidth = w(ball);
    var offs = $('#player').offset();
    offs = parseInt(offs.left);
    var head = new Draggable(ball, 0, 0, offs + offset(wrap) + offset(track) + (ballWidth / 2));
    head.setMax(trackWidth);

    // track click moves playHead
    track.onclick = function(e) {
        e = fixE(e);
        if (e.target == ball) {
            return false;
        }
        head.dragTo(e.layerX - ballWidth / 2);
    //console.log(e.layerX);
    };

    // play/pause button
    var play = byClass(opts.playClass);

    play.onclick = function() {
        if (self.isLoaded()) {
            self.toggle();
        } else {
            self.play();
        }
    };
	
    // setup timer
    var timer = null;
    var ajaxRequest = null;
	
    function getMax(len, total) {
        var x = parseInt(Math.min(len / total * trackWidth, trackWidth - ballWidth / 2), 10);
        return isNaN(x) ? 0 : x;
    }
    
    self.onBeforeBegin(function() {
        ajaxRequest.abort();
        ajaxRequest = null;
    });

    var timerCount = 0;
	
    self.onStart(function(clip) {
        var duration = clip.duration || 0;

        $('.active').removeClass('active');
        var prevIndex = self.getClip().index - 1;
        var nextIndex = self.getClip().index + 1;

        if(self.getClip().index == 0 && self.getPlaylist().length > 1){
            $('.arrow-r').show();
            $('.arrow-l').hide();
            $('.arrow-r').click(function(){
                self.play(nextIndex);
            });
        } else if (self.getClip().index > 0 && self.getClip().index < (self.getPlaylist().length-1)){
            $('.arrow-r').show();
            $('.arrow-l').show();
            $('.arrow-r').click(function(){
                self.play(nextIndex);
            });
            $('.arrow-l').click(function(){
                self.play(prevIndex);
            });
        } else if (self.getClip().index == (self.getPlaylist().length-1)) {
            $('.arrow-r').hide();
            $('.arrow-l').show();
            $('.arrow-l').click(function(){
                self.play(prevIndex);
            });
        }
        
        // clear previous timer
        clearInterval(timer);
        timerCount = 0;
        // begin timer
        timer = setInterval(function()  {
            var status = self.getStatus();

            // time display
            if (status.time)  {
                time.innerHTML = getTime(status.time, duration);
            }
			
            if (status.time === undefined) {
                // clearInterval(timer);
                return;
            }
			
            // buffer width
            var x = getMax(status.bufferEnd, duration);
            bufferBar.style.width = x + "px";
            if(status.bufferEnd == duration 
                && (self.getClip().index+1) < self.getPlaylist().length
                && !ajaxRequest){
                ajaxRequest = $.ajax({
                    type: "GET",
                    url: self.getClip(self.getClip().index+1).url
                });
            }
         
            // progress width
            if (!self.isPaused() && !head.isDragging()) {
                x = getMax(status.time, duration);

                progressBar.style.width = (x) + "px";
                ball.style.left = (x - ballWidth / 2) + "px";
            }
        }, 20);
    });
	
    self.onBegin(function() {
        play.className = opts.pauseClass;
    });

	
    // pause / resume states
    self.onPause(function() {
        play.className = opts.playClass;
    });

    self.onResume(function() {
        play.className = opts.pauseClass;
    });
	
    // clear timer when clip ends
    self.onFinish(function(clip) {
        clearInterval(timer); 
    });
	
    self.onUnload(function() {
        time.innerHTML = getTime(0, duration);
    });
	
    ball.onDragEnd = function(x) {
        var to = parseInt(x / trackWidth  * 100, 10) + "%";
        progressBar.style.width = x + "px";
        if (self.isLoaded()) {
            self.seek(to);
        }
        if (beforeDragState == 1) {
            self.play();
        }
    };

    ball.onDrag = function(x) {
        progressBar.style.width = x + "px";
    };


    var beforeDragState = 1;
    ball.onDragStart = function(x) {
        if(self.isPlaying()){
            self.pause();
            beforeDragState = 1;
        } else {
            beforeDragState = 0;
        }
    };
	
    // return player instance to enable plugin chaining
    return self;
	
});
		
			
			

