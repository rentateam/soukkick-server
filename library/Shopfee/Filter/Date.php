<?php
class Shopfee_Filter_Date implements Zend_Filter_Interface
{
    public function filter($value)
    {
        if(preg_match('/^(\d{2})\.(\d{4})$/',$value,$matches))
            return $matches[2].'-'.$matches[1].'-01';
        else
            return $value;
    }
}