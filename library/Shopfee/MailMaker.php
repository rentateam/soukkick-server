<?php 
class Shopfee_MailMaker
{
    /*
     * @var Shopfee_MailMaker
     */
    protected static $instance;
    
    protected static $_mailScriptsPath;
    
    /**
     * @return Shopfee_MailMaker
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    protected function __construct()
    {
        self::$_mailScriptsPath = $_SERVER['DOCUMENT_ROOT'].Application::MAIL_SCRIPT_PATH;
    }
    
    /**
     * returns mail body with processed params
     *
     * @param string $lang
     * @param string $mailScript
     * @param array $params params to set
     */
    public function getMailBody($mailScript,array $params)
    {
        $view = new Zend_View();
        $view->setScriptPath(realpath(self::$_mailScriptsPath));
        $view->assign($params);
        return $view->render($mailScript);
    }
}
?>