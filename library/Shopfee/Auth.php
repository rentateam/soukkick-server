<?php
class Shopfee_Auth extends Zend_Auth
{
    /**
     * Auth 
     * 
     * @var array of Shopfee_Auth
     */
    protected static $_instance = array();
    
    /**
     * Return auth 
     * 
     * @return Zend_Auth
     */
    static public function get($name) {
        if( isset(self::$_instance[$name])) {
            return self::$_instance[$name];
        }
        self::$_instance[$name] = new self();
        self::$_instance[$name]->setStorage(new Shopfee_Auth_Storage($name));
        
        return self::$_instance[$name];
    }
    
    public static function ins()
    {
        return self::$_instance;
    }
}
?>