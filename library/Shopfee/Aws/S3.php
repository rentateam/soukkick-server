<?php
require_once 'AmazonSDK-1.5.14/sdk.class.php';
class Shopfee_Aws_S3
{
    protected $_fileFolder;
    protected $_type;
    
    
    public function __construct($type)
    {
        $this->_fileFolder = self::getFileFolder();
        $this->_type = $type;
    }
    
    /**
     * @param string $sourceFileName
     * @param string $destinationFilePath
     * @return boolean
     */
    public function get($sourceFileName,$destinationFilePath)
    {
        $s3 = new AmazonS3();
        $bucket = $this->getBucket();
        $sourceFullPath = self::cleanS3Url($this->_fileFolder.$sourceFileName);
        $fileDownloadResponse = $s3->get_object($bucket, $sourceFullPath, array(
            'fileDownload' => $destinationFilePath
        ));
        if ($fileDownloadResponse->isOK()){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @param string $sourceFileName
     * @param string|null $destinationFileName
     * @return string
     * @throws Shopfee_Aws_Exception
     */
    public function put($sourceFileName, $destinationFileName = null)
    {
        if(!file_exists($sourceFileName)){
            throw new Shopfee_Aws_Exception('File '.$sourceFileName.' does not exist!');
        }
        
        $s3 = new AmazonS3();
        $bucket = $this->getBucket();
        
        if($destinationFileName === null){
            $destinationFileName = basename($sourceFileName);
        }
        $destinationFullPath = self::cleanS3Url($this->_fileFolder.$destinationFileName);
        $s3->batch()->create_object($bucket, $destinationFullPath, array(
            'acl' => AmazonS3::ACL_PUBLIC,
            'fileUpload' => $sourceFileName
        ));
        
        $fileUploadResponse = $s3->batch()->send();

        if ($fileUploadResponse->areOK()){
            $s3Url = $s3->get_object_url($bucket, $destinationFullPath);
            return $this->replaceS3WithCloudFront($s3Url);
        } else {
            throw new Shopfee_Aws_Exception('File '.$sourceFileName.' was unable to be put to '.$bucket);
        }
    }
    
    /**
     * @param string $sourceFileName
     * @param string|null $destinationFileName
     * @return string
     * @throws Shopfee_Aws_Exception
     */
    public function move($sourceFileName, $destinationFileName = null)
    {
        $s3Url = $this->put($sourceFileName,$destinationFileName);
        unlink($sourceFileName);
        return $s3Url;
    }
    
    public function delete($fileName)
    {
        $s3 = new AmazonS3();
        $bucket = $this->getBucket();
        $fileDownloadResponse = $s3->delete_object($bucket, self::cleanS3Url($fileName));
        if ($fileDownloadResponse->isOK()){
            return true;
        } else {
            return false;
        };
        
    }

    public function fileExists($fileName)
    {
        $s3 = new AmazonS3();
        $bucket = $this->getBucket();
        $metadata = $s3->get_object_metadata($bucket, self::cleanS3Url($this->_fileFolder.$fileName));
        return $metadata ? true : false;
    }
    
    protected function replaceS3WithCloudFront($s3Url)
    {
        $cloudFrontHostName = $this->getHostName();
        return str_replace($this->getBucket().'.s3.amazonaws.com',$cloudFrontHostName,$s3Url);
    }
    
    public static function getFileFolder()
    {
        return Application::getAmazonS3Folder().'/';
    }
    
    protected function getBucket()
    {
        switch($this->_type){
            case Application::STATIC_IMAGE:
                return 'defeni-shopfee.ru';/*'shopfee-files';*/
                break;
            default:
                throw new Shopfee_Aws_Exception('Undefined type '.$this->_type);
        }
    }
    
    protected function getHostName()
    {
        return self::getHostNameByType($this->_type);
    }
    
    public static function getHostNameByType($type)
    {
        switch($type){
            case Application::STATIC_IMAGE:
                return 'd2rhhgptgeyohx.cloudfront.net';
                break;
            default:
                throw new Shopfee_Aws_Exception('Undefined type '.$this->_type);
        }
    }
    
    protected static function cleanS3Url($s3Url)
    {
        return str_replace('//','/',$s3Url);
    }
}