<?php
require_once "sphinxapi.php";
require_once "Letsee/Search/Exception.php";

abstract class Letsee_Search_Sphinx_Abstract extends SphinxClient 
{
    const MAX_RESULTS = 1000;
    const BODY_SIZE = 400;
    const SORT_RELEVANCE = SPH_SORT_RELEVANCE;
    const DIRECTION_ASC = SPH_SORT_ATTR_ASC;
    const DIRECTION_DESC = SPH_SORT_ATTR_DESC;
    
    /**
     * Максимальная дата, которая подставляется как верхняя граница.
     * strtotime('2038-01-01')
     * @var integer
     */
    const MAX_DATE = 2145906000;
    const MIN_DATE = 0;         

    public function __construct($host = null, $port = null, $matchMode = SPH_MATCH_ANY, $rankingMode = SPH_RANK_PROXIMITY_BM25)
    {
        parent::SphinxClient();
        if($host === null)
            $host = Application::getSphinxHost();
        if($port === null)
            $port = (int)Application::getSphinxPort();
        $this->SetServer($host, $port);
        $this->SetConnectTimeout(Application::getSphinxTimeout());
        $this->adjustFieldWeights();
        $this->SetMatchMode($matchMode);
        $this->SetRankingMode($rankingMode);
        $this->SetArrayResult(true);
        
        //needs to convert multibyte strings properly
        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");
    }
    
    protected function getSphinxResults($text, &$totalFound = null, $pageNumber = 1, $perPage = self::MAX_RESULTS, $index = null)
    { 
        $pageNumber--;    
        $this->SetLimits($pageNumber*$perPage, $perPage, self::MAX_RESULTS);
            
        if($index === null)
            $index = $this->getIndexName();
        $res = $this->Query($text,$index);
        
        if($res === false)
            throw new Letsee_Search_Exception($this->GetLastError());
           
        $results = array();
        //strange sphinx logic. If there are no matches, then matches is not set
        if(isset($res['matches']) && ($res['matches'] !== null))
            $results = $res['matches'];
            
        if($totalFound !== null)
            $totalFound = $res['total_found'];
        
        //do not return statistics so far
        return $results;     
    }
    
    /**
     * Highlights text in body
     * 
     * @param $q string text to highlight   
     * @param $body array strings to search in
     * @param $index integer index to search in
     * 
     * @return string highlighted string
     */         
    protected function getTextPassage($q,array $body,$index)
    {
        $options = array
        (
                'before_match'          => '<b>',
                'after_match'           => '</b>',
                'chunk_separator'       => ' ... ',
                'limit'                 => self::BODY_SIZE,
                'around'                => 3,
        );
         
        return $this->BuildExcerpts($body, $index, $q, $options);
    }
    
    protected function getIndexName()
    {
        return '*';
    }
    
    /**
     * @return SFM_Mapper
     */
    protected function getMapper()
    {
        return null;
    }
    
    /**
     * Получение агрегата найденных в методе getSphinxResults id-шников.
     * @param array $results Список, полученный из метода getSphinxResults.
     * @param string|null $cacheKey Ключ кэширования
     * @return SFM_Aggregate
     */
    public function getAggregate(array $results = array(),$cacheKey = null)
    {
        $mapper = $this->getMapper();
        if($mapper !== null) {
            
            $proto = array();
            foreach($results as $item) {
                $proto[] = $item['id'];
            }
            return $mapper->createAggregate($proto,$cacheKey);
        } else {
            throw new Letsee_Search_Exception('The mapper is not set!');
        }
    }

    public function adjustFieldWeights()
    {
        
    }
}