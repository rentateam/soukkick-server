<?php
class Shopfee_Logger
{
    static private $_instance = null;
    /**
     * Direct instatination forbiden
     */
    private function __constuctor() {}
    
    /**
     * Return logger
     * 
     * @return Zend_Log
     */
    static public function getInstance() {
        if( false === is_null(self::$_instance)) {
            return self::$_instance;
        }
        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Firebug();
        $logger->addWriter($writer);
        self::$_instance = $logger;
        
        return self::$_instance; 
    }
}
?>