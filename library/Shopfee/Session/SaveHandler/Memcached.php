<?php
require_once 'Zend/Session/SaveHandler/Interface.php';

/**
 * Memcached session storage
 * 
 */
class Shopfee_Session_SaveHandler_Memcached implements Zend_Session_SaveHandler_Interface
{
    private $maxLifeTime = 3600;
    
    /**
     * Cache handler 
     * @var SFM_Cache_Memory
     */
    protected $_cache = null;
    
    /*  Mysterious property. Is is a copy of Zend_Session::$_writeClosed. Without it there is a mistake. Don't know why.*/ 
    public static $_writeClosed = false;
    
    protected static $_sessionPrefix = 'SESSION_MEMCACHE';
    
    /**
     * @param SFM_Cache $cacheHandler
     */
    public function __construct($cacheHandler, $maxLifeTime = 3600) 
    {
        $this->_cache = $cacheHandler;
        $this->maxLifeTime = $maxLifeTime;
    }
    
    protected static function processKey($key)
    {
        return self::$_sessionPrefix.SFM_Cache::KEY_DILIMITER.$key;
    }
    
    public function open($savePath, $sessionName) 
    {
        return true;
    }
    
    public function close() 
    {
        return true;
    }
    
    public function read($key) 
    {
        return $this->_cache->getRaw(self::processKey($key));       
    }
    
    public function write($key, $sessionData) 
    {
        $this->_cache->setRaw(self::processKey($key), $sessionData, $this->maxLifeTime);
        return true;
    }
    
    public function destroy($key) 
    {
        $this->_cache->delete(self::processKey($key));
        return true;
    }
    
    public function gc($notusedformemcache) 
    {
        return true;
    }
}

?>