<?php

abstract class Shopfee_Controller_Action_Authorized extends Shopfee_Controller_Action
{
    public function preDispatch()
    {
        //Все действия требуют авторизации
        $this->_authActions = array($this->getRequest()->getActionName());
        parent::preDispatch();
    }
}