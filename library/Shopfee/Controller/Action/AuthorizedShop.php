<?php

abstract class Shopfee_Controller_Action_AuthorizedShop extends Shopfee_Controller_Action
{
    public function preDispatch()
    {
        //Все действия требуют авторизации пользователя магазина
        //$this->_authShopActions = array($this->getRequest()->getActionName());
        parent::preDispatch();
    }
}