<?php

abstract class Shopfee_Controller_Action_Admin extends Shopfee_Controller_Action
{
    protected $_modules = array(
        'user-shop' => 'Trade Network',
        //'approve' => 'Регистрация',
        'users' => 'Users',
        //'reports' => 'Отчеты',
        //'mall' => 'ТЦ',
        'prize' => 'Rewards',
        'index/reward' => 'Reward Delivery',
        'promocode' => 'Promocodes',
        //'settings'  => 'Настройки'
    );
    protected $_currentModule = 'user-shop';

    /**
     * @var Shopfee_Controller_Parameters
     */
    protected $parameters;

    public function postDispatch()
    {
        $this->view->parameters = $this->parameters;
        parent::postDispatch();
    }

    public function preDispatch()
    {
        parent::preDispatch();

        $backurl = $_SERVER['REQUEST_URI'];
        $post = $this->getRequest()->getPost();
        $this->currentAdmin = Auth::getCurrentAdmin();

        $this->parameters = new Shopfee_Controller_Parameters();

        $isLoginController = $this->getRequest()->getControllerName() == 'login';
        $isLoginAction = $this->getRequest()->getActionName() == 'index';
        if (!($isLoginController && $isLoginAction || $this->currentAdmin !== null)) {
            if (!empty($_SERVER['QUERY_STRING']))
                $backurl .= '?' . $_SERVER['QUERY_STRING'];
            else if (!empty($post)) {
                $backurl .= '?';
                foreach ($this->getRequest()->getPost() as $key => $value) {
                    $backurl .= $key . '=' . $value . '&';
                }
                $backurl = ereg_replace('&$', '', $backurl);
            }

            $this->_redirect('/admin/login/index/?backurl=' . urlencode($backurl));
        }
    }

    public function init()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $layout->setLayoutPath('../application/admin/layouts/');

        //$this->view->addScriptPath(Application::VIEWS_PATH);
        $this->view->modules = $this->_modules;
        $this->view->currentModule = $this->_currentModule;

        $this->view->menu = array();

        $this->view->breadcrumbs = array();

        //indicating current page in paginator
        $this->view->pageNum = $this->getRequest()->getParam('page') != null ? $this->getRequest()->getParam('page') : 1;
    }

    protected function handleAuthAction()
    {

    }
}
