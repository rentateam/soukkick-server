<?php
class Shopfee_Controller_Parameters
{
    protected $parameters = array();

    public function __set($key, $value) {
        $this->parameters[$key] = $value;
    }

    public function __get($key) {
        return isset($this->parameters[$key]) ? $this->parameters[$key] : NULL;
    }

    public function getQueryString(array $newParameters = array())
    {
        $parameters = array_merge($this->parameters, $newParameters);

        foreach ($parameters as $key => $value) {
            if (is_null($value)) {
                unset($parameters[$key]);
            }
        }

        $query = (count($parameters) > 0) ? '?' . http_build_query($parameters) : NULL;
        return $query;
    }

    public function getAll()
    {
        return $this->parameters;
    }

    public function import($array)
    {
        $this->parameters = $array;
    }

}
