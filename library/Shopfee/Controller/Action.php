<?php
class Shopfee_Controller_Action extends Zend_Controller_Action
{
    /**
     * @var array array of action names that require authorization
     */
    protected $_authActions = array();
    /**
     * @var array array of action names that require authorization of shop users
     */
    protected $_authShopActions = array();

    /**
     * @var array array of action names that require authorization of admin
     */
    protected $_authAdminActions = array();

    /**
     * @var Entity_User
     */
    protected $currentUser;


    public function preDispatch()
    {
        $this->currentUser = Auth::getCurrentUser();
        $this->currentTradeNetwork = Auth::getCurrentTradeNetwork();
        $this->currentUserAdmin = Auth::getCurrentAdmin();
        $this->isJson = $this->getRequest()->getParam('format') == 'json';

        //if this method requires authorization
        $this->handleAuthAction();
        $this->handleAuthShopAction();
        $this->handleAuthAdminAction();

        $this->handleIE6();

        if( null !== $this->currentUser ) {
            $this->view->currentUser = $this->currentUser;
        }
    }

    public function postDispatch()
    {
        if($this->isJson) {
            /**
             * @see ErrorController
             */
            $this->view->json_succesful = 1;
            unset($this->view->currentUser);
            $this->logClientVersion();
        }
    }

    /**
     * Checks if the entity is null. If it is so throws Exception
     *
     * @param SFM_Entity|null $entity
     * @throws Shopfee_Exception_Entity
     */
    protected function checkForNullEntity($entity)
    {
        if(null === $entity)
            throw new Shopfee_Exception_Entity();
    }

    protected function handleIE6()
    {
        if($this->getRequest()->getControllerName() !== 'ie6')
        {
            $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
            if (stripos($user_agent, 'MSIE 6.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 7.0') === false)
            {
                if (!isset($_COOKIE["ie"]))
                {
                    setcookie("ie", "yes", time()+60*60*24*360);
                    $this->_redirect('/ie6/');
                }
            }
        }
    }

    protected function getQuotedParam($name, $default = null)
    {
        $param = $this->getRequest()->getParam($name,$default);
        if($param !== null)
        {
            return htmlspecialchars($param);
        }
        else
            return $default;
    }

    /**
     * automatically extract backurl from request or Http_referer parameter.
     *
     */
    protected function getBackUrl()
    {
        if ($this->getQuotedParam('backurl'))
            return urldecode($this->getQuotedParam('backurl'));

        else if (isset($_SERVER['HTTP_REFERER']))
            return str_replace('http://'.Application::getHostName(), '', $_SERVER['HTTP_REFERER']);
        else
            return '/';
    }

    protected function handleAuthAction()
    {
        $actionName = $this->getRequest()->getActionName();
        if(in_array($actionName,$this->_authActions))
        {
            $backurl = $_SERVER['REQUEST_URI'];
            $post = $this->getRequest()->getPost();

            if(null === $this->currentUser)
            {
                if(!$this->isJson)
                {
                    if(!empty($_SERVER['QUERY_STRING']))
                        $backurl .= '?'.$_SERVER['QUERY_STRING'];
                    else if(!empty($post))
                    {
                        $backurl .= '?';
                        foreach($this->getRequest()->getPost() as $key => $value)
                        {
                            $backurl .= $key.'='.$value.'&';
                        }
                        $backurl = preg_replace('&$','',$backurl);
                    }

                    $this->_redirect('/login/index/?backurl='.urlencode($backurl));
                }
                else
                {
                    $this->_redirect('/login/not-authorized-phone/format/json/');
                }
            }
        }
    }

    protected function handleAuthShopAction()
    {
        $actionName = $this->getRequest()->getActionName();
        if(in_array($actionName,$this->_authShopActions))
        {
            $backurl = $_SERVER['REQUEST_URI'];
            $post = $this->getRequest()->getPost();

            if(null === $this->currentTradeNetwork)
            {
                if(!empty($_SERVER['QUERY_STRING']))
                    $backurl .= '?'.$_SERVER['QUERY_STRING'];
                else if(!empty($post))
                {
                    $backurl .= '?';
                    foreach($this->getRequest()->getPost() as $key => $value)
                    {
                        $backurl .= $key.'='.$value.'&';
                    }
                    $backurl = preg_replace('&$','',$backurl);
                }

                $this->_redirect('/edit/index/login/?backurl='.urlencode($backurl));
            }
        }
    }

    protected function handleAuthAdminAction()
    {
        $actionName = $this->getRequest()->getActionName();
        if(in_array($actionName,$this->_authAdminActions))
        {
            $backurl = $_SERVER['REQUEST_URI'];
            $post = $this->getRequest()->getPost();

            if(null === $this->currentAdmin)
            {
                if(!empty($_SERVER['QUERY_STRING']))
                    $backurl .= '?'.$_SERVER['QUERY_STRING'];
                else if(!empty($post))
                {
                    $backurl .= '?';
                    foreach($this->getRequest()->getPost() as $key => $value)
                    {
                        $backurl .= $key.'='.$value.'&';
                    }
                    $backurl = preg_replace('&$','',$backurl);
                }

                $this->_redirect('/login/index/?backurl='.urlencode($backurl));
            }
        }
    }

    protected function isSetMobile304($maxAge = 1)
    {
        $response = $this->getResponse();
        $this->setCacheHeaders($maxAge);

        $userETag = isset($_SERVER['HTTP_IF_NONE_MATCH'])?$_SERVER['HTTP_IF_NONE_MATCH']:'';
        if($userETag == $this->calculateEtag())
        {
            $response->setHttpResponseCode(304);
            return true;
        }
        else
        {
            return false;
        }


        //А этот код вместо него
        $response->setHttpResponseCode(304);
        return true;


    }

    protected function allowMobileCaching($maxAge = 1)
    {
        $response = $this->getResponse();
        $response->setHeader('Expires', '', true);
        $this->setCacheHeaders($maxAge);
        $response->setHeader('Etag', $this->calculateEtag(), true);
    }

    protected function setCacheHeaders($maxAge = 1)
    {
        $response = $this->getResponse();
        $response->setHeader('Cache-Control', 'private', true);
        $response->setHeader('Cache-Control', 'max-age='.$maxAge);
        $response->setHeader('Cache-Control', 'must-revalidate');
        $response->setHeader('Pragma', 'Cache', true);
    }

    protected function calculateEtag()
    {
        return '1';
    }

    protected function set404($message = 'Страница не найдена')
    {
        throw new Zend_Controller_Action_Exception($message, 404);
    }

    protected function logClientVersion($clientVersion = null)
    {
        $versionController = new Version(Auth::getCurrentUser());
        if($clientVersion === null)
            $clientVersion = $this->getRequest()->getParam('my_version');
        $versionController->setSupportStatusToView($this->view, $clientVersion, $_SERVER['REQUEST_URI']);
    }
    
    public function __call($name , array $arguments)
    {
        $this->_helper->contextSwitch()
                ->addActionContext($this->getRequest()->getActionName(), 'json');
        $this->_helper->contextSwitch()->initContext();
        //Вызываем метод с явно неподдерживаемой версией - чтобы проставилась необходимость обновления
        $this->logClientVersion('0.0');
    }
}