<?php
class Shopfee_Auth_Result extends Zend_Auth_Result
{
    const FAILURE_NOT_ACTIVE = -5;
    const FAILURE_NOT_ADMIN = -6;
    
    public function __construct($code, $identity, array $messages = array())
    {
        $code = (int) $code;

        $this->_code     = $code;
        $this->_identity = $identity;
        $this->_messages = $messages;
    }
} 