<?php
class Shopfee_Auth_Adapter_Udid {
    /**
     * Auth adapter
     * 
     * @var array of Shopfee_Auth_Adapter_DbTable_Udid
     */
    static private $_instance = array();
    /**
     * Direct instatination forbiden
     */
    private function __constuctor() {}
    
    /**
     * Return auth adapter
     * 
     * @return Zend_Auth_Adapter_DbTable
     */
    static public function getInstance($className) {
        if( isset(self::$_instance[$className])) {
            return self::$_instance[$className];
        }
        self::$_instance[$className] = new $className(
                                        SFM_DB::getInstance()->getAdapter(),
                                        'user',
                                        'udid',
                                        'udid',
                                        "?"
                                    );
        return self::$_instance[$className];
    }
}

?>