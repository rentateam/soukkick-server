<?php
class Shopfee_Auth_Adapter_DbTable extends Zend_Auth_Adapter_DbTable
{
    public function authenticate()
    {
        $result = parent::authenticate();
        return $result;
    }

    protected function _authenticateCreateAuthResult()
    {
        return new Shopfee_Auth_Result(
            $this->_authenticateResultInfo['code'],
            $this->_authenticateResultInfo['identity'],
            $this->_authenticateResultInfo['messages']
            );
    }
}