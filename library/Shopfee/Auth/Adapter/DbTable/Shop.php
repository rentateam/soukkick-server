<?php
class Shopfee_Auth_Adapter_DbTable_Shop extends Shopfee_Auth_Adapter_DbTable
{
    public function authenticate()
    {
        $result = parent::authenticate();
        if($result->getCode() == Zend_Auth_Result::SUCCESS) {
            $accountEntity = Mapper_Bpa_Manager::getInstance()->getEntityById($this->getResultRowObject("id")->id);
            if (!$accountEntity->isActive()) {
                $result = new Shopfee_Auth_Result(Shopfee_Auth_Result::FAILURE_NOT_ACTIVE,$this->_authenticateResultInfo['identity']);
            }
        }
        return $result;
    }
}