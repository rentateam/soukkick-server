<?php
class Shopfee_Auth_Adapter_DbTable_Admin extends Shopfee_Auth_Adapter_DbTable
{
    public function authenticate()
    {
        $result = parent::authenticate();
        if($result->getCode() == Zend_Auth_Result::SUCCESS) {
            $accountEntity = Mapper_User_Admin::getInstance()->getEntityById($this->getResultRowObject("id")->id);
        }
        return $result;
    }
}