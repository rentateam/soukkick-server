<?php
abstract class Shopfee_Queue_Abstract
{
    protected $_mcQueue;
    protected $_name;
    protected $_enableLogging = true;
    protected static $_instance;
    
    protected function __construct($name = null)
    {
        $this->_mcQueue = memcache_connect(Application::getMemcacheqHost(), Application::getMemcacheqPort());
        if($this->_mcQueue === false)
            throw new Queue_Exception();
            
        if(empty($name))
            $name = $this->_name;
            
        $this->setName($name);
    }
    
    /**
     * @param string $name
     * @return Letsee_Queue_Abstract
     */
    public static function getInstance($name = null)
    {
        if(!isset(self::$_instance))
            self::$_instance = new self($name);
        
        return self::$_instance;
    }
    
    public function setName($name)
    {
        $config = Zend_Registry::get(Application::CONFIG_NAME);
        $projectPrefix = '';
        if($config->memcachedAPI->projectPrefix)
            $projectPrefix = $config->memcachedAPI->projectPrefix;
        $this->_name = $projectPrefix.'_'.$name;
    }
    
    public function push($message)
    {
        $result = memcache_set($this->_mcQueue, $this->_name, $message, 0, 0);
        $this->log("pushing:".$message);
        return $result;
    }
    
    public function pop()
    {
        $message = memcache_get($this->_mcQueue, $this->_name); 
        if($message !== false)
            $this->log("popping:".$message);
        return $message;
    }
    
    public function processQueue()
    {
        while($message = $this->pop())
        {
            $this->processMessage($message);
        }
    }
    
    protected function log($message)
    {
        if($this->_enableLogging)
        {
            $messageString = '';
            if(is_array($message))
            {
              foreach($message as $k => $v)
              {
                  $messageString .= $k.'=>'.$v."\r\n";
              }
            }
            else
            {
                  $messageString = $message;
            }
            $f = fopen('/tmp/queue.log','a+');
            fwrite($f,$this->_name."\r\n".date('Y-m-d H:i:s')."\r\n".$messageString."\r\n\r\n");
            fclose($f);
        }
    }
    
    abstract public function processMessage($message);

}