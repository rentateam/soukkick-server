<?php
abstract class Shopfee_Queue_Mail_Abstract extends Shopfee_Queue_Abstract
{
    protected static $_instance;
    /*
     * @var Zend_Mail
     */
    protected $_mailer;
    
    /*
     * @var int in milliseconds
     */
    protected $_sleepBetweenLetters = 50;
    
    protected function __construct($name = null)
    {
        $this->_mailer = new Zend_Mail('utf-8');
        parent::__construct($name);
    }
    
    public static function getInstance($name = null)
    {
        if(!isset(self::$_instance))
            self::$_instance = new self($name);
        
        return self::$_instance;
    }
    
    protected function clearMailer()
    {
        $this->_mailer->clearFrom()
                      ->clearRecipients()
                      ->clearReturnPath()
                      ->clearSubject()
                      ->clearDate()
                      ->clearMessageId();
    }
    
    public function sendLetter($params,$template,$emailTo,$loginTo,$subject,$lang)
    {
        //composing a letter
        $params['hostName'] = Application::getHostName();   
        $this->_mailer->setBodyHtml(Letsee_MailMaker::getInstance()->getMailBody($lang,$template,$params));
        $this->_mailer->setFrom(Application::getDefaultEmailAddress(), Application::getDefaultEmailTitle());
        $this->_mailer->addTo($emailTo, $loginTo);
        $this->_mailer->setSubject($subject);
        try
        {
            $this->_mailer->send(); 
            if($this->_sleepBetweenLetters > 0)
                usleep($this->_sleepBetweenLetters);
            $this->clearMailer();
            return true;
        }
        //if an error occured during sending
        //push it back
        catch(Zend_Mail_Transport_Exception $e)
        {
            $this->pushBack($params,$emailTo,$loginTo,$lang);
            $this->clearMailer();
            return false;
        }
    }
    
    abstract public function pushBack($params,$emailTo,$loginTo,$lang);
}