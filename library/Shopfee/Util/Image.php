<?
class Shopfee_Util_Image extends SFM_Util_Image
{
    const IMAGE_FORMAT = 'png24';
    
    static public function imQualityScaleToSideWithBorders($filename, $newSideSize, $blur = 1)
    {
        list($width, $height) = getimagesize($filename);

        $image = new Imagick($filename);
        $image->setFormat(self::IMAGE_FORMAT);

        $fitbyWidth = (($newSideSize / $width) < ($newSideSize / $height)) ? true : false;

        if ($fitbyWidth) {
            if ($width > $newSideSize)
                $image->resizeImage($newSideSize, 0, imagick::FILTER_HERMITE, $blur, false);
        } else {
            if ($height > $newSideSize)
                $image->resizeImage(0, $newSideSize, imagick::FILTER_HERMITE, $blur, false);
        }
        $newDimentions = $image->getImageGeometry();

        /* The overlay x and y coordinates */
        $x = ( $newSideSize - $newDimentions['width'] ) / 2;
        $y = ( $newSideSize - $newDimentions['height'] ) / 2;
        $image->borderImage('transparent', $x, $y);

        $pngFileName = self::getPngFileName($filename);
        $image->writeImages($pngFileName, true);
        return $pngFileName;
    }
    
    static public function imQualityScaleToWidth($filename, $newWidth, $blur = 1)
    {
        list($width, $height) = getimagesize($filename);
        if ($width < $newWidth)
            return $filename;

        $image = new Imagick($filename);
        $image->setFormat(self::IMAGE_FORMAT);
        
        // If 0 is provided as a width or height parameter,
        // aspect ratio is maintained
        $image->resizeImage($newWidth, 0, imagick::FILTER_HERMITE, $blur, false);

        $pngFileName = self::getPngFileName($filename);
        $image->writeImages($pngFileName, true);
        return $pngFileName;
    }
    
    protected static function getPngFileName($fileName)
    {
        $ext = SFM_Util_File::getExt($fileName);
        return str_replace($ext,'png',$fileName);
    }
}