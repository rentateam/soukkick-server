<?php

class Shopfee_Util_Date
{
    protected static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
    }

    public function getMonth()
    {
        return array(
            1=>'января', 
             2=>'февраля',
             3=>'марта',
             4=>'апреля',
             5=>'мая', 
             6=>'июня',
             7=>'июля',
             8=>'августа',
             9=>'сентября',
             10=>'октября', 
             11=>'ноября', 
             12=>'декабря'
         );
    }
    
    public static function convert($date)
    {
        $monthArray=array('',
                  'января',
                  'февраля',
                  'марта',
                  'апреля',
                  'мая',
                  'июня',
                  'июля',
                  'августа',
                  'сентября',
                  'октября',
                  'ноября',
                  'декабря');
        
        $dateElements=explode('-',$date);
        $hourMinuteSecond = explode(' ',$dateElements[2]);
        $result = $hourMinuteSecond[0].' '.$monthArray[intval($dateElements[1])].' '.$dateElements[0];

        //if not only date but time also is set
        if(isset($hourMinuteSecond[1]))
            $result.=' '.$hourMinuteSecond[1];
              
        return $result;
        
    }    
    
    public static function threeLetterMonth($monthNumber)
    {
        $monthArray=array('',
                  'Янв',
                  'Фев',
                  'Мар',
                  'Апр',
                  'Май',
                  'Июн',
                  'Июл',
                  'Авг',
                  'Сен',
                  'Окт',
                  'Ноя',
                  'Дек');
        
        return $monthArray[intval($monthNumber)];
    }
    
    public static function fullMonth2($monthNumber)
    {
        $monthArray=array('',
                  'января',
                  'февраля',
                  'марта',
                  'апреля',
                  'мая',
                  'июня',
                  'июля',
                  'августа',
                  'сентября',
                  'октября',
                  'ноября',
                  'декабря');
        
        return $monthArray[intval($monthNumber)];
    }
    
    public static function fullMonth($monthNumber)
    {
        $monthArray=array('',
                  'январь',
                  'февраль',
                  'март',
                  'апрель',
                  'май',
                  'июнь',
                  'июль',
                  'август',
                  'сентябрь',
                  'октябрь',
                  'ноябрь',
                  'декабрь');
        
        return $monthArray[intval($monthNumber)];
    }
    
    public static function formatDate($date)
    {
        if($date === null)
            return '';
        $dateArr = getdate(strtotime($date));
        return $dateArr['mday'].' '.self::fullMonth2($dateArr['mon']).' '.' '.$dateArr['year'];
    } 
}
?>