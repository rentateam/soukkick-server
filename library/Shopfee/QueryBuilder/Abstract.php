<?php
class Shopfee_QueryBuilder_Abstract
{
    protected $params = array();
    protected $sql;
    protected $key;

    public function __construct(Shopfee_Criteria_Abstract $criteria)
    {
        $this->key = $criteria->getSerializedValue() . get_class($this);
    }

    public function getSQL()
    {
        return $this->sql;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getUniqueKey()
    {
        return $this->key;
    }
}