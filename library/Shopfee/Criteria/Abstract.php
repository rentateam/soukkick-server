<?php
abstract class Shopfee_Criteria_Abstract
{
    /**
     * Поле сортировки
     * @var array
     */
    protected $_sort;
    
    /**
     * Направление сортировки
     * @var array
     */
    protected $_direction;
    
    /**
     * Текущая страница
     * @var integer
     */
    protected $_page;
    
    /**
     * Количество на страницу
     * @var integer
     */
    protected $_perPage;
    
    /**
     * @return the $_page
     */
    public function getPage ()
    {
        return $this->_page;
    }

    /**
     * @return the $_perPage
     */
    public function getPerPage ()
    {
        return $this->_perPage;
    }

    /**
     * @param integer $page
     */
    public function setPage ($page)
    {
        $this->_page = $page;
    }

    /**
     * @param integer $perPage
     */
    public function setPerPage ($perPage)
    {
        $this->_perPage = $perPage;
    }

    /**
     * @return the $_direction
     */
    public function getDirection ()
    {
        return $this->_direction;
    }

    /**
     * @param string $dir
     */
    public function setDirection ($dir)
    {
        $this->_direction = $dir;
    }

    /**
     * @return the $_sort
     */
    public function getSort ()
    {
        return $this->_sort;
    }

    /**
     * @param string $_sort
     */
    public function setSort ($sort)
    {
        $this->_sort = $sort;
    }
    
    public function getSerializedValue()
    {
        $clone = clone($this);
        foreach(get_object_vars($clone) as $name => $val){
            if ($val instanceof SFM_Entity) {
                $clone->$name = get_class($val) . $val->getId();
            }
        }
        return serialize($clone);
    }
}