<?php
class Shopfee_Report_WriterFactory
{
    /**
     * @static
     * @param $format
     * @return Shopfee_Report_Writer_Abstract
     * @throws Shopfee_Exception
     */
    public static function create($format)
    {
        switch ($format) {
            case 'doc':
                $writer = new Shopfee_Report_Writer_Doc();
                break;
            case 'pdf':
                $writer = new Shopfee_Report_Writer_Pdf();
                break;
            case 'odt':
                $writer = new Shopfee_Report_Writer_Odt();
                break;
            default:
                throw new Shopfee_Exception("No writer");
        }

        return $writer;
    }
}