<?php
abstract class Shopfee_Report_Writer_Abstract
{
    /** @var Report_Abstract */
    protected $report;

    /** @var string */
    protected $path;

    /** @var string */
    protected $converter;

    /** @var int */
    protected $time;


    public function __construct()
    {
        $this->time = time();
        $this->converter = Application::getConverterCommand();
    }

    /**
     * @param Report_Abstract $report
     * @throws Exception
     * @return Report_Writer_Abstract
     */
    public function setReport(Report_Abstract $report)
    {
        $this->report = $report;

        $path = $this->getDirectoryPath();
        if (false === file_exists($path)) {
            $isCreated = mkdir($path, 0777, true);
            if (false === $isCreated) {
                throw new Exception("Can't create directory");
            }
        }

        $this->report->build();
        $this->report->save($this->getFilePath('odt'));

        return $this;
    }

    /**
     * @return string
     */
    protected function getDirectoryPath()
    {
        $path = Application::PUBLIC_PATH . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR .
            $this->report->getIdentity() . DIRECTORY_SEPARATOR . str_replace('Report_', '', get_class($this->report)) .
            DIRECTORY_SEPARATOR;
        return $path;
    }

    /**
     * @param null $ext
     * @return string
     */
    protected function getFilePath($ext = NULL)
    {
        $path = $this->getDirectoryPath() . $this->time;

        if (false === is_null($ext)) {
            $path .= '.' . $ext;
        }

        return $path;
    }

    /**
     * @return string
     */
    public function output() {
        return '';
    }

    /**
     * @param $format
     * @throws Exception
     */
    protected function convertTo($format)
    {
        $answer = shell_exec("abiword --to={$format} {$this->getFilePath()}.odt --to-name={$this->getFilePath()}.{$format}");
        if (false === empty($answer)) {
            throw new Exception($answer);
        }
    }
}
