<?php
class Shopfee_Mailer
{
    /*
     * @var Shopfee_Mailer
     */
    protected static $_instance;
    /*
     * @var Zend_Mail
     */
    protected $_mailer;
    
    protected function __construct()
    {
        //windows-1251 is set due to mail.ru
        //Their clever mail can't read utf-8. 
        //Great respect
        $this->_mailer = new Zend_Mail('windows-1251');
    }
    
    /**
     * Get a singleton
     *
     * @param string $name
     * @return Shopfee_Mailer
     */
    public static function getInstance($name = null)
    {
        if(!isset(self::$_instance))
            self::$_instance = new self($name);
        
        return self::$_instance;
    }
    
    protected function clearMailer()
    {
        $this->_mailer->clearFrom()
                      ->clearRecipients()
                      ->clearReturnPath()
                      ->clearSubject()
                      ->clearDate()
                      ->clearMessageId();
    }
    
    public function sendLetter($params,$template,$emailFrom,$emailTo,$nameFrom,$nameTo,$subject, $emailCopy=null)
    {
       
        //composing a letter
        $this->_mailer->setBodyHtml(iconv('utf-8','windows-1251',Shopfee_MailMaker::getInstance()->getMailBody($template,$params)));
        $this->_mailer->setFrom($emailFrom, iconv('utf-8','windows-1251',$nameFrom));
        $this->_mailer->addTo($emailTo, iconv('utf-8','windows-1251',$nameTo));
                $this->_mailer->addBcc($emailCopy);
                $this->_mailer->setSubject(iconv('utf-8','windows-1251',$subject));

        try
        {
            $this->_mailer->send(); 
            $this->clearMailer();
            return true;
        }
        //if an error occured during sending
        //push it back
        catch(Zend_Mail_Transport_Exception $e)
        {
            $this->clearMailer();
            return false;
        }
    }
}