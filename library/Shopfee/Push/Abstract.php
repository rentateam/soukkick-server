<?
abstract class Shopfee_Push_Abstract
{
    protected $_environment;
    
    protected $_lastErrors = array();
    
    public function __construct($environment)
    {
        $this->_environment = $environment;
    }
    
    public function getLastErrors()
    {
        return $this->_lastErrors;
    }
    
    public function clearLastErrors()
    {
        $this->_lastErrors = array();
    }
    
    /**
     * Отсылка сообщения
     * @param string $messageText
     * @param integer $code
     * @return boolean
     */
    abstract public function sendMessage($messageText,$code);
}