<?
class Shopfee_Push_Android extends Shopfee_Push_Abstract
{
    protected $_apiKey;
    protected $_registrationId;
    
    public function __construct($environment)
    {
        parent::__construct($environment);
    }
    
    public function sendMessage($messageText,$code)
    {
        $this->clearLastErrors();
        
        $message = new Zend_Mobile_Push_Message_Gcm();
        $message->setId(time());
        $message->addToken($this->_registrationId);
        $message->setData(array(
            'message' => $messageText,
            'code' => $code
        ));
        
        $gcm = new Zend_Mobile_Push_Gcm();
        $gcm->setApiKey($this->_apiKey);
        
        $response = false;
        
        $failCount = 0;
        do {
            $tryAgain = false;  
            try {
                $response = $gcm->send($message);
            } catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
                $failCount++;
                $tryAgain = true;
                usleep($self::getBackOffTime($failCount, $c2dm->getLastResponse()) * 1000);
            } catch (Zend_Mobile_Push_Exception $e) {
                // all other exceptions only require action to be sent or implementation of exponential backoff.
                throw $e;
            }
        } while ($tryAgain);
        
        $hasErrors = false;
        // handle all errors and registration_id's
        foreach ($response->getResults() as $k => $v) 
        {
            if (isset($v['error']) && $v['error']) 
            {
                $hasErrors = true;
                $this->_lastErrors[$k] = $v['error'];
            }
        }
        return !$hasErrors;
    }
    
    /**
     * Exponential Backoff / Retry-After
     *
     * @param int $fails
     * @param Zend_Http_Response
     * @return int
     */
    protected static function getBackOffTime($fails, Zend_Http_Response $response)
    {
        if ($retry = $response->getHeader('Retry-After')) {
            if (is_string($retry)) {
                $retry = strtotime($retry) - time();
            }
            return (int) $retry;
        }
        return intval(pow(2, $fails) - 1);
    }
    
    public function setApiKey($apiKey)
    {
        $this->_apiKey = $apiKey;
    }
    
    public function setRegistrationId($registrationId)
    {
        $this->_registrationId = $registrationId;
    }
}