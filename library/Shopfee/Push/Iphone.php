<?
require_once 'ApnsPHP/Autoload.php';
class Shopfee_Push_Iphone extends Shopfee_Push_Abstract
{
    protected $_deviceToken;
    protected $_identifier;
    protected $_badgeNumber;
    protected $_certPassphrase;
    
    public function sendMessage($messageText,$code)
    {
        $this->clearLastErrors();
        
        $apnsEnvironment = $this->_environment == Application::ENVIROMENT_PRODUCTION ? ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION : ApnsPHP_Abstract::ENVIRONMENT_SANDBOX;
        
        // Instanciate a new ApnsPHP_Push object
        $push = new Shopfee_Push_ApnsPush(
            $apnsEnvironment,
            $this->getCertificate()
        );
        $push->setPassphrase($this->getPassphrase());
        
        // Set the Root Certificate Autority to verify the Apple remote peer
        //$push->setRootCertificationAuthority('entrust_root_certification_authority.pem');
        
        // Connect to the Apple Push Notification Service
        $push->connect();
        
        // Instantiate a new Message with a single recipient
        $message = new ApnsPHP_Message_Custom($this->_deviceToken);
        
        // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
        // over a ApnsPHP_Message object retrieved with the getErrors() message.
        if($this->_identifier)
            $message->setCustomIdentifier($this->_identifier);
        
        // Set badge icon to "3"
        if($this->_badgeNumber)
            $message->setBadge($this->_badgeNumber);
        
        // Set a simple welcome text
        $message->setText($messageText);
        
        // Play the default sound
        $message->setSound();
        
        // Set a custom property
        $message->setCustomProperty('code', $code);
        
        /*// Set the expiry value to 30 seconds
        $message->setExpiry(30);
        
        // Set the "View" button title.
        $message->setActionLocKey('Show me!');
        
        // Set the alert-message string and variable string values to appear in place of the format specifiers.
        $message->setLocKey('Hello %1$@, you have %2$@ new messages!'); // This will overwrite the text specified with setText() method.
        $message->setLocArgs(array('Steve', 5));
        
        // Set the filename of an image file in the application bundle.
        $message->setLaunchImage('DefaultAlert.png');*/
        
        // Add the message to the message queue
        $push->add($message);
        
        // Send all messages in the message queue
        $push->send();
        
        // Disconnect from the Apple Push Notification Service
        $push->disconnect();
        
        // Examine the error message container
        $aErrorQueue = $push->getErrors();
        if (!empty($aErrorQueue)) {
            $this->_lastErrors = $aErrorQueue;
            return false;
        }
        
        return true;
    }
    
    public function setDeviceToken($deviceToken)
    {
        $this->_deviceToken = $deviceToken;
    }
    
    public function setIdentifier($identifier)
    {
        $this->_identifier = $identifier;
    }
    
    public function setBadgeNumber($badgeNumber)
    {
        $this->_badgeNumber = $badgeNumber;
    }
    
    protected function getCertificateName()
    {
        return 'ShopfeeCertKey.pem';
    }
    
    protected function getPassphrase()
    {
        return 'ShopFee_1024';
    }
    
    protected function getCertificate()
    {
        $certificateFolder = $this->_environment == Application::ENVIROMENT_PRODUCTION ? 'prod' : 'dev';
        return realpath($_SERVER['DOCUMENT_ROOT'].'../').'/push/'.$certificateFolder.'/'.$this->getCertificateName();
    }
}