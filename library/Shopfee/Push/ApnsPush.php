<?
require_once 'ApnsPHP/Autoload.php';
class Shopfee_Push_ApnsPush extends ApnsPHP_Push
{
    protected $_passphrase;
    
    public function setPassphrase($passphrase)
    {
        $this->_passphrase = $passphrase;
    }
    
    /**
     * Connects to Apple Push Notification service server.
     *
     * @throws ApnsPHP_Exception if is unable to connect.
     * @return @type boolean True if successful connected.
     */
    protected function _connect()
    {
        $sURL = $this->_aServiceURLs[$this->_nEnvironment];
        unset($aURLs);

        $this->_log("INFO: Trying {$sURL}...");

        /**
         * @see http://php.net/manual/en/context.ssl.php
         */
        $streamContext = stream_context_create(array('ssl' => array(
            'verify_peer' => isset($this->_sRootCertificationAuthorityFile),
            'cafile' => $this->_sRootCertificationAuthorityFile,
            'local_cert' => $this->_sProviderCertificateFile,
            'passphrase' => $this->_passphrase
        )));

        $this->_hSocket = stream_socket_client($sURL, $nError, $sError,
            $this->_nConnectTimeout, STREAM_CLIENT_CONNECT, $streamContext);

        if (!$this->_hSocket) {
            throw new ApnsPHP_Exception(
                "Unable to connect to '{$sURL}': {$sError} ({$nError})"
            );
        }

        stream_set_blocking($this->_hSocket, 0);
        stream_set_write_buffer($this->_hSocket, 0);

        $this->_log("INFO: Connected to {$sURL}.");

        return true;
    }
}