<?
class Shopfee_Push_Factory
{
    const TYPE_IPHONE = 1;
    const TYPE_ANDROID = 2;
    
    public static function create($type,$environment)
    {
        switch($type)
        {
            case self::TYPE_IPHONE:
                return new Shopfee_Push_Iphone($environment);
            case self::TYPE_ANDROID:
                return new Shopfee_Push_Android($environment);
            default:
                throw new Shopfee_Exception('Unknown push type '.$type);
        }
    }
}