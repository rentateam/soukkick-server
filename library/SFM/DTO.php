<?php

abstract class DTO 
{
    /**
     *
     * @var int
     */
    public $id;
    /**
     *
     * @var string
     */
    public $name;
    /**
     *
     * @var string
     */
    public $created;
    /**
     *
     * @var string
     */
    public $creator;
}

?>