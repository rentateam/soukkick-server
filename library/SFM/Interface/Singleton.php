<?php

interface SFM_Interface_Singleton 
{
    /**
    * Generic Singleton's function
    */
    public static function getInstance();
}
?>