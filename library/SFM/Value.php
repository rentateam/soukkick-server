<?php

/**
 * Simplify common operations on values
 * 
 * @author andry 
 */
abstract class SFM_Value extends SFM_Value_Abstract
{
    
    public function set( $value ) 
    {
        parent::set($value);
    } 
}